#include "ct_reader_trajectory.h"

#include "ct_view/tools/ct_textfileconfigurationdialog.h"
#include "ct_itemdrawable/ct_scanpath.h"
#include "ct_coordinates/tools/ct_coordinatesystemmanager.h"

#define CHK_ERR(argFunc, argErrStr) if(!error && !argFunc) { error = true; PS_LOG->addErrorMessage(LogInterface::reader, argErrStr); }

#define X_COLUMN "Easting"
#define Y_COLUMN "Northing"
#define Z_COLUMN "Height"
#define GPS_TIME_COLUMN tr("GPStime")

CT_Reader_Trajectory::CT_Reader_Trajectory() : CT_AbstractReader(), CT_ReaderPointsFilteringExtension()
{
    m_firstConfiguration = true;
    m_columnXIndex = -1;
    m_columnYIndex = -1;
    m_columnZIndex = -1;
    m_columnGPSTimeIndex = -1;
    m_nLinesToSkip = 0;
    m_hasHeader = false;
    m_separator = ".";
    m_localeName = "";
}

QString CT_Reader_Trajectory::GetReaderName() const
{
    return tr("Trajectoire, Fichiers ASCII");
}

CT_StepsMenu::LevelPredefined CT_Reader_Trajectory::getReaderSubMenuName() const
{
    return CT_StepsMenu::LP_Points;
}

bool CT_Reader_Trajectory::configure()
{
    // Columns that can be found in the ascii file
    QList<CT_TextFileConfigurationFields> fieldList;
    fieldList.append(CT_TextFileConfigurationFields(X_COLUMN, QRegExp(" *[xX] *| *[eE][aA][sS][tT] *")));            // x
    fieldList.append(CT_TextFileConfigurationFields(Y_COLUMN, QRegExp(" *[yY] *| *[nN][oO][rR][tT][hH] *")));            // y
    fieldList.append(CT_TextFileConfigurationFields(Z_COLUMN, QRegExp(" *[zZ] *| *[hH][eE][iI][gG][hH][tT] *")));            // z
    fieldList.append(CT_TextFileConfigurationFields(GPS_TIME_COLUMN, QRegExp("[gG][pP][sS] *| *[tT][iI][mM][eE] *")));      // GPS Time


    // a configurable dialog that help the user to select the right column and auto-detect some columns
    CT_TextFileConfigurationDialog dialog(fieldList, NULL, filepath());
    dialog.setFileExtensionAccepted(readableFormats());
    dialog.setFilePathCanBeModified(filePathCanBeModified());

    if (QFileInfo(filepath()).suffix() == "ptx")
    {
        m_hasHeader = false;
        m_nLinesToSkip = 6;
        m_separator = " ";

        dialog.setHeader(m_hasHeader);
        dialog.setNLinesToSkip(m_nLinesToSkip);
        dialog.setSeparator(m_separator);
    }

    // table that link a sought column to a column in the ascii file
    QMap<QString, int> corresp;

    // if it is not the first configuration
    if(!m_firstConfiguration)
    {
        // we will set by default columns detected in last configuration
        corresp.insert(X_COLUMN, m_columnXIndex);
        corresp.insert(Y_COLUMN, m_columnYIndex);
        corresp.insert(Z_COLUMN, m_columnZIndex);
        corresp.insert(GPS_TIME_COLUMN, m_columnGPSTimeIndex);

        // and other elements like header, number of lines to skip, etc...
        dialog.setHeader(m_hasHeader);
        dialog.setNLinesToSkip(m_nLinesToSkip);
        dialog.setSeparator(m_separator);
        dialog.setQLocale(m_localeName);
        dialog.setFieldColumnsSelected(corresp);
    }              

    if(dialog.exec() != QDialog::Accepted)
        return false;

    // get the link between sought columns and column in the ascii file selected by the user
    corresp = dialog.getNeededFieldColumns();

    if((corresp.value(X_COLUMN, -1) < 0)
            || (corresp.value(Y_COLUMN, -1)  < 0)
            || (corresp.value(Z_COLUMN, -1)  < 0))
        return false;

    m_firstConfiguration = false;

    int columnXIndex = corresp.value(X_COLUMN);
    int columnYIndex = corresp.value(Y_COLUMN);
    int columnZIndex = corresp.value(Z_COLUMN);
    int columnGPSTimeIndex = corresp.value(GPS_TIME_COLUMN, -1);

    m_columnXIndex = columnXIndex;
    m_columnYIndex = columnYIndex;
    m_columnZIndex = columnZIndex;
    m_columnGPSTimeIndex = columnGPSTimeIndex;

    m_nLinesToSkip = dialog.getNlinesToSkip();
    m_hasHeader = dialog.hasHeader();
    m_separator = dialog.getSeparator();
    m_localeName = dialog.getQLocaleName();

    if(!filePathCanBeModified() && !filepath().isEmpty())
        return true;

    return setFilePath(dialog.getFileNameWithPath());
}

void CT_Reader_Trajectory::saveSettings(SettingsWriterInterface &writer)
{
    writer.addParameter(this, "HasHeader", m_hasHeader);
    writer.addParameter(this, "NLinesToSkip", m_nLinesToSkip);
    writer.addParameter(this, "Separator", m_separator);
    writer.addParameter(this, "LocaleName", m_localeName);
    writer.addParameter(this, "ColumnXIndex", m_columnXIndex);
    writer.addParameter(this, "ColumnYIndex", m_columnYIndex);
    writer.addParameter(this, "ColumnZIndex", m_columnZIndex);
    writer.addParameter(this, "ColumnGPSTimeIndex", m_columnGPSTimeIndex);

    SuperClass::saveSettings(writer);
}

bool CT_Reader_Trajectory::restoreSettings(SettingsReaderInterface &reader)
{
    QVariant value;

    if(reader.parameter(this, "HasHeader", value))
        m_hasHeader = value.toBool();

    if(reader.parameter(this, "NLinesToSkip", value))
        m_nLinesToSkip = value.toInt();

    if(reader.parameter(this, "Separator", value))
        m_separator = value.toString();

    if(reader.parameter(this, "LocaleName", value))
        m_localeName = value.toString();

    if(reader.parameter(this, "ColumnXIndex", value))
        m_columnXIndex = value.toInt();

    if(reader.parameter(this, "ColumnYIndex", value))
        m_columnYIndex = value.toInt();

    if(reader.parameter(this, "ColumnZIndex", value))
        m_columnZIndex = value.toInt();

    if(reader.parameter(this, "ColumnGPSTimeIndex", value))
        m_columnGPSTimeIndex = value.toInt();

    return SuperClass::restoreSettings(reader);
}

SettingsNodeGroup *CT_Reader_Trajectory::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractReader::getAllSettings();

    SettingsNodeGroup *group = new SettingsNodeGroup("CT_Reader_Trajectory_Settings");
    group->addValue(new SettingsNodeValue("Version", "1"));
    group->addValue(new SettingsNodeValue("HasHeader", m_hasHeader));
    group->addValue(new SettingsNodeValue("NLinesToSkip", m_nLinesToSkip));
    group->addValue(new SettingsNodeValue("Separator", m_separator));
    group->addValue(new SettingsNodeValue("LocaleName", m_localeName));
    group->addValue(new SettingsNodeValue("ColumnXIndex", m_columnXIndex));
    group->addValue(new SettingsNodeValue("ColumnYIndex", m_columnYIndex));
    group->addValue(new SettingsNodeValue("ColumnZIndex", m_columnZIndex));
    group->addValue(new SettingsNodeValue("ColumnGPSTimeIndex", m_columnGPSTimeIndex));

    root->addGroup(group);

    return root;
}

bool CT_Reader_Trajectory::setAllSettings(const SettingsNodeGroup *settings)
{
    if(CT_AbstractReader::setAllSettings(settings))
    {
        QList<SettingsNodeGroup*> groups = settings->groupsByTagName("CT_Reader_Trajectory_Settings");

        if(groups.isEmpty())
            return false;

        QList<SettingsNodeValue*> values = groups.first()->valuesByTagName("HasHeader");
        if(values.isEmpty()) {return false;}
        m_hasHeader = values.first()->value().toBool();

        values = groups.first()->valuesByTagName("NLinesToSkip");
        if(values.isEmpty()) {return false;}
        m_nLinesToSkip = values.first()->value().toInt();

        values = groups.first()->valuesByTagName("Separator");
        if(values.isEmpty()) {return false;}
        m_separator = values.first()->value().toString();

        values = groups.first()->valuesByTagName("LocaleName");
        if(values.isEmpty()) {return false;}
        m_localeName = values.first()->value().toString();

        values = groups.first()->valuesByTagName("ColumnXIndex");
        if(values.isEmpty()) {return false;}
        m_columnXIndex = values.first()->value().toInt();

        values = groups.first()->valuesByTagName("ColumnYIndex");
        if(values.isEmpty()) {return false;}
        m_columnYIndex = values.first()->value().toInt();

        values = groups.first()->valuesByTagName("ColumnZIndex");
        if(values.isEmpty()) {return false;}
        m_columnZIndex = values.first()->value().toInt();

        values = groups.first()->valuesByTagName("ColumnGPSTimeIndex");
        if(values.isEmpty()) {return false;}
        m_columnGPSTimeIndex = values.first()->value().toInt();

        m_firstConfiguration = false;

        return true;
    }

    return false;
}

void CT_Reader_Trajectory::setXColumnIndex(int index)
{
    m_columnXIndex = index;
}

void CT_Reader_Trajectory::setYColumnIndex(int index)
{
    m_columnYIndex = index;
}

void CT_Reader_Trajectory::setZColumnIndex(int index)
{
    m_columnZIndex = index;
}

void CT_Reader_Trajectory::setGPSTimeColumnIndex(int index)
{
    m_columnGPSTimeIndex = index;
}

void CT_Reader_Trajectory::setFirstConfiguration(bool first)
{
    m_firstConfiguration = first;
}

void CT_Reader_Trajectory::setLinesToSkip(int skip)
{
    m_nLinesToSkip = skip;
}

void CT_Reader_Trajectory::setHasHeader(bool hasHeader)
{
    m_hasHeader = hasHeader;
}

void CT_Reader_Trajectory::setValueSeparator(QString sep)
{
    m_separator = sep;
}

void CT_Reader_Trajectory::setLocaleName(QString locale)
{
    m_localeName = locale;
}

int CT_Reader_Trajectory::xColumnIndex() const
{
    return m_columnXIndex;
}

int CT_Reader_Trajectory::yColumnIndex() const
{
    return m_columnYIndex;
}

int CT_Reader_Trajectory::zColumnIndex() const
{
    return m_columnZIndex;
}

int CT_Reader_Trajectory::gpsTimeColumnIndex() const
{
    return m_columnGPSTimeIndex;
}

bool CT_Reader_Trajectory::canLoadPoints() const
{
    return (m_columnXIndex >= 0) && (m_columnYIndex >= 0) && (m_columnZIndex >= 0) && (m_columnGPSTimeIndex >= 0);
}

CT_AbstractReader* CT_Reader_Trajectory::copy() const
{
    return new CT_Reader_Trajectory();
}

void CT_Reader_Trajectory::protectedInit()
{
    addNewReadableFormat(FileFormat(QStringList() << "xyz" << "asc" << "txt" << "csv" << "ptx", tr("Fichiers ascii")));

    setToolTip(tr("Chargement d'un fichier de trajectoire au format ASCII.<br>"
                  "L'import est configurable, le fichier devant contenir les champs suivants :<br>"
                  "- X          : Coordonnée X du points<br>"
                  "- Y          : Coordonnée Y du point<br>"
                  "- Z          : Coordonnée Y du point<br><br>"
                  "- GPSTime    : Temps GPS du point<br>"));
}

void CT_Reader_Trajectory::protectedCreateOutItemDrawableModelList()
{
    CT_AbstractReader::protectedCreateOutItemDrawableModelList();

    if(canLoadPoints())
        addOutItemDrawableModel(DEF_CT_Reader_Trajectory_pointCloudOut, new CT_ScanPath());
}

bool CT_Reader_Trajectory::protectedReadFile()
{
    if(!canLoadPoints())
        return false;

    if(QFile::exists(filepath()))
    {
        QFile f(filepath());

        if(f.open(QIODevice::ReadOnly))
        {
            QTextStream stream(&f);
            QString     currentLine;
            QStringList wordsOfLine;
            CT_Point point;

            int nLine = 0;
            int maxIndex = maxColumnIndex();

            // Getting the file size to set progress
            qint64 fileSize = f.size();
            qint64 currentSizeRead = 0;

            skipLines(stream, currentSizeRead);

            QLocale locale(m_localeName);
            bool error = false;

            CT_ScanPath* scanPath = new CT_ScanPath(NULL, NULL);
            // While we did not reached the end of file
            while(!stream.atEnd() && !isStopped() && !error) {
                // Read the currentLine
                ++nLine;
                currentLine = stream.readLine();
                currentSizeRead += currentLine.size();
                setProgress((currentSizeRead*100) / fileSize);

                if(currentLine.isEmpty())
                    continue;

                // Read each word separately
                wordsOfLine = currentLine.split(m_separator, QString::SkipEmptyParts);

                // Checking for a valid line
                if(maxIndex >= wordsOfLine.size()) {
                    PS_LOG->addErrorMessage(LogInterface::reader, tr("Error loading at line %1: missing columns.").arg(nLine));
                    error = true;
                }

                Eigen::Vector3d pt;
                double gpsTime;

                CHK_ERR(readPoint(wordsOfLine, locale, pt, gpsTime), tr("Error loading point at line %1").arg(nLine));

                scanPath->addPathPoint(gpsTime, pt);
            }

            f.close();

            addOutItemDrawable(DEF_CT_Reader_Trajectory_pointCloudOut, scanPath);

            return !error;
        }
    }

    return false;
}

int CT_Reader_Trajectory::maxColumnIndex() const
{
    int index = -1;
    index = qMax(index, m_columnXIndex);
    index = qMax(index, m_columnYIndex);
    index = qMax(index, m_columnZIndex);
    index = qMax(index, m_columnGPSTimeIndex);

    return index;
}


void CT_Reader_Trajectory::skipLines(QTextStream &stream, qint64 &currentSizeRead) const
{
    for(int i = 0 ; i < m_nLinesToSkip; ++i)
        currentSizeRead += stream.readLine().size();

    if (m_hasHeader)
        currentSizeRead += stream.readLine().size();
}

bool CT_Reader_Trajectory::readPoint(const QStringList &wordsOfLine, const QLocale &locale, Eigen::Vector3d &point, double &gpsTime) const
{
    bool ok;

    point(0) = locale.toDouble(wordsOfLine.at(m_columnXIndex), &ok);

    if(!ok)
        return false;

    point(1) = locale.toDouble(wordsOfLine.at(m_columnYIndex), &ok);

    if(!ok)
        return false;

    point(2) = locale.toDouble(wordsOfLine.at(m_columnZIndex), &ok);

    if(!ok)
        return false;

    gpsTime = locale.toDouble(wordsOfLine.at(m_columnGPSTimeIndex), &ok);

    if(!ok)
        return false;


    return true;
}


