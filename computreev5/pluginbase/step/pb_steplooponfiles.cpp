#include "pb_steplooponfiles.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_genericconfigurablewidget.h"
#include "ct_abstractstepplugin.h"
#include "ct_reader/ct_standardreaderseparator.h"

#include "ct_view/ct_combobox.h"
#include "ct_global/ct_context.h"
#include "ct_model/tools/ct_modelsearchhelper.h"
#include "ct_itemdrawable/ct_readeritem.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"

#include <QDebug>

// Alias for indexing models
#define DEFout_res "res"
#define DEFout_grp "grp"
#define DEFout_grpHeader "grpHeader"
#define DEFout_header "header"
#define DEFout_reader "reader"

// Constructor : initialization of parameters
PB_StepLoopOnFiles::PB_StepLoopOnFiles(CT_StepInitializeData &dataInit) : CT_StepBeginLoop(dataInit)
{
    _readersListValue = "";
    _currentFile = 0;

    initListOfAvailableReaders();
}

PB_StepLoopOnFiles::~PB_StepLoopOnFiles()
{
    clear();
}

// Step description (tooltip of contextual menu)
QString PB_StepLoopOnFiles::getStepDescription() const
{
    return tr("5- Loops on files in selected directory");
}

// Step detailled description
QString PB_StepLoopOnFiles::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString PB_StepLoopOnFiles::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* PB_StepLoopOnFiles::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new PB_StepLoopOnFiles(dataInit);
}


//////////////////// PROTECTED METHODS //////////////////

void PB_StepLoopOnFiles::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    QStringList list_readersList;

    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while (it.hasNext())
        list_readersList.append(it.next()->GetReaderClassName());

    if (list_readersList.isEmpty())
        list_readersList.append(tr("ERREUR : aucun reader disponible"));

    configDialog->addStringChoice(tr("Choose file type"), "", list_readersList, _readersListValue);
}

bool PB_StepLoopOnFiles::postConfigure()
{
    CT_GenericConfigurableWidget configDialog;
    configDialog.addFileChoice(tr("Choose directory containing files"), CT_FileChoiceButton::OneExistingFolder, "", _filesfolder);

    if(CT_ConfigurableWidgetToDialog::exec(&configDialog) == QDialog::Accepted)
    {
        setSettingsModified(true);
        return true;
    }

    return false;
}

// Creation and affiliation of OUT models
void PB_StepLoopOnFiles::createOutResultModelListProtected(CT_OutResultModelGroup *firstResultModel)
{
    Q_UNUSED(firstResultModel);

    // create a new result
    CT_OutResultModelGroup *outRes = createNewOutResultModel(DEFout_res, tr("Liste de readers"));

    // add a root group
    outRes->setRootGroup(DEFout_grp, new CT_StandardItemGroup(), tr("Groupe"));

    // get the reader selected
    CT_AbstractReader *reader = getReader(_readersListValue);

    // if one reader was selected and at least one file is defined
    if (reader != NULL && _filesfolder.size() > 0)
    {
        // get the header
        CT_FileHeader *rHeader = reader->createHeaderPrototype();

        if(rHeader != NULL) {
            // copy the reader (copyFull = with configuration and models)
            CT_AbstractReader* readerCpy = reader->copyFull();

            outRes->addGroupModel(DEFout_grp, DEFout_grpHeader, new CT_StandardItemGroup(), tr("File"));
            outRes->addItemModel(DEFout_grpHeader, DEFout_reader, new CT_ReaderItem(NULL, NULL, readerCpy), tr("Reader"));
            outRes->addItemModel(DEFout_grpHeader, DEFout_header, rHeader, tr("Header"));
        }
    }
}

void PB_StepLoopOnFiles::compute(CT_ResultGroup *outRes, CT_StandardItemGroup* group)
{
    Q_UNUSED(outRes);
    Q_UNUSED(group);

    QList<CT_ResultGroup*> outResultList = getOutResultList();

    // get the out result
    CT_ResultGroup* resultOut = outResultList.at(1);

    int currentTurn = (int) _counter->getCurrentTurn();

    if (currentTurn == 1)
    {
        if(_filesfolder.isEmpty()) {_counter->setNTurns(1); return;}

        QStringList fileFilters = getFormat(_readersListValue);

        CT_AbstractReader *reader = getReader(_readersListValue);

        int nturns = 0;
        if(reader != NULL)
        {
            _dirIterator = new QDirIterator(_filesfolder.first(), fileFilters ,QDir::Files | QDir::NoSymLinks, QDirIterator::NoIteratorFlags);

            while (_dirIterator->hasNext()) {_dirIterator->next(); ++nturns;}
            delete _dirIterator;
            _dirIterator = new QDirIterator(_filesfolder.first(), fileFilters ,QDir::Files | QDir::NoSymLinks, QDirIterator::NoIteratorFlags);
        }

        _counter->setNTurns(nturns);
    }

    // search the model for headers
    CT_OutAbstractItemModel* headerModel = (CT_OutAbstractItemModel*)PS_MODELS->searchModelForCreation(DEFout_header, resultOut);

    // create the root group and add it to result
    CT_StandardItemGroup* grp = new CT_StandardItemGroup(DEFout_grp, resultOut);
    resultOut->addGroup(grp);

    CT_AbstractReader *reader = getReader(_readersListValue);

    if(reader != NULL)
    {
        // for each current file in the list
        // copy the reader (copyFull = with configuration and models)
        CT_AbstractReader* readerCpy = reader->copyFull();

        if (_dirIterator->hasNext())
        {
            QString filepath = _dirIterator->next();
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Chargement du fichier %1").arg(filepath));

            // set the new filepath and check if it is valid
            if (readerCpy->setFilePath(filepath))
            {
                if (readerCpy->configure())
                {

                    // create models of this reader
                    if(readerCpy->outItemDrawableModels().isEmpty() && reader->outGroupsModel().isEmpty())
                    {
                        readerCpy->createOutItemDrawableModelList();
                    }

                    // create the group that will contains header and reader (represent a File)
                    CT_StandardItemGroup* grpHeader = new CT_StandardItemGroup(DEFout_grpHeader, resultOut);

                    CT_FileHeader *header = readerCpy->readHeader();
                    header->changeResult(resultOut);
                    header->setModel(headerModel);

                    // add the header
                    grpHeader->addItemDrawable(header);

                    // add the reader
                    grpHeader->addItemDrawable(new CT_ReaderItem(DEFout_reader, resultOut, readerCpy));

                    // add the group to the root
                    grp->addGroup(grpHeader);
                }
            }
            else
            {
                PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Fichier %1 inexistant ou non valide").arg(filepath));
                delete readerCpy;
            }
        }
    }

}

SettingsNodeGroup *PB_StepLoopOnFiles::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractStepCanBeAddedFirst::getAllSettings();
    SettingsNodeGroup *group = new SettingsNodeGroup("PB_StepLoopOnFiles");
    group->addValue(new SettingsNodeValue("Version", "1"));

    SettingsNodeGroup *settings = new SettingsNodeGroup("Settings");
    settings->addValue(new SettingsNodeValue("ReaderSelected", _readersListValue));

    QString fileFolder = "";
    if (_filesfolder.size() > 0)
    {
        fileFolder = _filesfolder.first();
    }
    settings->addValue(new SettingsNodeValue("path", fileFolder));


    SettingsNodeGroup *readerSettings = new SettingsNodeGroup("ReaderSettings");
    settings->addGroup(readerSettings);

    CT_AbstractReader *reader = getReader(_readersListValue);

    if(reader != NULL)
        readerSettings->addGroup(reader->getAllSettings());

    group->addGroup(settings);
    root->addGroup(group);

    return root;
}

bool PB_StepLoopOnFiles::setAllSettings(const SettingsNodeGroup *settings)
{
    bool ok = CT_AbstractStepCanBeAddedFirst::setAllSettings(settings);

    if(ok)
    {
        QList<SettingsNodeGroup*> groups = settings->groupsByTagName("PB_StepLoopOnFiles");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groupsByTagName("Settings");
        if(groups.isEmpty()) {return false;}

        SettingsNodeGroup *settings2 = groups.first();

        QList<SettingsNodeValue*> values = settings2->valuesByTagName("ReaderSelected");
        if(values.isEmpty()) {return false;}

        _readersListValue = values.first()->value().toString();

        _filesfolder.clear();

        values = settings2->valuesByTagName("path");
        if(values.isEmpty()) {return false;}

        _filesfolder.append(values.first()->value().toString());


        groups = settings2->groupsByTagName("ReaderSettings");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groups();

        CT_AbstractReader *reader = getReader(_readersListValue);

        if(reader != NULL) {
            if(groups.isEmpty()) {return false;}

            ok = reader->setAllSettings(groups.first());
        }

        //        if(!groups.isEmpty())
        //            return false;

        return true;
    }

    return ok;
}

void PB_StepLoopOnFiles::initListOfAvailableReaders()
{
    clear();

    // get the plugin manager
    PluginManagerInterface *pm = PS_CONTEXT->pluginManager();
    int s = pm->countPluginLoaded();

    // for each plugin
    for(int i = 0; i < s; ++i)
    {
        CT_AbstractStepPlugin *p = pm->getPlugin(i);

        // get readers
        QList<CT_StandardReaderSeparator*> rsl = p->getReadersAvailable();
        QListIterator<CT_StandardReaderSeparator*> itR(rsl);

        while(itR.hasNext())
        {
            CT_StandardReaderSeparator *rs = itR.next();
            QListIterator<CT_AbstractReader*> itE(rs->readers());

            while(itE.hasNext())
            {
                CT_AbstractReader *reader = itE.next();

                // copy the reader
                CT_AbstractReader *readerCpy = reader->copy();
                readerCpy->init(false);

                // and add it to the list
                _readersInstancesList.append(readerCpy);
            }
        }
    }
}

void PB_StepLoopOnFiles::clear()
{
    qDeleteAll(_readersInstancesList.begin(), _readersInstancesList.end());
    _readersInstancesList.clear();
}

QStringList PB_StepLoopOnFiles::getFormat(QString readerClassName) const
{
    CT_AbstractReader *reader = getReader(readerClassName);

    QStringList formats;
    if(reader != NULL) {
        FileFormat fileFormat = reader->readableFormats().first();

        const QList<QString> &suffixes = fileFormat.suffixes();
        for (int i = 0 ; i < suffixes.size() ; i++)
        {
            QString formatText = "*.";
            formatText.append(suffixes.at(i));
            formats.append(formatText);
        }
    }
    return formats;
}

CT_AbstractReader *PB_StepLoopOnFiles::getReader(QString readerClassName) const
{
    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while(it.hasNext()) {
        CT_AbstractReader *reader = it.next();

        if(reader->GetReaderClassName() == readerClassName) {
            return reader;
        }
    }

    return NULL;
}

