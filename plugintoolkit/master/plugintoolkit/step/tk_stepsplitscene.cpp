#include "tk_stepsplitscene.h"

#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_itemdrawable/ct_scene.h"

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

#define DEF_SearchInResult "rin"
#define DEF_SearchInGroup   "gin"
#define DEF_SearchInScene   "scin"

TK_StepSplitScene::TK_StepSplitScene(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _size   = 20.0;
    _spacing = 15.0;
}

QString TK_StepSplitScene::getStepDescription() const
{
    return tr("Extraire des tranches horizontales");
}

QString TK_StepSplitScene::getStepDetailledDescription() const
{
    return tr("To Do");
}

CT_VirtualAbstractStep* TK_StepSplitScene::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new TK_StepSplitScene(dataInit);
}

//////////////////// PROTECTED //////////////////

void TK_StepSplitScene::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scène(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène"));
}

// Création et affiliation des modèles OUT
void TK_StepSplitScene::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL) {
        res->addGroupModel(DEF_SearchInGroup, _outGroup_ModelName, new CT_StandardItemGroup(), tr("Groupe"));
        res->addItemModel(_outGroup_ModelName, _outScene_ModelName, new CT_Scene(), tr("Scène extraite"));
    }
}

void TK_StepSplitScene::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Epaisseur d'une tranche :"), "m", -1e+10, 1e+10, 4, _size);
    configDialog->addDouble(tr("Espacement des tranches :"), "m", -1e+10, 1e+10, 4, _spacing);
}

void TK_StepSplitScene::compute()
{
    // récupération du résultats IN et OUT
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    while(!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup *grp = (CT_StandardItemGroup*) it.next();

        const CT_Scene *in_scene = (const CT_Scene*) grp->firstItemByINModelName(this, DEF_SearchInScene);
        const CT_AbstractPointCloudIndex *pointCloudIndex = in_scene->getPointCloudIndex();
        size_t n_points = pointCloudIndex->size();

        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène d'entrée comporte %1 points.")).arg(n_points));

        // Délimitation des sous-placettes
        QList<double> minX;
        QList<double> minY;
        QList<double> maxX;
        QList<double> maxY;
        QList<CT_PointCloudIndexVector*> indexVectors;

        double x = in_scene->minX();
        double y = in_scene->minY();

        while (y <= in_scene->maxY())
        {
            while (x <= in_scene->maxX())
            {
                minX.append(x);
                maxX.append(x + _size);
                minY.append(y);
                maxY.append(y + _size);
                indexVectors.append(new CT_PointCloudIndexVector());

                x += _spacing;
            }
            x = in_scene->minX();
            y += _spacing;
        }


        size_t i = 0;
        CT_PointIterator itP(pointCloudIndex);
        while(itP.hasNext() && (!isStopped()))
        {
            const CT_Point &point = itP.next().currentPoint();
            size_t index = itP.currentGlobalIndex();

            for (int i = 0 ; i < indexVectors.size() ; i++)
            {
                if (point(0) >= minX.at(i) &&
                    point(0) <  maxX.at(i) &&
                    point(1) >= minY.at(i) &&
                    point(1) <  maxY.at(i))
                {
                    indexVectors[i]->addIndex(index);
                }
            }

            // progres de 0 à 100
            if (++i % 10000 == 0) {setProgress(90.0*i/n_points);}
        }

        for (int i = 0 ; i < indexVectors.size() ; i++)
        {

            if (indexVectors.at(i)->size() > 0)
            {

                // creation et ajout de la scene
                CT_Scene *outScene = new CT_Scene(_outScene_ModelName.completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(indexVectors[i]));
                outScene->updateBoundingBox();

                CT_StandardItemGroup *outGroup = new CT_StandardItemGroup(_outGroup_ModelName.completeName(), outResult);
                outGroup->addItemDrawable(outScene);

                // ajout au résultat
                grp->addGroup(outGroup);

            } else {
                delete indexVectors[i];
           }
        }
    }

}
