#include "step/tk_stepextractcylinder.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_math/ct_mathpoint.h"

#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

// Constructor : initialization of parameters
TK_StepExtractCylinder::TK_StepExtractCylinder(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _radius = 1;
    _centerX = 0;
    _centerY = 0;
    _minHeight = -10;
    _maxHeight = 10;
}

// Step description (tooltip of contextual menu)
QString TK_StepExtractCylinder::getStepDescription() const
{
    return tr("Extraire les points dans un cylindre");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepExtractCylinder::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepExtractCylinder(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepExtractCylinder::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_StepExtractCylinder::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_groupIn_inputScene, _outSceneModelName, new CT_Scene(), tr("Extracted Scene (cylinder)"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepExtractCylinder::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Rayon du cylindre"), "", 0, 100, 2, _radius);
    configDialog->addText(tr("Centre du cylindre"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _centerX);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _centerY);
    configDialog->addText(tr("Hauteur (Z)"), "", "");
    configDialog->addDouble(tr("Z Minimum"), "", -1e+10, 1e+10, 4, _minHeight);
    configDialog->addDouble(tr("Z Maximum"), "", -1e+10, 1e+10, 4, _maxHeight);
}

void TK_StepExtractCylinder::compute()
{
    CT_ResultGroup* resultOut = getOutResultList().first();

    CT_ResultGroupIterator it(resultOut, this, DEF_groupIn_inputScene);
    while (it.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Scene* itemIn_scene = (const CT_Scene*)group->firstItemByINModelName(this, DEF_itemIn_scene);
            if (itemIn_scene != NULL)
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = itemIn_scene->getPointCloudIndex();
                size_t nbPoints = pointCloudIndex->size();

                // On Cree un nouveau nuage qui sera le translate
                CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();
                Eigen::Vector3d cylinderCenter( _centerX, _centerY, 0.0 );

                CT_PointIterator itP(pointCloudIndex);

                size_t i = 0;
                while (itP.hasNext() && !isStopped())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    size_t index = itP.currentGlobalIndex();

                    if ( CT_MathPoint::distance2D( point, cylinderCenter ) <= _radius &&
                         point(2) >= _minHeight &&
                         point(2) <= _maxHeight )
                    {
                        extractedCloud->addIndex(index);

                    }

                    setProgress( 100.0*i++ /nbPoints );

                    waitForAckIfInDebugMode();
                }

                if (extractedCloud->size() > 0)
                {
                    CT_Scene* itemOut_scene = new CT_Scene(_outSceneModelName.completeName(), resultOut, PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                    itemOut_scene->updateBoundingBox();

                    group->addItemDrawable(itemOut_scene);
                } else {
                    delete extractedCloud;
                }
            }
        }
    }
}
