#ifndef TK_STEPPLUGINMANAGER_H
#define TK_STEPPLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class TK_StepPluginManager : public CT_AbstractStepPlugin
{
public:
    TK_StepPluginManager();
    ~TK_StepPluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-toolkit/wiki");}

    QString getPluginRISCitation() const;
protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();
};

#endif // TK_STEPPLUGINMANAGER_H
