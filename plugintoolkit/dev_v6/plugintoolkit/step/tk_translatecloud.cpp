#include "tk_translatecloud.h"

TK_TranslateCloud::TK_TranslateCloud() : SuperClass()
{
    _x = 0;
    _y = 0;
    _z = 0;

    // Cette etape est CT_debugable
    setDebuggable( true );
}


QString TK_TranslateCloud::description() const
{
    return tr("Translation des points");
}


CT_VirtualAbstractStep* TK_TranslateCloud::createNewInstance() const
{
    return new TK_TranslateCloud();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_TranslateCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène à translater"));
}


void TK_TranslateCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène translatée"));
}


void TK_TranslateCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Translation selon X"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _x);
    postInputConfigDialog->addDouble(tr("Translation selon Y"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _y);
    postInputConfigDialog->addDouble(tr("Translation selon Z"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _z);
}

void TK_TranslateCloud::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *cloudIndex = inScene->pointCloudIndex();
            size_t cloudSize = cloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_NMPCIR translatedCloud = PS_REPOSITORY->createNewPointCloud(cloudSize);
            CT_MutablePointIterator itPM(translatedCloud);

            // On cree le vecteur de translation
            Eigen::Vector3d translateVector(_x, _y, _z);

            size_t i = 0;
            // On applique la translation a tous les points du nuage
            CT_PointIterator itP(cloudIndex);
            while (itP.hasNext() && itPM.hasNext())
            {
                itP.next();
                itPM.next().replaceCurrentPoint(itP.currentPoint() + translateVector);

                // Barre de progression
                setProgress(float(100.0*i++ / cloudSize));

                // On regarde si on est en debug mode
                waitForAckIfInDebugMode();
            }

            CT_Scene* outScene = new CT_Scene(translatedCloud);
            outScene->setBoundingBox( inScene->minX() + _x, inScene->minY() + _y, inScene->minZ() + _z, inScene->maxX() + _x, inScene->maxY() + _y, inScene->maxZ() + _z);

            group->addSingularItem(_outScene, outScene);
        }
    }
}
