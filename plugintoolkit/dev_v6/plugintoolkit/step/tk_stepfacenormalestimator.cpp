#include "tk_stepfacenormalestimator.h"

#include "ct_accessor/ct_pointaccessor.h"


TK_StepFaceNormalEstimator::TK_StepFaceNormalEstimator() : SuperClass()
{

}

QString TK_StepFaceNormalEstimator::description() const
{
    return tr("Calcul des normales des faces");
}


CT_VirtualAbstractStep* TK_StepFaceNormalEstimator::createNewInstance() const
{
    return new TK_StepFaceNormalEstimator();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepFaceNormalEstimator::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inMesh, tr("Mesh"));

}


void TK_StepFaceNormalEstimator::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outNormals, tr("Normales"));
}


void TK_StepFaceNormalEstimator::compute()
{
    CT_PointAccessor pAccess;
    CT_Point p1;
    CT_Point p2;
    CT_Point p3;

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_MeshModel* inMesh : group->singularItems(_inMesh))
        {
            if (isStopped()) {return;}

            const CT_AbstractFaceCloudIndex* faceIndexes = inMesh->faceCloudIndex();

            // On declare un nuage de normales que l'on va remplir
            CT_NormalCloudStdVector *normalCloud = new CT_NormalCloudStdVector( faceIndexes->size() );

            size_t index = 0;

            CT_FaceIterator fIt(faceIndexes);

            while(fIt.hasNext())
            {
                if (isStopped()) {delete normalCloud; return;}

                fIt.next();
                const CT_Face& face = fIt.cT();

                pAccess.pointAt(face.iPointAt(0), p1);
                pAccess.pointAt(face.iPointAt(1), p2);
                pAccess.pointAt(face.iPointAt(2), p3);

                const CT_Point vu = p2-p1;
                const CT_Point vv = p3-p1;
                const CT_Point normal = vu.normalized().cross(vv).normalized();

                CT_Normal normalTmp(float(normal[0]), float(normal[1]), float(normal[2]), 0);
                normalCloud->replaceNormal(index, normalTmp);
                index++;
            }

            // On cree les attributs que l'on met dans le groupe
            CT_FaceAttributesNormal* normalAttribute = new CT_FaceAttributesNormal(inMesh->faceCloudIndexRegistered(), normalCloud);
            group->addSingularItem(_outNormals, normalAttribute);
        }
    }
}
