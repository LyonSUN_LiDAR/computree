#ifndef TK_STEPTRANSFORMPOINTCLOUD_H
#define TK_STEPTRANSFORMPOINTCLOUD_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_transformationmatrix.h"

class TK_StepTransformPointCloud: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepTransformPointCloud();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    // No parameter for this step

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;

    CT_HandleInResultGroup<>                                                        _inResultMat;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupMat;
    CT_HandleInStdGroup<>                                                               _inGroupMat;
    CT_HandleInSingularItem<CT_TransformationMatrix>                                    _inTransformationMatrix;

};

#endif // TK_STEPTRANSFORMPOINTCLOUD_H
