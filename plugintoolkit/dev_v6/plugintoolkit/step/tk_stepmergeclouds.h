#ifndef TK_STEPMERGECLOUDS_H
#define TK_STEPMERGECLOUDS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"

class TK_StepMergeClouds : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepMergeClouds();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:
    CT_HandleInResultGroup<>                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>  _inScene;
    CT_HandleOutResultGroup                                         _outResult;
    CT_HandleOutStdGroup                                            _outRootGroup;
    CT_HandleOutSingularItem<CT_Scene>                              _outScene;

};

#endif // TK_STEPMERGECLOUDS_H
