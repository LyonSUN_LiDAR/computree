# Computree

You need CMake installed (I think, cause I use it).

Computree is from http://rdinnovation.onf.fr/,  
There you find a forum, wiki and up-to-date svn repository
Initial commit is of Computree v5.0.221b

* Nevertheless you should stick to the CT version from this repository. It includes two bug fixes for Computree v5.0.221b, which prevent compilation if not handled. If you are a C++ programmer experienced in compiling in general you can also fetch from the svn newer versions.

Please stick to the following compiler and qt version and install/use exactly those versions:
* Compiler Microsoft Visual C++ 16.4.29709.9
* Qt 5.9.8 MSVC2015 64bit
* QtCreator version of your choice
* For linux (Ubuntu 20.4) you need to install the following dependencies:
 * **PCL** : sudo apt install libpcl1-dev 
 * **OpenCv**  - 3.2 compilation from source
 * **GSL** sudo apt install libgsl-dev

Then you need to do the following:
* You can open the **all.pro** file in QT Creator.
* Select the correct kit with the above named compiler for 5.9.8
* Under project --> build --> deactivate shadow build both for release and debug.
* run qmake on all subdirectories of all>>base, then qmake base
* right click project base and compile base first, then the remaining plugins
* add the current supported version of SimpleForest go to (https://gitlab.com/SimpleForest/pluginSimpleForest). Modify the **all.pro** file by activating SimpleForest. Compile as well
* Windows:  when starting the compiled program you can receive a crash. You can still copy the fresh compiled (and previously modified) plugin SimpleForest dll from the compilation folder to the included binary folder. In that manner you can adjust SimpleForest and run it under window. For true bug fix, stable Computree version 6 release is awaited.

Summary:

* select the correct kit - you have to create the kit yourself (Compiler Microsoft Visual C++ 16.4.29709.9 and Qt 5.9.8 MSVC2015 64bit)
* select release for configuration 
* disable shadow build - do the same for debug build in case you really want debug



![Configuration](images/computreeConfiguration.png?raw=true "Configuration")
![Configuration2](images/kitConfiguration.png?raw=true "Configuration2")



