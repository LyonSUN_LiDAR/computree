<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>GEN_StepGenerateCone</name>
    <message>
        <source>Generation de points // cône</source>
        <translation type="vanished">Generation of  points // cone</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="49"/>
        <source>Créer un Cône de points</source>
        <translation>Create points on a cone</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="70"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="72"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="73"/>
        <source>Generated Cone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="81"/>
        <source>Sommet</source>
        <translation>Summit</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="85"/>
        <source>Resolutions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="86"/>
        <location filename="../step/gen_stepgeneratecone.cpp" line="91"/>
        <source>Hauteur</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="87"/>
        <location filename="../step/gen_stepgeneratecone.cpp" line="89"/>
        <source>Rayon</source>
        <translation>Radius</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="88"/>
        <source>Angle du cone</source>
        <translation>Cone angle</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecone.cpp" line="90"/>
        <source>Hauteur du cone</source>
        <translation>Cone height</translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateCubeCloud</name>
    <message>
        <source>Generation de points // cube</source>
        <translation type="vanished">Generation of points // cube</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="61"/>
        <source>Créer un Cube de points</source>
        <translation>Create points on a cube</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="113"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="115"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="116"/>
        <source>Generated Cube</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="124"/>
        <source>Bottom left point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="128"/>
        <source>Top right point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="132"/>
        <source>Direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="136"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecubecloud.cpp" line="140"/>
        <source>Add noise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateCylinder</name>
    <message>
        <source>Generation de points // cylindre</source>
        <translation type="vanished">Generation of points // cylinder</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="58"/>
        <source>Créer un Cylindre de points</source>
        <translation>Create points on a cylinder</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="79"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="81"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="82"/>
        <source>Generated Cylinder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="90"/>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="108"/>
        <source>Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="91"/>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="105"/>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="109"/>
        <source>Radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="92"/>
        <source>Minimum theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="93"/>
        <source>Maximum theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="94"/>
        <source>Base center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="98"/>
        <source>Direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="102"/>
        <source>Resolutions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="104"/>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="107"/>
        <source>Theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="106"/>
        <source>Add noise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindercloud.cpp" line="110"/>
        <source>Generate bot and top faces</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateCylinderMesh</name>
    <message>
        <source>Generation d&apos;un mesh // cylindre</source>
        <translation type="vanished">Generation of a mesh // cylinder</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="26"/>
        <source>Créer un Maillage Cylindrique</source>
        <translation>Create a cylindrical mesh</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="46"/>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="49"/>
        <source>Generated Mesh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="48"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="57"/>
        <source>Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="58"/>
        <source>Radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratecylindermesh.cpp" line="59"/>
        <source>Slices</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGeneratePlaneCloud</name>
    <message>
        <source>Generation de points // plan</source>
        <translation type="vanished">Genration of points // plane</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="54"/>
        <source>Créer un Plan de points</source>
        <translation>Create points on a plane</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="75"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="77"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="78"/>
        <source>Generated Plane</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="86"/>
        <source>Plane height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="87"/>
        <source>Bottom left point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="90"/>
        <source>Top right point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="93"/>
        <source>Direction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="97"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateplanecloud.cpp" line="100"/>
        <source>Add noise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateQuadraticSurface</name>
    <message>
        <source>Generation de points // surface quadratique</source>
        <translation type="vanished">Generation of points // quadratic surface</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="51"/>
        <source>Créer une Surface Quadrique de points</source>
        <translation>Create points on a quadric surface</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="72"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="74"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="75"/>
        <source>Generated Quadratic surface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="86"/>
        <source>Parametres</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="94"/>
        <source>Limites</source>
        <translation>Limits</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratequadraticsurface.cpp" line="100"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateRandomCloud</name>
    <message>
        <source>Generation de points au hazard</source>
        <translation type="vanished">Random generation of points</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="42"/>
        <source>Créer des points Aléatoires</source>
        <translation>Create random points</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="63"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="65"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="66"/>
        <source>Generated Random Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="74"/>
        <source>Number fo points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="75"/>
        <source>Bottom left extremum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneraterandomcloud.cpp" line="79"/>
        <source>Top right extremum</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateRaster2DFloat</name>
    <message>
        <source>Generation d&apos;un raster2D de décimaux</source>
        <translation type="vanished">Generatin of a 2D raster with float numbers</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="35"/>
        <source>Créer un Raster (2D)</source>
        <translation>Create a Raster (2D)</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="56"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="58"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="59"/>
        <source>Generated 2D Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="67"/>
        <source>Raster height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="68"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="69"/>
        <source>Bottom left point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="72"/>
        <source>Top right point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="75"/>
        <source>Value range</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="76"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster2d.cpp" line="77"/>
        <source>Max</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateRaster3DFloat</name>
    <message>
        <source>Generation d&apos;une grille 3D de décimaux</source>
        <translation type="vanished">Generation of a 3D grid with float numbers</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="37"/>
        <source>Créer une grille Voxel (3D)</source>
        <translation>Create a voxel grid (3D)</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="58"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="60"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="61"/>
        <source>Generated 3D Grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="69"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="70"/>
        <source>Bottom left point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="74"/>
        <source>Top right point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="78"/>
        <source>Value range</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="79"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster3d.cpp" line="80"/>
        <source>Max</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateRaster4DFloat</name>
    <message>
        <source>Generation d&apos;une grille 4D de décimaux</source>
        <translation type="vanished">Generation of a 4D grid with float number</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="52"/>
        <source>Créer une grille Voxel (4D)</source>
        <translation>Create a voxel grid (4D)</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="73"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="75"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="76"/>
        <source>Generated 4D Grid</source>
        <translation>Generated 4D Grid</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="84"/>
        <source>Resolution W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="85"/>
        <source>Resolution X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="86"/>
        <source>Resolution Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="87"/>
        <source>Resolution Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="88"/>
        <source>Bot W</source>
        <translation>Min W</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="89"/>
        <source>Bot X</source>
        <translation>Min X</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="90"/>
        <source>Bot Y</source>
        <translation>Min Y</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="91"/>
        <source>Bot Z</source>
        <translation>Min Z</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="92"/>
        <source>Top W</source>
        <translation>Max W</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="93"/>
        <source>Top X</source>
        <translation>Max X</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="94"/>
        <source>Top Y</source>
        <translation>Max Y</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="95"/>
        <source>Top Z</source>
        <translation>Max Z</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="96"/>
        <source>Val Min</source>
        <translation>Min value</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateraster4d.cpp" line="97"/>
        <source>Val Max</source>
        <translation>Max value</translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateRay</name>
    <message>
        <source>Generation d&apos;un rayon</source>
        <translation type="vanished">Generation of a beam</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="29"/>
        <source>Créer un Rayon</source>
        <translation>Create a beam</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="50"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="52"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="53"/>
        <source>Generated Beam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="61"/>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateray.cpp" line="65"/>
        <source>Direction</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateScanner</name>
    <message>
        <source>Generation d&apos;une position de scan</source>
        <translation type="vanished">Generation of a scan position</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="33"/>
        <source>Créer une Position de Scan</source>
        <translation>Create a scan position</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="54"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="56"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="57"/>
        <source>Generated Scanner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="65"/>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="69"/>
        <source>Initial angles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="70"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="73"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="76"/>
        <source>Theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="70"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="71"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="73"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="74"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="76"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="77"/>
        <source>degres</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="71"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="74"/>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="77"/>
        <source>Phi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="72"/>
        <source>Field of view</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="75"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratescanner.cpp" line="79"/>
        <source>Clockwise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateShape2D</name>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="54"/>
        <source>Créer des Formes Géométriques 2D</source>
        <translation>Create 2D geometrical shapes</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="75"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="77"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="85"/>
        <source>Generated Box 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="86"/>
        <source>Generated Circle 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="87"/>
        <source>Generated Point 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="88"/>
        <source>Generated Line 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="89"/>
        <source>Generated Polygon 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="90"/>
        <source>Generated Polyline 2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="99"/>
        <source>Nombre de rectangles 2D</source>
        <translation>Number of 2D rectangles</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="100"/>
        <source>Nombre de cercles 2D</source>
        <translation>Number of 2D circles</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="101"/>
        <source>Nombre de points 2D</source>
        <translation>Number of 2D points</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="102"/>
        <source>Nombre de lignes 2D</source>
        <translation>Number of 2D lines</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="103"/>
        <source>Nombre de polygones 2D</source>
        <translation>Number of 2D polygons</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="104"/>
        <source>Nombre de polylignes 2D</source>
        <translation>Number of 2D polylines</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="105"/>
        <source>Xmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="106"/>
        <source>Xmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="107"/>
        <source>Ymin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape2d.cpp" line="108"/>
        <source>Ymax</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateShape3D</name>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="55"/>
        <source>Créer des Formes Géométriques 3D</source>
        <translation>Create 3D geometrical shapes</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="61"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="89"/>
        <source>Generated Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="91"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="98"/>
        <source>Generated Circle 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="99"/>
        <source>Generated Cylinder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="100"/>
        <source>Generated Ellipse 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="101"/>
        <source>Generated Line 3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="102"/>
        <source>Generated Sphere</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="110"/>
        <source>Nombre de cercles 3D</source>
        <translation>Number of 3D circles</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="111"/>
        <source>Nombre de cylindres</source>
        <translation>Number of cylinders</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="112"/>
        <source>Nombre d&apos;ellipses 3D</source>
        <translation>Number of 3D ellipses</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="113"/>
        <source>Nombre de lignes 3D</source>
        <translation>Number of 3D lines</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="114"/>
        <source>Nombre de sphères</source>
        <translation>Number of 3D spheres</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="115"/>
        <source>Xmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="116"/>
        <source>Xmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="117"/>
        <source>Ymin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="118"/>
        <source>Ymax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="119"/>
        <source>Zmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgenerateshape3d.cpp" line="120"/>
        <source>Zmax</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateSphereCloud</name>
    <message>
        <source>Generation de points // cylindre</source>
        <translation type="vanished">Generation of points // cylinder</translation>
    </message>
    <message>
        <source>Generation de points // sphere</source>
        <translation type="vanished">Generation of points // sphere</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="52"/>
        <source>Créer une Sphère de points</source>
        <translation>Create points on a sphere</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="73"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="75"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="76"/>
        <source>Generated Sphere</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="84"/>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="100"/>
        <source>Radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="85"/>
        <source>Sphere center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="89"/>
        <source>Limits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="90"/>
        <source>Minimum theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="91"/>
        <source>Maximum theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="92"/>
        <source>Minimum phi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="93"/>
        <source>Maximum phi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="94"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="95"/>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="98"/>
        <source>Theta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="96"/>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="99"/>
        <source>Phi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratespherecloud.cpp" line="97"/>
        <source>Add noise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GEN_StepGenerateTorus</name>
    <message>
        <source>Generation de points // tore</source>
        <translation type="vanished">Generation of points // torus</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="54"/>
        <source>Créer un Torus de points</source>
        <translation>Create points on a torus</translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="75"/>
        <source>Generated Point Cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="77"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="78"/>
        <source>Generated Torus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="86"/>
        <source>Thore center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="90"/>
        <source>Thore radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="91"/>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="104"/>
        <source>Circle radius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="92"/>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="105"/>
        <source>Distance to center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="93"/>
        <source>Limits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="94"/>
        <source>Minimum alpha</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="95"/>
        <source>Maximum alpha</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="96"/>
        <source>Minimum beta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="97"/>
        <source>Maximum beta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="98"/>
        <source>Resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="99"/>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="102"/>
        <source>Alpha</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="100"/>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="103"/>
        <source>Beta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/gen_stepgeneratetorus.cpp" line="101"/>
        <source>Add noise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../gen_steppluginmanager.cpp" line="64"/>
        <location filename="../gen_steppluginmanager.cpp" line="65"/>
        <location filename="../gen_steppluginmanager.cpp" line="66"/>
        <location filename="../gen_steppluginmanager.cpp" line="67"/>
        <location filename="../gen_steppluginmanager.cpp" line="68"/>
        <location filename="../gen_steppluginmanager.cpp" line="69"/>
        <location filename="../gen_steppluginmanager.cpp" line="70"/>
        <location filename="../gen_steppluginmanager.cpp" line="71"/>
        <location filename="../gen_steppluginmanager.cpp" line="73"/>
        <location filename="../gen_steppluginmanager.cpp" line="75"/>
        <location filename="../gen_steppluginmanager.cpp" line="78"/>
        <location filename="../gen_steppluginmanager.cpp" line="81"/>
        <location filename="../gen_steppluginmanager.cpp" line="83"/>
        <location filename="../gen_steppluginmanager.cpp" line="84"/>
        <location filename="../gen_steppluginmanager.cpp" line="85"/>
        <location filename="../gen_steppluginmanager.cpp" line="86"/>
        <source>Générer (test)</source>
        <translation>Generate (test)</translation>
    </message>
</context>
</TS>
