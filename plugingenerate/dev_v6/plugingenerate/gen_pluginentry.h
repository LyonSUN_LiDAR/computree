#ifndef GEN_PLUGINENTRY_H
#define GEN_PLUGINENTRY_H

#include "pluginentryinterface.h"

class GEN_StepPluginManager;

class GEN_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    GEN_PluginEntry();
    ~GEN_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    GEN_StepPluginManager *_stepPluginManager;
};

#endif // GEN_PLUGINENTRY_H
