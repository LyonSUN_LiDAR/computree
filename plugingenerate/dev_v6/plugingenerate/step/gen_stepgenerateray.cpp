#include "step/gen_stepgenerateray.h"

#include "ct_itemdrawable/ct_beam.h"

// Alias for indexing out models

GEN_StepGenerateRay::GEN_StepGenerateRay() : CT_AbstractStepCanBeAddedFirst()
{
    _posX = 0;
    _posY = 0;
    _posZ = 0;
    _dirX = 1;
    _dirY = 1;
    _dirZ = 1;
}

QString GEN_StepGenerateRay::description() const
{
    return tr("Créer un Rayon");
}

CT_VirtualAbstractStep* GEN_StepGenerateRay::createNewInstance()
{
    return new GEN_StepGenerateRay();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateRay::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateRay::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRay, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRay, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRay, DEF_itemOut_itemRay, new CT_Beam(), tr("Generated Beam"));
}

void GEN_StepGenerateRay::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText(tr("Position"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posZ, 0);
    postInputConfigDialog->addText(tr("Direction"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _dirX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _dirY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _dirZ, 0);
}

void GEN_StepGenerateRay::compute()
{
    CT_ResultGroup* resultOut_resultRay = getOutResultList().first();

    assert( !(_dirX == 0 && _dirY == 0 && _dirZ == 0 ) );

    // ------------------------------
    // Create OUT groups and items
    CT_StandardItemGroup* groupOut_groupRay = new CT_StandardItemGroup(DEF_groupOut_groupRay, resultOut_resultRay);

    CT_Beam* itemOut_itemRay = new CT_Beam(DEF_itemOut_itemRay, resultOut_resultRay, Eigen::Vector3d( _posX, _posY, _posZ ), Eigen::Vector3d( _dirX, _dirY, _dirZ ) );

    groupOut_groupRay->addItemDrawable(itemOut_itemRay);
    resultOut_resultRay->addGroup(groupOut_groupRay);

}
