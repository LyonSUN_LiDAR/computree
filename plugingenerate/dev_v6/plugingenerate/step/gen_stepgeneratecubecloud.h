#ifndef GEN_STEPGENERATECUBECLOUD_H
#define GEN_STEPGENERATECUBECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCubeCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateCubeCloud();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    // Step parameters
    double    _botX;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botY;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botZ;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _topX;    /*!< Coordonnees du point superieur droit du cube */
    double    _topY;    /*!< Coordonnees du point superieur droit du cube */
    double    _topZ;    /*!< Coordonnees du point superieur droit du cube */
    double    _resX;    /*!< Espace entre deux points selon Ox */
    double    _resY;    /*!< Espace entre deux points selon Oy */
    double    _resZ;    /*!< Espace entre deux points selon Oz */
    double    _noiseX;
    double    _noiseY;
    double    _noiseZ;
    double    _axisX;
    double    _axisY;
    double    _axisZ;

//    // Useful parameters for debug visualisation, also use to be the result of the step
//    DocumentInterface*          _m_doc;
//    CT_PointCloudStdVector*     _resultPointCloud;
//    CT_PointCloudIndexVector*   _resultIndexCloud;
//    CT_Scene*                   _resultScene;
//    CT_AxisAlignedBoundingBox*  _resultBoundingBox;
};

#endif // GEN_STEPGENERATECUBECLOUD_H
