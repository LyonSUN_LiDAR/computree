#ifndef GEN_STEPGENERATERASTER4DFLOAT_H
#define GEN_STEPGENERATERASTER4DFLOAT_H

#ifdef USE_OPENCV

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateRaster4DFloat  : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateRaster4DFloat();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _resW;    /*!<  */
    double    _resX;    /*!<  */
    double    _resY;    /*!<  */
    double    _resZ;    /*!<  */
    double    _botW;    /*!<  */
    double    _botX;    /*!<  */
    double    _botY;    /*!<  */
    double    _botZ;    /*!<  */
    double    _topW;    /*!<  */
    double    _topX;    /*!<  */
    double    _topY;    /*!<  */
    double    _topZ;    /*!<  */
    double    _valMin;    /*!<  */
    double    _valMax;    /*!<  */
};

#endif

#endif // GEN_STEPGENERATERASTER4DFLOAT_H
