#ifndef GEN_STEPGENERATERANDOMCLOUD_H
#define GEN_STEPGENERATERANDOMCLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"
#include "ct_itemdrawable/ct_scene.h"

class GEN_StepGenerateRandomCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStepCanBeAddedFirst;

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateRandomCloud();

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString description() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private :

    // Step parameters
    int       _nPts;    /*!<  */
    double    _botX;    /*!<  */
    double    _botY;    /*!<  */
    double    _botZ;    /*!<  */
    double    _topX;    /*!<  */
    double    _topY;    /*!<  */
    double    _topZ;    /*!<  */

    CT_HandleOutResultGroup                 m_hOutResult;
    CT_HandleOutStdGroup                    m_hOutRootGroup;
    CT_HandleOutSingularItem<CT_Scene>      m_hOutScene;
};

#endif // GEN_STEPGENERATERANDOMCLOUD_H
