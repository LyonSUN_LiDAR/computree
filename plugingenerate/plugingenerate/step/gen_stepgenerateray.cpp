#include "step/gen_stepgenerateray.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/ct_beam.h"

// Alias for indexing out models
#define DEF_resultOut_resultRay "GeneratedRay"
#define DEF_groupOut_groupRay "RayGroup"
#define DEF_itemOut_itemRay "Ray"

// Constructor : initialization of parameters
GEN_StepGenerateRay::GEN_StepGenerateRay(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _posX = 0;
    _posY = 0;
    _posZ = 0;
    _dirX = 1;
    _dirY = 1;
    _dirZ = 1;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRay::getStepDescription() const
{
    return tr("Créer un Rayon");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRay::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateRay(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateRay::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateRay::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRay, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRay, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRay, DEF_itemOut_itemRay, new CT_Beam(), tr("Generated Beam"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateRay::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText(tr("Position"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _posX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _posY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _posZ, 0);
    configDialog->addText(tr("Direction"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _dirX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _dirY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _dirZ, 0);
}

void GEN_StepGenerateRay::compute()
{
    CT_ResultGroup* resultOut_resultRay = getOutResultList().first();

    assert( !(_dirX == 0 && _dirY == 0 && _dirZ == 0 ) );

    // ------------------------------
    // Create OUT groups and items
    CT_StandardItemGroup* groupOut_groupRay = new CT_StandardItemGroup(DEF_groupOut_groupRay, resultOut_resultRay);

    CT_Beam* itemOut_itemRay = new CT_Beam(DEF_itemOut_itemRay, resultOut_resultRay, Eigen::Vector3d( _posX, _posY, _posZ ), Eigen::Vector3d( _dirX, _dirY, _dirZ ) );

    groupOut_groupRay->addItemDrawable(itemOut_itemRay);
    resultOut_resultRay->addGroup(groupOut_groupRay);

}
