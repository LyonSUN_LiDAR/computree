#include "gen_stepgenerateraster4d.h"

#ifdef USE_OPENCV

// Using float max
#include <limits>

// Initialise random
#include <ctime>

// Affiche des logs
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Include results group
#include "ct_result/ct_resultgroup.h"

// Alias for indexing out models
#define DEF_resultOut_resultRaster4DFloat "Generated4DRaster"
#define DEF_groupOut_groupRaster4DFloat "4DRasterGroup"
#define DEF_itemOut_itemRaster4DFloat "4DRasters"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid4d_dense.h"

typedef CT_Grid4D_Dense<float> CT_Grid4DFloat;

// Constructor : initialization of parameters
GEN_StepGenerateRaster4DFloat::GEN_StepGenerateRaster4DFloat(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _resW = 1;
    _resX = 1;
    _resY = 1;
    _resZ = 1;
    _botW = -5;
    _botX = -5;
    _botY = -5;
    _botZ = -5;
    _topW = 5;
    _topX = 5;
    _topY = 5;
    _topZ = 5;
    _valMin = 0;
    _valMax = 100;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRaster4DFloat::getStepDescription() const
{
    return tr("Créer une grille Voxel (4D)");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRaster4DFloat::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateRaster4DFloat(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateRaster4DFloat::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateRaster4DFloat::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster4DFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster4DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster4DFloat, DEF_itemOut_itemRaster4DFloat, new CT_Grid4DFloat(), tr("Generated 4D Grid"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateRaster4DFloat::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Resolution W"), "", 0.0001, 1e+10, 4, _resW, 0);
    configDialog->addDouble(tr("Resolution X"), "", 0.0001, 1e+10, 4, _resX, 0);
    configDialog->addDouble(tr("Resolution Y"), "", 0.0001, 1e+10, 4, _resY, 0);
    configDialog->addDouble(tr("Resolution Z"), "", 0.0001, 1e+10, 4, _resZ, 0);
    configDialog->addDouble(tr("Bot W"), "", -1e+10, 1e+10, 4, _botW, 0);
    configDialog->addDouble(tr("Bot X"), "", -1e+10, 1e+10, 4, _botX, 0);
    configDialog->addDouble(tr("Bot Y"), "", -1e+10, 1e+10, 4, _botY, 0);
    configDialog->addDouble(tr("Bot Z"), "", -1e+10, 1e+10, 4, _botZ, 0);
    configDialog->addDouble(tr("Top W"), "", -1e+10, 1e+10, 4, _topW, 0);
    configDialog->addDouble(tr("Top X"), "", -1e+10, 1e+10, 4, _topX, 0);
    configDialog->addDouble(tr("Top Y"), "", -1e+10, 1e+10, 4, _topY, 0);
    configDialog->addDouble(tr("Top Z"), "", -1e+10, 1e+10, 4, _topZ, 0);
    configDialog->addDouble(tr("Val Min"), "", -1e+10, 1e+10, 4, _valMin, 0);
    configDialog->addDouble(tr("Val Max"), "", -1e+10, 1e+10, 4, _valMax, 0);
}

void GEN_StepGenerateRaster4DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster4DFloat = getOutResultList().first();

    // ----------------------------------------------------------------------------

    // UNCOMMENT Following lines and complete parameters of the item's contructor
    CT_Grid4DFloat* itemOut_itemRaster4dFloat = CT_Grid4D_Dense<float>::createGrid4DFromWXYZCoords(DEF_itemOut_itemRaster4DFloat,
                                                                                             resultOut_resultRaster4DFloat,
                                                                                             _botW, _botX, _botY, _botZ, _topW,
                                                                                             _topX, _topY, _topZ,
                                                                                             _resW,  _resX, _resY, _resZ,
                                                                                             -std::numeric_limits<float>::max(), 0);

    // Initialise random
    srand( time(0) );

    // Fill with random values between min and max
    size_t nbVoxels = itemOut_itemRaster4dFloat->nCells();
    PS_LOG->addMessage( LogInterface::info, LogInterface::step, "Raster avec " + QString::number(nbVoxels) + " cellules" );
    for ( size_t i = 0 ; (i < nbVoxels) && !isStopped() ; i++ )
    {
        itemOut_itemRaster4dFloat->setValueAtIndex( i, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
    }

    if(!isStopped()) {
        // Update min and max
        itemOut_itemRaster4dFloat->computeMinMax();

        // Works on the result corresponding to DEF_resultOut_resultRaster4DFloat
        CT_StandardItemGroup* groupOut_groupRaster4DFLoat = new CT_StandardItemGroup(DEF_groupOut_groupRaster4DFloat, resultOut_resultRaster4DFloat);

        groupOut_groupRaster4DFLoat->addItemDrawable(itemOut_itemRaster4dFloat);
        resultOut_resultRaster4DFloat->addGroup(groupOut_groupRaster4DFLoat);
    } else {
        delete itemOut_itemRaster4dFloat;
    }
}

#endif
