#ifndef LVOX3_STEPEXPORTCROWNSTATS_H
#define LVOX3_STEPEXPORTCROWNSTATS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include "../tools/3dgrid/abstract/lvox3_abstractgrid3d.h"

class LVOX3_StepExportCrownStats: public CT_AbstractStep
{
    Q_OBJECT
public:
    LVOX3_StepExportCrownStats(CT_StepInitializeData &dataInit);

    /**
     * @brief Return a short description of what do this class
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;
    /**
     * @brief Return a new empty instance of this class
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
protected:
    /**
     * @brief This method defines what kind of input the step can accept
     */
    void createInResultModelListProtected();

    /**
     * @brief Show the post configuration dialog.
     *
     * If you want to show your own configuration dialog your must overload this method and show your dialog when this method is called. Don't forget
     * to call the method "setSettingsModified(true)" if your settings is modified (if user accept your dialog).
     *
     * @return true if the settings was modified.
     */
    void createPostConfigurationDialog();

    /**
     * @brief This method defines what kind of output the step produces
     */
    void createOutResultModelListProtected();

    /**
     * @brief This method do the job
     */
    void compute();

private:
    QStringList             _saveDirList;
    QString                 _saveDir;
    QString                 _filePrefix;
    bool                    _underTheCanopy;
};

#endif // LVOX3_STEPEXPORTCROWNSTATS_H
