#include "lvox3_stepaddgridtonext.h"

//In/Out dependencies
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools dependencies
#include "ct_view/ct_stepconfigurabledialog.h"
#include "mk/tools/lvox3_gridtype.h"

//Source models(result where the grid will go)
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInSourceItem        "its"

//Target models(result of the grid)
#define DEF_SearchInTargetResult      "rt"
#define DEF_SearchInTargetGroup       "gt"
#define DEF_SearchInGrid              "grid"

LVOX3_StepAddGridToNext::LVOX3_StepAddGridToNext(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    m_gridType = tr("3D Grid");
    m_gridTypeCollection.insert(m_gridType,Grid3D);
    m_gridTypeCollection.insert(tr("2D Grid (MNT)"),Grid2D);
    m_gridType3D = tr("Ni");
    m_gridType3DCollection.insert(m_gridType3D, Ni);
    m_gridType3DCollection.insert(tr("Nt"), Nt);
    m_gridType3DCollection.insert(tr("Nb"), Nb);
}

QString LVOX3_StepAddGridToNext::getStepDescription() const
{
    return tr("Ajouter une grille 2D(MNT)/3D(Ni/Nt/Nb) au résultat d'une étape précédente");
}

// Step detailed description
QString LVOX3_StepAddGridToNext::getStepDetailledDescription() const
{
    return tr("Cette étape permet de rajouter une grille 2D ou 3D à un résultat précédent. L'on peut ensuite s'en servir pour le calcul de caractéristiques des grilles. "
              "À noter que les paramètres devraient être les mêmes pour la grille 3D importée et les grilles 3D calculées présentement pour empêcher des résultats incohérents. (résolution de grille et dimension)");
}

CT_VirtualAbstractStep* LVOX3_StepAddGridToNext::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepAddGridToNext(dataInit);
}

// Choose the grid type to give a little more flexibility in grid choice MNT or 3D Grid
void LVOX3_StepAddGridToNext::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addStringChoice(tr("Type de grille"), "", m_gridTypeCollection.keys(), m_gridType);
}

void LVOX3_StepAddGridToNext::createInResultModelListProtected()
{
    // We must have
    // - at least one grid
    CT_InResultModelGroupToCopy *inResultRefCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Étape d'entrée"), "", true);
    inResultRefCopy->setZeroOrMoreRootGroup();
    inResultRefCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe de référence"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);

    CT_InResultModelGroupToCopy *inResultAffCopy = createNewInResultModelForCopy(DEF_SearchInTargetResult, tr("Grille à affilier"), "", true);
    inResultAffCopy->setZeroOrMoreRootGroup();
    inResultAffCopy->addGroupModel("", DEF_SearchInTargetGroup);
    //If pre configure is 3D, expects a 3D grid or sends an error
    if(m_gridTypeCollection.value(m_gridType) == Grid3D){
        inResultAffCopy->addItemModel(DEF_SearchInTargetGroup, DEF_SearchInGrid, /*LVOX3_AbstractGrid3D::staticGetType()*/lvox::Grid3Di::staticGetType(), tr("Grille 3D"));
    }else{//2D pre configure
        inResultAffCopy->addItemModel(DEF_SearchInTargetGroup, DEF_SearchInGrid, CT_Image2D<float>::staticGetType(), tr("MNT"));
    }
}

void LVOX3_StepAddGridToNext::createPostConfigurationDialog()
{
    //Only adds post configure if you chose the 3D Grid option
    if(m_gridTypeCollection.value(m_gridType) == Grid3D){
        CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

        configDialog->addStringChoice(tr("Type de grille 3D"), "", m_gridType3DCollection.keys(), m_gridType3D);
    }

}

void LVOX3_StepAddGridToNext::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;

    if(m_gridTypeCollection.value(m_gridType) == Grid3D){
        //Gives the correct qualifier to make it visible in the merge grids step
        if(m_gridType3DCollection.value(m_gridType3D) == Ni){
            resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new lvox::Grid3Di(), tr("Grille 3D(Ni)"));
            resultModel->addItemAttributeModel(_grid_ModelName, _NiFlag_ModelName, new CT_StdItemAttributeT<bool>("LVOX_GRD_NI"), tr("isNi"));
        }else if(m_gridType3DCollection.value(m_gridType3D) == Nt){
            resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new lvox::Grid3Di(), tr("Grille 3D(Nt)"));
            resultModel->addItemAttributeModel(_grid_ModelName, _NtFlag_ModelName, new CT_StdItemAttributeT<bool>("LVOX_GRD_NT"), tr("isNt"));
        }else{
            resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new lvox::Grid3Di(), tr("Grille 3D(Nb)"));
            resultModel->addItemAttributeModel(_grid_ModelName, _NbFlag_ModelName, new CT_StdItemAttributeT<bool>("LVOX_GRD_NB"), tr("isNb"));
        }
    }else{
        resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new CT_Image2D<float>(), tr("MNT"));
    }

    //Used as a result in the compute method to associate the target result with the grid result
    CT_OutResultModelGroupToCopyPossibilities *inResultAffCopy = createNewOutResultModelToCopy(DEF_SearchInTargetResult);
}

void LVOX3_StepAddGridToNext::compute()
{
    QList<CT_ResultGroup*> resultList = getOutResultList();

    CT_ResultGroup *sourceRes = resultList.at(0);
    CT_ResultGroup *gridRes = resultList.at(1);

    CT_ResultGroupIterator itR(sourceRes, this, DEF_SearchInSourceGroup); //Iterator for the source group
    CT_ResultGroupIterator itInR(gridRes, this, DEF_SearchInTargetGroup); //Iterator for the target group
    lvox::Grid3Di* outGrid;
    CT_Image2D<float>* outMnt;

    //For every grid in the affilitated result add them to the source result
    while (itInR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        CT_StandardItemGroup *groupGrid = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itInR.next());

        //If the result asked is a 3D Grid
        if(m_gridTypeCollection.value(m_gridType) == Grid3D){
            lvox::Grid3Di* grid = dynamic_cast<lvox::Grid3Di*>(groupGrid->firstItemByINModelName(this, DEF_SearchInGrid));
            if(m_gridType3DCollection.value(m_gridType3D) == Ni){
                grid->addItemAttribute(new CT_StdItemAttributeT<bool>(_NiFlag_ModelName.completeName(), "LVOX_GRD_NI", sourceRes, true));
            }else if(m_gridType3DCollection.value(m_gridType3D) == Nt){
                grid->addItemAttribute(new CT_StdItemAttributeT<bool>(_NtFlag_ModelName.completeName(), "LVOX_GRD_NT", sourceRes, true));
            }else{
                grid->addItemAttribute(new CT_StdItemAttributeT<bool>(_NbFlag_ModelName.completeName(), "LVOX_GRD_NB", sourceRes, true));
            }
            //If the grid has data in it
            if (grid != NULL)
            {
                outGrid = (lvox::Grid3Di*)grid->copy(_grid_ModelName.completeName(),sourceRes,CT_ResultCopyModeList());
                group->addItemDrawable(outGrid);
                outGrid->computeMinMax();
            }
        }else{
            CT_Image2D<float>* mnt = dynamic_cast<CT_Image2D<float>*>(groupGrid->firstItemByINModelName(this, DEF_SearchInGrid));

            //If the mnt has data in it
            if (mnt != NULL)
            {
                outMnt = (CT_Image2D<float>*)mnt->copy(_grid_ModelName.completeName(),sourceRes,CT_ResultCopyModeList());
                group->addItemDrawable(outMnt);
                outMnt->computeMinMax();
            }
        }
    }
}


