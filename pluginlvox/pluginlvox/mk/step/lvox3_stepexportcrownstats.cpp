#include "lvox3_stepexportcrownstats.h"

//In/Out
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"
#include "mk/tools/lvox3_errorcode.h"

//Drawables
#include "mk/tools/lvox3_gridtype.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"

//Models
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInGroup             "grp"
#define DEF_SearchInGridMask          "gridMask"
#define DEF_SearchInGridValue         "gridValue"
#define DEF_SearchInGridSurface       "gridSurface"
#define DEF_SearchInScene             "scn"
#define DEF_SearchInNameGroup         "nameGroup"
#define DEF_SearchInNameField         "nameField"

LVOX3_StepExportCrownStats::LVOX3_StepExportCrownStats(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _filePrefix = "crownStats";
    _saveDirList.clear();
    _underTheCanopy = true;
}

QString LVOX3_StepExportCrownStats::getStepDescription() const
{
    return tr("Exporter des statistiques de couronnes");
}

// Step detailed description (Note that the projected surface right now is not calculated as it's being worked on)
QString LVOX3_StepExportCrownStats::getStepDetailledDescription() const
{
    return tr("Cette étape prend une grille masque, une grille avec de la matière et un nom de fichier en entrée. Cette étape calcule: "
              "- Surface projetée de la couronne"
              "- Surface exposée de la couronne(accessible pour la photosynthèse)"
              "- Volume de la couronne"
              "- Densité de la couronne"
              "- Profondeur de la couronne"
              "- Hauteur de l'arbre(si normalisé)");
}

CT_VirtualAbstractStep* LVOX3_StepExportCrownStats::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepExportCrownStats(dataInit);
}

void LVOX3_StepExportCrownStats::createInResultModelListProtected()
{
    // We must have
    // - at least one grid
    CT_InResultModelGroupToCopy *inResultRefCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Grille d'entrée"), "", true);
    inResultRefCopy->setZeroOrMoreRootGroup();
    inResultRefCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInGridMask, LVOX3_AbstractGrid3D::staticGetType(), tr("Grille de masque"),"",CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInGridSurface, LVOX3_AbstractGrid3D::staticGetType(), tr("Grille de surface"), "", CT_InAbstractModel::C_ChooseOneIfMultiple);
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInGridValue, LVOX3_AbstractGrid3D::staticGetType(), tr("Grille de hits"), "", CT_InAbstractModel::C_ChooseOneIfMultiple);
    inResultRefCopy->addItemModel( DEF_SearchInSourceGroup, DEF_SearchInNameGroup, CT_AbstractSingularItemDrawable::staticGetType(), tr("Entete de fichier"), "", CT_InAbstractModel::C_ChooseOneIfMultiple);
    inResultRefCopy->addItemAttributeModel(DEF_SearchInNameGroup, DEF_SearchInNameField, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("Nom du fichier de nuage de points"), "", CT_InAbstractModel::C_ChooseMultipleIfMultiple);
}

void LVOX3_StepExportCrownStats::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( tr("Dossier de sortie"), CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
    configDialog->addString(tr("Nom du fichier de sortie"), "", _filePrefix );
    configDialog->addBool(tr("Retirer le dessous de la canopé "),"", "", _underTheCanopy);
    configDialog->addEmpty();
    configDialog->addText(tr("Retirer le dessous de la canopé avec une trop grande résolution de grille peut causer des résultats abérrants."),"", "");
}

void LVOX3_StepExportCrownStats::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;
}

void LVOX3_StepExportCrownStats::compute()
{
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itR(outResult, this, DEF_SearchInSourceGroup);

    if( _saveDirList.empty() )
    {
        qDebug() << "Il faut selectionner un dossier de sortie !";
        return;
    }
    _saveDir = _saveDirList.first();

    QString outputFileName = _saveDir + "/" + _filePrefix + ".csv";
    QFile outputFile( outputFileName );
    if( !outputFile.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ouvrir le fichier " << outputFileName << " pour y ecrire";
        return;
    }
    QTextStream outputFileStream( &outputFile );

    //File header
    outputFileStream        << "filename "
                            << "voxelInCrown "
                            << "voxelWithMaterial "
                            << "voxelWithoutMaterial "
                            << "voxelSurface "
                            << "ExposedCrownSurfaceArea(m^2) "
                            << "CrownVolume(m^3) "
                            << "CrownDensity(m^3) "
                            << "CrownDepth(m) "
                            << "CrownHeight(m)(if normalized) "
                            << "Zmin "
                            << "XGridResolution(m) "
                            << "YGridResolution(m) "
                            << "ZGridResolution(m)\n";


    //For every grid in the result
    while (itR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        const LVOX3_AbstractGrid3D* inGridValues = (const LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGridValue);
        const LVOX3_AbstractGrid3D* inGridMask = (const LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGridMask);
        const LVOX3_AbstractGrid3D* inGridSurface = (const LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGridSurface);
        const CT_AbstractSingularItemDrawable* itemIn_name = (CT_AbstractSingularItemDrawable*)group->firstItemByINModelName(this, DEF_SearchInNameGroup);
        QString fileName;

        //if name isnt null
        if(itemIn_name != NULL)
            fileName = itemIn_name->firstItemAttributeByINModelName(outResult, this, DEF_SearchInNameField)->toString(itemIn_name, NULL);

        //if entry results arent null
        if(inGridValues != NULL && inGridSurface != NULL){
            //counters for amount of voxels with/without value in entry and in convex hull
            size_t inHull = 0;
            size_t inHullGoodValue = 0;
            size_t inHullSurface = 0;
            double lowestVoxelZ = std::numeric_limits<double>::max();
            double highestVoxelZ = -std::numeric_limits<double>::max();
            QVector<size_t> lowestVoxelIndexPerZ;
    /*
    //        outputFileStream        << "Entry file name "
    //                                << "\nCrown Projected Surface\n"
    //                                << "ID(col_line) "
    //                                << "X "
    //                                << "Y "
    //                                << "Z "
    //                                << "nVoxelsPerZSlice\n";



            //Iterates through the 3d ingrid
            for(size_t col = 0;col <inGridMask->xdim() && (!isStopped());col++){
                for(size_t lin = 0;lin <inGridMask->ydim() && (!isStopped());lin++){
                    int voxelsPerZSlice = 0;
                    for(size_t level = 0;level <inGridMask->zdim() && (!isStopped());level++){
                        size_t index;
                        double value;
                        inGridMask->index(col, lin, level, index);
                        value = inGridMask->valueAtIndexAsDouble(index);
                        //If mask grid voxel is inside of the convex hull, then look at the value in the entry grid
                        if(value == 1.0){
                            inHull++;
                            size_t indexInGridValues;
                            double valueInGridValues;
                            inGridValues->index(col, lin, level, indexInGridValues);
                            valueInGridValues = inGridValues->valueAtIndexAsDouble(indexInGridValues);
                            if(valueInGridValues > 0){
                                Eigen::Vector3d centerCoordVoxelDensity;
                                inGridMask->getCellCenterCoordinates(indexInGridValues,centerCoordVoxelDensity);
                                inHullGoodValue++;
                                voxelsPerZSlice++; //voxels with data per slice(given that all points should be in the mask, it's okay to assume that they encompass all voxels with a value > 0
                                //intermediary variable for crown depth calculation
                                if(centerCoordVoxelDensity.z() < lowestVoxelZ){
                                    lowestVoxelZ = centerCoordVoxelDensity.z();
                                }
                                if(centerCoordVoxelDensity.z() > highestVoxelZ){
                                    highestVoxelZ = centerCoordVoxelDensity.z();
                                }
                            }
                        }
                    }

    //                //Add line in csv file for projected surface
    //                Eigen::Vector3d centerCoordVoxel;
    //                size_t indexZSlice;
    //                inGridMask->index(col, lin, 0, indexZSlice);
    //                inGridMask->getCellCenterCoordinates(indexZSlice,centerCoordVoxel);

    //                outputFileStream        << col<<"_"<< lin  << " "
    //                                        << centerCoordVoxel.x()  << " "
    //                                        << centerCoordVoxel.y() << " "
    //                                        << centerCoordVoxel.z() << " "
    //                                        << voxelsPerZSlice << "\n";

                }
            }
    */
            //Iterates through the 3d ingrid
            for(size_t col = 0;col <inGridSurface->xdim() && (!isStopped());col++){
                for(size_t lin = 0;lin <inGridSurface->ydim() && (!isStopped());lin++){
                    int voxelsPerZSlice = 0;
                    size_t lowestVoxelIndex = NULL;
                    double lowestVoxelZSlice = std::numeric_limits<double>::max();
                    for(size_t level = 0;level <inGridSurface->zdim() && (!isStopped());level++){
                        size_t index;
                        double value;
                        inGridSurface->index(col, lin, level, index);
                        value = inGridSurface->valueAtIndexAsDouble(index);
                        //If mask grid voxel is inside of the convex hull, then look at the value in the entry grid
                        if(value == 1.0){
                            inHull++;
                            inHullSurface++;
                            inHullGoodValue++;
                            voxelsPerZSlice++;
                            Eigen::Vector3d centerCoordVoxelDensity;
                            inGridSurface->getCellCenterCoordinates(index,centerCoordVoxelDensity);
                            voxelsPerZSlice++; //voxels with data per slice(given that all points should be in the mask, it's okay to assume that they encompass all voxels with a value > 0
                            //intermediary variable for crown depth calculation
                            if(centerCoordVoxelDensity.z() < lowestVoxelZ){
                                lowestVoxelZ = centerCoordVoxelDensity.z();
                                lowestVoxelIndex = index;
                            }
                            if(centerCoordVoxelDensity.z() > highestVoxelZ){
                                highestVoxelZ = centerCoordVoxelDensity.z();
                            }

                            //intermediary variable for crown surface without voxel under the trees
                            if(centerCoordVoxelDensity.z() < lowestVoxelZSlice){
                                lowestVoxelZSlice = centerCoordVoxelDensity.z();
                                lowestVoxelIndex = index;
                            }
                            //value == 2 is a voxel that is in the crown and might have density or no density
                            //helps us quantify gap fraction
                        }else if(value == 2.0){
                            inHull++;
                            size_t indexInGridValues;
                            double valueInGridValues;
                            inGridValues->index(col, lin, level, indexInGridValues);
                            valueInGridValues = inGridValues->valueAtIndexAsDouble(indexInGridValues);
                            //If it's inside the crown and has density
                            if(valueInGridValues > 0){
                                Eigen::Vector3d centerCoordVoxelDensity;
                                inGridSurface->getCellCenterCoordinates(indexInGridValues,centerCoordVoxelDensity);
                                inHullGoodValue++;
                                voxelsPerZSlice++; //voxels with data per slice(given that all points should be in the mask, it's okay to assume that they encompass all voxels with a value > 0
                                //intermediary variable for crown depth calculation
                                if(centerCoordVoxelDensity.z() < lowestVoxelZ){
                                    lowestVoxelZ = centerCoordVoxelDensity.z();
                                }
                                if(centerCoordVoxelDensity.z() > highestVoxelZ){
                                    highestVoxelZ = centerCoordVoxelDensity.z();
                                }
                                //intermediary variable for crown surface without voxel under the trees
                                if(centerCoordVoxelDensity.z() < lowestVoxelZSlice){
                                    lowestVoxelZSlice = centerCoordVoxelDensity.z();
                                    lowestVoxelIndex = indexInGridValues;
                                }
                            }
                        }
                    }
                    //Add to the vector if the index has a nonnull value (to remove voxels that are not exposed for photosynthesis
                    if(lowestVoxelIndex != NULL){
                        lowestVoxelIndexPerZ.append(lowestVoxelIndex);
                    }
    /*
    //                //Add line in csv file for projected surface
    //                Eigen::Vector3d centerCoordVoxel;
    //                size_t indexZSlice;
    //                inGridMask->index(col, lin, 0, indexZSlice);
    //                inGridMask->getCellCenterCoordinates(indexZSlice,centerCoordVoxel);

    //                outputFileStream        << col<<"_"<< lin  << " "
    //                                        << centerCoordVoxel.x()  << " "
    //                                        << centerCoordVoxel.y() << " "
    //                                        << centerCoordVoxel.z() << " "
    //                                        << voxelsPerZSlice << "\n";
                    */
                }
            }

            //if mesh was chosen in the previous step
            if(inGridMask != NULL){
                //Iterates through the 3d inGridMask to compare with surface and find gap fraction inside the alpha shape but outside of the surface of the tree
                for(size_t col = 0;col <inGridMask->xdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGridMask->ydim() && (!isStopped());lin++){
                        for(size_t level = 0;level <inGridMask->zdim() && (!isStopped());level++){
                            size_t index;
                            double value;
                            inGridMask->index(col, lin, level , index);
                            value = inGridMask->valueAtIndexAsDouble(index);
                            //Inside of the mesh (alpha shape)
                            if(value == 1){
                                //0 represents every voxel that is outside of the crown surface
                                if(inGridSurface->valueAtIndexAsDouble(index) == 0){
                                    inHull++;
                                }
                            }
                        }
                    }
                }
            }

            //Remove the number of voxels that are ostensibly not part of the tree that partakes in photosynthesis
            if(_underTheCanopy){
                inHullSurface -= lowestVoxelIndexPerZ.size();
            }

            //Outputs the data for the entirety of the the tree's stats (except for projected surface)
            outputFileStream        << fileName<<" "
                                    << inHull<<" "
                                    << inHullGoodValue<<" "
                                    << inHull-inHullGoodValue<<" "
                                    << inHullSurface<<" "
                                    << inHullSurface*(inGridSurface->xresolution()*inGridSurface->yresolution())<<" "
                                    << inHull*(inGridSurface->xresolution()*inGridSurface->yresolution()*inGridSurface->zresolution())<<" "
                                    << inHullGoodValue*(inGridSurface->xresolution()*inGridSurface->yresolution()*inGridSurface->zresolution())<<" "
                                    << sqrt(pow(highestVoxelZ-lowestVoxelZ,2))<<" "
                                    << highestVoxelZ+(inGridSurface->zresolution()/2)<<" "
                                    << lowestVoxelZ<<" "
                                    << inGridSurface->xresolution()<<" "
                                    << inGridSurface->yresolution()<<" "
                                    << inGridSurface->zresolution()<<"\n";
        }     
    }   
    outputFile.close();
}
