/************************************************************************************
* Filename :  norm_octreenodepca.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_OCTREENODEPCA_H
#define NORM_OCTREENODEPCA_H

#include "norm_octreenode.h"
#include "ct_point.h"

typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;

class NORM_OctreePCA;

class NORM_OctreeNodePCA : public NORM_OctreeNode
{
public :
/* **************************************************************** */
/* Constructuors and destructors                                    */
/* **************************************************************** */
    // Le calcul de la PCA se fait directement dans le constructeur !!
    NORM_OctreeNodePCA(NORM_OctreePCA* octree,
                        NORM_OctreeNodePCA* father,
                        size_t first,
                        size_t last,
                        const CT_Point& center,
                        size_t codex,
                        size_t codey,
                        size_t codez,
                        bool computePCA);

    void createOctreeRecursive( int minPcaDepth );

    // Redefinition from NORM_OctreeNode class
    bool isSubdivisible();

    // Redefinition from NORM_OctreeNode class
    virtual void subdivide();

    // Subdivision en prenant en compte la profondeur minimale a partir de laquelle calculer la pca
    void subdivide( int minPcaDepth );

    void setSigmaAttributeFromNodeRecursive(AttributFloat* attributs);

    inline NORM_OctreeNodePCA* getChild( size_t i )
    {
        return ((NORM_OctreeNodePCA*)_children[i]);
    }

/* **************************************************************** */
/* Getters and setters                                              */
/* **************************************************************** */
    float lambda1() const;
    void setLambda1(float lambda1);

    float lambda2() const;
    void setLambda2(float lambda2);

    float lambda3() const;
    void setLambda3(float lambda3);

    float sigma() const;
    void setSigma(float sigma);

    CT_Point v1() const;
    void setV1(const CT_Point &v1);

    CT_Point v2() const;
    void setV2(const CT_Point &v2);

    CT_Point v3() const;
    void setV3(const CT_Point &v3);

    CT_Point centroid() const;
    void setCentroid(const CT_Point &centroid);

    QVector<CT_Point>* getPointCloudInAcpBasis();

    QVector<CT_Point>* getPointCloudInGlobalBasis();

    void getQuadraticSurface();

    QVector<float>* getErrorOnQuadraticSurface();

    float a() const;
    void setA(float a);

    float b() const;
    void setB(float b);

    float c() const;
    void setC(float c);

    float d() const;
    void setD(float d);

    float e() const;
    void setE(float e);

    float f() const;
    void setF(float f);

protected :
    /* **************************************************************** */
/* Member attributes                                                */
/* **************************************************************** */
    float _lambda1;
    float _lambda2;
    float _lambda3;
    float _sigma;
    float _a;
    float _b;
    float _c;
    float _d;
    float _e;
    float _f;
    CT_Point    _v1;
    CT_Point    _v2;
    CT_Point    _v3;
    CT_Point    _centroid;
};

#endif // NORM_OCTREENODEPCA_H
