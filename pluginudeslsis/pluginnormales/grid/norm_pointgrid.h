#ifndef NORM_POINTGRID_H
#define NORM_POINTGRID_H

#include "ct_itemdrawable/ct_grid3d.h"
#include "ct_cloudindex/abstract/ct_abstractmodifiablecloudindex.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"

#include "norm_pairint.h"

typedef CT_Grid3D<size_t>  Image3DSizeT;
typedef CT_Grid3D<size_t>& Image3DSizeTRef;
typedef CT_Grid3D<size_t>* Image3DSizeTPtr;

class NORM_PointGridDrawManager;

class NORM_PointGrid : public CT_AbstractItemDrawableWithoutPointCloud
{
public:
    NORM_PointGrid();

    NORM_PointGrid(const CT_OutAbstractSingularItemModel *model,
                   const CT_AbstractResult *result,
                   double xmin,
                   double ymin,
                   double zmin,
                   size_t dimx,
                   size_t dimy,
                   size_t dimz,
                   double resolution);

    NORM_PointGrid(const QString& model,
                   const CT_AbstractResult *result,
                   double xmin,
                   double ymin,
                   double zmin,
                   size_t dimx,
                   size_t dimy,
                   size_t dimz,
                   double resolution);

private :
    Image3DSizeTPtr _firstImage;        // Image contenant le premier indice de la sous partie du nuage contenu dans chaque case
    Image3DSizeTPtr _lastImage;         // Image contenant le dernier indice (exclu) de la sous partie du nuage contenu dans chaque case

    const static NORM_PointGridDrawManager POINT_GRID_DRAW_MANAGER;
};

#endif // NORM_POINTGRID_H
