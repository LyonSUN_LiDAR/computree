/********************************************************************************
* Filename :  NORM_acp.h							*
*																				*
* Copyright (C) 2015 Joris RAVAGLIA												*
* Author: Joris Ravaglia														*
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                        *
*																				*
* This file is part of the oc_pluginoctree plugin                               *
* for the CompuTree v2.0 software.                                              *
* The oc_pluginoctree plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the               *
* GNU Lesser General Public License as published by the Free Software Foundation*
* either version 3 of the License, or (at your option) any later version.       *
*																				*
* The oc_pluginoctree plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY*
* or FITNESS FOR A PARTICULAR PURPOSE.                                          *
* See the GNU Lesser General Public License for more details.                   *
*																				*
* You should have received a copy of the GNU Lesser General Public License		*
* along with this program. If not, see <http://www.gnu.org/licenses/>.			*
*																				*
*********************************************************************************/

#ifndef NORM_ACP_H
#define NORM_ACP_H

#include "ct_global/ct_repository.h"
#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"

namespace NORM_Tools
{
    namespace NORM_PCA
    {
        /*!
         * \brief pca
         *
         * Runs a PCA on a given indNORM cloud :
         * - center the point cloud at its centroid
         * - compute the covariance matrix
         * - compute the eigenvalues and eigenvectors of thecovariance matrix
         * \warning The eigenvalues (and the corresponding eigenvectors) are sorted in decrescentorder (i.e. outL1 > outL2 > outL3 )
         *
         * \param indexCloud : input indNORM cloud
         * \param outL1 : first eigenvalue of the
         * \param outL2 : second eigenvalue of the
         * \param outL3 : third eigenvalue of the
         * \param outV1 : first eigenvector of the
         * \param outV2 : second eigenvector of the
         * \param outV3 : third eigenvector of the
         */
        void pca( const CT_MPCIR indexCloud,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        void pca( const CT_PCIR indexCloud,
                  const CT_Point& centroid,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        void pca( const CT_MPCIR indexCloud,
                  const CT_Point& centroid,
                  size_t first,
                  size_t last,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );
        /*!
         * \brief pca
         *
         * Runs a PCA on a given indNORM cloud :
         * - center the point cloud at its centroid
         * - compute the covariance matrix
         * - compute the eigenvalues and eigenvectors of thecovariance matrix
         * \warning The eigenvalues (and the corresponding eigenvectors) are sorted in decrescentorder (i.e. outL1 > outL2 > outL3 )
         *
         * \param indexCloud : input indNORM cloud
         * \param outL1 : first eigenvalue of the
         * \param outL2 : second eigenvalue of the
         * \param outL3 : third eigenvalue of the
         * \param outV1 : first eigenvector of the
         * \param outV2 : second eigenvector of the
         * \param outV3 : third eigenvector of the
         */
        void pca( const CT_AbstractPointCloudIndex *indexCloud,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        void pca( const CT_AbstractPointCloudIndex *indexCloud,
                  const CT_Point& centroid,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        void pca( const QVector<int>* indexCloud,
                  const CT_Point& centroid,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        void pca( const CT_AbstractPointCloudIndex *indexCloud,
                  const CT_Point& centroid,
                  size_t first,
                  size_t last,
                  float& outL1, float& outL2, float &outL3,
                  CT_Point& outV1, CT_Point& outV2, CT_Point& outV3 );

        /*!
         * \brief sortEigenValues
         *
         * Sort eigenvalues and correspondingeigenvectors in decresent order (i.e. l1 > l2 > l3)
         *
         * \param eigenValues : eigenvalues from a pca
         * \param eigenVectors : corresponding eigenvectors (each column of this matrix is n eigenvector)
         */
        void sortEigenValues( Eigen::Vector3cd& eigenValues,
                              Eigen::Matrix3cd& eigenVectors);

        /*!
         * \brief getCentroid
         *
         * Computes the centroid of an indNORM cloud
         *
         * \param indexCloud : indNORM cloud of interest
         *
         * \return the centroid of the indNORM cloud of interest
         */
        CT_Point getCentroid(const CT_AbstractPointCloudIndex *indexCloud );

        CT_Point getCentroid(const CT_AbstractPointCloudIndex *indexCloud, size_t indexFirst, size_t indexLast );

        /*!
         * \brief getCentroid
         *
         * Computes the centroid of an indNORM cloud
         *
         * \param indexCloud : indNORM cloud of interest
         *
         * \return the centroid of the indNORM cloud of interest
         */
        CT_Point getCentroid(const CT_MPCIR &indexCloud);

        CT_Point getCentroid(const CT_MPCIR &indexCloud, size_t indexFirst, size_t indexLast );
    }
}

#endif // NORM_ACP_H
