#ifndef NORM_NEWSTEPCREATEOCTREE_H
#define NORM_NEWSTEPCREATEOCTREE_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class NORM_NewStepCreateOctree : public CT_AbstractStep
{
    Q_OBJECT

public:
    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    NORM_NewStepCreateOctree(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private :
    CT_AutoRenameModels     _autoRenameModelOctree;
    CT_AutoRenameModels     _autoRenameModelPointsNodeId;
    CT_AutoRenameModels     _autoRenameModelPointsNodeL1;
    CT_AutoRenameModels     _autoRenameModelPointsNodeL2;
    CT_AutoRenameModels     _autoRenameModelPointsNodeL3;
    CT_AutoRenameModels     _autoRenameModelPointsNodeSigma;
    CT_AutoRenameModels     _autoRenameModelPointsNodeL3L2;

    double                  _dimensionMin;
    int                     _nPointsMinForPca;
    double                  _dimensionMaxForPca;
    double                  _sigma3Max;
    double                  _sigma2Min;
    double                  _rmseMax;

    QStringList             _criteriaList;
    QString                 _criteriaChoice;
};

#endif // NORM_NEWSTEPCREATEOCTREE_H
