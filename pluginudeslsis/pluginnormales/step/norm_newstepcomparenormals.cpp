#include "norm_newstepcomparenormals.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/ct_scene.h"

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include <QDebug>

// Alias for indexing out models
#define DEF_rsltComparison "rsltComparison"
#define DEF_grpComparison "grpComparison"
#define DEF_itmScene "itmScene"
#define DEF_itmAngle "itmAngle"

#define NORM_NewStepCompareNormals_epsilon 1e-6

// Constructor : initialization of parameters
NORM_NewStepCompareNormals::NORM_NewStepCompareNormals(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{            
    _skipLinesFile1 = 0;
    _separator1 = " ";
    _fileType1 = 0;

    _skipLinesFile2 = 0;
    _separator2 = " ";
    _fileType2 = 0;
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepCompareNormals::getStepDescription() const
{
    return tr("0 - Compare les normales calculees par deux methodes sur un meme nuage de points");
}

// Step detailled description
QString NORM_NewStepCompareNormals::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepCompareNormals::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepCompareNormals::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepCompareNormals(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepCompareNormals::createInResultModelListProtected()
{
}

// Creation and affiliation of OUT models
void NORM_NewStepCompareNormals::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_RsltComparison = createNewOutResultModel( DEF_rsltComparison,
                                                                          "Normal Comparison",
                                                                          "Normal Comparison Disp",
                                                                          "Resultat contenant la comparaison de deux jeux de normales");

    res_RsltComparison->setRootGroup(DEF_grpComparison, new CT_StandardItemGroup(),tr("Group"));
    res_RsltComparison->addItemModel(DEF_grpComparison, DEF_itmScene, new CT_Scene(), tr("Input Scene"));
    res_RsltComparison->addItemModel(DEF_grpComparison, DEF_itmAngle, new CT_PointsAttributesScalarTemplated<float>(), tr("Angle"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepCompareNormals::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "Log file", CT_FileChoiceButton::OneNewFile, "*.*", _logFileList );

    configDialog->addEmpty();

    configDialog->addFileChoice( "File 1", CT_FileChoiceButton::OneExistingFile, "*.*", _fileList1 );
    configDialog->addInt( "Skip Line file1", "", 0, 9999, _skipLinesFile1 );

    CT_ButtonGroup& bg_fileType1 = configDialog->addButtonGroup( _fileType1 );
    configDialog->addExcludeValue("", "", tr("x y z nx ny nz"), bg_fileType1, 0);
    configDialog->addExcludeValue("", "", tr("x y z r g b nx ny nz"), bg_fileType1, 1);

    configDialog->addEmpty();

    configDialog->addFileChoice( "File 2", CT_FileChoiceButton::OneExistingFile, "*.*", _fileList2 );
    configDialog->addInt( "Skip Line file1", "", 0, 9999, _skipLinesFile2 );

    CT_ButtonGroup& bg_fileType2 = configDialog->addButtonGroup( _fileType2 );
    configDialog->addExcludeValue("", "", tr("x y z nx ny nz"), bg_fileType2, 0);
    configDialog->addExcludeValue("", "", tr("x y z r g b nx ny nz"), bg_fileType2, 1);
}

void NORM_NewStepCompareNormals::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltComparison = outResultList.at(0);

    if( _fileList1.empty() || _fileList2.empty() || _logFileList.empty() )
    {
        qDebug() << "Erreur : Il manque un fichier a selectionner";
        return;
    }
    else
    {
        _file1 = _fileList1.first();
        _file2 = _fileList2.first();
        _logFile = _logFileList.first();
    }

    // On ouvre les deux fichier et on les compare ligne a ligne pour en sortir la difference d'angle
    CT_AbstractUndefinedSizePointCloud* inputPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();    // On en profite pour creer la scene
    CT_StandardCloudStdVectorT<float>* diffAnglesCloud = new CT_StandardCloudStdVectorT<float>();

    QFile file1( _file1 );
    if( !file1.open( QIODevice::ReadOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier" << _file1;
        return;
    }
    QFile file2( _file2 );
    if( !file2.open( QIODevice::ReadOnly ) )
    {
        file1.close();
        qDebug() << "Erreur lors de l'ouverture du fichier" << _file2;
        return;
    }
    QFile logFile( _logFile );
    if( !logFile.open( QIODevice::WriteOnly ) )
    {
        file1.close();
        file2.close();
        qDebug() << "Erreur lors de l'ouverture du fichier" << _logFile;
        return;
    }

    QTextStream streamf1( &file1 );
    QTextStream streamf2( &file2 );
    QTextStream streamflog( &logFile );
    int nLines = 0;

    float x, y, z;
    CT_Point p;
    float nx1, ny1, nz1;
    float nx2, ny2, nz2;
    QString line1;
    QStringList listWords1;
    QString line2;
    QStringList listWords2;

    // Skip lines
    for( int i = 0 ; i < _skipLinesFile1 ; i++ )
    {
        streamf1.readLine();
        nLines++;
    }
    for( int i = 0 ; i < _skipLinesFile2 ; i++ )
    {
        streamf2.readLine();
    }

    int nValidAngles = 0;
    while( !streamf1.atEnd() )
    {
        if( streamf2.atEnd() )
        {
            file1.close();
            file2.close();

            qDebug() << "Erreur : Les fichiers n'ont pas le meme nombre de lignes" << nLines;

            return;
        }

        line1 = streamf1.readLine();
        listWords1 = line1.split(" ", QString::SkipEmptyParts);
        if( _fileType1 == 0 )
        {
            if( listWords1.size() != 6 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 1 ?";
                qDebug() << "Ligne lue" << line1;
                qDebug() << "Soit " << listWords1.size() << " champs (6 attendus)";
                return;
            }

            x = listWords1.at(0).toFloat();
            y = listWords1.at(1).toFloat();
            z = listWords1.at(2).toFloat();
            nx1 = listWords1.at(3).toFloat();
            ny1 = listWords1.at(4).toFloat();
            nz1 = listWords1.at(5).toFloat();
        }
        else if( _fileType1 == 1 )
        {
            if( listWords1.size() != 9 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 1 ?";
                qDebug() << "Ligne lue" << line1;
                qDebug() << "Soit " << listWords1.size() << " champs (9 attendus)";
                return;
            }

            x = listWords1.at(0).toFloat();
            y = listWords1.at(1).toFloat();
            z = listWords1.at(2).toFloat();
            nx1 = listWords1.at(6).toFloat();
            ny1 = listWords1.at(7).toFloat();
            nz1 = listWords1.at(8).toFloat();
        }
        else
        {
            qDebug() << "Erreur dans le type de fichier on quitte";
            return;
        }

        line2 = streamf2.readLine();
        listWords2 = line2.split(" ", QString::SkipEmptyParts);
        if( _fileType2 == 0 )
        {
            if( listWords2.size() != 6 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 2 ?";
                qDebug() << "Ligne lue" << line2;
                qDebug() << "Soit " << listWords2.size() << " champs (6 attendus)";
                return;
            }


            nx2 = listWords2.at(3).toFloat();
            ny2 = listWords2.at(4).toFloat();
            nz2 = listWords2.at(5).toFloat();
        }
        else if( _fileType2 == 1 )
        {
            if( listWords2.size() != 9 )
            {
                qDebug() << "Erreur il manque des informations a la ligne" << nLines << "(peut etre pas le bon format de fichier 2 ?";
                qDebug() << "Ligne lue" << line2;
                qDebug() << "Soit " << listWords2.size() << " champs (9 attendus)";
                return;
            }

            nx2 = listWords2.at(6).toFloat();
            ny2 = listWords2.at(7).toFloat();
            nz2 = listWords2.at(8).toFloat();
        }
        else
        {
            qDebug() << "Erreur dans le type de fichier on quitte";
            return;
        }

        // Augmente le nombre de lignes lues
        nLines++;

        // Ajout du point au tableau de points crees
        p.setValues( x, y, z );
        inputPointCloud->addPoint( p );

        // Calcule l'angle entre les deux normales
        float angle = angle3D( nx1, ny1, nz1,
                               nx2, ny2, nz2 );

        if( angle >= 0 && angle <= 90 )
        {
            streamflog << x << " " << y << " " << z << " " << nx1 << " " << ny1 << " " << nz1 << " " << angle << "\n";
            nValidAngles++;
        }

        diffAnglesCloud->addT( angle );
    }

    if( !streamf2.atEnd() )
    {
        file1.close();
        file2.close();

        qDebug() << "Erreur : Les fichiers n'ont pas le meme nombre de lignes (" << nLines << ")";
        qDebug() << streamf2.readLine();

        return;
    }

    file1.close();
    file2.close();

    qDebug() << "J'ai eu " << nValidAngles << " angles sur " << inputPointCloud->size() << " points, soit " << nValidAngles * 100.0 / (float)inputPointCloud->size() << "%";

    // Cree la scene
    CT_NMPCIR inputPointCloudRegistered = PS_REPOSITORY->registerUndefinedSizePointCloud( inputPointCloud );
    CT_Scene* inputScene = new CT_Scene( DEF_itmScene,
                                         res_RsltComparison,
                                         inputPointCloudRegistered );

    assert( inputScene->getPointCloudIndex()->size() == diffAnglesCloud->size() );

    // Cree l'attribut
    CT_PointsAttributesScalarTemplated<float>* diffAnglesAttribute = new CT_PointsAttributesScalarTemplated<float>( DEF_itmAngle,
                                                                                                                    res_RsltComparison,
                                                                                                                    inputScene->getPointCloudIndexRegistered(),
                                                                                                                    diffAnglesCloud );

    // Cree un groupe
    CT_StandardItemGroup* grpComparison = new CT_StandardItemGroup(DEF_grpComparison, res_RsltComparison);
    res_RsltComparison->addGroup(grpComparison);

    // Auquel on ajoute les items
    grpComparison->addItemDrawable( inputScene );
    grpComparison->addItemDrawable( diffAnglesAttribute );
}

float NORM_NewStepCompareNormals::angle3D(float x1, float y1, float z1,
                                          float x2, float y2, float z2)
{
    float scalarProduct = x1*x2 + y1*y2 + z1*z2;
    float norm1 = sqrt(x1*x1 + y1*y1 + z1*z1);
    float norm2 = sqrt(x2*x2 + y2*y2 + z2*z2);

    // Si on a une normale nulle alors on ne peut pas comparer
    if( norm1 == 0 || norm2 == 0 )
    {
        return -1;
    }

    // Calcul du cos selon le signe du produit scalaire pour obtenir un angle entre 0 et 90
    float cos;
    if( scalarProduct > 0 )
    {
        cos = scalarProduct / ( norm1 * norm2 );
    }
    else
    {
        cos = (-scalarProduct) / ( norm1 * norm2 );
    }

    // On s'assure qu'on est bien dans le range (on accepte les petites marges d'erreurs dues a la precision numerique)
    if( cos > 1 )
    {
        if( cos > 1 + NORM_NewStepCompareNormals_epsilon )
        {
            // Ne devrait pas arriver
            return -2;
        }
        else
        {
            cos = 1;
        }
    }

    if( cos < -1 )
    {
        if( cos < -1 - NORM_NewStepCompareNormals_epsilon )
        {
            // Ne devrait pas arriver
            return -2;
        }
        else
        {
            cos = -1;
        }
    }

    // On revient a l'angle
    float angleRad = acos( cos );

    // On transforme en degres
    return ( angleRad * 180.0 / M_PI );
}
