#include "norm_newstepfittsurfaces.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

#include "ct_itemdrawable/ct_scene.h"
#include "new/norm_newoctreev2.h"

#include <QElapsedTimer>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepFittSurfaces::NORM_NewStepFittSurfaces(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepFittSurfaces::getStepDescription() const
{
    return tr("2 - Ajuste une surface quadratique dans les cellules de l'octree");
}

// Step detailled description
QString NORM_NewStepFittSurfaces::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepFittSurfaces::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepFittSurfaces::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepFittSurfaces(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepFittSurfaces::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
}

// Creation and affiliation of OUT models
void NORM_NewStepFittSurfaces::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelEcartSurface, new CT_PointsAttributesScalarTemplated<float>(), tr("Ecart a la surface (point - surface)"));
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelGaussianCurvature, new CT_PointsAttributesScalarTemplated<float>(), tr("Courbure Gaussienne"));
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelMinCurvatureDirection, new CT_PointsAttributesNormal(), tr("Courbure Min Dir"));
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelMaxCurvatureDirection, new CT_PointsAttributesNormal(), tr("Courbure Max Dir"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepFittSurfaces::createPostConfigurationDialog()
{
//    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void NORM_NewStepFittSurfaces::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        if ( itemIn_itmOctree != NULL )
        {
            // Fitting de surface
            QElapsedTimer timer;
            timer.start();
            itemIn_itmOctree->fitSurfaces();
            qDebug() << "Temps de fitting des surfaces " << timer.elapsed();

//            // Calcul des courbures
//            timer.start();
//            CT_PointsAttributesScalarTemplated<float>* gaussianCurvature = itemIn_itmOctree->getGaussianCurvature(_autoRenameModelGaussianCurvature.completeName(),
//                                                                                                                  res_RsltOctree,
//                                                                                                                  itemIn_itmOctree->getScene()->getPointCloudIndexRegistered() );
//            qDebug() << "Temps de calcul des courbures" << timer.elapsed();
//            grpIn_grpOctree->addItemDrawable( gaussianCurvature );

            timer.start();
            CT_PointsAttributesScalarTemplated<float>* gaussianCurvature;
            CT_PointsAttributesNormal* minCurvatureDir;
            CT_PointsAttributesNormal* maxCurvatureDir;
            itemIn_itmOctree->getCurvatures( _autoRenameModelGaussianCurvature.completeName(),
                                             _autoRenameModelMinCurvatureDirection.completeName(),
                                             _autoRenameModelMaxCurvatureDirection.completeName(),
                                             res_RsltOctree,
                                             itemIn_itmOctree->getScene()->getPointCloudIndexRegistered(),
                                             gaussianCurvature,
                                             minCurvatureDir,
                                             maxCurvatureDir );
            qDebug() << "Temps de calcul des courbures" << timer.elapsed();
            grpIn_grpOctree->addItemDrawable( gaussianCurvature );
            grpIn_grpOctree->addItemDrawable( minCurvatureDir );
            grpIn_grpOctree->addItemDrawable( maxCurvatureDir );

            // Calcul de l'erreur verticale
            timer.start();
            CT_PointsAttributesScalarTemplated<float>* errorAttribute = itemIn_itmOctree->getErrorSurface( _autoRenameModelEcartSurface.completeName(),
                                                                                                           res_RsltOctree,
                                                                                                           itemIn_itmOctree->getScene()->getPointCloudIndexRegistered() );
            qDebug() << "Temps de calcul d'erreur" << timer.elapsed();
            grpIn_grpOctree->addItemDrawable( errorAttribute );
        }
    }
}
