#include "lsis_stepexportcylindersofbillons.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_shapedata/ct_circledata.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_image2d.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEFin_rsltEstimatedBillons "DEFin_rsltEstimatedBillons"
#define DEFin_grpBillon "DEFin_grpBillon"
#define DEFin_grpCluster "DEFin_grpCluster"
#define DEFin_itmCylinder "DEFin_itmCylinder"
#define DEFin_itmDtm "DEFin_itmDtm"
#define DEFin_itmPosMnt "DEFin_itmPosMnt"

// Constructor : initialization of parameters
LSIS_StepExportCylindersOfBillons::LSIS_StepExportCylindersOfBillons(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _saveDirList.clear();
    _filePrefix = "placette_arbre";
    _optionalMnt = NULL;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExportCylindersOfBillons::getStepDescription() const
{
    return tr("Enregistre cylindres de billons");
}

// Step detailled description
QString LSIS_StepExportCylindersOfBillons::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepExportCylindersOfBillons::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExportCylindersOfBillons::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExportCylindersOfBillons(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExportCylindersOfBillons::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltHitGridSparse = createNewInResultModelForCopy(DEFin_rsltEstimatedBillons,"Set of Billons");
    resIn_rsltHitGridSparse->setZeroOrMoreRootGroup();
    resIn_rsltHitGridSparse->addGroupModel("", DEFin_grpBillon, CT_AbstractItemGroup::staticGetType(), tr("Input billons Group"));
    resIn_rsltHitGridSparse->addGroupModel(DEFin_grpBillon, DEFin_grpCluster, CT_AbstractItemGroup::staticGetType(), tr("Input cluster Group"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpCluster, DEFin_itmCylinder, CT_Cylinder::staticGetType(), tr("Cylinder"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);

    resIn_rsltHitGridSparse->addItemModel(DEFin_grpBillon, DEFin_itmPosMnt, CT_ReferencePoint::staticGetType(), tr("Position Mnt"));
}

// Creation and affiliation of OUT models
void LSIS_StepExportCylindersOfBillons::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEFin_rsltEstimatedBillons);
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExportCylindersOfBillons::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "OutputDir", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
    configDialog->addString("OutputFilePrefix", "", _filePrefix );
}

void LSIS_StepExportCylindersOfBillons::compute()
{
    if( _saveDirList.empty() )
    {
        qDebug() << "Il faut selectionner un dossier de sortie !";
        return;
    }
    _saveDir = _saveDirList.first();

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rsltSetOfBillons= outResultList.at(0);

    QString outputMntZFileName = _saveDir + "/" + _filePrefix + "_mnt_z.csv";
    QFile outputMntZFile( outputMntZFileName );
    if( !outputMntZFile.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ouvrir le fichier " << outputMntZFileName << " pour y ecrire";
        return;
    }
    QTextStream outputMntzFileStream( &outputMntZFile );

    // IN results browsing
    int billonId = 0;
    CT_ResultGroupIterator itIn_grpBillon( res_rsltSetOfBillons, this, DEFin_grpBillon );
    while ( itIn_grpBillon.hasNext() && !isStopped() )
    {
        CT_StandardItemGroup* grpIn_grpBillon = (CT_StandardItemGroup*) itIn_grpBillon.next();
        CT_ReferencePoint* itemIn_itmReferencePoint = (CT_ReferencePoint*)grpIn_grpBillon->firstItemByINModelName(this, DEFin_itmPosMnt);

        if( itemIn_itmReferencePoint != NULL )
        {
            billonId++;

            float baseHeight = itemIn_itmReferencePoint->z();
            qDebug() << "Nouveau billon " << billonId << "Height " << baseHeight;
            outputMntzFileStream << _saveDir + "/" + _filePrefix + "_billon_" + QString::number( billonId ) + ".csv " << baseHeight << "\n";

            // On prend la hauteur minimum des cylindres du billon
            QString outputBillonFileName = _saveDir + "/" + _filePrefix + "_billon_" + QString::number( billonId ) + ".csv";
            QFile outputBillonFile( outputBillonFileName );
            if( !outputBillonFile.open( QIODevice::WriteOnly ) )
            {
                qDebug() << "Impossible d'ouvrir le fichier " << outputBillonFileName << " pour y ecrire";
                outputMntZFile.close();
                return;
            }
            QTextStream outputFileStream( &outputBillonFile );
            outputFileStream << "W X Y Z";

            int clusterId = 0;
            CT_GroupIterator itIn_grpCluster( grpIn_grpBillon, this, DEFin_grpCluster );
            while ( itIn_grpCluster.hasNext() && !isStopped() )
            {
                clusterId++;
                CT_StandardItemGroup* grpIn_grpCluster = (CT_StandardItemGroup*) itIn_grpCluster.next();
                const CT_Cylinder* itemIn_itmCylinder = (CT_Cylinder*)grpIn_grpCluster->firstItemByINModelName(this, DEFin_itmCylinder);

                if( itemIn_itmCylinder != NULL )
                {
                    outputFileStream << "\n" << itemIn_itmCylinder->getRadius() << " "
                                     << itemIn_itmCylinder->getCenterX() << " "
                                     << itemIn_itmCylinder->getCenterY() << " "
                                     << itemIn_itmCylinder->getCenterZ();
                }
            }

            outputBillonFile.close();
        }
    }
    outputMntZFile.close();
}
