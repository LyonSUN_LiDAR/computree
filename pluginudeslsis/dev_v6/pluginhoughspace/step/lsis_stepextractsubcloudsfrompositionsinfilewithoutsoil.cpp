/************************************************************************************
* Filename :  lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_scene.h"

//Tools dependencies
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_point.h"
#include "ct_iterator/ct_pointiterator.h"

// Alias for indexing models
#define DEFin_rsltPositions "rsltPositions"
#define DEFin_groGrpCircles "groGrpCircles"
#define DEFin_grpCircles "grpCircles"
#define DEFin_itmCircle "itmCircle"

#define DEFin_rsltInitialPointCloud "rsltInitialPointCloud"
#define DEFin_grpPointCloud "grpPointCloud"
#define DEFin_itmPointCloud "itmPointCloud"

#define DEFout_rsltSubClouds "rsltSubClouds"
#define DEFout_grpGrpPointCloud "grpGrpPointCloud"
#define DEFout_grpPointCloud "grpPointCloud"
#define DEFout_itmPointCloud "itmPointCloud"


// Constructor : initialization of parameters
LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _size = 2;
    _height = 0.3;
    _saveMode = false;
    _outputDirPath = ".";
    _inputCloudFileName = ".";
    _inputMntFileName = ".";
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::getStepDescription() const
{
    return tr("Extrait des sous nuages de points a partir des estimations de position de troncs");
}

// Step detailled description
QString LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltPositions = createNewInResultModel(DEFin_rsltPositions, tr("Result Positions"));
    resIn_rsltPositions->setRootGroup(DEFin_groGrpCircles, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltPositions->addGroupModel(DEFin_groGrpCircles, DEFin_grpCircles, CT_AbstractItemGroup::staticGetType(), tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltPositions->addItemModel(DEFin_grpCircles, DEFin_itmCircle, CT_Circle::staticGetType(), tr("Position"));
}

// Creation and affiliation of OUT models
void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltSubClouds = createNewOutResultModel(DEFout_rsltSubClouds, tr("Result Extraction"));
    res_rsltSubClouds->setRootGroup(DEFout_grpGrpPointCloud, new CT_StandardItemGroup(), tr("Group"));
    res_rsltSubClouds->addGroupModel(DEFout_grpGrpPointCloud, DEFout_grpPointCloud, new CT_StandardItemGroup(), tr("Group"));
    res_rsltSubClouds->addItemModel(DEFout_grpPointCloud, DEFout_itmPointCloud, new CT_Scene(), tr("Sub Cloud around position"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Taille de decoupe", "de chaque cote", 0.0001, 9999, 4, _size, 1);
    configDialog->addBool("Save","","",_saveMode);
    configDialog->addString("OutputPath","",_outputDirPath);
    configDialog->addString("Original file Name","",_inputCloudFileName);
    configDialog->addString("Mnt File","",_inputMntFileName);
    configDialog->addDouble("Eppaissauer sol", "de chaque cote", 0.0001, 9999, 4, _height, 1);
}

void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::compute()
{
    // Recupere le resultat de sortie que l'on va remplir avec le(s) sous nuage(s)
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _rsltSubClouds = outResultList.at(0);
    _grpGrpPointClouds = new CT_StandardItemGroup( DEFout_grpGrpPointCloud, _rsltSubClouds );
    _rsltSubClouds->addGroup(_grpGrpPointClouds);

    // Recupere le(s) nuage(s) d'entree
    QVector< CT_Circle* > circles;
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltCircles = inResultList.at(0);

    CT_ResultGroupIterator itIn_grpCircle(resIn_rsltCircles, this, DEFin_grpCircles);
    while (itIn_grpCircle.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpCircle = (CT_AbstractItemGroup*) itIn_grpCircle.next();

        CT_Circle* itemIn_itmCircle = (CT_Circle*)grpIn_grpCircle->firstItemByINModelName(this, DEFin_itmCircle );
        if (itemIn_itmCircle != NULL)
        {
            circles.push_back( itemIn_itmCircle );
        }
    }

    CT_Grid2DXY<float>* mnt = loadMnt( _inputMntFileName, "", NULL );

    if( _saveMode )
    {
        computeAndSave( _inputCloudFileName, circles, mnt, _outputDirPath );
    }

    else
    {
        compute( _inputCloudFileName, circles, mnt );
    }
}

void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::computeAndSave(QString &fileCloudFullName,
                                                                             QVector<CT_Circle *> &circles,
                                                                             CT_Grid2DXY<float>* mnt,
                                                                             QString outDirPath )
{
    int nCircles = circles.size();
    CT_Circle* curCircle;

    // On regarde pour chaque cercle si le point est dans une zone autour du centre
    for( int j = 0 ; j < nCircles ; j++ )
    {
        curCircle = circles.at(j);

        QFile fileInput( fileCloudFullName );
        if( !fileInput.open( QIODevice::ReadOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << fileCloudFullName;
            exit(0);
        }
        QTextStream streamInput( &fileInput );

        bool first = true;
        QString outputFileName = "tree" + QString::number(j) + ".xyz";
        QFile fileOutput( outDirPath + "/" + outputFileName );
        if( !fileOutput.open( QIODevice::WriteOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << outDirPath + "/" + outputFileName;
            exit(0);
        }
        QTextStream streamOutput( &fileOutput );

        // Parcours  tous les points
        float x, y, z;
        while( !streamInput.atEnd() )
        {
            streamInput >> x >> y >> z;
            // Avance l'iterateur et recupere le point sous l'iterateur
            CT_Point currentPoint = createCtPoint( x, y, z );

            if( isPointIn2dBox( currentPoint,
                                curCircle->getCenterX() - _size,
                                curCircle->getCenterY() - _size,
                                curCircle->getCenterX() + _size,
                                curCircle->getCenterY() + _size)
                &&
                currentPoint.z() > ( mnt->valueAtXY( currentPoint.x(), currentPoint.y() ) + _height ) )
            {
                if( first == false )
                {
                    streamOutput << "\n";
                }

                else
                {
                    first = false;
                }

                // Alors on l'ajoute au nuage de points de sortie correspondant au cercle
                streamOutput << currentPoint.x() << " "
                             << currentPoint.y() << " "
                             << currentPoint.z();
            }
        }

        fileInput.close();
        fileOutput.close();
    }
}

void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::compute(QString& fileCloudFullName,
                                                                      QVector< CT_Circle* >& circles,
                                                                      CT_Grid2DXY<float> *mnt)
{
    int nCircles = circles.size();
    CT_Circle* curCircle;

    // On regarde pour chaque cercle si le point est dans une zone autour du centre
    for( int j = 0 ; j < nCircles ; j++ )
    {
        curCircle = circles.at(j);

        QFile fileInput( fileCloudFullName );
        if( !fileInput.open( QIODevice::ReadOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << fileCloudFullName;
            exit(0);
        }
        QTextStream streamInput( &fileInput );

        CT_AbstractUndefinedSizePointCloud* curOutScene = PS_REPOSITORY->createNewUndefinedSizePointCloud();;
        CT_Point bboxBot = createCtPoint( std::numeric_limits<float>::max(),
                                          std::numeric_limits<float>::max(),
                                          std::numeric_limits<float>::max());
        CT_Point bboxTop = createCtPoint( -std::numeric_limits<float>::max(),
                                          -std::numeric_limits<float>::max(),
                                          -std::numeric_limits<float>::max());

        // Parcours  tous les points
        float x, y, z;
        while( !streamInput.atEnd() )
        {
            streamInput >> x >> y >> z;
            // Avance l'iterateur et recupere le point sous l'iterateur
            CT_Point currentPoint = createCtPoint( x, y, z );

            if( isPointIn2dBox( currentPoint,
                                curCircle->getCenterX() - _size,
                                curCircle->getCenterY() - _size,
                                curCircle->getCenterX() + _size,
                                curCircle->getCenterY() + _size)
                &&
                currentPoint.z() > ( mnt->valueAtXY( currentPoint.x(), currentPoint.y() ) + _height ) )
            {
                // Alors on l'ajoute au nuage de points de sortie correspondant au cercle
                curOutScene->addPoint( currentPoint );
                updateBBox( bboxBot, bboxTop, currentPoint );
            }
        }

        CT_NMPCIR registeredCurSubCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( curOutScene );
        CT_Scene* curScene = new CT_Scene( DEFout_itmPointCloud, _rsltSubClouds, registeredCurSubCloud );
        curScene->setBoundingBox( bboxBot.x(), bboxBot.y(), bboxBot.z(),
                                  bboxTop.x(), bboxTop.y(), bboxTop.z() );

        CT_StandardItemGroup* grpCurExtractedCloud = new CT_StandardItemGroup( DEFout_grpPointCloud, _rsltSubClouds );
        grpCurExtractedCloud->addItemDrawable( curScene );
        _grpGrpPointClouds->addGroup( grpCurExtractedCloud );
        _rsltSubClouds->addGroup(grpCurExtractedCloud);
    }
}

bool LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::isPointIn2dBox(const CT_Point &point, float xmin, float ymin, float xmax, float ymax)
{
    return ( point.x() >= xmin &&
             point.x() <= xmax &&
             point.y() >= ymin &&
             point.y() <= ymax );
}

void LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::updateBBox(CT_Point &bot, CT_Point &top, const CT_Point &point)
{
    for( int i = 0 ; i < 3 ; i++ )
    {
        if( point(i) < bot(i) )
        {
            bot(i) = point(i);
        }

        if( point(i) > top(i) )
        {
            top(i) = point(i);
        }
    }
}

CT_Grid2DXY<float>* LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil::loadMnt(QString filename,
                                                                                     const QString& modelName,
                                                                                     CT_ResultGroup* result )
{
    QFile file( filename );

    if( !file.open( QIODevice::ReadOnly ) )
    {
        PS_LOG->addErrorMessage( PS_LOG->error, "Can not access file : \""+file.fileName()+"\"" );
        return NULL;
    }

    // Outils de lecture
    QTextStream fileStream(&file);
    QString     currentLine;
    QStringList splitline;
    bool*       read = new bool();

    // parametres de la grille
    size_t nCols;
    size_t nRows;
    float minX;
    float minY;
    float cellSize;
    float noDataVal;

    // Initialise les parametres de la grille
    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    nCols = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 1")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 1") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 1"));
    }

    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    nRows = splitline.at(1).toInt(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to an integer at line 2")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a integer at line 2") );
        qDebug() << QString(QString("Unable to convert the ascii format to a integer at line 2"));
    }

    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    minX = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 4")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 4") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 4"));
    }

    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    minY = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 5")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 5") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 5"));
    }

    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    cellSize = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 7")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 7") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 7"));
    }

    currentLine = fileStream.readLine();
    splitline = currentLine.split('\t');
    noDataVal = splitline.at(1).toFloat(read);
    if( !read )
    {
        setErrorCode(2);
        setErrorMessage(2, QString(QString("Unable to convert the ascii format to a floatting point at line 8")));
        PS_LOG->addMessage( LogInterface::error, LogInterface::reader, tr("Unable to convert the ascii format to a floatting point at line 8") );
        qDebug() << QString(QString("Unable to convert the ascii format to a floatting point at line 8"));
    }

    // Creating a grid with the parameters read from the header
    CT_Grid2DXY<float>* loadedGrid = new CT_Grid2DXY<float>( NULL,
                                                             NULL,
                                                             minX,
                                                             minY,
                                                             nCols,
                                                             nRows,
                                                             cellSize,
                                                             0,
                                                             noDataVal,
                                                             0);

    // Fill the grid values
    float currentValue;
    for ( int j = nRows - 1 ; j >= 0 ; j-- )
    {
        for ( int k = 0 ; k < nCols ; k++ )
        {
            fileStream >> currentValue;
            loadedGrid->setValue( k, j, currentValue );
        }
    }

    loadedGrid->computeMinMax();
    loadedGrid->setlevel( loadedGrid->dataMin() );

    return loadedGrid;
}
