/************************************************************************************
* Filename :  lsis_activecontour4d.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_ACTIVECONTOUR4D_HPP
#define LSIS_ACTIVECONTOUR4D_HPP
#include "lsis_activecontour4dcontinuous.h"

#include <iostream>

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_mesh/ct_mesh.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_mesh/tools/ct_meshallocator.h"
#include "ct_itemdrawable/ct_grid2dxy.h"

//Tools dependencies
#include "../streamoverload/streamoverload.h"
#include "../tools/lsis_pcatools.h"
#include "../tools/lsis_mathtools.h"
#include "ct_point.h"
#include "ct_result/ct_resultgroup.h"
#include "../step/lsis_stepsnakesinhoughspace.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_math/ct_mathpoint.h"

//Qt dependencies
#include <QtConcurrent/QtConcurrent>
#include <QDir>

// TEMPORAIRE :
#include "../histogram/lsis_histogram1d.h"
#include "../tools/lsis_mathtools.h"
// TEMPORAIRE FIN

#define PI 3.14159265

//template <>
//CT_DEFAULT_IA_INIT(LSIS_ActiveContour4DContinuous<int>)

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous():CT_AbstractItemDrawableWithoutPointCloud()
{
    _image = NULL;
    _repulseImage = NULL;
    _step = NULL;
    _intensityMin = 0;
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                                                          const CT_AbstractResult *result)
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _image( NULL ),
      _repulseImage( NULL ),
      _step(NULL),
      _intensityMin(0)
{
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                                                          const CT_AbstractResult *result,
                                                                          const CT_Grid4D_Sparse<DataImage> *image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage)
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _image( image ),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const QString &modelName,
                                                                          const CT_AbstractResult *result,
                                                                          const CT_Grid4D_Sparse<DataImage> *image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage)
    : CT_AbstractItemDrawableWithoutPointCloud(modelName, result),
      _image( image ),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                                                          const CT_AbstractResult *result,
                                                                          CT_Grid4D_Sparse<DataImage> const * image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage,
                                                                          LSIS_Pixel4D const & head,
                                                                          LSIS_Pixel4D const & back )
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _image(image),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
    assertWithMessage( head.isIn(image),
                       "La tete du contour a cree n'est pas dans l'image dans laquelle il se deplace :\n" <<
                       "Tete = " << head << "\n" <<
                       "Image = " << image );

    assertWithMessage( back.isIn(image),
                       "La queue du contour a cree n'est pas dans l'image dans laquelle il se deplace :\n" <<
                       "Queue = " << back << "\n" <<
                       "Image = " << image );

    assertWithMessage( !head.equals3D(back),
                       "\n" <<
                       "Tete = " << head << "\n" <<
                       "Queue = " << back << "\n" );

    // On ajoute les deux points (transformes en coordonnees cartesiennes) dans la matrice
    _points.resize( 2, 4 );
    _points.row(0) = head.getCartesian( image );
    _points.row(1) = back.getCartesian( image );

    // Et on resample pour en avoir quatre
    // Avoir quatre points revient a resample avec une resolution suffisante
    // Dans notre cas pour avoir quatre points il faut une resolution de longueur/4
    resample( length3D() / 3.0 );
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const QString &modelName,
                                                                          const CT_AbstractResult *result,
                                                                          CT_Grid4D_Sparse<DataImage> const * image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage,
                                                                          LSIS_Pixel4D const & head,
                                                                          LSIS_Pixel4D const & back )
    : CT_AbstractItemDrawableWithoutPointCloud(modelName, result),
      _image(image),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
    assertWithMessage( head.isIn(image),
                       "La tete du contour a cree n'est pas dans l'image dans laquelle il se deplace :\n" <<
                       "Tete = " << head << "\n" <<
                       "Image = " << image );

    assertWithMessage( back.isIn(image),
                       "La queue du contour a cree n'est pas dans l'image dans laquelle il se deplace :\n" <<
                       "Queue = " << back << "\n" <<
                       "Image = " << image );

    assertWithMessage( !head.equals3D(back),
                       "\n" <<
                       "Tete = " << head << "\n" <<
                       "Queue = " << back << "\n" );

    // On ajoute les deux points (transformes en coordonnees cartesiennes) dans la matrice
    _points.resize( 2, 4 );
    _points.row(0) = head.getCartesian( image );
    _points.row(1) = back.getCartesian( image );

    // Et on resample pour en avoir quatre
    // Avoir quatre points revient a resample avec une resolution suffisante
    // Dans notre cas pour avoir quatre points il faut une resolution de longueur/4
    resample( length3D() / 3.0 );
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                                                          const CT_AbstractResult *result,
                                                                          CT_Grid4D_Sparse<DataImage> const * image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage,
                                                                          LSIS_Pixel4D const & head,
                                                                          int size )
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _image(image),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
    // qDebug() << "Start snake1";
    // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
    LSIS_Pixel4D coneBBoxBot = head - LSIS_Pixel4D( 0, size, size, size);
    LSIS_Pixel4D coneBBoxTop = head + LSIS_Pixel4D( 0, size, size, size);

    LSIS_Point4DFloat headPoint;
    headPoint.set( head.toCartesianW( _image ),
                   head.toCartesianX( _image ),
                   head.toCartesianY( _image ),
                   head.toCartesianZ( _image ) );
    // qDebug() << "Start snake2";

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    for( int axe = 0 ; axe < 4 ; axe++ )
    {
        if( coneBBoxBot(axe) < 0 )
        {
            coneBBoxBot(axe) = 0;
        }

        if( coneBBoxBot(axe) >= _image->dim()(axe) )
        {
            coneBBoxBot(axe) = _image->dim()(axe) - 1;
        }

        if( coneBBoxTop(axe) < 0 )
        {
            coneBBoxTop(axe) = 0;
        }

        if( coneBBoxTop(axe) >= _image->dim()(axe) )
        {
            coneBBoxTop(axe) = _image->dim()(axe) - 1;
        }
    }

    // qDebug() << "Start snake22";
    // Il ne reste plus qu'a regarder dans la boite pour faire une acp des directions
    // On parcour donc toute la bbox
    int nMaxMax = 40;
    QList< LSIS_Point4DFloat > maxiDirections;
    QList< int > maxiScores;
    QList< float > maxiDistances;

    // qDebug() << "Start snake222";
    LSIS_Pixel4D curInBBoxPixel;
    LSIS_Point4DFloat curInBBoxPoint;
    for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
    {
        for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
        {
            for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
            {
                for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                {
                    if( curInBBoxPixel.valueIn( _image ) > 3 && head != curInBBoxPixel )
                    {
                        curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                            curInBBoxPixel.toCartesianX( _image ),
                                            curInBBoxPixel.toCartesianY( _image ),
                                            curInBBoxPixel.toCartesianZ( _image ) );
                        float score = curInBBoxPixel.valueIn( _image );

                        // qDebug() << "Start TTTTTTTTTTTTTTT";
                        // Calcul de la direction potentielle de croissance
                        LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - headPoint;
                        float norm3D = potentialGrowDirPoint.norm3D();

                        // qDebug() << "Start OOOOOOOOO";
                        // Si on a des listes vides tout est simple
                        if( maxiScores.empty() )
                        {
                            // qDebug() << "Start AAAAA111111";
                            maxiScores.append( score );
                            maxiDirections.append( potentialGrowDirPoint );
                            maxiDistances.append( norm3D );
                            // qDebug() << "Start AAAAA22222";
                        }
                        else
                        {
                            // qDebug() << "Start UUUUUUU";
                            // Sinon, on regarde ou inserer l'element dans la liste en fonction de son score
                            // La liste est triee decroissant ( maxiscore(0) > maxiscore(1) > ... )
                            int idElement = 0;
                            while( idElement < maxiScores.size() && maxiScores.at(idElement) > score )
                            {
                                idElement++;
                            }
                            // qDebug() << "Start VVVVVVVV";

                            // Si on a pas atteint la fin de la liste en cherchant sa place alors on peut l'inserer a cet endroit
                            if( idElement < nMaxMax )
                            {
                                // qDebug() << "Start WWWWWWW1111111111";
                                maxiScores.insert(idElement, score);
                                maxiDirections.insert( idElement, potentialGrowDirPoint );
                                maxiDistances.insert( idElement, norm3D );

                                // qDebug() << "Start ZZZZZZ";
                                // Si apres insertion la liste est trop grande alors on supprime le dernier element
                                if( maxiScores.size() > nMaxMax )
                                {
                                    // qDebug() << "Start ZZZZZZZ11111111";
                                    maxiScores.removeLast();
                                    maxiDirections.removeLast();
                                }
                                // qDebug() << "Start WWWWWWWW2222222222";
                            }
                        }
                    }
                }
            }
        }
    }
    // qDebug() << "Start snake4";

    QVector< LSIS_Point4DFloat > dirInCone;
    int nDirections = maxiDirections.size();
        // qDebug() << "Start snake uuuuezjkgfjbdkjfhviug";
    for( int i = 0 ; i < nDirections ; i++ )
    {
        dirInCone.push_back( maxiDirections.at(i) * maxiScores.at(i) * ((size * _image->xres() *1.1 ) - maxiDistances.at(i) ) );
    }
        // qDebug() << "Start snake tototototot";

    // Effectue l'ACP 4D non centree des directions de croissance potentielles
    LSIS_Point4DFloat v1, v2, v3, v4;
    float l1, l2, l3, l4;
    LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                    v1, v2, v3, v4,
                                    l1, l2, l3, l4 );
//    qDebug() << "Start snake7";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();

    // On normalise la direction v1
    if( v1.norm() != 0 )
    {
        v1.normalize();
    }

    v1 *= _image->wres();
//    qDebug() << "Start snake8";

    // On ajoute la tete et la queue comme (head + v1)
    // On ajoute les deux points (transformes en coordonnees cartesiennes) dans la matrice
    _points.resize( 2, 4 );
    _points.row(0) = head.getCartesian( image ) - v1;
    _points.row(1) = head.getCartesian( image ) + v1;
//    qDebug() << "Start snake9";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();
//    qDebug() << "head" << _points(0,0) << _points(0,1) << _points(0,2) << _points(0,3);
//    qDebug() << "head" << _points(1,0) << _points(1,1) << _points(1,2) << _points(1,3);

    // Et on resample pour en avoir quatre
    resample( length3D() / 3.0 );
//    qDebug() << "Start snake10";
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(const QString &modelName,
                                                                          const CT_AbstractResult *result,
                                                                          CT_Grid4D_Sparse<DataImage> const * image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage,
                                                                          LSIS_Pixel4D const & head,
                                                                          int size )
    : CT_AbstractItemDrawableWithoutPointCloud(modelName, result),
      _image(image),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
    // qDebug() << "Start snake1";
    // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
    LSIS_Pixel4D coneBBoxBot = head - LSIS_Pixel4D( 0, size, size, size);
    LSIS_Pixel4D coneBBoxTop = head + LSIS_Pixel4D( 0, size, size, size);

    LSIS_Point4DFloat headPoint;
    headPoint.set( head.toCartesianW( _image ),
                   head.toCartesianX( _image ),
                   head.toCartesianY( _image ),
                   head.toCartesianZ( _image ) );
    // qDebug() << "Start snake2";

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    for( int axe = 0 ; axe < 4 ; axe++ )
    {
        if( coneBBoxBot(axe) < 0 )
        {
            coneBBoxBot(axe) = 0;
        }

        if( coneBBoxBot(axe) >= _image->dim()(axe) )
        {
            coneBBoxBot(axe) = _image->dim()(axe) - 1;
        }

        if( coneBBoxTop(axe) < 0 )
        {
            coneBBoxTop(axe) = 0;
        }

        if( coneBBoxTop(axe) >= _image->dim()(axe) )
        {
            coneBBoxTop(axe) = _image->dim()(axe) - 1;
        }
    }

    // qDebug() << "Start snake22";
    // Il ne reste plus qu'a regarder dans la boite pour faire une acp des directions
    // On parcour donc toute la bbox
    int nMaxMax = 40;
    QList< LSIS_Point4DFloat > maxiDirections;
    QList< int > maxiScores;
    QList< float > maxiDistances;

    // qDebug() << "Start snake222";
    LSIS_Pixel4D curInBBoxPixel;
    LSIS_Point4DFloat curInBBoxPoint;
    for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
    {
        for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
        {
            for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
            {
                for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                {
                    if( curInBBoxPixel.valueIn( _image ) > 3 && head != curInBBoxPixel )
                    {
                        curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                            curInBBoxPixel.toCartesianX( _image ),
                                            curInBBoxPixel.toCartesianY( _image ),
                                            curInBBoxPixel.toCartesianZ( _image ) );
                        float score = curInBBoxPixel.valueIn( _image );

                        // qDebug() << "Start TTTTTTTTTTTTTTT";
                        // Calcul de la direction potentielle de croissance
                        LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - headPoint;
                        float norm3D = potentialGrowDirPoint.norm3D();

                        // qDebug() << "Start OOOOOOOOO";
                        // Si on a des listes vides tout est simple
                        if( maxiScores.empty() )
                        {
                            // qDebug() << "Start AAAAA111111";
                            maxiScores.append( score );
                            maxiDirections.append( potentialGrowDirPoint );
                            maxiDistances.append( norm3D );
                            // qDebug() << "Start AAAAA22222";
                        }
                        else
                        {
                            // qDebug() << "Start UUUUUUU";
                            // Sinon, on regarde ou inserer l'element dans la liste en fonction de son score
                            // La liste est triee decroissant ( maxiscore(0) > maxiscore(1) > ... )
                            int idElement = 0;
                            while( idElement < maxiScores.size() && maxiScores.at(idElement) > score )
                            {
                                idElement++;
                            }
                            // qDebug() << "Start VVVVVVVV";

                            // Si on a pas atteint la fin de la liste en cherchant sa place alors on peut l'inserer a cet endroit
                            if( idElement < nMaxMax )
                            {
                                // qDebug() << "Start WWWWWWW1111111111";
                                maxiScores.insert(idElement, score);
                                maxiDirections.insert( idElement, potentialGrowDirPoint );
                                maxiDistances.insert( idElement, norm3D );

                                // qDebug() << "Start ZZZZZZ";
                                // Si apres insertion la liste est trop grande alors on supprime le dernier element
                                if( maxiScores.size() > nMaxMax )
                                {
                                    // qDebug() << "Start ZZZZZZZ11111111";
                                    maxiScores.removeLast();
                                    maxiDirections.removeLast();
                                }
                                // qDebug() << "Start WWWWWWWW2222222222";
                            }
                        }
                    }
                }
            }
        }
    }
    // qDebug() << "Start snake4";

    QVector< LSIS_Point4DFloat > dirInCone;
    int nDirections = maxiDirections.size();
        // qDebug() << "Start snake uuuuezjkgfjbdkjfhviug";
    for( int i = 0 ; i < nDirections ; i++ )
    {
        dirInCone.push_back( maxiDirections.at(i) * maxiScores.at(i) * ((size * _image->xres() *1.1 ) - maxiDistances.at(i) ) );
    }
        // qDebug() << "Start snake tototototot";

    // Effectue l'ACP 4D non centree des directions de croissance potentielles
    LSIS_Point4DFloat v1, v2, v3, v4;
    float l1, l2, l3, l4;
    LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                    v1, v2, v3, v4,
                                    l1, l2, l3, l4 );
//    qDebug() << "Start snake7";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();

    // On normalise la direction v1
    if( v1.norm() != 0 )
    {
        v1.normalize();
    }

    v1 *= _image->wres();
//    qDebug() << "Start snake8";

    // On ajoute la tete et la queue comme (head + v1)
    // On ajoute les deux points (transformes en coordonnees cartesiennes) dans la matrice
    _points.resize( 2, 4 );
    _points.row(0) = head.getCartesian( image ) - v1;
    _points.row(1) = head.getCartesian( image ) + v1;
//    qDebug() << "Start snake9";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();
//    qDebug() << "head" << _points(0,0) << _points(0,1) << _points(0,2) << _points(0,3);
//    qDebug() << "head" << _points(1,0) << _points(1,1) << _points(1,2) << _points(1,3);

    // Et on resample pour en avoir quatre
    resample( length3D() / 3.0 );
//    qDebug() << "Start snake10";
}

template< typename DataImage >
LSIS_ActiveContour4DContinuous<DataImage>::LSIS_ActiveContour4DContinuous(CT_Grid4D_Sparse<DataImage> const * image,
                                                                          CT_Grid4D_Sparse<bool>* repulseImage,
                                                                          LSIS_Pixel4D const & head,
                                                                          int size )
    : _image(image),
      _repulseImage( repulseImage ),
      _step(NULL),
      _intensityMin(0)
{
    // qDebug() << "Start snake1";
    // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
    LSIS_Pixel4D coneBBoxBot = head - LSIS_Pixel4D( 0, size, size, size);
    LSIS_Pixel4D coneBBoxTop = head + LSIS_Pixel4D( 0, size, size, size);

    LSIS_Point4DFloat headPoint;
    headPoint.set( head.toCartesianW( _image ),
                   head.toCartesianX( _image ),
                   head.toCartesianY( _image ),
                   head.toCartesianZ( _image ) );
    // qDebug() << "Start snake2";

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    for( int axe = 0 ; axe < 4 ; axe++ )
    {
        if( coneBBoxBot(axe) < 0 )
        {
            coneBBoxBot(axe) = 0;
        }

        if( coneBBoxBot(axe) >= _image->dim()(axe) )
        {
            coneBBoxBot(axe) = _image->dim()(axe) - 1;
        }

        if( coneBBoxTop(axe) < 0 )
        {
            coneBBoxTop(axe) = 0;
        }

        if( coneBBoxTop(axe) >= _image->dim()(axe) )
        {
            coneBBoxTop(axe) = _image->dim()(axe) - 1;
        }
    }

    // qDebug() << "Start snake22";
    // Il ne reste plus qu'a regarder dans la boite pour faire une acp des directions
    // On parcour donc toute la bbox
    int nMaxMax = 40;
    QList< LSIS_Point4DFloat > maxiDirections;
    QList< int > maxiScores;
    QList< float > maxiDistances;

    // qDebug() << "Start snake222";
    LSIS_Pixel4D curInBBoxPixel;
    LSIS_Point4DFloat curInBBoxPoint;
    for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
    {
        for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
        {
            for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
            {
                for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                {
                    if( curInBBoxPixel.valueIn( _image ) > 3 && head != curInBBoxPixel )
                    {
                        curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                            curInBBoxPixel.toCartesianX( _image ),
                                            curInBBoxPixel.toCartesianY( _image ),
                                            curInBBoxPixel.toCartesianZ( _image ) );
                        float score = curInBBoxPixel.valueIn( _image );

                        // qDebug() << "Start TTTTTTTTTTTTTTT";
                        // Calcul de la direction potentielle de croissance
                        LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - headPoint;
                        float norm3D = potentialGrowDirPoint.norm3D();

                        // qDebug() << "Start OOOOOOOOO";
                        // Si on a des listes vides tout est simple
                        if( maxiScores.empty() )
                        {
                            // qDebug() << "Start AAAAA111111";
                            maxiScores.append( score );
                            maxiDirections.append( potentialGrowDirPoint );
                            maxiDistances.append( norm3D );
                            // qDebug() << "Start AAAAA22222";
                        }
                        else
                        {
                            // qDebug() << "Start UUUUUUU";
                            // Sinon, on regarde ou inserer l'element dans la liste en fonction de son score
                            // La liste est triee decroissant ( maxiscore(0) > maxiscore(1) > ... )
                            int idElement = 0;
                            while( idElement < maxiScores.size() && maxiScores.at(idElement) > score )
                            {
                                idElement++;
                            }
                            // qDebug() << "Start VVVVVVVV";

                            // Si on a pas atteint la fin de la liste en cherchant sa place alors on peut l'inserer a cet endroit
                            if( idElement < nMaxMax )
                            {
                                // qDebug() << "Start WWWWWWW1111111111";
                                maxiScores.insert(idElement, score);
                                maxiDirections.insert( idElement, potentialGrowDirPoint );
                                maxiDistances.insert( idElement, norm3D );

                                // qDebug() << "Start ZZZZZZ";
                                // Si apres insertion la liste est trop grande alors on supprime le dernier element
                                if( maxiScores.size() > nMaxMax )
                                {
                                    // qDebug() << "Start ZZZZZZZ11111111";
                                    maxiScores.removeLast();
                                    maxiDirections.removeLast();
                                }
                                // qDebug() << "Start WWWWWWWW2222222222";
                            }
                        }
                    }
                }
            }
        }
    }
    // qDebug() << "Start snake4";

    QVector< LSIS_Point4DFloat > dirInCone;
    int nDirections = maxiDirections.size();
        // qDebug() << "Start snake uuuuezjkgfjbdkjfhviug";
    for( int i = 0 ; i < nDirections ; i++ )
    {
        dirInCone.push_back( maxiDirections.at(i) * maxiScores.at(i) * ((size * _image->xres() *1.1 ) - maxiDistances.at(i) ) );
    }
        // qDebug() << "Start snake tototototot";

    // Effectue l'ACP 4D non centree des directions de croissance potentielles
    LSIS_Point4DFloat v1, v2, v3, v4;
    float l1, l2, l3, l4;
    LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                    v1, v2, v3, v4,
                                    l1, l2, l3, l4 );
//    qDebug() << "Start snake7";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();

    // On normalise la direction v1
    if( v1.norm() != 0 )
    {
        v1.normalize();
    }

    v1 *= _image->wres();
//    qDebug() << "Start snake8";

    // On ajoute la tete et la queue comme (head + v1)
    // On ajoute les deux points (transformes en coordonnees cartesiennes) dans la matrice
    _points.resize( 2, 4 );
    _points.row(0) = head.getCartesian( image ) - v1;
    _points.row(1) = head.getCartesian( image ) + v1;
//    qDebug() << "Start snake9";
//    qDebug() << "v1" << v1.x() << v1.y() << v1.z();
//    qDebug() << "head" << _points(0,0) << _points(0,1) << _points(0,2) << _points(0,3);
//    qDebug() << "head" << _points(1,0) << _points(1,1) << _points(1,2) << _points(1,3);

    // Et on resample pour en avoir quatre
    resample( length3D() / 3.0 );
//    qDebug() << "Start snake10";
}

template< typename DataImage >
CT_AbstractItemDrawable* LSIS_ActiveContour4DContinuous<DataImage>::copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList)
{
    LSIS_ActiveContour4DContinuous<DataImage> *activeContour = new LSIS_ActiveContour4DContinuous<DataImage>((const CT_OutAbstractSingularItemModel *)model,result);

    activeContour->_points = this->_points;
    activeContour->_forkStartPoints = this->_forkStartPoints;
    activeContour->_isFork = this->_isFork;
    activeContour->_image = this->_image;
    activeContour->_repulseImage = this->_repulseImage;
    activeContour->_step = this->_step;
    activeContour->_gradw = this->_gradw;
    activeContour->_gradx = this->_gradx;
    activeContour->_grady = this->_grady;
    activeContour->_gradz = this->_gradz;
    activeContour->_imageEnergy = this->_imageEnergy;
    activeContour->_optionalMnt = this->_optionalMnt;
    activeContour->_intensityMin = this->_intensityMin;
    activeContour->_id = this->_id;
    activeContour->_ordre = this->_ordre;
    activeContour->_parent = this->_parent;
    activeContour->_children = this->_children;

    return activeContour;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::tangentAtPoint( int i ) const
{
    LSIS_Point4DFloat rslt;
    LSIS_Point4DFloat curr( _points.row(i) );

    if( i == 0 )
    {
        LSIS_Point4DFloat next( _points.row( i+1 ) );

        // Tangente par difference finie
        rslt = next - curr;
    }

    else if( i == _points.rows() - 1 )
    {
        LSIS_Point4DFloat prev( _points.row( i-1 ) );

        // Tangente par difference finie
        rslt = curr - prev;
    }

    else
    {
        // Acces aux points autour du point d'interet
        LSIS_Point4DFloat prev( _points.row( i-1 ) );
        LSIS_Point4DFloat next( _points.row( i+1 ) );

        // Tangente par difference finie des positions
        rslt = next - prev;
    }

    // Normalise la tangente
    rslt.normalize();
    return rslt;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::directionContoursAtPoint(int indexPoint) const
{
    assertWithMessage( indexPoint >= 0 && indexPoint < npoints(), "On demande la direction du contour sur un point du contours qui n'existe pas : " << indexPoint);
    LSIS_Point4DFloat rslt;

    if( indexPoint == 0 )
    {
        rslt = tangentAtHead();
    }

    else if ( indexPoint == npoints() - 1 )
    {
        rslt = tangentAtBack();
    }

    else
    {
        rslt = pointAt( indexPoint+1 ) - pointAt( indexPoint-1 );
    }

    return rslt;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::secondDifferentialAtPoint( int indexPoint ) const
{
    if( indexPoint == 0 || indexPoint == npoints()-1 )
    {
        return LSIS_Point4DFloat(0,0,0,0);
    }

    LSIS_Point4DFloat prevPoint = pointAt( indexPoint - 1 );
    LSIS_Point4DFloat currPoint = pointAt( indexPoint );
    LSIS_Point4DFloat nextPoint = pointAt( indexPoint + 1 );

    return ( prevPoint - (2*currPoint) + nextPoint );
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::averageSampling() const
{
    int npts = npoints();
    float rslt = 0;

    for( int i = 0 ; i < npts-1 ; i++ )
    {
        rslt += LSIS_Point4DFloat::distance3D( pointAt(i), pointAt(i+1) );
    }

    rslt /= npts;

    return rslt;
}

template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::hasToBeResampled() const
{
    int npts = npoints();
    float distThresh = 2 * _image->xres();

    for( int i = 0 ; i < npts-1 ; i++ )
    {
        if( LSIS_Point4DFloat::distance3D( pointAt(i), pointAt(i+1) ) > distThresh )
        {
            return true;
        }
    }

    return false;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::resample( float sampleRes )
{
    // On recupere la resolution 3D de l'image sur laquelle se deplace le contour
    // C'est cette resolution que l'on cherche a atteindre
    float totalLength = length3D();
    if( totalLength <= sampleRes )
    {
        return;
    }

    float currLength = 0;
    float currSegmentLength = 0;
    float nextPointLength = sampleRes;

    // On copie la matrice de points du snake avant resampling
    int noldpts = npoints();
    Eigen::MatrixXf oldpts = _points;

    // On redimensionne la matrice pour le resampling
    int   nbpointsresampled = floor(totalLength / sampleRes) + 1;    // +1 pour la queue qu'on rajoute toujours
//    qDebug() << "Npoints = " << nbpointsresampled << "Sample res " << sampleRes << "Total length " << totalLength;
    int   indexNewPoint = 1;
    _points.resize( nbpointsresampled, 4 );

    int currPointIndex = 0;
    LSIS_Point4DFloat currPix;

    int nextPointIndex = 1;
    LSIS_Point4DFloat nextPix;

    // On ajoute la tete qui ne bouge pas
    _points.row(0) = oldpts.row(0);

    // Tant qu'il reste de la place dans laquelle ajouter un (des) point(s)
    while( nextPointLength < totalLength && nextPointIndex < noldpts )
    {
        // On avance jusqu'a trouver le segment dans lequel inserer un (ou plusieurs) point(s)
        while( nextPointLength > currLength )
        {
            currPix = oldpts.row( currPointIndex );
            nextPix = oldpts.row( nextPointIndex );

            currPointIndex++;
            nextPointIndex++;

            currSegmentLength = LSIS_Point4DFloat::distance3D( currPix, nextPix );
            currLength += currSegmentLength;
        }

        // On insere autant de points que necessaire dans ce segment
        while( currLength >= nextPointLength )
        {
            // Inserer un point au bon endroit sur le segment
            if( indexNewPoint < _points.rows() )
            {
                float distToAddToSegmentEnd = currLength - nextPointLength;
                float ratio = distToAddToSegmentEnd / currSegmentLength;

                // On prend la direction du segment
                LSIS_Point4DFloat segmentDir = currPix - nextPix;

                // Ajout dans la matrice de points
                _points.row( indexNewPoint ) = nextPix + ratio * segmentDir;
                indexNewPoint++;
            }

            // Avancer l'abscisse du point a inserer
            nextPointLength += sampleRes;
        }
    }

    // On ajoute la queue
    _points.row(nbpointsresampled-1) = oldpts.row( noldpts-1 );
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::length4D() const
{
    float length = 0;
    int npts = npoints();

    for ( int i = 0 ; i < npts-1 ; i++ )
    {
        length += ( pointAt(i) - pointAt(i+1) ).norm();
    }

    return length;
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::length3D() const
{
    float length = 0;
    int npts = npoints();
//    qDebug() << "Dans length 3d" << length << npts;

    for ( int i = 0 ; i < npts-1 ; i++ )
    {
        length += ( pointAt(i) - pointAt(i+1) ).norm3D();
//        qDebug() << "Dans length 3d BOUCLE" << length << npts
//                 << pointAt(i).x() << pointAt(i).y() << pointAt(i).z() << "||||"
//                 << pointAt(i+1).x() << pointAt(i+1).y() << pointAt(i+1).z();
    }

//    qDebug() << "Dans length 3d SORTIE = " << length << npts;

    return length;
}

template< typename DataImage >
QVector<CT_CircleData *> LSIS_ActiveContour4DContinuous<DataImage>::toCircleData(int smoothSize) const
{
    int                         npts = npoints();
    QVector<CT_CircleData*>     rslt( npts );
    LSIS_Point4DFloat           curr(0,0,0,0);
    CT_Point                    dir;
    CT_Point                    pos;

    for ( int i = 0 ; i < npts ; i++ )
    {
        // Recupere le point courant
        curr = pointAt(i);

        dir.setZero();
        pos.setZero();

        // On convertit en CT_Point
        pos.x() = curr.x();
        pos.y() = curr.y();
        pos.z() = curr.z();

        // A partir du point courant on va chercher les sizeSmooth points precedents et suivants pour calculer la direction
        for ( int j = 1 ; j <= smoothSize ; j++ )
        {
            // SI le i+j eme point existe (on arrive pas aux extremites)
            // On met a jour la dir
            if ( i+j < npts )
            {
                dir(0) = ( dir.x() + ( pointAt(i+j).x() - curr.x() ) );
                dir(1) = ( dir.y() + ( pointAt(i+j).y() - curr.y() ) );
                dir(2) = ( dir.z() + ( pointAt(i+j).z() - curr.z() ) );
            }

            // SI le i-j eme point existe (on arrive pas aux extremites)
            // On met a jour la dir
            if ( i-j >= 0 )
            {
                dir(0) = ( dir.x() + ( curr.x() - pointAt(i-j).x() ) );
                dir(1) = ( dir.y() + ( curr.y() - pointAt(i-j).y() ) );
                dir(2) = ( dir.z() + ( curr.z() - pointAt(i-j).z() ) );
            }
        }

        // On cree le cercle correspondant
        // Pour le rayon on n'utilise pas non plus l'image (voir commentaire plus haut en reference aux positions)
        CT_CircleData* toAdd = new CT_CircleData( pos,
                                                  dir,
                                                  curr.w() );

        // Et on l'ajoute au vecteur resultat
        rslt[i] = toAdd;
    }

    return rslt;
}

template< typename DataImage >
CT_Mesh* LSIS_ActiveContour4DContinuous<DataImage>::toMesh( QVector< CT_CircleData* >& circles, int nSidesCylinder ) const
{
    Eigen::Vector3d unitx;
    unitx << 1,0,0;
    Eigen::Vector3d unitz;
    unitz << 0,0,1;

    CT_Mesh* rsltMesh = new CT_Mesh();

    double step = (2.0 * M_PI) / ((double)nSidesCylinder);
    double theta = 0;

    CT_CircleData* firstCircle = circles.at(0);
    CT_Point firstCircleCenter = firstCircle->getCenter();
    CT_Point firstCircleDirection = firstCircle->getDirection();
    double firstCircleRadius = firstCircle->getRadius();
    Eigen::Matrix3d firstCircleChangementBase = getBaseAvecXAligneFromDirection( firstCircleDirection );

    // Calcul des points sur le cercle du bas du premier cylindre
    // Et ajout des points du cylindre courant dans le mesh
    CT_MutablePointIterator firstVi = CT_MeshAllocatorT<CT_Mesh>::AddVertices(rsltMesh, nSidesCylinder);
    QVector< size_t > indicesPointsOnFirstCylinder( nSidesCylinder );
    CT_Point currPointOnFirstCircle;
    CT_Point currPointOnFirstCircleRotated;
    CT_Point currPointOnFirstCircleRotatedTranslated;
    for( int i = 0 ; i < nSidesCylinder ; i++ )
    {        
        // On considere le cercle horizontal centre a 0 0 0
        currPointOnFirstCircle(0) = firstCircleRadius * cos(theta);
        currPointOnFirstCircle(1) = firstCircleRadius * sin(theta);
        currPointOnFirstCircle(2) = 0;

        // On applique la transformation pour l'orienter dans la bonne direction
        currPointOnFirstCircleRotated = firstCircleChangementBase * currPointOnFirstCircle;

        // On applique la translation du centre
        currPointOnFirstCircleRotatedTranslated = currPointOnFirstCircleRotated + firstCircleCenter;

        // Et on ajoute le point au mesh
        firstVi.next().replaceCurrentPoint( currPointOnFirstCircleRotatedTranslated );

        // On recupere l'indice du vertex que l'on vient d'ajouter (pour creer les aretes avec le cylindre suivant)
        indicesPointsOnFirstCylinder[i] = firstVi.cIndex();

        // On avance d'un pas angulaire
        theta += step;
    }

    Eigen::Matrix3d prevCircleChangementBase = firstCircleChangementBase;
    Eigen::Matrix3d nextCircleChangementBase;

    int nCircles = circles.size();
    for( int i = 1 ; i < nCircles ; i++ )
    {
        CT_CircleData* nextCircle = circles.at(i);
        CT_Point nextCircleCenter = nextCircle->getCenter();
        CT_Point nextCircleDirection = nextCircle->getDirection();
        double nextCircleRadius = nextCircle->getRadius();
        nextCircleChangementBase = getNewCircleBaseFromPreviousBase( prevCircleChangementBase, nextCircleDirection );
        theta = 0;  // On remet theta a zero

        // Calcul des points sur le cylindre suivant
        CT_Point currPointOnNextCircle;
        CT_Point currPointOnNextCircleRotated;
        CT_Point currPointOnNextCircleRotatedTranslated;
        QVector< size_t > indicesPointsOnSecondCylinder( nSidesCylinder );
        CT_MutablePointIterator vi = CT_MeshAllocatorT<CT_Mesh>::AddVertices(rsltMesh, nSidesCylinder);
        for( int j = 0 ; j < nSidesCylinder ; j++ )
        {
            // On considere le cercle horizontal centre a 0 0 0
            currPointOnNextCircle(0) = nextCircleRadius * cos(theta);
            currPointOnNextCircle(1) = nextCircleRadius * sin(theta);
            currPointOnNextCircle(2) = 0;

            // On applique la transformation pour l'orienter dans la bonne direction
            currPointOnNextCircleRotated = nextCircleChangementBase * currPointOnNextCircle;

            // On applique la translation du centre
            currPointOnNextCircleRotatedTranslated = currPointOnNextCircleRotated + nextCircleCenter;

            // On ajoute le vertex au mesh
            vi.next().replaceCurrentPoint( currPointOnNextCircleRotatedTranslated );

            // On recupere l'indice du vertex que l'on vient d'ajouter (pour creer les aretes avec le cylindre suivant)
            indicesPointsOnSecondCylinder[j] = vi.cIndex();

            // On avance d'un pas angulaire
            theta += step;
        }

        // Ajout du cylindre courant dans le mesh (ajout des faces et arretes)
        CT_MutableFaceIterator fi = CT_MeshAllocatorT<CT_Mesh>::AddFaces(rsltMesh, 2*nSidesCylinder);
        CT_MutableEdgeIterator ei = CT_MeshAllocatorT<CT_Mesh>::AddHEdges(rsltMesh, 6*nSidesCylinder);

        for( int j = 0 ; j < nSidesCylinder ; j++ )
        {
            // On ajoute le point au maillage
            // Et on avance l'iterateur
            //
            //
            //    q *---* r
            //      | / |
            //    p *---* s
            //
            size_t indexP = indicesPointsOnFirstCylinder[j % nSidesCylinder];
            size_t indexQ = indicesPointsOnSecondCylinder[j % nSidesCylinder];
            size_t indexR = indicesPointsOnSecondCylinder[(j+1) % nSidesCylinder];
            size_t indexS = indicesPointsOnFirstCylinder[(j+1) % nSidesCylinder];

            CT_Edge& edgeBot = ei.next().cT();
            size_t edgeBotIndex = ei.cIndex();
            edgeBot.setPoint0( indexP );
            edgeBot.setPoint1( indexS );

            CT_Edge& edgeRight = ei.next().cT();
            size_t edgeRightIndex = ei.cIndex();
            edgeRight.setPoint0( indexS );
            edgeRight.setPoint1( indexR );

            CT_Edge& edgeDiagTopDown = ei.next().cT();
            size_t edgeDiagTopDownIndex = ei.cIndex();
            edgeDiagTopDown.setPoint0( indexR );
            edgeDiagTopDown.setPoint1( indexP );

            CT_Edge& edgeDiagBotTop = ei.next().cT();
            size_t edgeDiagBotTopIndex = ei.cIndex();
            edgeDiagBotTop.setPoint0( indexP );
            edgeDiagBotTop.setPoint1( indexR );

            CT_Edge& edgeTop = ei.next().cT();
            size_t edgeTopIndex = ei.cIndex();
            edgeTop.setPoint0( indexR );
            edgeTop.setPoint1( indexQ );

            CT_Edge& edgeLeft = ei.next().cT();
            size_t edgeLeftIndex = ei.cIndex();
            edgeLeft.setPoint0( indexQ );
            edgeLeft.setPoint1( indexP );

            // On donne les suivants et precedents aux aretes
            edgeBot.setNext( edgeRightIndex );
            edgeBot.setPrevious( edgeDiagTopDownIndex );
            edgeRight.setNext( edgeDiagTopDownIndex );
            edgeRight.setPrevious( edgeBotIndex );
            edgeDiagTopDown.setNext( edgeBotIndex );
            edgeDiagTopDown.setPrevious( edgeRightIndex );

            edgeTop.setNext( edgeLeftIndex );
            edgeTop.setPrevious( edgeDiagBotTopIndex );
            edgeLeft.setNext( edgeDiagBotTopIndex );
            edgeLeft.setPrevious( edgeTopIndex );
            edgeDiagBotTop.setNext( edgeTopIndex );
            edgeDiagBotTop.setPrevious( edgeLeftIndex );

            // Creation des deux faces avec ces aretes
            CT_Face& faceBotRight = fi.next().cT();
            size_t faceBotRightIndex = fi.cIndex();
            faceBotRight.setEdge( edgeBotIndex );
            edgeBot.setFace( faceBotRightIndex );
            edgeRight.setFace( faceBotRightIndex );
            edgeDiagTopDown.setFace( faceBotRightIndex );

            CT_Face& faceTopLeft = fi.next().cT();
            size_t faceTopLeftIndex = fi.cIndex();
            faceTopLeft.setEdge( edgeTopIndex );
            edgeTop.setFace( faceTopLeftIndex );
            edgeLeft.setFace( faceTopLeftIndex );
            edgeDiagBotTop.setFace( faceTopLeftIndex );
        }

        // On avance dans les cylindres
        indicesPointsOnFirstCylinder = indicesPointsOnSecondCylinder;

        prevCircleChangementBase = nextCircleChangementBase;
    }

    return rsltMesh;
}

template< typename DataImage >
Eigen::Matrix3d LSIS_ActiveContour4DContinuous<DataImage>::getBaseAvecXAligneFromDirection(const CT_Point &dir) const
{
    // On trouve la projection du vecteur ox sur le plan orthogonal a dir
    // Sachant que l'equation de ce plan est dirx*X + diry*Y + dirz*Z + d = 0 et d = 0 (pas d'offset)
    // Z = -( dirx*X + diry*Y ) / dirZ
    CT_Point xProjette;
    xProjette.setValues( 1,
                         0,
                         -(dir.x())/dir.z());

    // Normalisation pour avoir une base orthonormee
    xProjette.normalize();

    // On recupere le troisieme vecteur de la base en utilisant le produit vectoriel
    CT_Point vecProd = dir.cross( xProjette );
    vecProd.normalize();

    Eigen::Matrix3d rslt;
    rslt.col(0) = xProjette;
    rslt.col(1) = vecProd;
    rslt.col(2) = dir;

    return rslt;
}

template< typename DataImage >
Eigen::Matrix3d LSIS_ActiveContour4DContinuous<DataImage>::getNewCircleBaseFromPreviousBase( const Eigen::Matrix3d& prevBase, CT_Point &dir ) const
{
    Eigen::Matrix3d rslt;
    CT_Point oldX = prevBase.col(0);

    // La premiere direction (nouveau "x") est la projection du "vecteur x" de l'ancienne base sur le plan de la nouvelle base
    dir.normalize();
    float pscal = oldX.x() * dir.x() + oldX.y() * dir.y() + oldX.z() * dir.z();
    CT_Point xProjette = oldX - ( pscal ) * dir;

    // Normalisation pour avoir une base orthonormee
    xProjette.normalize();

    // La seconde direction (nouveau "y") est le produit vectoriel entre le nouveau z et le nouveau x
    CT_Point vecProd = dir.cross( xProjette );
    vecProd.normalize();

    rslt.col(0) = xProjette;
    rslt.col(1) = vecProd;
    // On garde comme nouveau z la tangente au snake
    rslt.col(2) = dir;

    return rslt;
}

template< typename DataImage >
CT_MeshModel* LSIS_ActiveContour4DContinuous<DataImage>::toMeshModel(QString modelName,
                                                                     CT_AbstractResult* result,
                                                                     int smoothSize,
                                                                     int nSidesCylinder) const
{
    QVector<CT_CircleData*> circles = toCircleData( smoothSize );
    CT_Mesh* mesh = toMesh( circles, nSidesCylinder );

    CT_MeshModel* rslt = new CT_MeshModel( modelName, result, mesh );

    return rslt;
}

template< typename DataImage >
CT_MeshModel* LSIS_ActiveContour4DContinuous<DataImage>::toMeshModelParall(QString modelName,
                                                                     CT_AbstractResult* result,
                                                                     int smoothSize,
                                                                     int nSidesCylinder) const
{
    QVector<CT_CircleData*> circles = toCircleData( smoothSize );
    CT_Mesh* mesh = toMesh( circles, nSidesCylinder );

    CT_MeshModel* rslt = new CT_MeshModel( modelName, result, mesh );

    return rslt;
}

template< typename DataImage >
QVector<CT_Circle*> LSIS_ActiveContour4DContinuous<DataImage>::toCircles(int smoothSize,
                                                                         ColorMode color,
                                                                         const QString &modelName,
                                                                         CT_AbstractResult *result) const
{
    QVector<CT_CircleData*> rsltData;
    rsltData = toCircleData( smoothSize );

    DataImage scoreMin;
    DataImage scoreMax;
    if( color == INTERNAL_SCORE )
    {
        getScoreStats( scoreMin, scoreMax );
    }

    else if ( color == GLOBAL_SCORE )
    {
        scoreMin = _image->dataMin();
        scoreMax = _image->dataMax();
    }

    QVector<CT_Circle*> rslt;
    int i = 0;
    foreach ( CT_CircleData* curCircleData, rsltData )
    {
        CT_Circle* circ;
        if( modelName == "" )
        {
            circ = new CT_Circle( NULL, result, curCircleData );
        }
        else
        {
            circ = new CT_Circle( modelName, result, curCircleData );
        }

        rslt.push_back( circ );

        if( color != NO_COLOR && pixelAt(i).valueIn( _image ) > 0 )
        {
            // Interpolation de la valeur de la couleur entre 0 et 240 a partir du nombre de vote min et max
            int colorValue = lsis_MathTools::linearInterpolation( scoreMin, scoreMax, 0, 240, pixelAt(i).valueIn( _image ) );
            circ->setDefaultColor( QColor::fromHsv( colorValue, 255, 255 ) );
        }

        i++;
    }

    return rslt;
}

template< typename DataImage >
CT_MeshModel* LSIS_ActiveContour4DContinuous<DataImage>::toMeshModel(int smoothSize,
                                                                     ColorMode color,
                                                                     const QString &modelName,
                                                                     CT_AbstractResult *result) const
{
//    CT_Mesh* rslt = new CT_Mesh();

//    QVector<CT_CircleData*> circleData;
//    circleData = toCircleData( smoothSize );

//    int nCircles = circleData.size();
//    int i = 0;
//    for( int i = 1 ; i < nCircles ; i++ )
//    {
//        CT_CircleData* prevCircle = circleData.at(i-1);
//        CT_CircleData* curCircle = circleData.at(i);
//        CT_Point prevCenter = prevCircle->getCenter();
//        CT_Point currCenter = curCircle->getCenter();
//        float height = sqrt( (prevCenter.x() - currCenter.x())*(prevCenter.x() - currCenter.x()) +
//                             (prevCenter.y() - currCenter.y())*(prevCenter.y() - currCenter.y()) +
//                             (prevCenter.z() - currCenter.z())*(prevCenter.z() - currCenter.z()) );

//        rslt->addCylinder( prevCircle->getRadius(), height, 40,  );
//    }
//    foreach ( CT_CircleData* curCircleData, rsltData )
//    {
//        CT_Circle* circ;
//        if( modelName == "" )
//        {
//            circ = new CT_Circle( NULL, result, curCircleData );
//        }
//        else
//        {
//            circ = new CT_Circle( modelName, result, curCircleData );
//        }

//        rslt.push_back( circ );

//        if( color != NO_COLOR )
//        {
//            // Interpolation de la valeur de la couleur entre 0 et 240 a partir du nombre de vote min et max
//            int colorValue = lsis_MathTools::linearInterpolation( scoreMin, scoreMax, 0, 240, pixelAt(i).valueIn( _image ) );
//            circ->setDefaultColor( QColor::fromHsv( colorValue, 255, 255 ) );
//        }

//        i++;
//    }

//    return rslt;
}

template< typename DataImage >
QVector<CT_LineData *> LSIS_ActiveContour4DContinuous<DataImage>::toLineData() const
{
    int nbLines = npoints() - 1;
    QVector<CT_LineData*> skeleton(nbLines);
    LSIS_Point4DFloat curr;
    LSIS_Point4DFloat next;

    for ( int i = 0 ; i < nbLines ; i++ )
    {
        curr = pointAt(i);
        next = pointAt(i+1);

        skeleton[i] = new CT_LineData( createCtPoint( curr.x(), curr.y(), curr.z() ),
                                       createCtPoint( next.x(), next.y(), next.z() ) );
    }

    return skeleton;
}

template< typename DataImage >
QVector<CT_Line *> LSIS_ActiveContour4DContinuous<DataImage>::toLines() const
{
    int nbLines = npoints() - 1;
    QVector<CT_Line*> skeleton(nbLines);
    LSIS_Point4DFloat curr;
    LSIS_Point4DFloat next;

    for ( int i = 0 ; i < nbLines ; i++ )
    {
        curr = pointAt(i);
        next = pointAt(i+1);

        CT_Line* toAdd = new CT_Line( NULL, NULL, new CT_LineData( createCtPoint( curr.x(), curr.y(), curr.z() ),
                                                                   createCtPoint( next.x(), next.y(), next.z() ) ) );
        toAdd->setCenterX( (next.x() + curr.x() ) / 2.0 );
        toAdd->setCenterY( (next.y() + curr.y() ) / 2.0 );
        toAdd->setCenterZ( (next.z() + curr.z() ) / 2.0 );
        skeleton[i] = toAdd;
    }

    return skeleton;
}

template< typename DataImage >
Eigen::MatrixXf* LSIS_ActiveContour4DContinuous<DataImage>::getGeometricMatrix(float alpha, float beta) const
{
    int npts = npoints();
    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, npts );
    rslt->setZero();

    // On remplit toutes les lignes sauf les deux de chaque bord (conditions aux bords)
    for ( int i = 0 ; i < npts ; i++ )
    {
        if ( i >= 2 && i < npts-2 )
        {
            if (  i-2 >= 0 )
            {
                (*rslt)(i,i-2) = beta;
            }

            if (  i-1 >= 0 )
            {
                (*rslt)(i,i-1) = -alpha - 4*beta;
            }

            (*rslt)(i,i) = 2*alpha + 6*beta ;

            if (  i+1 <= npts-1 )
            {
                (*rslt)(i,i+1) = -alpha - 4*beta;
            }

            if (  i+2 <= npts-1 )
            {
                (*rslt)(i,i+2) = beta;
            }
        }
    }

    // TEMPORAIRE : commentaire des lignes ci dessous
//    qDebug() << "Warning : faire le travail sur les conditions aux bords de la matricede geometrie";
//    qDebug() << "Warning : faire le travail de division par h pour avoir une homogeneite (dans la plupart des cas on est a peu pres homogene puisque lesnake est souvent resample mais ce n'est pas le cas exactement)";
    // TEMPORAIRE FIN

    return rslt;
}

template< typename DataImage >
Eigen::MatrixXf* LSIS_ActiveContour4DContinuous<DataImage>::getGeometricMatrixHomogeneousElasticityCurvature(float alpha, float beta) const
{
    float averageDist = averageSampling();
    alpha = alpha / averageDist;
    beta /= averageDist;
    int npts = npoints();
    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, npts );
    rslt->setZero();

    // On remplit toutes les lignes sauf les deux de chaque bord (conditions aux bords)
    for ( int i = 0 ; i < npts ; i++ )
    {
        if ( i >= 2 && i < npts-2 )
        {
            if (  i-2 >= 0 )
            {
                (*rslt)(i,i-2) = beta;
            }

            if (  i-1 >= 0 )
            {
                (*rslt)(i,i-1) = -alpha - 4*beta;
            }

            (*rslt)(i,i) = 2*alpha + 6*beta ;

            if (  i+1 <= npts-1 )
            {
                (*rslt)(i,i+1) = -alpha - 4*beta;
            }

            if (  i+2 <= npts-1 )
            {
                (*rslt)(i,i+2) = beta;
            }
        }
    }

    // TEMPORAIRE : commentaire des lignes ci dessous
//    qDebug() << "Warning : faire le travail sur les conditions aux bords de la matricede geometrie";
//    qDebug() << "Warning : faire le travail de division par h pour avoir une homogeneite (dans la plupart des cas on est a peu pres homogene puisque lesnake est souvent resample mais ce n'est pas le cas exactement)";
    // TEMPORAIRE FIN

    return rslt;
}

template< typename DataImage >
Eigen::MatrixXf* LSIS_ActiveContour4DContinuous<DataImage>::getAPlusRhoIInverse(float alpha, float beta, float timeStep )
{
    // Calcul de A+rho*I
    int npts = npoints();
    float rho = 1.0 / timeStep;
    Eigen::MatrixXf* rslt= new Eigen::MatrixXf( npts, npts );

    // Matrice identite I
    Eigen::MatrixXf* identity = new Eigen::MatrixXf( npts, npts );
    identity->setIdentity(npts, npts);

    // Matrice geometrique A
    Eigen::MatrixXf* matrixA = getGeometricMatrix( alpha, beta );

    // Calcul de l'inverse voulu
    Eigen::MatrixXf* aPlusRhoI = new Eigen::MatrixXf( npts, npts );
    (*aPlusRhoI) = (*matrixA) + ( rho * (*identity) );
    (*rslt) = aPlusRhoI->inverse();

    // Libere la memoire
    delete identity;
    delete matrixA;
    delete aPlusRhoI;

    return rslt;
}

template< typename DataImage >
Eigen::MatrixXf* LSIS_ActiveContour4DContinuous<DataImage>::getRhoIMoinsAInverse(float alpha, float beta, float timeStep )
{
    // Calcul de A+rho*I
    int npts = npoints();
    float rho = 1.0 / timeStep;
    Eigen::MatrixXf* rslt= new Eigen::MatrixXf( npts, npts );

    // Matrice identite I
    Eigen::MatrixXf* identity = new Eigen::MatrixXf( npts, npts );
    identity->setIdentity(npts, npts);

    // Matrice geometrique A
    Eigen::MatrixXf* matrixA = getGeometricMatrix( alpha, beta );

    // Calcul de l'inverse voulu
    Eigen::MatrixXf* aRhoIMoinsA = new Eigen::MatrixXf( npts, npts );
    (*aRhoIMoinsA) = ( rho * (*identity) ) - (*matrixA);
    (*rslt) = aRhoIMoinsA->inverse();

    // Libere la memoire
    delete identity;
    delete matrixA;
    delete aRhoIMoinsA;

    return rslt;
}

template< typename DataImage >
Eigen::MatrixXf *LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyGrowMatrix( float angleDegresMax,
                                                                                         int size,
                                                                                         float seuilVariance,
                                                                                         int tmpIteration,
                                                                                         float seuilSigma,
                                                                                         float seuilSigma2,
                                                                                         CT_Grid4D<bool>* repulseImage,
                                                                                         QList<float>& snrHead,
                                                                                         QList<float>& snrBack,
                                                                                         QList<bool>& growHead,
                                                                                         QList<bool>& growBack,
                                                                                         QList<float>& sigmasHead,
                                                                                         QList<float>& sigmasBack,
                                                                                         int& nManquePixelsDansConeHead,
                                                                                         int& nManquePixelsDansConeBack,
                                                                                         int nSigmaSmooth,
                                                                                         bool& hasGrownHead,
                                                                                         bool& hasGrownBack,
                                                                                         float globalWeight) const
{
    int npts = npoints();
    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, 4 );
    rslt->setZero();

    // Le gradient est nul partout sauf pour la tete et la queue du contour
    // Pour plus de precision, voir le calcul des directions de croissance d'un contour
    // c.f. methode gradientEnergyCroissanceAtPoint( int i )
    LSIS_Point4DFloat gradCroissanceHead(0,0,0,0);
    LSIS_Point4DFloat gradCroissanceBack(0,0,0,0);

    LSIS_Pixel4D headPix;
    headPix.setFromCartesian( head(), _image );
    LSIS_Point4DFloat directionHead = tangentAtHead();
    directionHead *= -1;
    LSIS_Pixel4D backPix;
    backPix.setFromCartesian( back(), _image );
    LSIS_Point4DFloat directionBack = tangentAtBack();

    if( /*headPix.valueIn( repulseImage ) == false &&*/ hasGrownHead && nManquePixelsDansConeHead < 10 )
    {
        gradCroissanceHead = getGradientEnergyGrowProportionalResolution( headPix,
                                                                          directionHead,
                                                                          angleDegresMax,
                                                                          size,
                                                                          seuilVariance,
                                                                          tmpIteration,
                                                                          seuilSigma,
                                                                          seuilSigma2,
                                                                          sigmasHead,
                                                                          nSigmaSmooth,
                                                                          hasGrownHead,
                                                                          nManquePixelsDansConeHead,
                                                                          growHead,
                                                                          globalWeight,
                                                                          repulseImage );
    }

    if( /*backPix.valueIn( repulseImage ) == false &&*/ hasGrownBack  && nManquePixelsDansConeBack < 10 )
    {
        gradCroissanceBack = getGradientEnergyGrowProportionalResolution( backPix,
                                                                          directionBack,
                                                                          angleDegresMax,
                                                                          size,
                                                                          seuilVariance,
                                                                          tmpIteration,
                                                                          seuilSigma,
                                                                          seuilSigma2,
                                                                          sigmasBack,
                                                                          nSigmaSmooth,
                                                                          hasGrownBack,
                                                                          nManquePixelsDansConeBack,
                                                                          growBack,
                                                                          globalWeight,
                                                                          repulseImage );
    }

    // On affecte ces gradients a la premiere et derniere ligne de la matrice resultat
    for( int j = 0 ; j < 4 ; j++ )
    {
        (*rslt)( 0, j ) = gradCroissanceHead(j);
        (*rslt)( npts-1, j ) = gradCroissanceBack(j);
    }

    return rslt;
}

//template< typename DataImage >
//Eigen::MatrixXf *LSIS_ActiveContour4DContinuous< DataImage >::getGradientEnergyGrowMatrixv2(CT_Grid4D<bool> *repulseImage,
//                                                                                            float coneAngleDegres,
//                                                                                            int coneSizePixels,
//                                                                                            float seuilSigmaL1,
//                                                                                            float& outSigmaL1Head,
//                                                                                            float& outSigmaL1Back,
//                                                                                            bool& outHasGrownHead,
//                                                                                            bool& outHasGrownBack,
//                                                                                            bool& outHasGrown3DHead,
//                                                                                            bool& outHasGrown3DBack) const
//{
//    int npts = npoints();
//    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, 4 );
//    rslt->setZero();

//    // Le gradient est nul partout sauf pour la tete et la queue du contour
//    // Pour plus de precision, voir le calcul des directions de croissance d'un contour
//    LSIS_Point4DFloat gradCroissanceHead(0,0,0,0);
//    LSIS_Point4DFloat gradCroissanceBack(0,0,0,0);

//    // Si le snake avait grandi en tete la derniere iteration on le refait croitre
//    // Sinon il s'etait arrete pour une bonne raison et on ne le fait plus croitre
//    if( outSigmaL1Head > seuilSigmaL1 && outHasGrownHead && outHasGrown3DHead )
//    {
//        // Calcul de la croissance en tete
//        LSIS_Point4DFloat headSnake = head();
//        LSIS_Pixel4D pixHead;
//        pixHead.setFromCartesian( headSnake, _image );
//        LSIS_Point4DFloat tangentHead = tangentAtHead();
//        tangentHead *= -1;

//        gradCroissanceHead = getGradientEnergyGrowProportionalResolutionv2( repulseImage,
//                                                                            headSnake,
//                                                                            pixHead,
//                                                                            tangentHead,
//                                                                            coneAngleDegres,
//                                                                            coneSizePixels,
//                                                                            outSigmaL1Head,
//                                                                            outHasGrownHead,
//                                                                            outHasGrown3DHead );
//    }

//    // Si le snake avait grandi en queue la derniere iteration on le refait croitre
//    // Sinon il s'etait arrete pour une bonne raison et on ne le fait plus croitre
//    if( outSigmaL1Back > seuilSigmaL1 && outHasGrownBack && outHasGrown3DBack )
//    {
//        // Calcul de la croissance en queue
//        LSIS_Point4DFloat backSnake = back();
//        LSIS_Pixel4D pixBack;
//        pixBack.setFromCartesian( backSnake, _image );
//        LSIS_Point4DFloat tangentBack = tangentAtBack();

//        gradCroissanceBack = getGradientEnergyGrowProportionalResolutionv2( repulseImage,
//                                                                            backSnake,
//                                                                            pixBack,
//                                                                            tangentBack,
//                                                                            coneAngleDegres,
//                                                                            coneSizePixels,
//                                                                            outSigmaL1Back,
//                                                                            outHasGrownBack,
//                                                                            outHasGrown3DBack );
//    }

//    // On affecte ces gradients a la premiere et derniere ligne de la matrice resultat
//    for( int j = 0 ; j < 4 ; j++ )
//    {
//        (*rslt)( 0, j ) = gradCroissanceHead(j);
//        (*rslt)( npts-1, j ) = gradCroissanceBack(j);
//    }

//    return rslt;
//}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous< DataImage >::getGrowingDirections(CT_Grid4D_Sparse<bool> *repulseImage,
                                                                       float coneAngleDegres,
                                                                       int coneSizePixels,
                                                                       float seuilSigmaL1,
                                                                       float &outSigmaL1Head,
                                                                       float &outSigmaL1Back,
                                                                       bool& outHasGrownHead,
                                                                       bool& outHasGrownBack,
                                                                       LSIS_Point4DFloat& outGrowDirHead,
                                                                       LSIS_Point4DFloat& outGrowDirBack ) const

{
    // Le gradient est nul partout sauf pour la tete et la queue du contour
    // Pour plus de precision, voir le calcul des directions de croissance d'un contour
    for( int i = 0 ; i < 4 ; i++ )
    {
        outGrowDirHead(i) = 0;
        outGrowDirBack(i) = 0;
    }

    // Si le snake avait grandi en tete la derniere iteration on le refait croitre
    // Sinon il s'etait arrete pour une bonne raison et on ne le fait plus croitre
    if( outSigmaL1Head > seuilSigmaL1 && outHasGrownHead )
    {
        // Calcul de la croissance en tete
        LSIS_Point4DFloat headSnake = head();
        LSIS_Pixel4D pixHead;
        pixHead.setFromCartesian( headSnake, _image );
        LSIS_Point4DFloat tangentHead = tangentAtHead();
        tangentHead *= -1;

        outGrowDirHead = getGradientEnergyGrowProportionalResolutionv4( repulseImage,
                                                                        headSnake,
                                                                        pixHead,
                                                                        tangentHead,
                                                                        coneAngleDegres,
                                                                        coneSizePixels,
                                                                        outSigmaL1Head,
                                                                        outHasGrownHead );
    }

    // Si le snake avait grandi en queue la derniere iteration on le refait croitre
    // Sinon il s'etait arrete pour une bonne raison et on ne le fait plus croitre
    if( outSigmaL1Back > seuilSigmaL1 && outHasGrownBack )
    {
        // Calcul de la croissance en queue
        LSIS_Point4DFloat backSnake = back();
        LSIS_Pixel4D pixBack;
        pixBack.setFromCartesian( backSnake, _image );
        LSIS_Point4DFloat tangentBack = tangentAtBack();

        outGrowDirBack = getGradientEnergyGrowProportionalResolutionv4( repulseImage,
                                                                        backSnake,
                                                                        pixBack,
                                                                        tangentBack,
                                                                        coneAngleDegres,
                                                                        coneSizePixels,
                                                                        outSigmaL1Back,
                                                                        outHasGrownBack );
    }
//    else
//    {
//        qDebug() << "Queue stop" << outSigmaL1Back << seuilSigmaL1;
//    }
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyGrowProportionalResolution(const LSIS_Pixel4D &pix,
                                                                                                         const LSIS_Point4DFloat &direction,
                                                                                                         float angleDegresMax,
                                                                                                         int size,
                                                                                                         float seuilVariance,
                                                                                                         int tmpIteration,
                                                                                                         float seuilSigma,
                                                                                                         float seuilSigma2,
                                                                                                         QList<float>& sigmas,
                                                                                                         int nSigmasSmooth,
                                                                                                         bool& outGrow,
                                                                                                         int& nIterationsManquePixelsInCone,
                                                                                                         QList<bool>& grow,
                                                                                                         float globalWeight,
                                                                                                         CT_Grid4D<bool>*repulseImage ) const
{
    LSIS_Point4DFloat   directionNormalized = direction / direction.norm();

//    // Si le pixel est hors de l'image ou qu'il vaut 0 on ne donne pas de croissance
//    if( !pix.isIn( _image ) || pix.valueIn( _image) <= 0 )
//    {
//        grow.append( false );
//        if( grow.size() >= 5 )
//        {
//            grow.erase( grow.begin() );
//        }
//        qDebug() << "Pixel a zero alors on ne croit plus";
//        return LSIS_Point4DFloat(0,0,0,0);
//    }

    // Recupere l'ensemble des pixels situes dans un cone autour de la direction tangente
    // En vue d'en faire une acp ponderee
    QVector< LSIS_Point4DFloat > dirInCone;
    QVector< float > poidsACP;
    LSIS_Point4DFloat centroidDirInCone(0,0,0,0);
    int sumWeight = 0;
    LSIS_Pixel4D pixelInCone;
    int nPixInConeTmp = 0;
    beginForAllPixelsInCone( pix, directionNormalized, angleDegresMax, size, _image->wdim(), _image->xdim(), _image->ydim(), _image->zdim(), pixelInCone )
    {
        // TEMPORAIRE
        if( pixelInCone.valueIn( _image ) > 0 )
        {
            nPixInConeTmp++;
        }
        // TEMPORAIRE FIN

        // Ne tient compte que des valeurs positives
        if( fabs( (long double)pix.w() - pixelInCone.w() ) < 2 && pixelInCone.valueIn( _image ) > 0 /*&& pixelInCone.valueIn( repulseImage ) == false*/ )

        {
            // On l'ajoute au vecteur de pixels contenus dans le cone
            // En le ponderant par sa valeur dans l'image
            LSIS_Point4DFloat curDirInConde (  (pixelInCone.toCartesianW( _image ) - pix.toCartesianW( _image )),
                                               (pixelInCone.toCartesianX( _image ) - pix.toCartesianX( _image )),
                                               (pixelInCone.toCartesianY( _image ) - pix.toCartesianY( _image )),
                                               (pixelInCone.toCartesianZ( _image ) - pix.toCartesianZ( _image )) );
            curDirInConde.normalize();
            poidsACP.push_back( pixelInCone.valueIn( _image ) );
            curDirInConde(0) *= pixelInCone.valueIn( _image );
            curDirInConde(1) *= pixelInCone.valueIn( _image );
            curDirInConde(2) *= pixelInCone.valueIn( _image );
            curDirInConde(3) *= pixelInCone.valueIn( _image );
            dirInCone.push_back( curDirInConde );

            // Pour le barycentre
            centroidDirInCone = centroidDirInCone + curDirInConde;
            sumWeight += pixelInCone.valueIn( _image );
        }
    }
    endForAllPixelsInCone( pix, directionNormalized, angleDegresMax, size, _image->wdim(), _image->xdim(), _image->ydim(), _image->zdim(), pixelInCone )

    if( dirInCone.size() <= 4 )
    {
//        grow.append( false );
//        if( grow.size() >= 5 )
//        {
//            grow.erase( grow.begin() );
//        }

//        outGrow = false;
        qDebug() << "Pas assez de vecteurs pour l'acp alors on ne croit plus : " << dirInCone.size() << " sur " << nPixInConeTmp;
        nIterationsManquePixelsInCone++;
        return LSIS_Point4DFloat(0,0,0,0);
    }

    // Maintenant qu'on a recupere les pixels d'interet
    // On lance une acp dessus
    if( sumWeight != 0 )
    {
        centroidDirInCone /= sumWeight;
    }

    LSIS_Point4DFloat v1, v2, v3, v4;
    float l1, l2, l3, l4;
    LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                    v1, v2, v3, v4,
                                    l1, l2, l3, l4 );

    float sigma = l4 / (l1+l2+l3+l4);
    float sigma2 = l1 / (l1+l2+l3+l4);

    // Le gradient de l'energie de croissance est defini comme la projection de ce vecteur sur la tangente a la tete
    // On normalise la direction v1, on lui donne une norme egale a la resolution en 3D en fait
//    v1.w() = direction.w();
//    v1 = meanDir;
//    v1 = v4;
    if( v1.norm() != 0 )
    {
        v1.normalize();
    }

    float scalProd = v1.scalarProduct3D( directionNormalized );

    if( v1.norm() != 0 )
    {
        v1.normalize();

        // Le vecteur v1 en sortie d'ACP est pas forcément orienté vers la sortie du snake
        // On le réoriente en lui donnant la même orientation que la tangente au snake
        if( scalProd < 0 )
        {
            v1 *= -1;
        }
    }

    // Normalise par le produit scalaire et la resolution de l'espace de Hough
//    v1.w() = direction.w() * _image->wres();
    v1.w() = v1.w() * _image->wres();
    v1.x() = v1.x() * _image->xres();
    v1.y() = v1.y() * _image->yres();
    v1.z() = v1.z() * _image->zres();
    LSIS_Point4DFloat growDir = v1 * fabs( scalProd );

    // Ajoute le sigma a la liste pour pouvoir faire une moyenne sur les 10 dernieres iterations
    sigmas.push_back( sigma2 );
    if( sigmas.size() > nSigmasSmooth )
    {
        sigmas.erase( sigmas.begin() );
        float sigmaMean = 0;
        for( int i = 0 ; i < nSigmasSmooth ; i++ )
        {
            sigmaMean += sigmas.at(i);
        }
        sigmaMean /= (float)nSigmasSmooth;
//        qDebug() << "Sigma moyen = " << sigmaMean;

        if( sigmaMean < seuilSigma2 )
        {
//            qDebug() << "Sigmas sortie = " << sigmas;
//            qDebug() << "Snake " << (*this);
            outGrow = false;
            return LSIS_Point4DFloat(0,0,0,0);
        }
    }

    nIterationsManquePixelsInCone = 0;
//    qDebug() << "Grow Dir = " << growDir;
//    qDebug() << "Croissance w " << growDir.w();
    return ( growDir * -1 );
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous< DataImage >::getGradientEnergyGrowProportionalResolutionv2(CT_Grid4D_Sparse<bool> *repulseImage,
                                                                                                             LSIS_Point4DFloat& snakeExtremityPoint,
                                                                                                             LSIS_Pixel4D &snakeExtremityPix,
                                                                                                             const LSIS_Point4DFloat &snakeTangentAtExtremity,
                                                                                                             float coneAngleDegres,
                                                                                                             int coneSizePixels,
                                                                                                             float& outSigmaL1,
                                                                                                             bool& outHasGrown,
                                                                                                             bool verbose) const
{
//    if( _optionalMnt != NULL )
//    {
//        if( fabs(snakeExtremityPoint.z() - _optionalMnt->valueAtCoords( snakeExtremityPoint.x(), snakeExtremityPoint.y() ) ) < snakeExtremityPoint.w() )
//        {
//            qDebug() << "On stoppe la croissance pour ne pas toucher le mnt";
//            outHasGrown = false;
//            return LSIS_Point4DFloat(0,0,0,0);
//        }
//    }
//    else
//    {
//        if( fabs(snakeExtremityPoint.z() - repulseImage->minZ()) < snakeExtremityPoint.w() )
//        {
//            qDebug() << "On stoppe la croissance pour ne pas touher le z min";
//            outHasGrown = false;
//            return LSIS_Point4DFloat(0,0,0,0);
//        }
//    }

    float tangentNorm3D = snakeTangentAtExtremity.norm3D();
    LSIS_Point4DFloat tangentNormalized3D( 0,
                                           snakeTangentAtExtremity.x() / tangentNorm3D,
                                           snakeTangentAtExtremity.y() / tangentNorm3D,
                                           snakeTangentAtExtremity.z() / tangentNorm3D );

//    if( tangentNorm3D < 0.000001 )
//    {
//        qDebug() << "Extremity " << snakeExtremityPoint;
//        qDebug() << "Extremity pix " << snakeExtremityPix;
//        qDebug() << "Tangente " << snakeTangentAtExtremity;
//        qDebug() << "Tangente3DNormalisee " << tangentNormalized3D;
//    }

    // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
    // ATTENTION on dit que le rayon change peu donc on prend seulement quelques rayons autour
//    int nPixelsInWDir = coneSizePixels * _image->xres() / _image->wres();
    int nPixelsInWDir = 5;
    LSIS_Pixel4D coneBBoxBot = snakeExtremityPix - LSIS_Pixel4D( nPixelsInWDir, coneSizePixels, coneSizePixels, coneSizePixels);
    LSIS_Pixel4D coneBBoxTop = snakeExtremityPix + LSIS_Pixel4D( nPixelsInWDir, coneSizePixels, coneSizePixels, coneSizePixels);

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    for( int axe = 0 ; axe < 4 ; axe++ )
    {
        if( coneBBoxBot(axe) < 0 )
        {
            coneBBoxBot(axe) = 0;
        }

        if( coneBBoxBot(axe) >= _image->dim()(axe) )
        {
            coneBBoxBot(axe) = _image->dim()(axe) - 1;
        }

        if( coneBBoxTop(axe) < 0 )
        {
            coneBBoxTop(axe) = 0;
        }

        if( coneBBoxTop(axe) >= _image->dim()(axe) )
        {
            coneBBoxTop(axe) = _image->dim()(axe) - 1;
        }
    }

    // Il ne reste plus qu'a tester les angles des directions a l'interieur de la bbox
    // On parcour donc toute la bbox
    QVector< LSIS_Point4DFloat > dirInCone;
    QVector< int > scoreInCone;
    LSIS_Pixel4D curInBBoxPixel;
    LSIS_Point4DFloat curInBBoxPoint;
    for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
    {
        for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
        {
            for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
            {
                for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                {
                    if( curInBBoxPixel.valueIn( _image ) > 3 /*&& curInBBoxPixel.valueIn( repulseImage ) == false*/ )
                    {
                        if( !( curInBBoxPixel == snakeExtremityPix ) )
                        {
                            curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                                curInBBoxPixel.toCartesianX( _image ),
                                                curInBBoxPixel.toCartesianY( _image ),
                                                curInBBoxPixel.toCartesianZ( _image ) );
                            float score = curInBBoxPixel.valueIn( _image );


                            // Calcul de la direction potentielle de croissance
                            LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - snakeExtremityPoint;

                            // On normalise pour avoir la direction potentielle sur la pshere unite
                            potentialGrowDirPoint.normalize();

                            // Test de l'angle en 3D
                            float norm3D = potentialGrowDirPoint.norm3D();
                            LSIS_Point4DFloat potentialGrowDirPoint3DNormalised( 0,
                                                                                 potentialGrowDirPoint.x() / norm3D,
                                                                                 potentialGrowDirPoint.y() / norm3D,
                                                                                 potentialGrowDirPoint.z() / norm3D );

                            float scalarProduct = potentialGrowDirPoint3DNormalised.scalarProduct3D( tangentNormalized3D );

    //                        if( verbose && std::isnan(scalarProduct) )
    //                        {
    //                            qDebug() << "Extremity " << snakeExtremityPoint;
    //                            qDebug() << "Extremity pix " << snakeExtremityPix;
    //                            qDebug() << "Pixel dans bbox" << curInBBoxPixel;
    //                            qDebug() << "Pixel value" << score;
    //                            qDebug() << "Tangente " << snakeTangentAtExtremity;
    //                            qDebug() << "Tangente3DNormalisee " << tangentNormalized3D;
    //                            qDebug() << "Direction croissance " << potentialGrowDirPoint3DNormalised;
    //                            qDebug() << "Produit scalaire " << scalarProduct;
    //                        }

                            // Il faut que les deux vecteurs soient de meme sens
                            if( scalarProduct >= 0 )
                            {
                                float potentialGrowDirAngle = (180.0/M_PI) * acos( scalarProduct ); // Pas besoin de diviser par la norme car potentielGrowDirPoint et tangentNormalized ont une norme de 1

                                if( potentialGrowDirAngle <= coneAngleDegres )
                                {
    //                                // Pondere par le score dans l'espace de Hough
                                    for( int i = 0 ; i < 4 ; i++ )
                                    {
                                        potentialGrowDirPoint(i) *= score * scalarProduct;
                                    }

                                    // Ajout de la direction potentielle ponderee au tableau de directions pontentielles dans le cone
                                    dirInCone.push_back( potentialGrowDirPoint );
                                    scoreInCone.push_back( score );
                                }

    //                            if( verbose )
    //                            {
    //                                qDebug() << "Rejete car hors de l'angle : " << coneAngleDegres << "\n------";
    //                            }
                            }

                            else
                            {
    //                            if( verbose )
    //                            {
    //                                qDebug() << "Rejete car produit scalaire negatif\n------";
    //                            }
                            }
                        }
                    }
                }
            }
        }
    }

    // On teste si le cone contenait assez de directions potentielles de croissance pour faire une ACP 4D dessus
    if( dirInCone.size() < 4 )
    {
//        qDebug() << "Pas assez de cellules pour acp" << coneSizePixels * _image->xres();
        // Si ce n'est pas le cas, alors il ne faut pas croitre : message de debug et on renvoie une croissance nulle

        // On ajoute 20cm a chaque fois
        float tailleNouveauConePixel = coneSizePixels + ( 0.20 / _image->xres() );
        float tailleNouveauCone = tailleNouveauConePixel * _image->xres();
        float tailleCone = coneSizePixels * _image->xres();

//        qDebug() << dirInCone;
        if( tailleCone < 1.5 ) // cone d'1.5 metre max
        {
//            qDebug() << "Nouvel appel avec " << tailleNouveauCone;
            return getGradientEnergyGrowProportionalResolutionv2(repulseImage,
                                                                 snakeExtremityPoint,
                                                                 snakeExtremityPix,
                                                                 snakeTangentAtExtremity,
                                                                 coneAngleDegres,
                                                                 tailleNouveauConePixel,
                                                                 outSigmaL1,
                                                                 outHasGrown,
                                                                 true );
        }

        else
        {
            qDebug() << "Toujours pas trouve avec un cone de recherche de taille " << tailleCone;
            outHasGrown = false;
            return LSIS_Point4DFloat(0,0,0,0);
        }
    }

    // Effectue l'ACP 4D non centree des directions de croissance potentielles
    LSIS_Point4DFloat v1, v2, v3, v4;
    float l1, l2, l3, l4;
    LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                    v1, v2, v3, v4,
                                    l1, l2, l3, l4 );

    // Calcul du sigmaL1 pour cette ACP
    outSigmaL1 = l1 / (l1+l2+l3+l4);

    // On normalise la direction v1
    if( v1.norm() != 0 )
    {
        v1.normalize();
    }

//    // TEMPORAIRE ON REESSAIE LA MOYENNE PONDEREE
//    v1.set(0,0,0,0);
//    for( int toto = 0 ; toto < scoreInCone.size() ; toto++ )
//    {
//        for( int titi = 0 ; titi < 4 ; titi++ )
//        {
//            v1[titi] += ( scoreInCone.at(toto) * dirInCone.at(toto)[titi] );
//        }
//    }
//    v1.normalize();

    float v1Norm3D = v1.norm3D();
    LSIS_Point4DFloat v1Normalized3D( 0,
                                      v1.x() / v1Norm3D,
                                      v1.y() / v1Norm3D,
                                      v1.z() / v1Norm3D );

    // Produit scalaire entre la direction de croissance et la tangente au snake pour calculer la projection
    float scalProd = v1Normalized3D.scalarProduct3D( tangentNormalized3D );

    // Le vecteur v1 en sortie d'ACP est pas forcément orienté vers l'extremite du snake
    // On le réoriente en lui donnant la même orientation que la tangente au snake (verification du signe du produit scalaire)
    if( scalProd < 0 )
    {
        v1 *= -1;
    }

// QUESTIONNEMENT ICI : Est ce bien de rescaler v1 ?
// QUESTIONNEMENT ICI : On voudrait qu'il avance de 1 case de l'espace de Hough environ donc on le rescale pour que sa norme soit environ egale a une taille de cellule de Hough
    // On rescale la direction de croissance conformement a la resolution de l'espace de Hough
//    qDebug() << "V1\n" << v1;
    v1.normalize();

    if( v1.norm3D() < 0.00001 )
    {
//        qDebug() << "Direction de croissance presque nulle" << v1;
//        outHasGrown = false;
//        return LSIS_Point4DFloat(0,0,0,0);

        // On ajoute 20cm a chaque fois
        float tailleNouveauConePixel = coneSizePixels + ( 0.20 / _image->xres() );
        float tailleNouveauCone = tailleNouveauConePixel * _image->xres();
        float tailleCone = coneSizePixels * _image->xres();

//        qDebug() << dirInCone;
        if( tailleCone < 1.5 ) // cone d'1.5 metre max
        {
//            qDebug() << "Nouvel appel avec " << tailleNouveauCone;
            return getGradientEnergyGrowProportionalResolutionv2(repulseImage,
                                                                 snakeExtremityPoint,
                                                                 snakeExtremityPix,
                                                                 snakeTangentAtExtremity,
                                                                 coneAngleDegres,
                                                                 tailleNouveauConePixel,
                                                                 outSigmaL1,
                                                                 outHasGrown,
                                                                 true );
        }

        else
        {
            qDebug() << "Toujours pas trouve avec un cone de recherche de taille (2)" << tailleCone;
            outHasGrown = false;
            return LSIS_Point4DFloat(0,0,0,0);
        }
    }


    v1 *= _image->wres();
//    qDebug() << "V1 normalise\n" << v1;
//    v1.w() = v1.w() * _image->wres();
//    v1.x() = v1.x() * _image->xres();
//    v1.y() = v1.y() * _image->yres();
//    v1.z() = v1.z() * _image->zres();

    // Le gradient de l'energie de croissance est defini comme la projection de ce vecteur sur la tangente a la tete
    // Donc on multiplie cette direction par le produit scalaire avec la tangente
//    LSIS_Point4DFloat growDir = v1 * fabs( scalProd );
    LSIS_Point4DFloat growDir = v1;

    // Renvoie l'oppose de la direction de croissance
    return ( growDir * -1 );
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous< DataImage >::getGradientEnergyGrowProportionalResolutionv3(CT_Grid4D_Sparse<bool>* repulseImage,
                                                                                                             LSIS_Point4DFloat& snakeExtremityPoint,
                                                                                                             LSIS_Pixel4D &snakeExtremityPix,
                                                                                                             const LSIS_Point4DFloat &snakeTangentAtExtremity,
                                                                                                             float coneAngleDegres,
                                                                                                             int coneSizePixels,
                                                                                                             float& outSigmaL1,
                                                                                                             bool& outHasGrown ) const
{
    float tangentNorm3D = snakeTangentAtExtremity.norm3D();
    LSIS_Point4DFloat tangentNormalized3D( 0,
                                           snakeTangentAtExtremity.x() / tangentNorm3D,
                                           snakeTangentAtExtremity.y() / tangentNorm3D,
                                           snakeTangentAtExtremity.z() / tangentNorm3D );

    // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
    LSIS_Pixel4D coneBBoxBot = snakeExtremityPix - LSIS_Pixel4D( 0, coneSizePixels, coneSizePixels, coneSizePixels);
    LSIS_Pixel4D coneBBoxTop = snakeExtremityPix + LSIS_Pixel4D( 0, coneSizePixels, coneSizePixels, coneSizePixels);

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    coneBBoxBot.w() = 0;
    coneBBoxTop.w() = _image->wdim() -1;
    for( int axe = 1 ; axe < 4 ; axe++ )
    {
        if( coneBBoxBot(axe) < 0 )
        {
            coneBBoxBot(axe) = 0;
        }

        if( coneBBoxBot(axe) >= _image->dim()(axe) )
        {
            coneBBoxBot(axe) = _image->dim()(axe) - 1;
        }

        if( coneBBoxTop(axe) < 0 )
        {
            coneBBoxTop(axe) = 0;
        }

        if( coneBBoxTop(axe) >= _image->dim()(axe) )
        {
            coneBBoxTop(axe) = _image->dim()(axe) - 1;
        }
    }

    // Il ne reste plus qu'a tester les angles des directions a l'interieur de la bbox
    // On parcour donc toute la bbox
    int nMaxMax = 50;
    QList< LSIS_Point4DFloat > maxiDirections;
    QList< int > maxiScores;

    LSIS_Pixel4D curInBBoxPixel;
    LSIS_Point4DFloat curInBBoxPoint;
    for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
    {
        for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
        {
            for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
            {
                for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                {
                    if( curInBBoxPixel.valueIn( _image ) > 1 && curInBBoxPixel.valueIn( repulseImage ) == false )
                    {
                        curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                            curInBBoxPixel.toCartesianX( _image ),
                                            curInBBoxPixel.toCartesianY( _image ),
                                            curInBBoxPixel.toCartesianZ( _image ) );
                        float score = curInBBoxPixel.valueIn( _image );

                        // Calcul de la direction potentielle de croissance
                        LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - snakeExtremityPoint;

                        // Test de l'angle en 3D
                        float norm3D = potentialGrowDirPoint.norm3D();
                        LSIS_Point4DFloat potentialGrowDirPoint3DNormalised( 0,
                                                                             potentialGrowDirPoint.x() / norm3D,
                                                                             potentialGrowDirPoint.y() / norm3D,
                                                                             potentialGrowDirPoint.z() / norm3D );

                        float scalarProduct = potentialGrowDirPoint3DNormalised.scalarProduct3D( tangentNormalized3D );

                        // Il faut que les deux vecteurs soient de meme sens
                        if( scalarProduct >= 0 )
                        {
                            float potentialGrowDirAngle = (180.0/M_PI) * acos( scalarProduct ); // Pas besoin de diviser par la norme car potentielGrowDirPoint et tangentNormalized ont une norme de 1
                            if( potentialGrowDirAngle <= coneAngleDegres )
                            {
                                // Si on a des listes vides tout est simple
                                if( maxiScores.empty() )
                                {
                                    maxiScores.append( score );
                                    maxiDirections.append( potentialGrowDirPoint );
                                }
                                else
                                {
                                    // Sinon, on regarde ou inserer l'element dans la liste en fonction de son score
                                    // La liste est triee decroissant ( maxiscore(0) > maxiscore(1) > ... )
                                    int idElement = 0;
                                    while( maxiScores.at(idElement) > score )
                                    {
                                        idElement++;
                                    }

                                    // Si on a pas atteint la fin de la liste en cherchant sa place alors on peut l'inserer a cet endroit
                                    if( idElement < nMaxMax )
                                    {
                                        maxiScores.insert(idElement, score);
                                        maxiDirections.insert( idElement, potentialGrowDirPoint );

                                        // Si apres insertion la liste est trop grande alors on supprime le dernier element
                                        if( maxiScores.size() > nMaxMax )
                                        {
                                            maxiScores.removeLast();
                                            maxiDirections.removeLast();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // Si on a rien trouve dans notre cone
    if( maxiScores.empty() )
    {
        // Alors on renvoie rien et on arete la croissance
        outHasGrown = false;
        return LSIS_Point4DFloat(0,0,0,0);
    }

    // On prend la moyenne ponderee des directions de score maximas trouvees dans le cone
    LSIS_Point4DFloat growDir; growDir.set(0,0,0,0);
    for( int idElem = 0 ; idElem < maxiScores.size() ; idElem++ )
    {
        if( maxiDirections.at(idElem).norm3D() != 0 )
        {
            LSIS_Point4DFloat normalisedMaxiDir = maxiDirections.at(idElem).normalized();
            int maxiScore = maxiScores.at( idElem );
            growDir = growDir + (normalisedMaxiDir * maxiScore );
//            growDir = growDir + (normalisedMaxiDir * ( ( coneSizePixels * _image->xres() ) - maxiDirections.at(idElem).norm3D() ) );
        }
    }

    // On normalise le vecteur de sortie
    growDir.normalize();

    // Et on le rescale pour aller pas trop vite dans l'espace de Hough
    growDir *= _image->wres();

    // Renvoie l'oppose de la direction de croissance
    return (growDir * -1 );
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous< DataImage >::getGradientEnergyGrowProportionalResolutionv4(CT_Grid4D_Sparse<bool>* repulseImage,
                                                                                                             LSIS_Point4DFloat& snakeExtremityPoint,
                                                                                                             LSIS_Pixel4D &snakeExtremityPix,
                                                                                                             const LSIS_Point4DFloat &snakeTangentAtExtremity,
                                                                                                             float coneAngleDegres,
                                                                                                             int coneSizePixels,
                                                                                                             float& outSigmaL1,
                                                                                                             bool& outHasGrown ) const
{
//        qDebug() << "Enter with extremity" << snakeExtremityPoint;
        float tangentNorm3D = snakeTangentAtExtremity.norm3D();
        LSIS_Point4DFloat tangentNormalized3D( 0,
                                               snakeTangentAtExtremity.x() / tangentNorm3D,
                                               snakeTangentAtExtremity.y() / tangentNorm3D,
                                               snakeTangentAtExtremity.z() / tangentNorm3D );

        // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille coneSizePix
        LSIS_Pixel4D coneBBoxBot = snakeExtremityPix - LSIS_Pixel4D( 5, coneSizePixels, coneSizePixels, coneSizePixels);
        LSIS_Pixel4D coneBBoxTop = snakeExtremityPix + LSIS_Pixel4D( 5, coneSizePixels, coneSizePixels, coneSizePixels);

        // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
//        coneBBoxBot.w() = 0;
//        coneBBoxTop.w() = _image->wdim() -1;
        for( int axe = 0 ; axe < 4 ; axe++ )
        {
            if( coneBBoxBot(axe) < 0 )
            {
                coneBBoxBot(axe) = 0;
            }

            if( coneBBoxBot(axe) >= _image->dim()(axe) )
            {
                coneBBoxBot(axe) = _image->dim()(axe) - 1;
            }

            if( coneBBoxTop(axe) < 0 )
            {
                coneBBoxTop(axe) = 0;
            }

            if( coneBBoxTop(axe) >= _image->dim()(axe) )
            {
                coneBBoxTop(axe) = _image->dim()(axe) - 1;
            }
        }

//        qDebug() << "Bbox cone";
//        qDebug() << coneBBoxBot;
//        qDebug() << coneBBoxTop;

        // Il ne reste plus qu'a tester les angles des directions a l'interieur de la bbox
        // On parcour donc toute la bbox
        int nMaxMax = 100;
        QList< LSIS_Point4DFloat > maxiDirections;
        QList< int > maxiScores;
        QList< float > maxiDistances;

        LSIS_Pixel4D curInBBoxPixel;
        LSIS_Point4DFloat curInBBoxPoint;
        for ( curInBBoxPixel.w() = coneBBoxBot.w() ; curInBBoxPixel.w() <= coneBBoxTop.w() ; curInBBoxPixel.w()++ )
        {
            for ( curInBBoxPixel.x() = coneBBoxBot.x(); curInBBoxPixel.x() <= coneBBoxTop.x() ; curInBBoxPixel.x()++ )
            {
                for ( curInBBoxPixel.y() = coneBBoxBot.y() ; curInBBoxPixel.y() <= coneBBoxTop.y() ; curInBBoxPixel.y()++ )
                {
                    for ( curInBBoxPixel.z() = coneBBoxBot.z() ; curInBBoxPixel.z() <= coneBBoxTop.z(); curInBBoxPixel.z()++ )
                    {
                        if( curInBBoxPixel.valueIn( _image ) > 3 /*&& curInBBoxPixel.valueIn( repulseImage ) == false*/ )
                        {
                            curInBBoxPoint.set( curInBBoxPixel.toCartesianW( _image ),
                                                curInBBoxPixel.toCartesianX( _image ),
                                                curInBBoxPixel.toCartesianY( _image ),
                                                curInBBoxPixel.toCartesianZ( _image ) );
                            float score = curInBBoxPixel.valueIn( _image );

                            // Calcul de la direction potentielle de croissance
                            LSIS_Point4DFloat potentialGrowDirPoint = curInBBoxPoint - snakeExtremityPoint;

                            // Test de l'angle en 3D
                            float norm3D = potentialGrowDirPoint.norm3D();
                            LSIS_Point4DFloat potentialGrowDirPoint3DNormalised( 0,
                                                                                 potentialGrowDirPoint.x() / norm3D,
                                                                                 potentialGrowDirPoint.y() / norm3D,
                                                                                 potentialGrowDirPoint.z() / norm3D );

                            float scalarProduct = potentialGrowDirPoint3DNormalised.scalarProduct3D( tangentNormalized3D );

                            // Il faut que les deux vecteurs soient de meme sens
                            if( scalarProduct >= 0 )
                            {
                                float potentialGrowDirAngle = (180.0/M_PI) * acos( scalarProduct ); // Pas besoin de diviser par la norme car potentielGrowDirPoint et tangentNormalized ont une norme de 1
                                if( potentialGrowDirAngle <= coneAngleDegres )
                                {
                                    // Si on a des listes vides tout est simple
                                    if( maxiScores.empty() )
                                    {
                                        maxiScores.append( score );
                                        maxiDirections.append( potentialGrowDirPoint);
                                        maxiDistances.append( norm3D );
                                    }
                                    else
                                    {
                                        // Sinon, on regarde ou inserer l'element dans la liste en fonction de son score
                                        // La liste est triee decroissant ( maxiscore(0) > maxiscore(1) > ... )
                                        int idElement = 0;
                                        while( idElement < maxiScores.size() && maxiScores.at(idElement) > score )
                                        {
                                            idElement++;
                                        }

                                        // Si on a pas atteint la fin de la liste en cherchant sa place alors on peut l'inserer a cet endroit
                                        if( idElement < nMaxMax )
                                        {
                                            maxiScores.insert(idElement, score);
                                            maxiDirections.insert( idElement, potentialGrowDirPoint );
                                            maxiDistances.insert( idElement, norm3D );

                                            // Si apres insertion la liste est trop grande alors on supprime le dernier element
                                            if( maxiScores.size() > nMaxMax )
                                            {
                                                maxiScores.removeLast();
                                                maxiDirections.removeLast();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // On teste si le cone contenait assez de directions potentielles de croissance pour faire une ACP 4D dessus
        if( maxiDirections.size() < 5 )
        {
            // Si ce n'est pas le cas, alors il ne faut pas croitre : message de debug et on renvoie une croissance nulle
//            qDebug() << "Pas assez de cellules pour continuer (moins de 5 pixels dans le cone)";
            outHasGrown = false;
            return LSIS_Point4DFloat(0,0,0,0);
        }

        QVector< LSIS_Point4DFloat > dirInCone;
        int nDirections = maxiDirections.size();
//        qDebug() << "--------------";
//        qDebug() << snakeExtremityPoint;
//        qDebug() << "Nb pixels pris en compte" << maxiDirections.size();
//        qDebug() << "tangente" << tangentNormalized3D;
//        qDebug() << "--------------";
        for( int i = 0 ; i < nDirections ; i++ )
        {
//            dirInCone.push_back( maxiDirections.at(i) * maxiScores.at(i) * ((coneSizePixels * _image->xres() *1.1 ) - maxiDistances.at(i) ) );
            dirInCone.push_back( maxiDirections.at(i) * maxiScores.at(i) );
//            qDebug() << dirInCone.last();
        }

        // On teste si le cone contenait assez de directions potentielles de croissance pour faire une ACP 4D dessus
        if( dirInCone.size() < 4 )
        {
            // Si ce n'est pas le cas, alors il ne faut pas croitre : message de debug et on renvoie une croissance nulle
//            qDebug() << "Pas assez de cellules pour acp";
            outHasGrown = false;
            return LSIS_Point4DFloat(0,0,0,0);
        }

        // Effectue l'ACP 4D non centree des directions de croissance potentielles
        LSIS_Point4DFloat v1, v2, v3, v4;
        float l1, l2, l3, l4;
        LSIS_PcaTools::acp4DNonCentree( dirInCone,
                                        v1, v2, v3, v4,
                                        l1, l2, l3, l4 );

        // Calcul du sigmaL1 pour cette ACP
        outSigmaL1 = l1 / (l1+l2+l3+l4);

        // On normalise la direction v1
        if( v1.norm() != 0 )
        {
            v1.normalize();
        }

        float v1Norm3D = v1.norm3D();
        LSIS_Point4DFloat v1Normalized3D( 0,
                                          v1.x() / v1Norm3D,
                                          v1.y() / v1Norm3D,
                                          v1.z() / v1Norm3D );

        // Produit scalaire entre la direction de croissance et la tangente au snake pour calculer la projection
        float scalProd = v1Normalized3D.scalarProduct3D( tangentNormalized3D );

        // Le vecteur v1 en sortie d'ACP est pas forcément orienté vers l'extremite du snake
        // On le réoriente en lui donnant la même orientation que la tangente au snake (verification du signe du produit scalaire)
        if( scalProd < 0 )
        {
            v1 *= -1;
        }

        // On rescale la direction de croissance conformement a la resolution de l'espace de Hough
        v1.normalize();
        v1 *= _image->wres();

        // Le gradient de l'energie de croissance est defini comme la projection de ce vecteur sur la tangente a la tete
        // Donc on multiplie cette direction par le produit scalaire avec la tangente
//        qDebug() << "v1" << v1;
//        qDebug() << "Scalar " << scalProd;
        LSIS_Point4DFloat growDir = v1 * fabs( scalProd );
//        qDebug() << "Growdir " << growDir;
//        LSIS_Point4DFloat growDir = v1;

        // Renvoie l'oppose de la direction de croissance
        return ( growDir * -1 );
}

template< typename DataImage >
Eigen::MatrixXf *LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyOrthogonalImageMatrix( float globalWeight ) const
{
    int npts = npoints();
    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, 4 );

    // Calcul du gradient de l'energie d'image en chaque point du contours
    // Classiquement : Pour plus de precision ce gradient se calcule par interpolation lineaire
    //                 c.f. methode gradientEnergyImageAtPoint( int i, float globalWeight )
    // Maintenant : on ne considere que la partie de ce gradient orthogonale au snake
    LSIS_Point4DFloat currGrad;

    for( int i = 0 ; i < npts ; i++ )
    {
//        // On recupere le gradient classique
//        currGrad = gradientEnergyImageInterpolatedAtPoint( i, globalWeight );

//        // Il ne faut tenir compte que de la composante orthogonale a la direction du snake
//        // Pour ca il faut avoir la direction du snake
//        LSIS_Point4DFloat directionContour = directionContoursAtPoint( i );
//        directionContour.normalize();

        // NOUVELLE VERSION AVEC PARALLELISATION
        QFuture<LSIS_Point4DFloat> futureCurrGrad = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::gradientEnergyImageInterpolatedAtPoint, i, globalWeight );
        QFuture<LSIS_Point4DFloat> futureDirectionContours = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::directionContoursAtPoint, i );

        futureCurrGrad.waitForFinished();
        futureDirectionContours.waitForFinished();

        currGrad = futureCurrGrad.result();
        LSIS_Point4DFloat directionContour = futureDirectionContours.result();
        directionContour.normalize();
        // NOUVELLE VERSION AVEC PARALLELISATION FIN

        // On peut calculer la composante orthogonale de currGrad a directionContours
        currGrad = currGrad - ( directionContour * currGrad.scalarProduct4D( directionContour ) );

        for( int j = 0 ; j < 4 ; j++ )
        {
            (*rslt)( i, j ) = currGrad(j);
        }
    }

    return rslt;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyOrthogonalImageAtIndex(float globalWeight, int index)
{
//    // NOUVELLE VERSION AVEC PARALLELISATION
//    QFuture<LSIS_Point4DFloat> futureCurrGrad = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::gradientEnergyImageInterpolatedAtPoint, i, globalWeight );
//    QFuture<LSIS_Point4DFloat> futureDirectionContours = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::directionContoursAtPoint, i );

//    futureCurrGrad.waitForFinished();
//    futureDirectionContours.waitForFinished();

//    currGrad = futureCurrGrad.result();
//    directionContour = futureDirectionContours.result();
//    directionContour.normalize();
//    // NOUVELLE VERSION AVEC PARALLELISATION FIN

//    // On peut calculer la composante orthogonale de currGrad a directionContours
//    currGrad = currGrad - ( directionContour * currGrad.scalarProduct4D( directionContour ) );

//    for( int j = 0 ; j < 4 ; j++ )
//    {
//        (*rslt)( i, j ) = currGrad(j);
//    }
}

template< typename DataImage >
Eigen::MatrixXf *LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyOrthogonalImageMatrixMultipliedByTangentNorm( float globalWeight ) const
{
    Eigen::MatrixXf* rslt = getGradientEnergyOrthogonalImageMatrix( globalWeight );

    float curTangentNorm;
    int npts = npoints();
    for( int i = 0 ; i < npts; i++ )
    {
        curTangentNorm = tangentNormAtPoint(i);
        (*rslt)(i,0) *= curTangentNorm;
        (*rslt)(i,1) *= curTangentNorm;
        (*rslt)(i,2) *= curTangentNorm;
        (*rslt)(i,3) *= curTangentNorm;
    }

    return rslt;
}

template< typename DataImage >
Eigen::MatrixXf *LSIS_ActiveContour4DContinuous<DataImage>::getSecondDifferentialOnTangentDividedByTangentNormMultiplyedByImageEnergy( float globalWeight )
{
    int npts = npoints();

    Eigen::MatrixXf* rslt = new Eigen::MatrixXf( npts, 4 );

    LSIS_Point4DFloat curTangent;
    LSIS_Point4DFloat curSecondDiff;
    float tangentNorm;
    float scalProd;
    float scalProdNormalized;
    for( int i = 0 ; i < npts; i++ )
    {
        curTangent = tangentAtPoint( i );
        curSecondDiff = secondDifferentialAtPoint( i );
        tangentNorm = curTangent.norm();
        scalProd = curTangent.scalarProduct4D( curSecondDiff );
        scalProdNormalized = scalProd / ( tangentNorm * tangentNorm );

        for( int c = 0 ; c < 4 ; c++ )
        {
            (*rslt)( i, c ) = ( ( curSecondDiff( c ) - ( scalProdNormalized * curTangent(c) ) ) / tangentNorm ) * energyImageAtPixel( pixelAt(i), globalWeight );
//            (*rslt)( i, c ) = ( ( curSecondDiff( c ) - ( scalProdNormalized * curTangent(c) ) ) / tangentNorm ) * pixelAt(i).valueIn( _imageEnergy );
        }
    }

    return rslt;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::gradientEnergyImageInterpolatedAtPoint(int indexPoint, float globalWeight) const
{
    assertWithMessage( globalWeight >= 0 && globalWeight <= 1, "Le coefficient de ponderation des termes d'energies globales et locales doit etre inclu dans [0,1]. Or, il est actuellement de " << globalWeight );

    LSIS_Point4DFloat   currPoint = pointAt(indexPoint);    // ieme point du contour
    LSIS_Pixel4D        currPixel;                          // Pixel contenant le ieme point
    LSIS_Point4DFloat   currPixelCenter;                    // Centre en cartesien du pixel contenant le ieme point

    // Conversion du point 4D en pixel
    currPixel.setFromCartesian( currPoint, _image );
    assertWithMessage( currPixel.isIn( _image ), "On essaie de calculer le gradient de l'image sur un pixel en dehors de l'image" );
    currPixelCenter = currPixel.getCartesian(_image );

    // Pour plus de precision et de stabilite, le gradient de l'energie image est interpole lineairement entre deux valeurs :
    // - si le point courant est dans la moitie gauche du pixel on interpole le gradient entre
    //   Grad( prevPixel ) et Grad( currPixel )
    // - si il est dans la partie droite l'interpolation se fait entre
    //   Grad( currPixel ) et Grad( nextPixel )

    // Interpolation des gradients selon chaque coordonnee
    LSIS_Point4DFloat gradInterpolated;
    for( int i = 0 ; i < 4 ; i++ )
    {
//        // On recupere la coordonnee i du gradient d'energie au pixel courant (on est sur que le pixel est dans l'image)
        float curPixelGrad = partialDerivativeEnergyImageAtPixel( currPixel, globalWeight, i );

//        float curPixelGrad;
//        if( i == 0 )
//        {
//            curPixelGrad = currPixel.valueIn( _gradw );
//        }

//        else if( i == 1 )
//        {
//            curPixelGrad = currPixel.valueIn( _gradx );
//        }

//        else if( i == 2 )
//        {
//            curPixelGrad = currPixel.valueIn( _grady );
//        }

//        else if( i == 3 )
//        {
//            curPixelGrad = currPixel.valueIn( _gradz );
//        }

        // On regarde si le point courrant est dans la partie gauche ou droite du pixel courant
        if( currPoint(i) < currPixelCenter(i) )
        {
            // Assertion commentee puisque la methode partialDerivativeEnergyImageAtPixel tient compte de ce cas
//            assertWithMessage( false, "Attention le pixel precedent peut etre hors image" );

            LSIS_Pixel4D prevPixel = currPixel;
            prevPixel(i) -= 1;
            float prevPixelCenter = prevPixel.toCartesian( _image, i );

            // On calcule la coordonnee i du gradient d'image au pixel precedent
            float prevPixelGrad = partialDerivativeEnergyImageAtPixel( prevPixel, globalWeight, i );

//            float prevPixelGrad;
//            if( i == 0 )
//            {
//                prevPixelGrad = prevPixel.valueIn( _gradw );
//            }

//            else if( i == 1 )
//            {
//                prevPixelGrad = prevPixel.valueIn( _gradx );
//            }

//            else if( i == 2 )
//            {
//                prevPixelGrad = prevPixel.valueIn( _grady );
//            }

//            else if( i == 3 )
//            {
//                prevPixelGrad = prevPixel.valueIn( _gradz );
//            }

            // Partie gauche, interpolation avec gradPrevPixel
            gradInterpolated(i) = lsis_MathTools::linearInterpolation( prevPixelCenter, currPixelCenter(i), prevPixelGrad, curPixelGrad, currPoint(i) );
        }

        else
        {
            // Assertion commentee puisque la methode partialDerivativeEnergyImageAtPixel tient compte de ce cas
//            assertWithMessage( false, "Attention le pixel suivant peut etre hors image" );

            LSIS_Pixel4D nextPixel = currPixel;
            nextPixel(i) += 1;
            float nextPixelCenter = nextPixel.toCartesian( _image, i );

            // On calcule la coordonnee i du gradient d'image au pixel precedent
            float nextPixelGrad = partialDerivativeEnergyImageAtPixel( nextPixel, globalWeight, i );

//            float nextPixelGrad;
//            if( i == 0 )
//            {
//                nextPixelGrad = nextPixel.valueIn( _gradw );
//            }

//            else if( i == 1 )
//            {
//                nextPixelGrad = nextPixel.valueIn( _gradx );
//            }

//            else if( i == 2 )
//            {
//                nextPixelGrad = nextPixel.valueIn( _grady );
//            }

//            else if( i == 3 )
//            {
//                nextPixelGrad = nextPixel.valueIn( _gradz );
//            }

            // Partie droite, interpolation avec gradNextPixel
            gradInterpolated(i) = lsis_MathTools::linearInterpolation( currPixelCenter(i), nextPixelCenter, curPixelGrad, nextPixelGrad, currPoint(i) );
        }
    }

    return gradInterpolated;
}

template< typename DataImage >
LSIS_Point4DFloat LSIS_ActiveContour4DContinuous<DataImage>::gradientEnergyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight) const
{
    LSIS_Point4DFloat grad( 100, 100, 100, 100 );

    // Si le pixel n'est pas dans l'image
    // On renvoie un gradient nul
    if( !p.isIn( _image ) )
    {
        qDebug() << "Attention ici il faut le traiter autrement ! puisqu'il est possible que le resultat soir reutilise apres";
        return LSIS_Point4DFloat(0,0,0,0);
    }

    // Calcul des composantes du gradient selon chaque direction
    for( int i =0 ; i < 4 ; i++ )
    {
        // On recupere le pixel precedent dans cette direction
        LSIS_Pixel4D prevInDirection = p;
        prevInDirection(i) -= 1;
        bool isPrevInImage = prevInDirection.isIn( _image );

        // On recupere le pixel suivant dans cette direction
        LSIS_Pixel4D nextInDirection = p;
        nextInDirection(i) += 1;
        bool isNextInImage = nextInDirection.isIn( _image );

        // Si aucun des deux pixels (suivant et precedent) n'est dans l'image, on renvoie une gradient nul
        // i.e. l'image n'a qu'un seul pixel d'epaisseur selon cette dimension
        if( !isPrevInImage && !isNextInImage )
        {
            grad(i) = 0;
        }

        // Si les deux pixels (suivant et precedent) sont dans l'image, on calcule le gradient par difference centree
        else if( isPrevInImage && isNextInImage )
        {
            grad(i) = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) ) /2.0;
        }

        // Si seul le pixel precedent est dans l'image (on a atteint le bord droit de l'image), on calcule le gradient par difference a gauche (curr - prev)
        else if( isPrevInImage && !isNextInImage )
        {
            grad(i) = ( energyImageAtPixel( p, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) );
        }

        // Si seul le pixel suivant est dans l'image (on a atteint le bord gauche de l'image), on calcule le gradient par difference a droite (next - curr)
        else
        {
            grad(i) = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( p, globalWeight ) );
        }
    }

    return grad;
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::partialDerivativeEnergyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight, int coordToDerive) const
{
    float partialDerivate;

    // Si p est dans l'image, calculer sa derivee partielle est toujours possible :
    // p est dans le centre de l'image,on fait une difference finie centree
    // p est un bord de l'image, on peut faire une difference finie a droite/gauche
    if( p.isIn(_image) )
    {
        partialDerivate = partialDerivativeEnergyImageAtPixelInImage( p, globalWeight, coordToDerive );
    }

    // Si p est en dehors de l'image il faut calculer la derivee partielle autrement
    // Pour le coup je prend le mirroir
    // i.e. si p est a gauche de l'image on prend l'oppose de la derivee partielle en image(1)
    //      si p est a droite de l'image on prend l'oppose de la derivee partielle en image( dim-1 )
    else
    {
        partialDerivate = partialDerivativeEnergyImageAtPixelOutsideImage( p, globalWeight, coordToDerive );
    }

    return partialDerivate;
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::partialDerivativeEnergyImageAtPixelInImage(const LSIS_Pixel4D &p, float globalWeight, int coordToDerive) const
{
    float epsilon = 0;
    switch( coordToDerive )
    {
        case 0 :
        {
            epsilon = _image->wres();
            break;
        }
        case 1 :
        {
            epsilon = _image->xres();
            break;
        }
        case 2 :
        {
            epsilon = _image->yres();
            break;
        }
        case 3 :
        {
            epsilon = _image->zres();
            break;
        }
        default :
        {
            assertWithMessage(false, "Demande la derivee partielle d'une coordonnee inexistante");
            break;
        }
    }

    // On es sur que le pixel p soit dans l'image (peut etre est ce un bord quand meme)
    float partialDerivate;

    // Calcul de la derivee partielle selon la variable demandee
    // On recupere le pixel precedent selon cette variable
    LSIS_Pixel4D prevInDirection = p;
    prevInDirection( coordToDerive ) -= 1;
    bool isPrevInImage = prevInDirection.isIn( _image );

    // On recupere le pixel suivant selon cette variable
    LSIS_Pixel4D nextInDirection = p;
    nextInDirection( coordToDerive ) += 1;
    bool isNextInImage = nextInDirection.isIn( _image );

    // Si aucun des deux pixels (suivant et precedent) n'est dans l'image, on renvoie une derivee partielle nulle
    // i.e. l'image n'a qu'un seul pixel d'epaisseur selon cette variable
    if( !isPrevInImage && !isNextInImage )
    {
        partialDerivate = 0;
    }

    // Si les deux pixels (suivant et precedent) sont dans l'image, on calcule la derivee partielle par difference centree
    else if( isPrevInImage && isNextInImage )
    {
        partialDerivate = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) ) / ( 2.0 );
    }

    // Si seul le pixel precedent est dans l'image (on a atteint le bord droit de l'image), on calcule la derivee partielle par difference a gauche (curr - prev)
    else if( isPrevInImage && !isNextInImage )
    {
        partialDerivate = ( energyImageAtPixel( p, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) );
    }

    // Si seul le pixel suivant est dans l'image (on a atteint le bord gauche de l'image), on calcule la derivee partielle par difference a droite (next - curr)
    else
    {
        partialDerivate = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( p, globalWeight ) );
    }

    return partialDerivate;
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::partialDerivativeEnergyImageAtPixelOutsideImage(const LSIS_Pixel4D &p,
                                                                                                 float globalWeight,
                                                                                                 int coordToDerive) const
{
    if( _image->dim()(coordToDerive) == 1 )
    {
        return 0;
    }

    if( _image->dim()(coordToDerive) <= 2 )
    {
        assertWithMessage( _image->dim()(coordToDerive) >= 2, "Comment calculer cette derivee?");
    }

    // On est sur un pixel hors de l'image
    float partialDerivate = 0;

    // Ce cas ne devrait arriver que lorsqu'on essaie de calculer la derivee partielle d'un pixel au bord de l'image
    // avec le point P entre le centre du pixel et le bord
    // i.e. le point du snake est situe entre le centre du dernier pixel de l'image et le bord de l'image
    //
    // Dans ce cas, lorsqu'on va vouloir faire l'interpolation de la derivee partielle de l'energie d'image au pixel du point P
    // on va vouloir interpoler la valeur entre le pixel du point P et le pixel hors de l'image (celui qui est traite par cette methode)
    // C'est ce cas de figure que l'on traite ici

    // On traite ce cas en renvoyant un mirroir des derivees partielles de l'energie image
    // i.e. si le pixel est a gauche de l'image on renvoi la derivee partielle en 1
    //      si le pixel est a gauche de l'image on renvoi la derivee partielle en dim-2
    if( p(coordToDerive) < 0 )
    {
        // On est a gauche de l'image
        // On renvoit l'oppose de la derivee partielle en 1
        LSIS_Pixel4D pixel1 = p;
        pixel1(coordToDerive) = 1;  // Ce pixel est obligatoirement sur l'image car le seul appel possible a cette methode est
                                    // si on demande la derivee partielle interpolee a un pixel dans l'image

        partialDerivate = - partialDerivativeEnergyImageAtPixelInImage( pixel1, globalWeight, coordToDerive );
    }

    else
    {
        // On est a droite de l'image
        // On renvoit l'oppose de la derivee partielle en dim-2
        LSIS_Pixel4D pixelMinus2 = p;
        pixelMinus2(coordToDerive) = _image->dim()(coordToDerive) - 2;  // Ce pixel est obligatoirement sur l'image car le seul appel possible a cette methode est
                                                                        // si on demande la derivee partielle interpolee a un pixel dans l'image

        partialDerivate = - partialDerivativeEnergyImageAtPixelInImage( pixelMinus2, globalWeight, coordToDerive );
    }

    return partialDerivate;
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::energyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight) const
{
    assertWithMessage( globalWeight >= 0 && globalWeight <= 1, "Le coefficient de ponderation des termes d'energies globales et locales doit etre inclu dans [0,1]. Or, il est actuellement de " << globalWeight );

    return ( globalWeight * energyImageGlobalAtPixel(p) ) + ( ( 1 - globalWeight ) * energyImageLocalAtPixel(p) );
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::energyImageLocalAtPixel(const LSIS_Pixel4D &p) const
{
    DataImage min, max;

    // Recupere les min et max du voisinage
    p.getMinMaxInNeighbourhood( _image, min, max, 1 );

    if( min == max )
    {
        return -1;
    }

    // Renvoie l'energie image locale
    return (float)( min - p.valueIn(_image) ) / (float)( max - min );
}

template< typename DataImage >
float LSIS_ActiveContour4DContinuous<DataImage>::energyImageGlobalAtPixel(const LSIS_Pixel4D &p) const
{
    assert( _image->dataMax() != _image->dataMin() );
    return (float)( _image->dataMin() - p.valueIn( _image ) ) / (float)( _image->dataMax() - _image->dataMin() );
}

template< typename DataImage >
int LSIS_ActiveContour4DContinuous<DataImage>::updatePoints( Eigen::MatrixXf const * newPoints)
{
    return updatePoints( *newPoints );
}

template< typename DataImage >
int LSIS_ActiveContour4DContinuous<DataImage>::updatePoints( Eigen::MatrixXf const & newPoints)
{
    int npts = npoints();
    int rslt = 0;

    // On regarde le statut de chaque point
    LSIS_Point4DFloat   curPoint;
    LSIS_Pixel4D        curPix;
    for( int i = 0 ; i < npts ; i++ )
    {
        curPoint = newPoints.row(i);
        curPix.setFromCartesian( curPoint, _image );

        if( curPix.isIn( _image ) )
        {
            // La nouvelle position du point est dans l'image, on fait une simple copie
            _points.row(i) = newPoints.row(i);
        }

        else
        {
            rslt++;

            for( int coord = 0 ; coord < 4 ; coord++ )
            {
                if( curPix.isIn( _image, coord ) )
                {
                    _points(i, coord) = curPoint(coord);
                }

                else if( curPix(coord) < 0 )
                {
                    _points(i, coord) = _image->boundingBoxBot()(coord) + 0.00001;
                }

                else
                {
                    _points(i, coord) = _image->boundingBoxTop()(coord) - 0.00001;
                }
            }
        }
    }

    return rslt;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::updatePointsFromNewSize( Eigen::MatrixXf const & newPoints)
{
    _points.resize( newPoints.rows(), newPoints.cols() );

    int npts = npoints();

    // On regarde le statut de chaque point
    LSIS_Point4DFloat   curPoint;
    LSIS_Pixel4D        curPix;
    for( int i = 0 ; i < npts ; i++ )
    {
        curPoint = newPoints.row(i);
        curPix.setFromCartesian( curPoint, _image );

        if( curPix.isIn( _image ) )
        {
            // La nouvelle position du point est dans l'image, on fait une simple copie
            _points.row(i) = newPoints.row(i);
        }

        else
        {
            for( int coord = 0 ; coord < 4 ; coord++ )
            {
                if( curPix.isIn( _image, coord ) )
                {
                    _points(i, coord) = curPoint(coord);
                }

                else if( curPix(coord) < 0 )
                {
                    _points(i, coord) = _image->boundingBoxBot()(coord) + 0.00001;
                }

                else
                {
                    _points(i, coord) = _image->boundingBoxTop()(coord) - 0.00001;
                }
            }
        }
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::updatePointsAndGetAverageAndStDevMovement(const Eigen::MatrixXf &newPoints,
                                                                                          float& outAverageMovemet,
                                                                                          float& outStdDevMovement )
{
    int npts = npoints();
    float movement;
    float movementSum = 0;
    float movementSquareSum = 0;

    // On regarde le statut de chaque point
    LSIS_Point4DFloat   curPoint;
    LSIS_Pixel4D        curPix;
    for( int i = 0 ; i < npts ; i++ )
    {
        curPoint = newPoints.row(i);
        curPix.setFromCartesian( curPoint, _image );

        if( curPix.isIn( _image ) )
        {
            movement = LSIS_Point4DFloat::distance4D( _points.row(i), curPoint );
            movementSum += movement;
            movementSquareSum += movement*movement;

            // La nouvelle position du point est dans l'image, on fait une simple copie
            _points.row(i) = newPoints.row(i);
        }

        else
        {
            movement = LSIS_Point4DFloat::distance4D( _points.row(i), curPoint );
            movementSum += movement;
            movementSquareSum += movement*movement;

            for( int coord = 0 ; coord < 4 ; coord++ )
            {
                // Recopie de la valeur sur l'axe
                if( curPix.isIn( _image, coord ) )
                {
                    _points(i, coord) = curPoint(coord);
                }

                // Remise dans l'image
                else if( curPix(coord) < 0 )
                {
                    _points(i, coord) = _image->boundingBoxBot()(coord) + 0.00001;
                }

                // Remise dans l'image
                else
                {
                    _points(i, coord) = _image->boundingBoxTop()(coord) - 0.00001;
                }
            }

        }
    }

    outAverageMovemet = movementSum / npts;
    outStdDevMovement = ( movementSquareSum / npts ) - (outAverageMovemet*outAverageMovemet);
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::updatePoints()
{
    // On regarde le statut de chaque point
    int npts = npoints();
    LSIS_Point4DFloat   curPoint;
    LSIS_Pixel4D        curPix;
    for( int i = 0 ; i < npts ; i++ )
    {
        curPoint = _points.row(i);
        curPix.setFromCartesian( curPoint, _image );

        if( !curPix.isIn( _image ) )
        {
            for( int coord = 0 ; coord < 4 ; coord++ )
            {
                if( curPix(coord) < 0 )
                {
                    _points(i, coord) = _image->boundingBoxBot()(coord) + 0.00001;
                }

                else if( curPix(coord) > _image->dim()(coord) )
                {
                    _points(i, coord) = _image->boundingBoxTop()(coord) - 0.00001;
                }
            }
        }
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::grow(CT_Grid4D<bool>* repulseImage,
                                                     int nIterMax,
                                                     float alpha, float beta,
                                                     float gama,  float globalWeight,
                                                     float k, float angleMaxDegres, float size,
                                                     float timeStep,
                                                     float seuilVariance,
                                                     float seuilSigma,
                                                     float seuilSigma2,
                                                     float gradMovementMin,
                                                     float gradLengthMin )
{
    if( length3D() == 0 )
    {
        return;
    }

    // TEMPORAIRE : remettre tous les pixels dans l'image
    updatePoints( _points );
    // TEMPORAIRE FIN

//    if( _step != NULL )
//    {
//        _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
//    }

    int npts = npoints();
//    float currLength = length3D();
//    float nextLength;

    // On recupere le premier terme du systeme matriciel
//    Eigen::MatrixXf* aPlusRhoIInverse = getAPlusRhoIInverse( alpha, beta, timeStep );

    // On cree une matrice pour stocker la coordonnee des points a l'iteration suivante
    Eigen::MatrixXf* pointsNextIteration = new Eigen::MatrixXf( npts, 4 );

    QList<float> snrHead;
    QList<float> snrBack;
    QList<float> sigmasHead;
    QList<float> sigmasBack;
    bool hasGrownHead = true;
    bool hasGrownBack = true;
    QList<bool> growHead;
    QList<bool> growBack;

    int nIterManquePixelsInConeHead = 0;
    int nIterManquePixelsInConeBack = 0;
    for( int i = 0 ; i < nIterMax && ( hasGrownBack || hasGrownHead ) ; i++ )
    {
//        if( i%20 == 0 ) qDebug() << "Grow iteration " << i;

        if( _step != NULL & _step->isDebugModeOn() )
        {
            _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
        }

//        qDebug() << i << hasGrownHead << hasGrownBack;
//        Eigen::MatrixXf* savePoints = new Eigen::MatrixXf( _points );

//        // Calcule les gradients d'energie image et d'energie de croissance
//        Eigen::MatrixXf* gradientEnergyImageOrthoMulTangentNorm = getGradientEnergyOrthogonalImageMatrixMultipliedByTangentNorm(globalWeight);
//        Eigen::MatrixXf* secondDiffOnTangentDivByTanNorm = getSecondDifferentialOnTangentDividedByTangentNormMultiplyedByImageEnergy( globalWeight );
        Eigen::MatrixXf* gradientEnergyGrow = getGradientEnergyGrowMatrix( angleMaxDegres,
                                                                           size,
                                                                           seuilVariance,
                                                                           i,
                                                                           seuilSigma,
                                                                           seuilSigma2,
                                                                           repulseImage,
                                                                           snrHead,
                                                                           snrBack,
                                                                           growHead,
                                                                           growBack,
                                                                           sigmasHead,
                                                                           sigmasBack,
                                                                           nIterManquePixelsInConeHead,
                                                                           nIterManquePixelsInConeBack,
                                                                           10,
                                                                           hasGrownHead,
                                                                           hasGrownBack,
                                                                           globalWeight );

        if( nIterManquePixelsInConeBack >= 10 )
        {
            hasGrownBack = false;
        }

        if( nIterManquePixelsInConeHead >= 10 )
        {
            hasGrownHead = false;
        }

        // Calcul de la position a la prochaine iteration
        (*pointsNextIteration) = (_points) - ( k * (*gradientEnergyGrow) );

        if( !hasGrownBack && !hasGrownHead )
        {
            qDebug() << "Stop a cause du manque de pixels";
        }

        else
        {
            qDebug() << "Stop a cause du manque d'iterations";
        }

//        if( _step != NULL )
//        {
//            _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
//        }

//        (*pointsNextIteration) = (*aPlusRhoIInverse) * ( ( rho * (*pointsNextIteration) ) - ( gama * (*gradientEnergyImageOrthoMulTangentNorm) ) /*+ ( (*secondDiffOnTangentDivByTanNorm) )*/ );

        // Si on tient pas compte de la matrice geometrie :
//        (*pointsNextIteration) = (*pointsNextIteration) - ( ( ( gama * (*gradientEnergyImageOrthoMulTangentNorm) ) + ( (*secondDiffOnTangentDivByTanNorm) ) ) / rho );

        // SI l'on tient compte que de la matrice de geometrie
//        (*pointsNextIteration) = (*aPlusRhoIInverse) * ( rho * (*pointsNextIteration) );

        // On met a jour la position des points :
        // Pour lespoints dans l'image on fait une simple copie
        // Pour les points hors de l'image on les remet dans le dernier pixel traverse par la direction de mouvement du point
        updatePoints( pointsNextIteration );

        if( pixelAt(0).valueIn( repulseImage ) == true )
        {
            // On arrete la croissance
            hasGrownHead = false;
        }

        if( pixelAt( npoints()-1 ).valueIn( repulseImage ) == true )
        {
            // On arrete la croissance
            hasGrownBack = false;
        }

//        // Mise a jour de la longueur
//        nextLength = length3D();
//        qDebug() << nextLength - currLength;
//        if( gradLength.size() < 20 )
//        {
//            gradLength.append( fabs(nextLength - currLength) );
//        }
//        else
//        {
//            gradLength.erase( gradLength.begin() );
//            gradLength.append( fabs(nextLength - currLength) );
//            float gradLengthMean = lsis_MathTools::getMean( gradLength );
//            if( gradLengthMean < 0.005 )
//            {
//                // Libere la memoire
//                delete aPlusRhoIInverse;
//                delete pointsNextIteration;
//                qDebug() << "Stop length";
//                return;
//            }
//        }
//        currLength = nextLength;
//        if( _step != NULL )
//        {
//            _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
//        }

        // Libere la memoire
//        delete gradientEnergyImageOrthoMulTangentNorm;
        delete gradientEnergyGrow;
//        delete secondDiffOnTangentDivByTanNorm;

        // Toutes les 10 iterations on resample
        if( i%10 == 0 && hasToBeResampled() )
        {
            resample( _image->xres() );

//            // Il faut donc mettre a jour la matrice de geometrie (les matrices des gradients sont mises a jour en debut de boucle)
//            delete aPlusRhoIInverse;
//            aPlusRhoIInverse = getAPlusRhoIInverse( alpha, beta, timeStep );

            // Il faut aussi mettre a jour la matrice des points a l'iteration suivante (adapter sa taille)
            pointsNextIteration->resize( npoints(), npoints() );
        }

//        delete savePoints;
    }

    // Libere la memoire
//    delete aPlusRhoIInverse;
    delete pointsNextIteration;

    updatePoints( _points );
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous< DataImage >::growv2(CT_Grid4D_Sparse<bool>* repulseImage,
                                                         int nIterMax,
                                                         float growCoeff,
                                                         float coneAngleMaxDegres,
                                                         float coneSizeMaxPixels,
                                                         float seuilSigmaL1)
{
    const double minZAxisLevel = 0.35;
//    qDebug() << "New snake";
    // Remet les points dans l'image
    updatePoints();

    if( length3D() == 0 )
    {
        return;
    }

    bool firstheadmsg = true;
    bool firstbackmsg = true;

    // On cree une matrice pour stocker la coordonnee des points a l'iteration suivante
    float sigmaL1Head = std::numeric_limits<float>::max();
    float sigmaL1Back = std::numeric_limits<float>::max();
    bool hasGrownHead = true;
    bool hasGrownBack = true;
    LSIS_Point4DFloat growDirHead;
    LSIS_Point4DFloat growDirBack;
    int i;
    for( i = 0 ;
         i < nIterMax &&
         ( sigmaL1Head > seuilSigmaL1 || sigmaL1Back > seuilSigmaL1 ) &&
         ( hasGrownBack || hasGrownHead ) ;
         i++ )
    {
        if( _step != NULL & _step->isDebugModeOn() )
        {
            _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
        }

        //Giving a chance for the fork to get proper bearings on the first circle to prevent incremental recalculation
        if(isFork()&&i == 0){
            // Calcule la force de croissance
            getGrowingDirections( repulseImage,
                                  90,
                                  coneSizeMaxPixels,
                                  seuilSigmaL1,
                                  sigmaL1Head,
                                  sigmaL1Back,
                                  hasGrownHead,
                                  hasGrownBack,
                                  growDirHead,
                                  growDirBack );
        }else{
            // Calcule la force de croissance
            getGrowingDirections( repulseImage,
                                  coneAngleMaxDegres,
                                  coneSizeMaxPixels,
                                  seuilSigmaL1,
                                  sigmaL1Head,
                                  sigmaL1Back,
                                  hasGrownHead,
                                  hasGrownBack,
                                  growDirHead,
                                  growDirBack );
        }

        if( sigmaL1Head < seuilSigmaL1 )
        {
            if( firstheadmsg )
            {
                //qDebug() << "Tete stop par sigma" << sigmaL1Head << " seuil : " << seuilSigmaL1 << " a iteration " << i;
                firstheadmsg = false;
            }
            hasGrownHead = false;
        }

        LSIS_Pixel4D pixHead;
        pixHead.setFromCartesian( head(), _image );
        LSIS_Pixel4D pixHeadRepulseImage = pixHead;
        pixHeadRepulseImage.w() = 0;
        if( pixHeadRepulseImage.valueIn( _repulseImage ) == true )
        {
            if( firstheadmsg )
            {
                //qDebug() << "Tete stop par intersection" << sigmaL1Head << " seuil : " << seuilSigmaL1 << " a iteration " << i;
                firstheadmsg = false;
            }
            hasGrownHead = false;
        }

        if( sigmaL1Back < seuilSigmaL1 )
        {
            if( firstbackmsg )
            {
                //qDebug() << "Back stop par sigma" << sigmaL1Back << " seuil : " << seuilSigmaL1 << " a iteration " << i;
                firstbackmsg = false;
            }
            hasGrownBack = false;
        }

        LSIS_Pixel4D pixBack;
        pixBack.setFromCartesian( back(), _image );
        LSIS_Pixel4D pixBackRepulseImage = pixBack;
        pixBackRepulseImage.w() = 0;
        if( pixBackRepulseImage.valueIn( _repulseImage ) == true )
        {
            if( firstbackmsg )
            {
                //qDebug() << "Back stop par intersection" << sigmaL1Back << " seuil : " << seuilSigmaL1 << " a iteration " << i;
                firstbackmsg = false;
            }
            hasGrownBack = false;
        }

//        qDebug() << "\n\n\n";
//        qDebug() << "Head position " << head();
//        qDebug() << "Back position " << back();
//        qDebug() << "Direction de croissance apres " << i << "iterations";
//        if( hasGrownHead )
//        {
//            qDebug() << "Head\n" << growDirHead * -1;
//        }
//        else
//        {
//            qDebug() << "Head termine";
//        }
//        if( hasGrownBack )
//        {
//            qDebug() << "Back\n" << growDirBack * -1;
//        }
//        else
//        {
//            qDebug() << "Back termine";
//        }

        int npts = npoints();

//        if( !hasGrownHead )
//        {
//            qDebug() << "Head termine" << head();
//        }
//        if( !hasGrownBack )
//        {
//            qDebug() << "Back termine" << back();
//        }

        // Si le snake a pousse des deux cotes, il faut ajouter deux points aux extemites
        if( hasGrownBack && hasGrownHead )
        {
//            qDebug() << "Avant croissance";
//            qDebug() << this->points();

            LSIS_Point4DFloat headPoint = head();
            LSIS_Point4DFloat backPoint = back();

            Eigen::MatrixXf newPoints( npts + 2 , 4 );

            // Le premier point est le point de tete moins la direction de croissance
            newPoints.row(0) = headPoint - ( growCoeff * growDirHead );

            if(_optionalMnt != NULL){
                float mntZ = _optionalMnt->valueAtCoords(newPoints(0,1),newPoints(0,2));
                if( newPoints(0,3) <= mntZ + ( minZAxisLevel) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownHead = false;
                }
            }else{
                if( newPoints(0,3) <= _image->minZ() + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownHead = false;
                }
            }


//            if( newPoints(0,0) < (0.09 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            if( newPoints(0,0) < (0.045 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            {
                //qDebug() << "Stop head car la branche atteint un diametre trop petit";
                hasGrownHead = false;
            }

            // Le dernier point est le point de queue moins la direction de croissance
            newPoints.row( npts + 1 ) = backPoint - ( growCoeff * growDirBack );

            if(_optionalMnt != NULL){
                float mntZ = _optionalMnt->valueAtCoords(newPoints(npts + 1,1),newPoints(npts + 1,2));
                if( newPoints( npts + 1, 3 ) <= mntZ + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownBack = false;
                }
            }else{
                if( newPoints( npts + 1, 3 ) <= _image->minZ() + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Back car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints( npts + 1, 3 );
                    hasGrownBack = false;
                }
            }

//            if( newPoints(npts+1,0) < (0.09 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            if( newPoints(npts+1,0) < (0.045 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            {
                //qDebug() << "Stop back car la branche atteint un diametre trop petit";
                hasGrownBack = false;
            }

            // Le reste des points est inchange
            for( int j = 0 ; j < npts ; j++ )
            {
                newPoints.row( j+1 ) = _points.row(j);
            }

            // Copie des nouveaux points (nouvelle matrice) dans le snake tout en restant dans l'image
            updatePointsFromNewSize( newPoints );

//            qDebug() << "Apres croissance";
//            qDebug() << this->points();
        }

        // Si le snake n'a pousse que en tete il faut ajouter un point en debut de matrice
        else if ( hasGrownHead )
        {
//            qDebug() << "Le snake n'a pas pousse en tete";
            Eigen::MatrixXf newPoints( npts + 1 , 4 );

            // Le premier point est le point de tete moins la direction de croissance
            newPoints.row(0) = head() - ( growCoeff * growDirHead );
            if(_optionalMnt != NULL){
                float mntZ = _optionalMnt->valueAtCoords(newPoints(0,1),newPoints(0,2));
                if( newPoints(0,3) <= mntZ + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownHead = false;
                }
            }else{
                if( newPoints(0,3) <= _image->minZ() + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownHead = false;
                }
            }

            if( newPoints(0,0) < (0.045 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            {
                //qDebug() << "Stop head car la branche atteint un diametre trop petit";
                hasGrownHead = false;
            }

            // Le reste des points est inchange
            for( int j = 0 ; j < npts ; j++ )
            {
                newPoints.row( j+1 ) = _points.row(j);
            }

            // Copie des nouveaux points (nouvelle matrice) dans le snake tout en restant dans l'image
            updatePointsFromNewSize( newPoints );
        }

        // Si le snake n'a pousse que en queue il faut ajouter un point en fin de matrice
        else if ( hasGrownBack )
        {
//            qDebug() << "Le snake n'a pas pousse en queue";
            Eigen::MatrixXf newPoints( npts + 1 , 4 );

            // Le dernier point est le point de queue moins la direction de croissance
            newPoints.row( npts ) = back() - ( growCoeff * growDirBack );

            if(_optionalMnt != NULL){
                float mntZ = _optionalMnt->valueAtCoords(newPoints(npts,1),newPoints(npts,2));
                if( newPoints( npts, 3 ) <= mntZ + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Front car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints(0,3);
                    hasGrownBack = false;
                }
            }else{
                if( newPoints( npts, 3 ) <= _image->minZ() + ( minZAxisLevel ) )
                {
                    //qDebug() << "Stop ici Back car on a atteint le bas de la reconstruction" << _image->minZ() << _image->wres() << newPoints( npts + 1, 3 );
                    hasGrownBack = false;
                }
            }

            if( newPoints(npts,0) < (0.045 / 2.0) ) // Si le rayon est plus petit que 9 cm de diametre on arrete la croissance
            {
                //qDebug() << "Stop back car la branche atteint un diametre trop petit";
                hasGrownBack = false;
            }

            // Le reste des points est inchange
            for( int j = 0 ; j < npts ; j++ )
            {
                newPoints.row( j ) = _points.row(j);
            }

            // Copie des nouveaux points (nouvelle matrice) dans le snake tout en restant dans l'image
            updatePointsFromNewSize( newPoints );
        }
    }

    if( i >= nIterMax )
    {
        //qDebug() << "Sortie par manque d'iterations";
    }

    //qDebug() << "Fin de croissance apres " << i << "iterations" << "Head " << hasGrownHead << " back " << hasGrownBack;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::relax(int nIterMax,
                                                      float alpha, float beta,
                                                      float gama,  float globalWeight,
                                                      float timeStep,
                                                      float threshAverageMovement4D )
{
    updatePoints(_points);
    if( length3D() == 0 )
    {
        return;
    }

    if( _step != NULL )
    {
        _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
    }


    // On recupere le premier terme du systeme matriciel
    Eigen::MatrixXf* aPlusRhoIInverse = getAPlusRhoIInverse( alpha, beta, timeStep );
//    Eigen::MatrixXf* rhoIMoinsAInverse = getRhoIMoinsAInverse( alpha, beta, timeStep );

    // On cree une matrice pour stocker la coordonnee des points a l'iteration suivante
    int npts = npoints();
    float rho = 1.0/timeStep;
    Eigen::MatrixXf* pointsNextIteration = new Eigen::MatrixXf( npts, 4 );
    float averageMovement4D = std::numeric_limits<float>::max();
    float stDevMovement4D = std::numeric_limits<float>::max();
    int iNIterMax = nIterMax+1;
    int relaxStep;
    for( relaxStep = 1 ; relaxStep < iNIterMax && averageMovement4D > threshAverageMovement4D ; relaxStep++ )
    {
//        if( i%20 == 0 ) qDebug() << "Relax iteration " << i;
        Eigen::MatrixXf* savePoints = new Eigen::MatrixXf( _points );

        // Calcule les gradients d'energie image et d'energie de croissance
//        // ANCIENNE VERSION SANS PARALLELISATION
//        Eigen::MatrixXf* gradientEnergyImage = getGradientEnergyOrthogonalImageMatrixMultipliedByTangentNorm(globalWeight);
//        Eigen::MatrixXf* secondDiffOnTangentDivByTanNorm = getSecondDifferentialOnTangentDividedByTangentNormMultiplyedByImageEnergy( globalWeight );
//        // ANCIENNE VERSION SANS PARALLELISATION FIN

        // NOUVELLE VERSION AVEC PARALLELISATION
        QFuture<Eigen::MatrixXf*> futureGradientEnergyImage = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::getGradientEnergyOrthogonalImageMatrixMultipliedByTangentNorm, globalWeight );
        QFuture<Eigen::MatrixXf*> futureSecondDiffOnTangentDivByTanNorm = QtConcurrent::run( this, &LSIS_ActiveContour4DContinuous<DataImage>::getSecondDifferentialOnTangentDividedByTangentNormMultiplyedByImageEnergy, globalWeight );

        futureGradientEnergyImage.waitForFinished();
        futureSecondDiffOnTangentDivByTanNorm.waitForFinished();

        Eigen::MatrixXf* gradientEnergyImage = futureGradientEnergyImage.result();
        Eigen::MatrixXf* secondDiffOnTangentDivByTanNorm = futureSecondDiffOnTangentDivByTanNorm.result();
        // NOUVELLE VERSION AVEC PARALLELLISATION FIN

        // Calcul de la position a la prochaine iteration
        (*pointsNextIteration) = (*aPlusRhoIInverse) * ( ( rho * _points ) - ( gama * (*gradientEnergyImage) ) + ( (*secondDiffOnTangentDivByTanNorm) ) );
//        (*pointsNextIteration) = (*rhoIMoinsAInverse) * ( ( rho * _points ) + ( gama * (*gradientEnergyImage) ) - ( (*secondDiffOnTangentDivByTanNorm) ) );

        // On met a jour la position des points :
        // Pour lespoints dans l'image on fait une simple copie
        // Pour les points hors de l'image on les remet dans le dernier pixel traverse par la direction de mouvement du point
        updatePointsAndGetAverageAndStDevMovement( (*pointsNextIteration),
                                                   averageMovement4D,
                                                   stDevMovement4D );

        if( _step != NULL )
        {
            _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
        }

        // Libere la memoire
        delete gradientEnergyImage;
        delete secondDiffOnTangentDivByTanNorm;

//        // Toutes les 10 iterations on regarde si on doit resampler
//        if( i%10 == 0 && hasToBeResampled() )
//        {
//            resample( _image->xres() );

//            // Il faut donc mettre a jour la matrice de geometrie (les matrices des gradients sont mises a jour en debut de boucle)
//            delete aPlusRhoIInverse;
//            aPlusRhoIInverse = getAPlusRhoIInverse( alpha, beta, timeStep );

//            // Il faut aussi mettre a jour la matrice des points a l'iteration suivante (adapter sa taille)
//            pointsNextIteration->resize( npoints(), npoints() );
//        }

        delete savePoints;
    }

    //qDebug() << "Sortie de relaxation apres " << relaxStep << " iterations de relaxations";

    // Libere la memoire
    delete aPlusRhoIInverse;
//    delete rhoIMoinsAInverse;
    delete pointsNextIteration;

    if( _step != NULL )
    {
//        qDebug() << "visualisation apres relaxation suite a arret de croissance";
        _step->callDebugVisualization( (CT_Grid4D<int>*)_image, this );
    }

    updatePoints( _points );
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::goToMaximumScoreInRadius()
{
    int nPts = npoints();

    for( int i = 0 ; i < nPts ; i++ )
    {
        LSIS_Pixel4D curPix = pixelAt(i);
        int curw = curPix.w();
        int curx = curPix.x();
        int cury = curPix.y();
        int curz = curPix.z();

        int maxW = curw;
        DataImage maxValue = _image->value( curw, curx, cury, curz );
        for( int j = 0 ; j < _image->wdim() ; j++ )
        {
            DataImage curValue = _image->value( j, curx, cury, curz );
            if( j != curw && curValue > maxValue )
            {
                maxValue = curValue;
                maxW = j;
            }
        }

        _points(i,0) = _image->getCellCenterW( maxW );
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::markRepulsion(CT_Grid4D_Sparse<bool>* repulseImage, float repulseFactor )
{
    LSIS_Pixel4D bot;
    LSIS_Pixel4D top;
    LSIS_Pixel4D curInBBox;
    LSIS_Point4DFloat currPoint;
    float rElt;
    float currRadius;
    float sumRadius;
    int dimw = _image->wdim();

    // Pour chacun des pixels du contour on marque sa repulsion
    //
    // Les elements qui intersectent curPix verifient : r_p + r_elt < dist( p, elt )
    // On a une bbox des elements qui intersectent pour chaque rayon
    // Elle est donnee par : (p+(r_elt+r_p)) et (p-(r_elt+r_p))
    int npts = npoints();
    for( int i = 0 ; i < npts ; i++ )
    {
        // Recupere le point du contour
        currPoint = pointAt(i);

        // Et son rayon
        currRadius = currPoint.w();

        // Si son pixel n'est pas deja marque alors on fait la marque autour, sinon on ne fait rien
        LSIS_Pixel4D currPix;
        currPix.setFromCartesian( currPoint, _image );

        // Pour chaque rayon de l'espace de Hough on calcule la bbox des elements qui intersectent le cercle represente par currPoint
        for ( int currW = 0 ; currW < dimw ; currW++ )
        {
//            qDebug() << "Ici" << currW << dimw;
            // On calcule le rayon cartesien des elements de ce rayon
            rElt = _image->minW() + ( currW + 0.5 ) * _image->wres();

            // Ainsi on a acces a (r_elt+r_p) et on peut avoir la bbox en cartesien
            // i.e. currPoint - (r_elt+r_p) et currPoint + (r_elt+r_p)
            sumRadius = rElt + currRadius;

            // On transforme la bbox cartesienne en bbox de la grille
            // On elargi la bbox d'un facteur du rayon de repulsion
            bot.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                     currPoint.x() - (repulseFactor*sumRadius),
                                                     currPoint.y() - (repulseFactor*sumRadius),
                                                     currPoint.z() - (repulseFactor*sumRadius) ),
                                  repulseImage );

            top.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                     currPoint.x() + (repulseFactor*sumRadius),
                                                     currPoint.y() + (repulseFactor*sumRadius),
                                                     currPoint.z() + (repulseFactor*sumRadius) ),
                                  repulseImage );

            // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
            for( int axe = 0 ; axe < 4 ; axe++ )
            {
                if( bot(axe) < 0 )
                {
                    bot(axe) = 0;
                }

                if( bot(axe) >= repulseImage->dim()(axe) )
                {
                    bot(axe) = repulseImage->dim()(axe) - 1;
                }

                if( top(axe) < 0 )
                {
                    top(axe) = 0;
                }

                if( top(axe) >= repulseImage->dim()(axe) )
                {
                    top(axe) = repulseImage->dim()(axe) - 1;
                }
            }

            // Il ne reste plus qu'a tester les intersections des pixels a l'interieur de la bbox
            // On parcour donc toute la bbox
            for ( curInBBox.w()= bot.w() ; curInBBox.w() <= top.w() ; curInBBox.w()++ )
            {
                for ( curInBBox.x() = bot.x(); curInBBox.x() <= top.x() ; curInBBox.x()++ )
                {
                    for ( curInBBox.y() = bot.y() ; curInBBox.y() <= top.y() ; curInBBox.y()++ )
                    {
                        for ( curInBBox.z() = bot.z() ; curInBBox.z() <= top.z(); curInBBox.z()++ )
                        {
                            curInBBox.setValueIn( repulseImage, true );
                        }
                    }
                }
            }
//            beginForAllPixelsInBBox( bot, top, repulseImage->wdim(), repulseImage->xdim(), repulseImage->ydim(), repulseImage->zdim(), curInBBox )
//            {
//                // et mettre l'image de repulsion a true lorsqu'il y a effectivement intersection
//                if ( intersectFromFloatAndImage( curPix, curInBBox ) )
//                {
//                    curInBBox.setValueIn( repulseImage, true );
//                }
//            }
//            endForAllPixelsInBBox( bot, top, repulseImage->wdim(), repulseImage->xdim(), repulseImage->ydim(), repulseImage->zdim(), curInBBox )
        }
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::markRepulsionv2(CT_Grid4D_Sparse<bool>* repulseImage, float repulseFactor )
{
    LSIS_Pixel4D bot;
    LSIS_Pixel4D top;
    LSIS_Pixel4D curInBBox;
    LSIS_Point4DFloat currPoint;
    float rElt;
    float currRadius;
    float sumRadius;
    int dimw = _image->wdim();

    // Pour chacun des pixels du contour on marque sa repulsion
    //
    // Les elements qui intersectent curPix verifient : r_p + r_elt < dist( p, elt )
    // On a une bbox des elements qui intersectent pour chaque rayon
    // Elle est donnee par : (p+(r_elt+r_p)) et (p-(r_elt+r_p))
    int npts = npoints();
    for( int i = 0 ; i < npts ; i++ )
    {
        // Recupere le point du contour
        currPoint = pointAt(i);

        // Et son rayon
        currRadius = currPoint.w();

        // Pour chaque rayon de l'espace de Hough on calcule la bbox des elements qui intersectent le cercle represente par currPoint
        for ( int currW = 0 ; currW < dimw ; currW++ )
        {
//            // On calcule le rayon cartesien des elements de ce rayon
//            rElt = _image->minW() + ( currW + 0.5 ) * _image->wres();

//            // Ainsi on a acces a (r_elt+r_p) et on peut avoir la bbox en cartesien
//            // i.e. currPoint - (r_elt+r_p) et currPoint + (r_elt+r_p)
//            sumRadius = rElt + currRadius;

            // On transforme la bbox cartesienne en bbox de la grille
            // On elargi la bbox d'un facteur du rayon de repulsion
            bot.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                     currPoint.x() - (repulseFactor*sumRadius),
                                                     currPoint.y() - (repulseFactor*sumRadius),
                                                     currPoint.z() - (repulseFactor*sumRadius) ),
                                  repulseImage );

            top.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                     currPoint.x() + (repulseFactor*sumRadius),
                                                     currPoint.y() + (repulseFactor*sumRadius),
                                                     currPoint.z() + (repulseFactor*sumRadius) ),
                                  repulseImage );

            // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
            for( int axe = 0 ; axe < 4 ; axe++ )
            {
                if( bot(axe) < 0 )
                {
                    bot(axe) = 0;
                }

                if( bot(axe) >= repulseImage->dim()(axe) )
                {
                    bot(axe) = repulseImage->dim()(axe) - 1;
                }

                if( top(axe) < 0 )
                {
                    top(axe) = 0;
                }

                if( top(axe) >= repulseImage->dim()(axe) )
                {
                    top(axe) = repulseImage->dim()(axe) - 1;
                }
            }

            // Il ne reste plus qu'a tester les intersections des pixels a l'interieur de la bbox
            // On parcour donc toute la bbox
            for ( curInBBox.w()= bot.w() ; curInBBox.w() <= top.w() ; curInBBox.w()++ )
            {
                for ( curInBBox.x() = bot.x(); curInBBox.x() <= top.x() ; curInBBox.x()++ )
                {
                    for ( curInBBox.y() = bot.y() ; curInBBox.y() <= top.y() ; curInBBox.y()++ )
                    {
                        for ( curInBBox.z() = bot.z() ; curInBBox.z() <= top.z(); curInBBox.z()++ )
                        {
                            curInBBox.setValueIn( repulseImage, true );
                        }
                    }
                }
            }
//            beginForAllPixelsInBBox( bot, top, repulseImage->wdim(), repulseImage->xdim(), repulseImage->ydim(), repulseImage->zdim(), curInBBox )
//            {
//                // et mettre l'image de repulsion a true lorsqu'il y a effectivement intersection
//                if ( intersectFromFloatAndImage( curPix, curInBBox ) )
//                {
//                    curInBBox.setValueIn( repulseImage, true );
//                }
//            }
//            endForAllPixelsInBBox( bot, top, repulseImage->wdim(), repulseImage->xdim(), repulseImage->ydim(), repulseImage->zdim(), curInBBox )
        }
    }
}

template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::stopGrowing(const Eigen::MatrixXf *oldPoints,
                                                            const Eigen::MatrixXf *newPoints,
                                                            float gradMovementMin,
                                                            float gradLengthMin)
{
    float movement = 0;
    LSIS_Point4DFloat curPoint;
    LSIS_Point4DFloat curNewPoint;
    LSIS_Point4DFloat nextNewPoint;
    float newLength = 0;
    int npts = npoints();

    for( int i = 0 ; i < npts-1 ; i++ )
    {
        curPoint = pointAt(i);
        curNewPoint = newPoints->row(i);
        nextNewPoint = newPoints->row( i+1 );

        newLength += LSIS_Point4DFloat::distance3D( curNewPoint, nextNewPoint );
        movement += ( curPoint - curNewPoint ).norm();
    }

    // On rajoute le mouvement du dernier point qui n'a pas ete pris en compte dans la boucle precedente
    movement += ( back() - newPoints->row( npts-1 ) ).norm();

//    qDebug() << movement / length3D();
//    qDebug() << "Difference longueur " << fabs( newLength - length3D() );

    return ( ( movement / npts ) < gradMovementMin );
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::getScoreStats(DataImage &outMin, DataImage &outMax) const
{
    outMin = std::numeric_limits<DataImage>::max();
    outMax = -std::numeric_limits<DataImage>::max();
    DataImage val;

    int npts = npoints();
    for( int i = 0 ; i < npts ; i++ )
    {
        val = pixelAt(i).valueIn( _image );

        if( val < outMin )
        {
            outMin =val;
        }

        if( val > outMax )
        {
            outMax = val;
        }
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::getPointCloudAndNormalsAroundEachSnake(QVector< LSIS_ActiveContour4DContinuous<DataImage>* > &snakes,
                                                                                    CT_PCIR sceneIndexCloud,
                                                                                    CT_AbstractNormalCloud* sceneNormalCloud,
                                                                                    QVector< CT_PointCloudIndexVector* > &outIndexClouds,
                                                                                    QVector< CT_NormalCloudStdVector* > &outNormalClouds )
{
    // On cree autant de point cloud index que de contours actifs
    outIndexClouds.clear();
    size_t nSnakes= snakes.size();
    float resw = snakes.first()->resw();
    for ( size_t i = 0 ; i < nSnakes ; i++ )
    {
        outIndexClouds.push_back( new CT_PointCloudIndexVector() );
        outNormalClouds.push_back( new CT_NormalCloudStdVector() );
    }

    // On calcule la bounding box de chaque contour actif
    QVector<float> minx;
    QVector<float> miny;
    QVector<float> minz;
    QVector<float> maxx;
    QVector<float> maxy;
    QVector<float> maxz;
    float currMinw,currMinx, currMiny, currMinz, currMaxw, currMaxx, currMaxy, currMaxz;
    foreach ( LSIS_ActiveContour4DContinuous<DataImage>* currSnake, snakes )
    {
        currSnake->getCartesianBoundingBoxFromFloat(currMinw, currMinx, currMiny, currMinz,
                                                    currMaxw, currMaxx, currMaxy, currMaxz );

        // On transforme la bbox 4D en bbox 3D que l'on aggradit un peu
        minx.push_back( currMinx - (currMaxw + resw ) );
        miny.push_back( currMiny - (currMaxw + resw ) );
        minz.push_back( currMinz - (currMaxw + resw ) );
        maxx.push_back( currMaxx + (currMaxw + resw ) );
        maxy.push_back( currMaxy + (currMaxw + resw ) );
        maxz.push_back( currMaxz + (currMaxw + resw ) );
    }

    CT_PointAccessor pAccess;

    // On parcours tous les points pour les mettre dans les bbox appropriees
    size_t nbPoints = sceneIndexCloud->size();
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        // On regarde si ce point appartient a une (ou plusieurs) bounding box de snake
        for( size_t j = 0 ; j < nSnakes ; j++ )
        {
            const CT_Point &currPoint = pAccess.constPointAt(sceneIndexCloud->indexAt( i ));
            if( currPoint.x() >= minx.at(j) &&
                currPoint.y() >= miny.at(j) &&
                currPoint.z() >= minz.at(j) &&
                currPoint.x() <= maxx.at(j) &&
                currPoint.y() <= maxy.at(j) &&
                currPoint.z() <= maxz.at(j) )
            {
                // Si le point appartient a la bbox on l'ajoute au nuage d'indice qui correspond au snake
                outIndexClouds[j]->addIndex( sceneIndexCloud->indexAt(i) );

                // Ainsi que la normale qui va avec
                outNormalClouds[j]->addNormal( sceneNormalCloud->normalAt(i) );
            }
        }
    }
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::save ( QString directory, QString fileName )
{
    QDir dir( directory );
    if( !dir.exists() )
    {
        dir.mkpath( directory );
    }

    QFile file( directory + "/" + fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << directory + "/" + fileName;
        return;
    }

    QTextStream stream( &file );
    stream << "R X Y Z\n";

    LSIS_Point4DFloat curPoint;
    LSIS_Point4DFloat centroid;
    centroid.w() = 0;
    centroid.x() = 0;
    centroid.y() = 0;
    centroid.z() = 0;
    int npts = npoints();

    // On regarde dans quels sens on inscrit les cercles du snake pour que ca aille toujours du bas vers le haut
    if( this->head().z() < this->back().z() )
    {
        for ( int i = 0 ; i < npts ; i++ )
        {
            curPoint = pointAt(i);
            centroid.w() += curPoint.w();
            centroid.x() += curPoint.x();
            centroid.y() += curPoint.y();
            centroid.z() += curPoint.z();
            stream << curPoint.w() << " " << curPoint.x() << " " << curPoint.y() << " " << curPoint.z() << "\n";
        }
    }

    else
    {
        for ( int i = npts-1 ; i >= 0 ; i-- )
        {
            curPoint = pointAt(i);
            centroid.w() += curPoint.w();
            centroid.x() += curPoint.x();
            centroid.y() += curPoint.y();
            centroid.z() += curPoint.z();
            stream << curPoint.w() << " " << curPoint.x() << " " << curPoint.y() << " " << curPoint.z() << "\n";
        }
    }

    centroid.w() /= (float)npoints();
    centroid.x() /= (float)npoints();
    centroid.y() /= (float)npoints();
    centroid.z() /= (float)npoints();
//    stream << "Centroid " << centroid.w() << " " << centroid.x() << " " << centroid.y() << " " << centroid.z() << "\n";

    file.close();
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::saveWithMntHeight(QString directory, QString fileName, CT_Image2D<float>* mnt)
{
    QDir dir( directory );
    if( !dir.exists() )
    {
        dir.mkpath( directory );
    }

    QFile file( directory + "/" + fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << directory + "/" + fileName;
        return;
    }

    QTextStream stream( &file );
    stream << "R X Y Z\n";

    LSIS_Point4DFloat curPoint;
    LSIS_Point4DFloat centroid;
    centroid.w() = 0;
    centroid.x() = 0;
    centroid.y() = 0;
    centroid.z() = 0;
    int npts = npoints();

    // On regarde dans quels sens on inscrit les cercles du snake pour que ca aille toujours du bas vers le haut
    if( this->head().z() < this->back().z() )
    {
        // On prend la hauteur du premier cercle au dessus du mnt comme base
        float zBase = mnt->valueAtCoords( head().x(), head().y() );
        for ( int i = 0 ; i < npts ; i++ )
        {
            curPoint = pointAt(i);
            centroid.w() += curPoint.w();
            centroid.x() += curPoint.x();
            centroid.y() += curPoint.y();
            centroid.z() += curPoint.z();
            stream << curPoint.w() << " " << curPoint.x() << " " << curPoint.y() << " " << curPoint.z() - zBase << "\n";
        }
    }

    else
    {
        // On prend la hauteur du premier cercle au dessus du mnt comme base
        float zBase = mnt->valueAtCoords( this->back().x(), this->back().y() );
        for ( int i = npts-1 ; i >= 0 ; i-- )
        {
            curPoint = pointAt(i);
            centroid.w() += curPoint.w();
            centroid.x() += curPoint.x();
            centroid.y() += curPoint.y();
            centroid.z() += curPoint.z();
            stream << curPoint.w() << " " << curPoint.x() << " " << curPoint.y() << " " << curPoint.z() - zBase << "\n";
        }
    }

    centroid.w() /= (float)npoints();
    centroid.x() /= (float)npoints();
    centroid.y() /= (float)npoints();
    centroid.z() /= (float)npoints();
//    stream << "Centroid " << centroid.w() << " " << centroid.x() << " " << centroid.y() << " " << centroid.z() << "\n";

    file.close();
}


template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::getBoundingBox(float &outMinw, float &outMinx, float &outMiny, float &outMinz,
                                                               float &outMaxw, float &outMaxx, float &outMaxy, float &outMaxz)
{
    outMinw = outMinx = outMiny = outMinz = std::numeric_limits<float>::max();
    outMaxw = outMaxx = outMaxy = outMaxz = -std::numeric_limits<float>::max();
    LSIS_Point4DFloat currPoint;

    int npts = npoints();
    for ( int i = 0 ; i < npts ; i++ )
    {
        currPoint = pointAt(i);

        if ( currPoint.w() < outMinw )
        {
            outMinw = currPoint.w();
        }

        if ( currPoint.w() > outMaxw )
        {
            outMaxw = currPoint.w();
        }

        if ( currPoint.x() < outMinx )
        {
            outMinx = currPoint.x();
        }

        if ( currPoint.x() > outMaxx )
        {
            outMaxx = currPoint.x();
        }

        if ( currPoint.y() < outMiny )
        {
            outMiny = currPoint.y();
        }

        if ( currPoint.y() > outMaxy )
        {
            outMaxy = currPoint.y();
        }

        if ( currPoint.z() < outMinz )
        {
            outMinz = currPoint.z();
        }

        if ( currPoint.z() > outMaxz )
        {
            outMaxz = currPoint.z();
        }
    }
}

template< typename DataImage >
const LSIS_StepSnakesInHoughSpace *LSIS_ActiveContour4DContinuous<DataImage>::step() const
{
    return _step;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setStep(LSIS_StepSnakesInHoughSpace *step)
{
    _step = step;
}

template< typename DataImage >
const CT_Grid4D<DataImage> *LSIS_ActiveContour4DContinuous<DataImage>::image() const
{
    return _image;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setImage(const CT_Grid4D<DataImage> *image)
{
    _image = image;
}

template< typename DataImage >
CT_Grid4D<float> *LSIS_ActiveContour4DContinuous<DataImage>::gradw() const
{
    return _gradw;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setGradw(CT_Grid4D<float> *gradw)
{
    _gradw = gradw;
}

template< typename DataImage >
CT_Grid4D<float> *LSIS_ActiveContour4DContinuous<DataImage>::gradx() const
{
    return _gradx;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setGradx(CT_Grid4D<float> *gradx)
{
    _gradx = gradx;
}

template< typename DataImage >
CT_Grid4D<float> *LSIS_ActiveContour4DContinuous<DataImage>::grady() const
{
    return _grady;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setGrady(CT_Grid4D<float> *grady)
{
    _grady = grady;
}

template< typename DataImage >
CT_Grid4D<float> *LSIS_ActiveContour4DContinuous<DataImage>::gradz() const
{
    return _gradz;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setGradz(CT_Grid4D<float> *gradz)
{
    _gradz = gradz;
}

template< typename DataImage >
CT_Grid4D<float> *LSIS_ActiveContour4DContinuous<DataImage>::imageEnergy() const
{
    return _imageEnergy;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setImageEnergy(CT_Grid4D<float> *imageEnergy)
{
    _imageEnergy = imageEnergy;
}

template< typename DataImage >
CT_Image2D<float>* LSIS_ActiveContour4DContinuous<DataImage>::getOptionalMnt() const
{
    return _optionalMnt;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setOptionalMnt(CT_Image2D<float>* optionalMnt)
{
    _optionalMnt = optionalMnt;
}

template< typename DataImage >
DataImage LSIS_ActiveContour4DContinuous<DataImage>::getIntensityMin() const
{
    return _intensityMin;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setIntensityMin(const DataImage& intensityMin)
{
    _intensityMin = intensityMin;
}

template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::addFork(LSIS_Point4DFloat forkFirstPoint)
{
    _forkStartPoints.append(forkFirstPoint);

    return true;
}

template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::removeFork(int i)
{
//    _forkStartPoints.erase(i);
    _forkStartPoints.removeAt(i);

    return true;
}

template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::isFork() const
{
    return _isFork;
}

template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::setIsFork(bool isFork)
{
    _isFork = isFork;
}

//Sort the points, used for fork searching to start from the tree base to stop  forks being created higher and essentially stopping
//better forks at lower levels
template< typename DataImage >
void LSIS_ActiveContour4DContinuous<DataImage>::sortPointsbyHeight()
{
    //The first circle isnt always the highest, but i'd like it to be at least higher than -+ 50%
    //of circles after it.(This logic tends to doom irregular snakes to failure in calculateSnakeValidity(), but that's not any worse.)
    if(this->npoints() > 2){
//        for(int j = 0; j<this->npoints()-1;j++){
//            if((_points.row(0)).z() > (_points.row(j+1)).z())
//                largerThan++;
//        }
        Eigen::MatrixXf newPoints = this->points();
        if(head().z()>back().z()){
            int j = 0;
            for(int i = this->npoints() - 1; i >= 0;i--){
                _points.row(j) = newPoints.row(i);
                j++;
            }
        }
    }
}

//Legacy Idea to delete snakes that didn't follow tree propagation angles logic. Sadly, that removes good trees also most of the time.
//It is basically impossible for me to pinpoint criterions that encompass most things.
template< typename DataImage >
bool LSIS_ActiveContour4DContinuous<DataImage>::calculateSnakeValidity()
{
     //(Az - Bx) > 2 * (Ax - Bx) //63 degrees
    if(this->npoints() > 2){
        double fourthSnake = floor(npoints()/4.00);
        QList<LSIS_Point4DFloat> pointsForAngles;
        QList<double> angleList;
        pointsForAngles.append(head());
        pointsForAngles.append(_points.row(fourthSnake));
        pointsForAngles.append(_points.row(fourthSnake*2));
        pointsForAngles.append(_points.row(fourthSnake*3));
        pointsForAngles.append(back());

        double lowerThan = 0;
        double threshHold;
/*
//        double BzAz = ((_points.row(fourthSnake)).z()-(_points.row(0)).z());
//        double BxAx = ((_points.row(fourthSnake)).x()-(_points.row(0)).x());
//        double angle = atan(BzAz/BxAx) * 180 / PI;*/
        qDebug()<<"_points head z axis value:"<<head().z();
        qDebug()<<"_points back x axis value:"<<back().x();
        qDebug()<<"_points back z axis value:"<<back().z();
        qDebug()<<"point before back x axis"<< pointsForAngles.at(3).x();
        qDebug()<<"point before back z axis"<< pointsForAngles.at(3).z();


        for(int i = 0; i<4;i++){
            double BzAz = ((pointsForAngles.at(i+1)).z()-(pointsForAngles.at(i)).z());
            double BxAx = ((pointsForAngles.at(i+1)).x()-(pointsForAngles.at(i)).x());
            double angle = atan(BzAz/BxAx) * 180 / PI;
            qDebug()<<"Angle line number "<<i;

            //If the snake, is incongruant even after being put back in order
            if(((pointsForAngles.at(i)).x()>=(pointsForAngles.at(i+1)).x())&&((pointsForAngles.at(i)).z()>=(pointsForAngles.at(i+1)).z())){
                qDebug()<<"Angle of the two points: (Q3)"<<angle;
                angle += 180;
                qDebug()<<"Angle of the two points after transformation: (Q3)"<<angle;
            }else if(((pointsForAngles.at(i)).x()>=(pointsForAngles.at(i+1)).x())&&((pointsForAngles.at(i)).z()<=(pointsForAngles.at(i+1)).z())){
                qDebug()<<"Angle of the two points: (Q2)"<<angle;
                angle = angle + 180;
                qDebug()<<"Angle of the two points after transformation: (Q2)"<<angle;
            }else if(((pointsForAngles.at(i)).x()<=(pointsForAngles.at(i+1)).x())&&((pointsForAngles.at(i)).z()>=(pointsForAngles.at(i+1)).z())){
                qDebug()<<"Angle of the two points: (Q4)"<<angle;
                angle = angle + 360;
                qDebug()<<"Angle of the two points after transformation: (Q4)"<<angle;
            }else{
                qDebug()<<"Angle of the two points: (Q1)"<<angle;
            }

            angleList.append(angle);
        }
        int inRange = 0; //If the angle is in the range of angles
        for(int i = 0; i < 4; i++){
            if((angleList.at(i)>= angleList.at(0)-20.00) && (angleList.at(i)<= angleList.at(0)+20.00)){
                inRange++;
            }
        }
        if(inRange == 4){
           return true;
        }
        return false;
    }
    return true;
}

#endif // LSIS_ACTIVECONTOUR4D_HPP
