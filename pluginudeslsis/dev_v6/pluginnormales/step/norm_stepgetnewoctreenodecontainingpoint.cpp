/************************************************************************************
* Filename :  norm_stepgetnewoctreenodecontainingpoint.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepgetnewoctreenodecontainingpoint.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

#include "newoctree/norm_newoctree.h"
#include "newoctree/norm_newoctreenode.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

// Alias for indexing models
#define DEFin_Result "InRsltOctree"
#define DEFin_Group "InGrpOctree"
#define DEFin_ItemOctree "inItmOctree"

#define DEFout_Result "OutRsltScene"
#define DEFout_GroupScene "OutGrpScene"
#define DEFout_ItemScene "OutItmScene"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;

// Constructor : initialization of parameters
NORM_StepGetNewOctreeNodeContainingPoint::NORM_StepGetNewOctreeNodeContainingPoint(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _querryx = 0;
    _querryy = 0;
    _querryz = 0;
}

// Step description (tooltip of contextual menu)
QString NORM_StepGetNewOctreeNodeContainingPoint::getStepDescription() const
{
    return tr("Cherche la cellule a laquelle appartient un point");
}

// Step detailled description
QString NORM_StepGetNewOctreeNodeContainingPoint::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepGetNewOctreeNodeContainingPoint::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepGetNewOctreeNodeContainingPoint::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepGetNewOctreeNodeContainingPoint(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepGetNewOctreeNodeContainingPoint::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_Result = createNewInResultModel(DEFin_Result, tr("inResult"));
    resIn_Result->setRootGroup(DEFin_Group, CT_AbstractItemGroup::staticGetType(), tr("inGroup"));
    resIn_Result->addItemModel(DEFin_Group, DEFin_ItemOctree, NORM_NewOctree::staticGetType(), tr("inNewOctree"));
}

// Creation and affiliation of OUT models
void NORM_StepGetNewOctreeNodeContainingPoint::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_Result = createNewOutResultModel(DEFout_Result, tr("OutRsltScene"));
    res_Result->setRootGroup(DEFout_GroupScene, new CT_StandardItemGroup(), tr("OutGrpScene"));
    res_Result->addItemModel(DEFout_GroupScene, DEFout_ItemScene, new CT_Scene(), tr("OutNewOctreNode"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepGetNewOctreeNodeContainingPoint::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("x", "", -9999, 9999, 4, _querryx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _querryy, 1);
    configDialog->addDouble("", "", -9999, 9999, 4, _querryz, 1);
}

void NORM_StepGetNewOctreeNodeContainingPoint::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_Result = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_Group);
    while( itIn_Group.hasNext() && !isStopped() )
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();
        NORM_NewOctree* itemIn_NewOctree = (NORM_NewOctree*)grpIn_Group->firstItemByINModelName(this, DEFin_ItemOctree);

        if( itemIn_NewOctree != NULL )
        {
            // On recupere le nuage d'indices de la cellule contenant le noeud
            CT_PointCloudIndexVector* outIndexVector = itemIn_NewOctree->getNodeContainingPoint( createCtPoint( _querryx, _querryy, _querryz ) )->getIndexCloud();

            // Calcul de la bounding box
            CT_PointAccessor pAccessor;
            CT_Point curPoint;
            float xmin = std::numeric_limits<float>::max();
            float xmax = -std::numeric_limits<float>::max();
            float ymin = std::numeric_limits<float>::max();
            float ymax = -std::numeric_limits<float>::max();
            float zmin = std::numeric_limits<float>::max();
            float zmax = -std::numeric_limits<float>::max();
            size_t nbPoints = outIndexVector->size();
            for( size_t i = 0 ; i < nbPoints ; i++ )
            {
                pAccessor.pointAt( outIndexVector->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }

            // Creation de la scene
            CT_PCIR outIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexVector );
            CT_Scene* item_outItemScene = new CT_Scene(DEFout_ItemScene, res_Result);
            item_outItemScene->setPointCloudIndexRegistered( outIndexVectorRegistered );

            // On ajoute cette scene aux resultats
            CT_StandardItemGroup* grp_OutGrpScene = new CT_StandardItemGroup(DEFout_GroupScene, res_Result);
            grp_OutGrpScene->addItemDrawable(item_outItemScene);
            res_Result->addGroup(grp_OutGrpScene);
        }
    }
}
