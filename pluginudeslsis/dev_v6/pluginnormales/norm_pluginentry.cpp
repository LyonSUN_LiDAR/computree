#include "norm_pluginentry.h"
#include "norm_pluginmanager.h"

NORM_PluginEntry::NORM_PluginEntry()
{
    _pluginManager = new NORM_PluginManager();
}

NORM_PluginEntry::~NORM_PluginEntry()
{
    delete _pluginManager;
}

QString NORM_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* NORM_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_normales, NORM_PluginEntry)
#endif
