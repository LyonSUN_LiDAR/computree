/********************************************************************************
* Filename :  NORM_acp.cpp							*
*																				*
* Copyright (C) 2015 Joris RAVAGLIA												*
* Author: Joris Ravaglia														*
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                        *
*																				*
* This file is part of the oc_pluginoctree plugin                               *
* for the CompuTree v2.0 software.                                              *
* The oc_pluginoctree plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the               *
* GNU Lesser General Public License as published by the Free Software Foundation*
* either version 3 of the License, or (at your option) any later version.       *
*																				*
* The oc_pluginoctree plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY*
* or FITNESS FOR A PARTICULAR PURPOSE.                                          *
* See the GNU Lesser General Public License for more details.                   *
*																				*
* You should have received a copy of the GNU Lesser General Public License		*
* along with this program. If not, see <http://www.gnu.org/licenses/>.			*
*																				*
*********************************************************************************/

#include "norm_acp.h"

#include <Eigen/Eigenvalues>
#include "ct_cloudindex/registered/abstract/ct_abstractmodifiablecloudindexregisteredt.h"
#include "ct_point.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"
#include <QDebug>

void NORM_Tools::NORM_PCA::pca(CT_MPCIR indexCloud,
                           float &outL1, float &outL2, float &outL3,
                           CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    if( indexCloud.data() != NULL )
    {
        pca( indexCloud->abstractCloudIndexT(), outL1, outL2, outL3, outV1, outV2, outV3 );
    }
}

void NORM_Tools::NORM_PCA::pca(const CT_PCIR indexCloud,
                               const CT_Point &centroid,
                               float &outL1, float &outL2, float &outL3,
                               CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    if( indexCloud.data() != NULL )
    {
        pca( indexCloud->abstractCloudIndexT(),
             centroid,
             outL1, outL2, outL3,
             outV1, outV2, outV3 );
    }
}

void NORM_Tools::NORM_PCA::pca(CT_MPCIR indexCloud,
                               const CT_Point& centroid,
                               size_t first,
                               size_t last,
                               float &outL1, float &outL2, float &outL3,
                               CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    if( indexCloud.data() != NULL )
    {
        pca( indexCloud->abstractCloudIndexT(), centroid, first, last, outL1, outL2, outL3, outV1, outV2, outV3 );
    }
}

void NORM_Tools::NORM_PCA::pca(const CT_AbstractPointCloudIndex* indexCloud,
                           float &outL1, float &outL2, float &outL3,
                           CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    // 1 - Compute centroid
    CT_Point centroid = getCentroid( indexCloud );

    CT_PointAccessor ptAccessor;
    size_t nbPoints = indexCloud->size();

    // 2 - Create matrix of centered points
    Eigen::MatrixXd centeredPoints( 3, nbPoints );
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        centeredPoints.col(i) = ptAccessor.constPointAt( indexCloud->indexAt(i) ) - centroid;
    }

    // 2 - Create covariance matrix
    Eigen::Matrix3d cov = centeredPoints * centeredPoints.transpose();

    // 3 - Compute eigenvalues and eigenvectors
    Eigen::EigenSolver< Eigen::Matrix3d > eigenSolver;
    eigenSolver.compute( cov );

    // 4 - Get the eigenvalues and eigenvectors
    Eigen::Vector3cd eigenValues = eigenSolver.eigenvalues();   // Each row of the vector is an eigenvalue of the covariance matrix
    Eigen::Matrix3cd eigenVectors = eigenSolver.eigenvectors(); // The columnsof this matrix are the associated eigenvectors

    // 5 - Sort eigenvalues and eigenvectors in decrescent order
    sortEigenValues( eigenValues, eigenVectors );

    // 6 - Parse the results into float and CT_Point
    //     Eigenvalues of a covariancematrix are always real
    outL1 = eigenValues(0).real();
    outL2 = eigenValues(1).real();
    outL3 = eigenValues(2).real();

    for ( int i = 0 ; i < 3 ; i++ )
    {
        outV1(i) = eigenVectors(i,0).real();
        outV2(i) = eigenVectors(i,1).real();
        outV3(i) = eigenVectors(i,2).real();
    }

    outV1.normalize();
    outV2.normalize();
    outV3.normalize();
}

void NORM_Tools::NORM_PCA::pca(const CT_AbstractPointCloudIndex* indexCloud,
                               const CT_Point& centroid,
                               float &outL1, float &outL2, float &outL3,
                               CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    CT_PointAccessor ptAccessor;
    size_t nbPoints = indexCloud->size();

    // 2 - Create matrix of centered points
    Eigen::MatrixXd centeredPoints( 3, nbPoints );
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        centeredPoints.col(i) = ptAccessor.constPointAt( indexCloud->indexAt(i) ) - centroid;
    }

    // 2 - Create covariance matrix
    Eigen::Matrix3d cov = centeredPoints * centeredPoints.transpose();

    // 3 - Compute eigenvalues and eigenvectors
    Eigen::EigenSolver< Eigen::Matrix3d > eigenSolver;
    eigenSolver.compute( cov );

    // 4 - Get the eigenvalues and eigenvectors
    Eigen::Vector3cd eigenValues = eigenSolver.eigenvalues();   // Each row of the vector is an eigenvalue of the covariance matrix
    Eigen::Matrix3cd eigenVectors = eigenSolver.eigenvectors(); // The columnsof this matrix are the associated eigenvectors

    // 5 - Sort eigenvalues and eigenvectors in decrescent order
    sortEigenValues( eigenValues, eigenVectors );

    // 6 - Parse the results into float and CT_Point
    //     Eigenvalues of a covariancematrix are always real
    outL1 = eigenValues(0).real();
    outL2 = eigenValues(1).real();
    outL3 = eigenValues(2).real();

    for ( int i = 0 ; i < 3 ; i++ )
    {
        outV1(i) = eigenVectors(i,0).real();
        outV2(i) = eigenVectors(i,1).real();
        outV3(i) = eigenVectors(i,2).real();
    }

    outV1.normalize();
    outV2.normalize();
    outV3.normalize();
}

void NORM_Tools::NORM_PCA::pca(const QVector<int>* indexCloud,
                               const CT_Point& centroid,
                               float &outL1, float &outL2, float &outL3,
                               CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    CT_PointAccessor ptAccessor;
    size_t nbPoints = indexCloud->size();

    // 2 - Create matrix of centered points
    Eigen::MatrixXd centeredPoints( 3, nbPoints );
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        centeredPoints.col(i) = ptAccessor.constPointAt( indexCloud->at(i) ) - centroid;
    }

    // 2 - Create covariance matrix
    Eigen::Matrix3d cov = centeredPoints * centeredPoints.transpose();

    // 3 - Compute eigenvalues and eigenvectors
    Eigen::EigenSolver< Eigen::Matrix3d > eigenSolver;
    eigenSolver.compute( cov );

    // 4 - Get the eigenvalues and eigenvectors
    Eigen::Vector3cd eigenValues = eigenSolver.eigenvalues();   // Each row of the vector is an eigenvalue of the covariance matrix
    Eigen::Matrix3cd eigenVectors = eigenSolver.eigenvectors(); // The columnsof this matrix are the associated eigenvectors

    // 5 - Sort eigenvalues and eigenvectors in decrescent order
    sortEigenValues( eigenValues, eigenVectors );

    // 6 - Parse the results into float and CT_Point
    //     Eigenvalues of a covariancematrix are always real
    outL1 = eigenValues(0).real();
    outL2 = eigenValues(1).real();
    outL3 = eigenValues(2).real();

    for ( int i = 0 ; i < 3 ; i++ )
    {
        outV1(i) = eigenVectors(i,0).real();
        outV2(i) = eigenVectors(i,1).real();
        outV3(i) = eigenVectors(i,2).real();
    }

    outV1.normalize();
    outV2.normalize();
    outV3.normalize();

    // On regarde que la base de l'acp soit bien directe
    CT_Point produitVectorielV2V3 = createCtPoint( outV2.y()*outV3.z() - outV2.z()*outV3.y(),
                                                   outV2.z()*outV3.x() - outV2.x()*outV3.z(),
                                                   outV2.x()*outV3.y() - outV2.y()*outV3.x() );

    // Pour eviter le fait que l'egalite parfaite n'apparaisse entre le produitvectoriel et v1
    // On regarde le signe du produit scalaire
    float scalarProduct = ( outV1.x() * produitVectorielV2V3.x() +
                            outV1.y() * produitVectorielV2V3.y() +
                            outV1.z() * produitVectorielV2V3.z() );

    if( scalarProduct < 0 )
    {
        // La base est indirecte on change un vecteur
        outV1 *= -1;
    }
}

void NORM_Tools::NORM_PCA::pca(const CT_AbstractPointCloudIndex* indexCloud,
                               const CT_Point& centroid,
                               size_t first,
                               size_t last,
                               float &outL1, float &outL2, float &outL3,
                               CT_Point &outV1, CT_Point &outV2, CT_Point &outV3)
{
    CT_PointAccessor ptAccessor;

    // 2 - Create matrix of centered points
    Eigen::MatrixXd centeredPoints( 3, last - first );
    for ( size_t i = first ; i < last ; i++ )
    {
        centeredPoints.col(i - first) = ptAccessor.constPointAt( indexCloud->indexAt(i) ) - centroid;
    }

    // 2 - Create covariance matrix
    Eigen::Matrix3d cov = centeredPoints * centeredPoints.transpose();

    // 3 - Compute eigenvalues and eigenvectors
    Eigen::EigenSolver< Eigen::Matrix3d > eigenSolver;
    eigenSolver.compute( cov );

    // 4 - Get the eigenvalues and eigenvectors
    Eigen::Vector3cd eigenValues = eigenSolver.eigenvalues();   // Each row of the vector is an eigenvalue of the covariance matrix
    Eigen::Matrix3cd eigenVectors = eigenSolver.eigenvectors(); // The columnsof this matrix are the associated eigenvectors

    //assert( !(eigenVectors(0,0) == eigenVectors(0,1) && eigenVectors(1,0) == eigenVectors(1,1) && eigenVectors(2,0) == eigenVectors(2,1) ) );
    //assert( !(eigenVectors(0,0) == eigenVectors(0,2) && eigenVectors(1,0) == eigenVectors(1,2) && eigenVectors(2,0) == eigenVectors(2,2) ) );
    //assert( !(eigenVectors(0,1) == eigenVectors(0,2) && eigenVectors(1,1) == eigenVectors(1,2) && eigenVectors(2,1) == eigenVectors(2,2) ) );

    // 5 - Sort eigenvalues and eigenvectors in decrescent order
//    qDebug() << "Valeurs propres : " << eigenValues(0).real() << eigenValues(1).real() << eigenValues(2).real();
//    qDebug() << "Vec1 " << eigenVectors(0,0).real() << eigenVectors(0,1).real() << eigenVectors(0,1).real();
//    qDebug() << "Vec2 " << eigenVectors(1,0).real() << eigenVectors(1,1).real() << eigenVectors(1,1).real();
//    qDebug() << "Vec3 " << eigenVectors(2,0).real() << eigenVectors(2,1).real() << eigenVectors(2,1).real();
    sortEigenValues( eigenValues, eigenVectors );
//    qDebug() << "Valeurs propres : " << eigenValues(0).real() << eigenValues(1).real() << eigenValues(2).real();
//    qDebug() << "Vec1 " << eigenVectors(0,0).real() << eigenVectors(0,1).real() << eigenVectors(0,1).real();
//    qDebug() << "Vec2 " << eigenVectors(1,0).real() << eigenVectors(1,1).real() << eigenVectors(1,1).real();
//    qDebug() << "Vec3 " << eigenVectors(2,0).real() << eigenVectors(2,1).real() << eigenVectors(2,1).real();
//    qDebug() << "----------------------";

    //assert( !(eigenVectors(0,0) == eigenVectors(0,1) && eigenVectors(1,0) == eigenVectors(1,1) && eigenVectors(2,0) == eigenVectors(2,1) ) );
    //assert( !(eigenVectors(0,0) == eigenVectors(0,2) && eigenVectors(1,0) == eigenVectors(1,2) && eigenVectors(2,0) == eigenVectors(2,2) ) );
    //assert( !(eigenVectors(0,1) == eigenVectors(0,2) && eigenVectors(1,1) == eigenVectors(1,2) && eigenVectors(2,1) == eigenVectors(2,2) ) );

    // 6 - Parse the results into float and CT_Point
    //     Eigenvalues of a covariancematrix are always real
    outL1 = eigenValues(0).real();
    outL2 = eigenValues(1).real();
    outL3 = eigenValues(2).real();

    for ( int i = 0 ; i < 3 ; i++ )
    {
        outV1(i) = eigenVectors(i,0).real();
        outV2(i) = eigenVectors(i,1).real();
        outV3(i) = eigenVectors(i,2).real();
    }

    //assert( !( outV1(0) == outV2(0) && outV1(1) == outV2(1) && outV1(2) == outV2(2) ) );
    //assert( !( outV1(0) == outV3(0) && outV1(1) == outV3(1) && outV1(2) == outV3(2) ) );
    //assert( !( outV2(0) == outV3(0) && outV2(1) == outV3(1) && outV2(2) == outV3(2) ) );

    outV1.normalize();
    outV2.normalize();
    outV3.normalize();
}

void NORM_Tools::NORM_PCA::sortEigenValues(Eigen::Vector3cd& eigenValues,
                                           Eigen::Matrix3cd& eigenVectors )
{
    // On fait un tri brut, pour un vecteur de 3 valeurs ce n'est pas grave
    float maxValue;                     // eigenvalue max
    float currValue;                    // current eigenvalue
    int maxID;                          // id of themaximum eigenvalue
    std::complex<float> swapVal;        // swap value for permutation
    Eigen::Vector3cd swapVec;           // swap vector for permutation

    // For each eigenvalue
    for ( int i = 0 ; i < 3 ; i++ )
    {
        maxID = i;
        maxValue = eigenValues(i).real();

        // On la compare a toutes les autres restantes
        for ( int j = i+1 ; j < 3 ; j++ )
        {
            currValue = eigenValues(j).real();
            if( currValue > maxValue )
            {
                maxValue = currValue;
                maxID= j;
            }
        }

        // On permute les indices maxID et i
        swapVal = eigenValues(i);
        swapVec = eigenVectors.col(i);
        eigenValues(i) = eigenValues(maxID);
        eigenVectors.col(i) = eigenVectors.col(maxID);
        eigenValues(maxID) = swapVal;
        eigenVectors.col(maxID) = swapVec;
    }
}

CT_Point NORM_Tools::NORM_PCA::getCentroid(const CT_AbstractPointCloudIndex *indexCloud)
{
    CT_PointAccessor ptAccessor;
    CT_Point centroid = createCtPoint();

    size_t nbPoints = indexCloud->size();
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        centroid += ptAccessor.constPointAt( indexCloud->indexAt(i) );
    }


    if(nbPoints != 0)
        centroid /= nbPoints;

    return centroid;
}

CT_Point NORM_Tools::NORM_PCA::getCentroid(const CT_AbstractPointCloudIndex *indexCloud, size_t indexFirst, size_t indexLast )
{
    CT_Point centroid = createCtPoint();

    if ( indexFirst < indexLast )
    {
        CT_PointAccessor ptAccessor;

        for ( size_t i = indexFirst ; i < indexLast ; i++ )
        {
            centroid += ptAccessor.constPointAt( indexCloud->indexAt(i) );
        }

        centroid /= ( ((float)indexLast) - ((float)indexFirst) );
    }

    return centroid;
}

CT_Point NORM_Tools::NORM_PCA::getCentroid(const CT_MPCIR& indexCloud)
{
    if( indexCloud.data() != NULL )
    {
        return getCentroid( indexCloud->abstractCloudIndexT() );
    }

    return CT_Point();
}

CT_Point NORM_Tools::NORM_PCA::getCentroid(const CT_MPCIR &indexCloud, size_t indexFirst, size_t indexLast )
{
    if( indexCloud.data() != NULL )
    {
        return getCentroid( indexCloud->abstractCloudIndexT(), indexFirst, indexLast );
    }

    return CT_Point();
}
