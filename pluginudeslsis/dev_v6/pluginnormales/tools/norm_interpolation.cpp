#include "norm_interpolation.h"

float NORM_Tools::NORM_Interpolation::linearInterpolation( float xa, float xb, float ya, float yb, float valToInterpolate )
{
    float a = (ya-yb)/(xa-xb);
    return ( ( a * valToInterpolate ) + ( ya - (a*xa) ) );
}

void NORM_Tools::NORM_Interpolation::linearInterpolation( const CT_Point &inputLower, const CT_Point &inputUpper,
                                                          const CT_Point &outputLower, const CT_Point &outputUpper,
                                                          const CT_Point &pointToInterpolate,
                                                          CT_Point &outInterpolatedPoint)
{
    // Thre independent interpolations (one for each coordinate)
    for( size_t i = 0 ; i < 3 ; i++ )
    {
        outInterpolatedPoint(i) = NORM_Tools::NORM_Interpolation::linearInterpolation( inputLower(i), inputUpper(i),
                                                                                       outputLower(i), outputUpper(i),
                                                                                       pointToInterpolate(i) );
    }
}


float NORM_Tools::NORM_Interpolation::wendlandCoeff(float support, float radius)
{
    if( radius > support )
    {
        return 0;
    }

    float fakeRadius = radius / support;
    return ( (1 - fakeRadius )*(1 - fakeRadius )*(1 - fakeRadius )*(1 - fakeRadius )
             *
             (4 * fakeRadius + 1 ) );
}
