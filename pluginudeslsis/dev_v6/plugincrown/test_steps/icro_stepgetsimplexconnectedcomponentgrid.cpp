#include "icro_stepgetsimplexconnectedcomponentgrid.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_iterator/ct_pointiterator.h"

#include <QQueue>

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltResultConnectedComponentNonMultis "rsltResultConnectedComponentsGrid"

// Resultat contient un groupe general
#define DEFout_grpGeneral "grpGeneral"

// Groupe general contient
// - un groupe de groupe de cercles pour les arbres
// - une grille 3D qui contient les composantes connexes
#define DEFout_grpGrpTreeStems "grpGrpTreeStems"
#define DEFout_itmConnectedComponentsGrid "itmConnectedComponentsGrid"

// Le groupe de groupe de cercles contient un groupe de cercles
#define DEFout_grpTreeStems "grpTreeStems"

// le groupe de cercle contient un cercle
#define DEFout_itmTreeStems "itmTreeStems"

// Constructor : initialization of parameters
ICRO_StepGetSimplexConnectedComponentGrid::ICRO_StepGetSimplexConnectedComponentGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _intensityThreshold = 20;
    _minPixelsInComponent = 10;
    _resolution = 8;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetSimplexConnectedComponentGrid::getStepDescription() const
{
    return tr("Identifie les composantes connexes d'une grille 3D separes par une valeur minimum (int) selon une approche multi echelle (superpixels)");
}

// Step detailled description
QString ICRO_StepGetSimplexConnectedComponentGrid::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepGetSimplexConnectedComponentGrid::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetSimplexConnectedComponentGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetSimplexConnectedComponentGrid(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetSimplexConnectedComponentGrid::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster and scene"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("Intensity raster (int)"));
}

// Creation and affiliation of OUT models
void ICRO_StepGetSimplexConnectedComponentGrid::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponentNonMultis = createNewOutResultModel(DEFout_rsltResultConnectedComponentNonMultis, tr("Connected components (grid3d)"));

    // Resultat contient un groupe general
    res_rsltConnectedComponentNonMultis->setRootGroup(DEFout_grpGeneral, new CT_StandardItemGroup(), tr("TreeStems and Connected Components"));

    // Groupe general contient
    // - un groupe de groupe de cercles pour les arbres
    // - une grille 3D qui contient les composantes connexes
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGeneral, DEFout_grpGrpTreeStems, new CT_StandardItemGroup(), tr("Group Group Tree Stems"));
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpGeneral, DEFout_itmConnectedComponentsGrid, new CT_Grid3D<int>(), tr("Connected Components"));

    // Le groupe de groupe de cercles contient un groupe de cercles
    res_rsltConnectedComponentNonMultis->addGroupModel(DEFout_grpGrpTreeStems, DEFout_grpTreeStems, new CT_StandardItemGroup(), tr("Group Circle"));

    // le groupe de cercle contient un cercle
    res_rsltConnectedComponentNonMultis->addItemModel(DEFout_grpTreeStems, DEFout_itmTreeStems, new CT_Circle(), tr("Circle") );
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetSimplexConnectedComponentGrid::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Valeur min", "", 0, 9999, 4, _intensityThreshold );
    configDialog->addInt("n Min pixels","",1,999999999,_minPixelsInComponent);
    configDialog->addInt("Resolution","",1,999999999, _resolution);
}

void ICRO_StepGetSimplexConnectedComponentGrid::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmIntensityGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmIntensityGrid != NULL )
        {
            // Resultat contient un groupe general
            CT_StandardItemGroup* grpGeneral = new CT_StandardItemGroup(DEFout_grpGeneral, resOut_rsltRaster3D );
            resOut_rsltRaster3D->addGroup( grpGeneral );

            // Creation de la grille contenant les composantes connexes
            CT_Grid3D<int>* itemOut_itmConnectedComponentsGrid = new CT_Grid3D<int>( DEFout_itmConnectedComponentsGrid, resOut_rsltRaster3D,
                                                                                     itemIn_itmIntensityGrid->minX(), itemIn_itmIntensityGrid->minY(), itemIn_itmIntensityGrid->minZ(),
                                                                                     itemIn_itmIntensityGrid->xArraySize(), itemIn_itmIntensityGrid->yArraySize(), itemIn_itmIntensityGrid->zArraySize(),
                                                                                     itemIn_itmIntensityGrid->resolution(),
                                                                                     std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            // Charge les cercles a utiliser comme position de reference des troncs
            _circles = getCirclesFromFile( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );

            // Calcule la division en composantes connexes simplexes en multi resolution
            QVector<ConnectedComponent *> components;
            computeConnectedComponentsUntilSimplexes( itemIn_itmIntensityGrid,
                                                      itemOut_itmConnectedComponentsGrid,
                                                      _intensityThreshold,
                                                      _resolution,
                                                      _minPixelsInComponent,
                                                      _circles,
                                                      components );

            // Mise a jour des min max de la grille des composantes connexes pour affichage en couleurs
            itemOut_itmConnectedComponentsGrid->computeMinMax();

            // Ajout de la grille au resultat :
            // Le groupe general contient une grille 3D
            grpGeneral->addItemDrawable( itemOut_itmConnectedComponentsGrid );

            // Ajoute les cercles au resultat :
            // Le groupe general contient un groupe de groupe de cercles
            CT_StandardItemGroup* grpGrpCircle = new CT_StandardItemGroup(DEFout_grpGrpTreeStems, resOut_rsltRaster3D );
            grpGeneral->addGroup( grpGrpCircle );

            foreach( CT_CircleData* circle, _circles )
            {
                // Le groupe de groupe de cercles contient un groupe de cercles
                CT_StandardItemGroup* grpCircle = new CT_StandardItemGroup(DEFout_grpTreeStems, resOut_rsltRaster3D );
                grpGrpCircle->addGroup( grpCircle );

                // Le groupe de cercles contient un cercle
                grpCircle->addItemDrawable( new CT_Circle( DEFout_itmTreeStems, resOut_rsltRaster3D, circle ) );
            }
        }
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::computeConnectedComponentsUntilSimplexes(CT_Grid3D<int> *inputGrid,
                                                                                         CT_Grid3D<int> *outConnectedComponentsGrid,
                                                                                         double minIntensity,
                                                                                         int superPixelResolution,
                                                                                         int minComponentSize,
                                                                                         QVector< CT_CircleData* >& circles,
                                                                                         QVector<ConnectedComponent *>& outConnectedComponents)
{
    // Calcule les composantes connexes a la resolution initiale et les etiquette dans la grille des composantes connexes
    QVector< ConnectedComponent* > highResolutionConnectedComponents;
    labelSuperConnectedComponents( inputGrid,
                                   outConnectedComponentsGrid,
                                   minIntensity,
                                   superPixelResolution,
                                   highResolutionConnectedComponents );


    // Calcule le nombre de cercles qui intersectent chaque composante
    getComponentsType( outConnectedComponentsGrid,
                       circles,
                       highResolutionConnectedComponents,
                       minComponentSize );

    // Lance la division recursive pour obtenir des composantes SIMPLEX
    foreach( ConnectedComponent* component, highResolutionConnectedComponents )
    {
        splitComponentUntilSimplexes( component,
                                      outConnectedComponents,
                                      _resolution,
                                      _intensityThreshold,
                                      _minPixelsInComponent,
                                      inputGrid,
                                      outConnectedComponentsGrid );
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::labelSuperConnectedComponents(CT_Grid3D<int> *inputGrid,
                                                                              CT_Grid3D<int> *outConnectedComponentsGrid,
                                                                              double minIntensity,
                                                                              int superPixelResolution,
                                                                              QVector< ConnectedComponent* >& outConnectedComponents )
{
    // Calcule le nombre de super pixels dans la grille pour la resolution donnee
    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();

    int xdimSuperPixel = xdim / superPixelResolution;
    int ydimSuperPixel = ydim / superPixelResolution;
    int zdimSuperPixel = zdim / superPixelResolution;

    if( (xdim % superPixelResolution) != 0 )
    {
        xdimSuperPixel++;
    }

    if( (ydim % superPixelResolution) != 0 )
    {
        ydimSuperPixel++;
    }

    if( (zdim % superPixelResolution) != 0 )
    {
        zdimSuperPixel++;
    }

    // Recherche de composantes connexes de superpixels
    // On cherche dans tous les superpixels un superpixel qui peut servir de graine a unecomposante connexe
    for( int i = 0 ; i < xdimSuperPixel ; i++ )
    {
        for( int j = 0 ; j < ydimSuperPixel ; j++ )
        {
            for( int k = 0 ; k < zdimSuperPixel ; k++ )
            {
                SuperPixel currentPixel(i,j,k,superPixelResolution,inputGrid);

                // Si le super pixel courant contient un depart de graine pour une composante connexe
                if( currentPixel.containsNonLabeledPixelAboveValue(inputGrid,
                                                                   outConnectedComponentsGrid,
                                                                   minIntensity ) )
                {
                    // On recupere la composante connexe issue de ce super pixel
                    ConnectedComponent* currentConnectedComponent = new ConnectedComponent();
                    labelSuperConnectedComponentFromSeed( inputGrid,
                                                          outConnectedComponentsGrid,
                                                          minIntensity,
                                                          i, j, k,
                                                          ConnectedComponent::_labelID,
                                                          superPixelResolution,
                                                          currentConnectedComponent );

                    // On met a jour son label
                    currentConnectedComponent->_label = ConnectedComponent::_labelID;

                    // Et on l'ajoute a l'ensemble des composantes
                    outConnectedComponents.push_back( currentConnectedComponent );

                    // Et on avance le label de composante connexe
                    ConnectedComponent::_labelID++;
                }
            }
        }
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *hitGrid,
                                                                                     CT_Grid3D<int> *outLabeledGrid,
                                                                                     double minValue,
                                                                                     int startx,
                                                                                     int starty,
                                                                                     int startz,
                                                                                     int label,
                                                                                     int resolution,
                                                                                     ConnectedComponent* outSuperComponent)
{
    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< SuperPixel > f;

    // WARNING : Les startx starty startz sont les coordonnees du super pixel
    SuperPixel firstSuperPixel( startx,
                                starty,
                                startz,
                                resolution,
                                hitGrid );

    // On marque le premier super pixel
    firstSuperPixel.setValueInGrid( outLabeledGrid,
                                    label );

    // Ajoute le super pixel a la composante
    outSuperComponent->_superPixels.append( firstSuperPixel );

    // Ajoute les pixels du super pixels a la composante
    addPixelsToSuperConnectedComponent( firstSuperPixel,
                                        outSuperComponent );

    // On l'ajoute a la file
    f.enqueue( firstSuperPixel );

    // Tant que la file n'est pas vide
    SuperPixel currentSuperPixel( resolution );
    while( !f.empty() )
    {
        // On prend le premier element de la file
        currentSuperPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            if( currentSuperPixel._p.x() + i >= 0 )
            {
                for( int j = -1 ; j <= 1 ; j++ )
                {
                    if( currentSuperPixel._p.y() + j >= 0 )
                    {
                        for( int k = -1 ; k <= 1 ; k++ )
                        {
                            if( currentSuperPixel._p.z() + k >= 0 )
                            {
                                SuperPixel neiSuperPixel( currentSuperPixel._p.x() + i,
                                                          currentSuperPixel._p.y() + j,
                                                          currentSuperPixel._p.z() + k,
                                                          resolution,
                                                          hitGrid );

                                // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                                // Si le voisin n'est pas deja marque et si il a une valeur superieure a la valeur minimum souhaitee
                                if( neiSuperPixel.containsNonLabeledPixelAboveValue( hitGrid,
                                                                                     outLabeledGrid,
                                                                                     minValue ) )
                                {
                                    // Marque le super pixel
                                    neiSuperPixel.setValueInGrid( outLabeledGrid,
                                                                  label );

                                    // Ajoute le super pixel a la composante
                                    outSuperComponent->_superPixels.append( neiSuperPixel );

                                    // Ajoute les pixels a la composante
                                    addPixelsToSuperConnectedComponent( neiSuperPixel,
                                                                        outSuperComponent );

                                    // Ajoute le super pixel a al file
                                    f.enqueue( neiSuperPixel );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::markSuperComponent(ConnectedComponent *superComponent,
                                                                                CT_Grid3D<int> *grid,
                                                                                int label)
{
    foreach( SuperPixel p, superComponent->_superPixels )
    {
        p.setValueInGrid( grid, label );
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::updateComponentsType(QVector<ConnectedComponent *> &components,
                                                                     int minComponentSize)
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minComponentSize )
        {
            component->_type = TOO_SMALL;
        }

        else
        {
            if( component->_nCircles == 0 )
            {
                component->_type = EMPTY;
            }

            else if( component->_nCircles == 1 )
            {
                component->_type = SIMPLEX;
            }

            else if( component->_nCircles >= 2 )
            {
                component->_type = COMPLEX;
            }
        }
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::addPixelsToSuperConnectedComponent(SuperPixel& superPixel,
                                                                                                ConnectedComponent *outSuperComponent )
{
    // Recupere les pixels du super pixel
    QVector< Eigen::Vector3i > pixelsFromSuperPixel;
    superPixel.getPixels( pixelsFromSuperPixel );

    // Ajoute le vecteur de pixels
    outSuperComponent->_pixels << pixelsFromSuperPixel;
}

ConnectedComponent *ICRO_StepGetSimplexConnectedComponentGrid::getComponentFromLabel(QVector<ConnectedComponent *> &components,
                                                                                                  int label )
{
    foreach( ConnectedComponent* component, components )
    {
        if( component->_label == label )
        {
            return component;
        }
    }

    return NULL;
}

void ICRO_StepGetSimplexConnectedComponentGrid::getComponentsType(CT_Grid3D<int> *labeledGrid,
                                                                  QVector< CT_CircleData* >& circles,
                                                                  QVector< ConnectedComponent* >& components,
                                                                  int minLabelSize )
{
    // Attribue le type TOO_SMALL
    foreach( ConnectedComponent* component, components )
    {
        if( component->_pixels.size() < minLabelSize )
        {
            component->_type = TOO_SMALL;
        }
    }

    // Pour chaque cercle
    foreach( CT_CircleData* circleData, circles )
    {
        // Labels traverses par le cercle
        QVector<int> labelsOfCircle;

        // On regarde si le cercle touche un pixel de la composante courante
        // BBox du cercle dans la grille
        size_t botx, boty, botz;
        labeledGrid->colX( circleData->getCenter().x() - circleData->getRadius(), botx );
        labeledGrid->linY( circleData->getCenter().y() - circleData->getRadius(), boty );
        labeledGrid->levelZ( circleData->getCenter().z(), botz );

        size_t topx, topy, topz;
        labeledGrid->colX( circleData->getCenter().x() + circleData->getRadius(), topx );
        labeledGrid->linY( circleData->getCenter().y() + circleData->getRadius(), topy );
        labeledGrid->levelZ( circleData->getCenter().z(), topz );

        // Pour chaque pixel qu'intersecte le cercle courant
        for( int i = botx ; i <= topx ; i++ )
        {
            for( int j = boty ; j <= topy ; j++ )
            {
                for( int k = botz ; k <= topz ; k++ )
                {
                    // Si une etiquette coupe le cercle : la valeur a ete etiquettee (different de NA)
                    if( labeledGrid->value(i,j,k) != labeledGrid->NA() )
                    {
                        ConnectedComponent* fromLabel = getComponentFromLabel( components, labeledGrid->value(i,j,k) );

                        // Si la composante n'est pas trop petite
                        if( fromLabel != NULL && fromLabel->_type != TOO_SMALL )
                        {
                            // Et qu'on ne l'avait pas encore traversee pour ce cercle
                            if( !labelsOfCircle.contains( labeledGrid->value(i,j,k) ) )
                            {
                                // On recupere la composante associee
                                ConnectedComponent* component = getComponentFromLabel( components,
                                                                                       labeledGrid->value(i,j,k) );

                                if( component != NULL )
                                {
                                    // On l'ajoute au tableau des composantes traversees
                                    labelsOfCircle.push_back( labeledGrid->value(i,j,k) );

                                    // La composante intersecte un cercle de plus
                                    component->_nCircles++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // Mise a jour des types des composantes en fonction du nombre de cercles qui intersectent
    updateComponentsType( components, _minPixelsInComponent );
}

void ICRO_StepGetSimplexConnectedComponentGrid::splitComplexComponent(ConnectedComponent* componentToBeSplitted,
                                                                      CT_Grid3D<int> *valueGrid,
                                                                      CT_Grid3D<int> *labelGrid,
                                                                      int initialResolution,
                                                                      int minValue,
                                                                      QVector< ConnectedComponent* >& outComponents )
{
    // Si les super pixels sont divisibles
    if( initialResolution > 1 )
    {
        // On clear les labels de la composante
        markSuperComponent( componentToBeSplitted,
                            labelGrid,
                            labelGrid->NA() );

        // On transforme chaque super pixel de la composante en super pixel de resolution plus fine
        int currentResolution = initialResolution / 2;
        QVector< SuperPixel > superPixelsAtCurrentResolution;
        foreach( SuperPixel p, componentToBeSplitted->_superPixels )
        {
            // Chaque super pixel se transforme en huit super pixels de resolution plus fine
            for( int i = 0 ; i < 2 ; i++ )
            {
                for( int j = 0 ; j < 2 ; j++ )
                {
                    for( int k = 0 ; k < 2 ; k++ )
                    {
                        SuperPixel subSuperPixel( 2*p._p.x() + i,
                                                  2*p._p.y() + j,
                                                  2*p._p.z() + k,
                                                  currentResolution,
                                                  valueGrid );
                        superPixelsAtCurrentResolution.append( subSuperPixel );
                    }
                }
            }
        }

        // On remplace le tableau de super pixels
        componentToBeSplitted->_superPixels.clear();
        componentToBeSplitted->_superPixels = superPixelsAtCurrentResolution;

        // ////////////////////////////////////////////////////////////////////////////////
        // On lance une recherche de composantes connexes a partir de cette resolution
        // ////////////////////////////////////////////////////////////////////////////////
        foreach( SuperPixel sp, componentToBeSplitted->_superPixels )
        {
            if( sp.containsNonLabeledPixelAboveValue( valueGrid, labelGrid, minValue ) )
            {
                ConnectedComponent* connectedComponentAtCurrentResolution = new ConnectedComponent();
                labelSuperConnectedComponentFromSeed( valueGrid,
                                                      labelGrid,
                                                      minValue,
                                                      sp._p.x(), sp._p.y(), sp._p.z(),
                                                      ConnectedComponent::_labelID,
                                                      currentResolution,
                                                      connectedComponentAtCurrentResolution );

                    // On l'ajoute a l'ensemble des composantes
                    outComponents.push_back( connectedComponentAtCurrentResolution );

                    // On met a jour son label
                    connectedComponentAtCurrentResolution->_label = ConnectedComponent::_labelID;

                    // Et on avance le label
                    ConnectedComponent::_labelID++;
            }
        }
    }

    else
    {
        // La resolution est de 1 donc on renvoie la composante telle quelle (on est deja a l'echelle du pixel)
        outComponents.push_back( componentToBeSplitted );
    }
}

void ICRO_StepGetSimplexConnectedComponentGrid::splitComponentUntilSimplexes(ConnectedComponent* componentToBeSplitted,
                                                                             QVector<ConnectedComponent *>& outSplittedComponents,
                                                                             int initialResolution,
                                                                             int minValue,
                                                                             int minLabelSize,
                                                                             CT_Grid3D<int>* intesityGrid,
                                                                             CT_Grid3D<int>* labelGrid )
{
    // Si que la composante est complexe
    if( componentToBeSplitted->_type == COMPLEX )
    {
        // Et que les superpixels contiennent plus d'un super pixel (sinon on est deja au niveau du pixel, on ne peut pas descendre plus bas)
        if( initialResolution > 1 )
        {
            // On divise la composante complexe mere et on recupere les composantes filles resultantes
            QVector<ConnectedComponent *> splitedComponentsAtCurrentResolution;
            splitComplexComponent( componentToBeSplitted,
                                   intesityGrid,
                                   labelGrid,
                                   initialResolution,
                                   minValue,
                                   splitedComponentsAtCurrentResolution );

            // On rattache les composantes filles a leur mere
            foreach( ConnectedComponent* splitedComponentAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                componentToBeSplitted->_children.push_back( splitedComponentAtCurrentResolution );
            }

            // Calcule le nombre de cercles qui intersectent chaque composante
            getComponentsType( labelGrid,
                               _circles,
                               splitedComponentsAtCurrentResolution,
                               _minPixelsInComponent );

            // Pour chaque composante creee par la division on continue le processus
            foreach( ConnectedComponent* componentToBeSplittedAtCurrentResolution, splitedComponentsAtCurrentResolution )
            {
                splitComponentUntilSimplexes( componentToBeSplittedAtCurrentResolution,
                                              outSplittedComponents,
                                              initialResolution / 2,
                                              minValue,
                                              minLabelSize,
                                              intesityGrid,
                                              labelGrid );
            }
        }

        // Si la composante est complexe mais que l'on ne peut plus reduire la resolution
        else
        {
            // On ajoute simplement la composante au resultat
            componentToBeSplitted->_resolutionFinale = initialResolution;
            outSplittedComponents.push_back( componentToBeSplitted );
        }
    }

    // Si la composante est VIDE ou SIMPLEX (les TOO_SMALL ne sont pas traitees ici car elles n'apparaissent pas dans le tableau de composantes connexes)
    else
    {
        // On l'ajoute a la sortie
        componentToBeSplitted->_resolutionFinale = initialResolution;
        outSplittedComponents.push_back( componentToBeSplitted );
    }
}

QVector<CT_CircleData *> ICRO_StepGetSimplexConnectedComponentGrid::getCirclesFromFile(QString dir, QString fileName)
{
    QVector<CT_CircleData*> rslt;
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            CT_CircleData* currCircle = new CT_CircleData( center, direction, r );

            if( circleIntersect2D( rslt, currCircle ) )
            {
                delete currCircle;
            }

            else
            {
                rslt.push_back( currCircle );
            }

        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }

    qDebug() << "Loaded " << rslt.size() << " cercles";
    return rslt;
}

bool ICRO_StepGetSimplexConnectedComponentGrid::circleIntersect2D(QVector<CT_CircleData *> &circles, CT_CircleData *c)
{
    foreach( CT_CircleData* curr, circles )
    {
        if( circleIntersect2D( curr, c ) )
        {
            return true;
        }
    }

    return false;
}

bool ICRO_StepGetSimplexConnectedComponentGrid::circleIntersect2D(CT_CircleData *c1, CT_CircleData *c2)
{
    CT_Point ce1 = c1->getCenter();
    CT_Point ce2 = c2->getCenter();
    float dist2D = sqrt( (ce1.x()-ce2.x())*(ce1.x()-ce2.x()) + (ce1.y()-ce2.y())*(ce1.y()-ce2.y()) );

    if( dist2D < (c1->getRadius() + c2->getRadius()) )
    {
        return true;
    }

    return false;
}
