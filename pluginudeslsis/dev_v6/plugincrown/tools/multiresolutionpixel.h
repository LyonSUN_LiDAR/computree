#ifndef MULTIRESOLUTIONPIXEL_H
#define MULTIRESOLUTIONPIXEL_H

#include <algorithm>
#include <Eigen/Core>
#include "ct_itemdrawable/ct_grid3d.h"
#include "tools/icro_customvoronoigrid.h"

#define BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL(x, y, z) \
{ \
    /* Parcours tous les pixels du super pixel */ \
    for( x = _botx ; x < _topx ; x++ ) \
    { \
        for( y = _boty ; y < _topy ; y++ ) \
        { \
            for( z = _botz ; z < _topz ; z++ ) \
            {

#define END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL(x, y, z) \
            } \
        } \
    } \
}

class ICRO_CustomVoronoiGrid;
class ConnectedComponent;

class SuperPixel
{
public :
    SuperPixel();
    SuperPixel( const SuperPixel& sp );
    SuperPixel( int resolution );
    SuperPixel( int x,
                int y,
                int z,
                int res,
                CT_Grid3D<int>* grid );

    SuperPixel( int x,
                int y,
                int z,
                int res,
                ICRO_CustomVoronoiGrid* grid );

    SuperPixel(Eigen::Vector3i& v, int res, CT_Grid3D<int> *grid);

    SuperPixel& operator=(const SuperPixel& sp);

    bool containsNonLabeledPixelAboveValue( CT_Grid3D<int> *valueGrid,
                                            CT_Grid3D<int> *labelGrid,
                                            int valueMin );

    bool containsNonLabeledPixelAboveValueVoronoi( CT_Grid3D<int> *valueGrid,
                                                   ICRO_CustomVoronoiGrid *labelGrid,
                                                   int valueMin );

    bool containsValue( CT_Grid3D<int> *grid,
                        int value );

    bool containsValueAbove( CT_Grid3D<int> *grid,
                             int value );

    void setValueInGrid(CT_Grid3D<int>* grid,
                        int value );

    void setDistanceInVoronoiGrid(ICRO_CustomVoronoiGrid* grid,
                                  int value );

    void addComponentInVoronoiGrid(ICRO_CustomVoronoiGrid* grid,
                                   ConnectedComponent* component );

    void getPixels( QVector< Eigen::Vector3i >& outPixels );

    inline void setx( int x, CT_Grid3D<int>* grid )
    {
        _p.x() = x;
        _botx = _p.x() * _res;
        _topx = std::min( ((_p.x()+1) * _res), (int)(grid->xArraySize()-1 ));
    }

    inline void sety( int y, CT_Grid3D<int>* grid )
    {
        _p.y() = y;
        _boty = _p.y() * _res;
        _topy = std::min( ((_p.y()+1) * _res), (int)(grid->yArraySize()-1 ));
    }

    inline void setz( int z, CT_Grid3D<int>* grid )
    {
        _p.z() = z;
        _botz = _p.z() * _res;
        _topz = std::min( ((_p.z()+1) * _res), (int)(grid->zArraySize()-1 ));
    }

    template < typename DataT >
    inline void getBBoxInGrid( int& outBotx,
                               int& outBoty,
                               int& outBotz,
                               int& outTopx,
                               int& outTopy,
                               int& outTopz,
                               CT_Grid3D<DataT>* grid )
    {
        // Convertit le super pixel en pixels
        outBotx = _p.x() * _res;
        outBoty = _p.y() * _res;
        outBotz = _p.z() * _res;

        outTopx = std::min( ((_p.x()+1) * _res), (int)(grid->xArraySize()-1 ));
        outTopy = std::min( ((_p.y()+1) * _res), (int)(grid->yArraySize()-1 ));
        outTopz = std::min( ((_p.z()+1) * _res), (int)(grid->zArraySize()-1 ));
    }

    inline void getBBoxInVoronoiGrid( int& outBotx,
                                      int& outBoty,
                                      int& outBotz,
                                      int& outTopx,
                                      int& outTopy,
                                      int& outTopz,
                                      ICRO_CustomVoronoiGrid* grid );

    Eigen::Vector3i _p;
    int             _res;
    int             _botx;
    int             _boty;
    int             _botz;
    int             _topx;
    int             _topy;
    int             _topz;
};

#endif // MULTIRESOLUTIONPIXEL_H
