/************************************************************************************
* Filename :  lsis_exportergrid4d.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_exportergrid4d.h"

// Exporte une grille 4D
#include "ct_itemdrawable/ct_grid4d.h"

// Acces au systeme de log de computree
#include "ct_global/ct_context.h"

//Qt dependencies
#include <math.h>
#include <QMessageBox>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

LSIS_ExporterGrid4D::LSIS_ExporterGrid4D() : CT_AbstractExporter()
{
}

LSIS_ExporterGrid4D::~LSIS_ExporterGrid4D()
{

}

QString LSIS_ExporterGrid4D::getExporterCustomName() const
{
    return "Grilles 4D, ACSII ESRI Grid";
}

void LSIS_ExporterGrid4D::init()
{
    addNewExportFormat(FileFormat("ASC", tr("Fichiers Grid 4D (ASCII)")));
}

bool LSIS_ExporterGrid4D::setItemDrawableToExport(const QList<CT_AbstractItemDrawable*> &list)
{
    clearErrorMessage();

    QList<CT_AbstractItemDrawable*> myList;
    QListIterator<CT_AbstractItemDrawable*> it(list);
    int nGrids = 0;

    while(it.hasNext())
    {
        CT_AbstractItemDrawable *item = it.next();
        if(dynamic_cast<CT_AbstractGrid4D*>(item) != NULL)
        {
            myList.append(item);
            ++nGrids;
        }
    }

    if(nGrids == 0)
    {
        setErrorMessage(tr("Aucun ItemDrawable du type CT_AbstractGrid4D"));
        return false;
    }

    return CT_AbstractExporter::setItemDrawableToExport(myList);
}

bool LSIS_ExporterGrid4D::configureExport()
{
    if(!errorMessage().isEmpty())
    {
        QMessageBox::critical(NULL, tr("Erreur"), errorMessage(), QMessageBox::Ok);
        return false;
    }

    return true;
}

CT_AbstractExporter* LSIS_ExporterGrid4D::copy() const
{
    return new LSIS_ExporterGrid4D();
}

bool LSIS_ExporterGrid4D::protectedExportToFile()
{
    bool ok = true;

    QFileInfo exportPathInfo = QFileInfo(exportFilePath());
    QString path = exportPathInfo.path();
    QString baseName = exportPathInfo.baseName();
    QString suffix = "grid4d";

    QString indice = "";
    if (itemDrawableToExport().size() > 1) {indice = "_0";}
    int cpt = 0;

    QListIterator<CT_AbstractItemDrawable*> it(itemDrawableToExport());
    while (it.hasNext())
    {
        CT_AbstractGrid4D* item = dynamic_cast<CT_AbstractGrid4D*>(it.next());
        if (item != NULL)
        {
            QString filePath = QString("%1/%2%3.%4").arg(path).arg(baseName).arg(indice).arg(suffix);

            QFile file(filePath);

            if(file.open(QFile::WriteOnly))
            {
                QTextStream stream(&file);

                // write header
                size_t wdim = item->wdim();
                size_t xdim = item->xdim();
                size_t ydim = item->ydim();
                size_t zdim = item->zdim();

                stream << "wdim\t" << wdim << "\n";
                stream << "xdim\t" << xdim << "\n";
                stream << "ydim\t" << ydim << "\n";
                stream << "zdim\t" << zdim << "\n";

                stream << "wllcorner\t" << item->minW() << "\n";
                stream << "xllcorner\t" << item->minX() << "\n";
                stream << "yllcorner\t" << item->minY() << "\n";
                stream << "zllcorner\t" << item->minZ() << "\n";

                stream << "wcellsize\t" << item->wres() << "\n";
                stream << "xcellsize\t" << item->xres() << "\n";
                stream << "ycellsize\t" << item->yres() << "\n";
                stream << "zcellsize\t" << item->zres() << "\n";
                stream << "NODATA_value\t" << item->NAAsString() << "\n";

                // write data
                for (size_t ww = 0 ; ww < wdim ; ww++)
                {
                    for (size_t xx = 0 ; xx < xdim ; xx++)
                    {
                        for (size_t yy = 0 ; yy < ydim ; yy++)
                        {
                            for (size_t zz = 0 ; zz < zdim ; zz++)
                            {
                                size_t sIndex;
                                item->index(ww, xx, yy, zz, sIndex);
                                stream << item->valueAtIndexAsString(sIndex) << " ";
                            }
                            stream << "\n";
                        }
                    }
                }

                file.close();
            }

            else
            {
                ok = false;
            }

            indice = QString("_%1").arg(++cpt);
        }
    }

    return ok;
}
