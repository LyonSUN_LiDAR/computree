<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>LSIS_ExporterGrid4D</name>
    <message>
        <location filename="../exporter/lsis_exportergrid4d.cpp" line="56"/>
        <source>Fichiers Grid 4D (ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exporter/lsis_exportergrid4d.cpp" line="79"/>
        <source>Aucun ItemDrawable du type CT_AbstractGrid4D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exporter/lsis_exportergrid4d.cpp" line="90"/>
        <source>Erreur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_NewStepFastFilterHoughSpace</name>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="34"/>
        <source>2 - Filtre un espace de Hough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="40"/>
        <source>Si l&apos;algorithme qui crée l&apos;espace de Hough suppose que les directions des normales des points ne sont pas forcément dans la bonne direction, il génère des cercles dans les deux directions. C&apos;est pourquoi, si nous prenons l&apos;exemple d&apos;un tronc, il existe des valeurs élevées au centre du tronc mais aussi tout autour à une certaine distance. Ce filtre à pour but de supprimer les valeurs élevées en dehors du tronc. Pour chaque point du nuage l&apos;algorithme va cumuler les valeurs dans la direction de la normale et cumuler dans une autre variable les valeurs dans le sens opposé de la normale. Le ratio calculé est max(cumul1, cumul2)/min(cumul1, cumul2). Si ce ratio est inférieur au ratio minimum les deux valeurs sont supprimer de la grille, sinon la valeur la moins élevée est supprimée.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="70"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="71"/>
        <source>Normals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="72"/>
        <source>HoughSpace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="90"/>
        <source>Ratio minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_newstepfastfilterhoughspace.cpp" line="90"/>
        <source>Si le ratio calculé est inférieur à celui-ci toutes les valeurs du point sont supprimées, sinon les valeurs ayant eu le moins de votes cumulés sont supprimées. Voir la description de l&apos;étape pour plus d&apos;explication.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_ReaderGrid4D</name>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="60"/>
        <source>Chargement d&apos;un fichier grid4d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="71"/>
        <source>Grilles 4D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="101"/>
        <source>Unable to convert the ascii format to a integer at line 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="112"/>
        <source>Unable to convert the ascii format to a integer at line 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="123"/>
        <location filename="../reader/lsis_readergrid4d.cpp" line="134"/>
        <source>Unable to convert the ascii format to a integer at line 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="145"/>
        <location filename="../reader/lsis_readergrid4d.cpp" line="178"/>
        <source>Unable to convert the ascii format to a floatting point at line 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="156"/>
        <source>Unable to convert the ascii format to a floatting point at line 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="167"/>
        <source>Unable to convert the ascii format to a floatting point at line 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="189"/>
        <location filename="../reader/lsis_readergrid4d.cpp" line="200"/>
        <location filename="../reader/lsis_readergrid4d.cpp" line="211"/>
        <location filename="../reader/lsis_readergrid4d.cpp" line="222"/>
        <source>Unable to convert the ascii format to a floatting point at line 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readergrid4d.cpp" line="233"/>
        <source>Unable to convert the ascii format to a floatting point at line 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_ReaderHoughSpace4D</name>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="60"/>
        <source>Chargement d&apos;un espace de Hough 4D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="71"/>
        <source>Grilles 4D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="101"/>
        <source>Unable to convert the ascii format to a integer at line 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="112"/>
        <source>Unable to convert the ascii format to a integer at line 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="123"/>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="134"/>
        <source>Unable to convert the ascii format to a integer at line 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="145"/>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="178"/>
        <source>Unable to convert the ascii format to a floatting point at line 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="156"/>
        <source>Unable to convert the ascii format to a floatting point at line 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="167"/>
        <source>Unable to convert the ascii format to a floatting point at line 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="189"/>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="200"/>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="211"/>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="222"/>
        <source>Unable to convert the ascii format to a floatting point at line 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reader/lsis_readerhoughspace4d.cpp" line="233"/>
        <source>Unable to convert the ascii format to a floatting point at line 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepAddSetSnakes</name>
    <message>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="34"/>
        <source>Ajoute plusieurs snakes sous formede cercles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="40"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="68"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="69"/>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="70"/>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="71"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakes.cpp" line="72"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepAddSetSnakesOverDTM</name>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="37"/>
        <source>Charge des snakes et sort leur DBH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="43"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="64"/>
        <source>Result DTM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="66"/>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="75"/>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="76"/>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="77"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="67"/>
        <source>Digital Terrain Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="74"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsetsnakesoverdtm.cpp" line="78"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepAddSnake</name>
    <message>
        <location filename="../step/lsis_stepaddsnake.cpp" line="30"/>
        <source>Ajoute un ensemble de cercles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsnake.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsnake.cpp" line="64"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsnake.cpp" line="65"/>
        <location filename="../step/lsis_stepaddsnake.cpp" line="66"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepaddsnake.cpp" line="67"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepCreateHoughSpace</name>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="45"/>
        <source>1 - Créer un espace de Hough à partir d&apos;un nuage de points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="51"/>
        <source>Créer et remplir une grille 4D contenant les valeurs de la transformée de Hough pour chaque point du nuage. Cette étape à besoin d&apos;avoir un nuage de point avec des normales. Si votre nuage de points ne possède pas de normales vous pouvez les obtenir à l&apos;aide de l&apos;étape d&apos;estimation des normales contenue dans le plugin &quot;Toolkit&quot; par exemple. Chaque cellule de la grille représente un cercle potentiel en 3D et la transformée de Hough associe à chacun de ces cercles le nombre de points qui lui appartient. Des valeurs élevées vont se formées dans les cellules où le rayon du cercle s&apos;ajuste bien par rapport à un amat de points. L&apos;algorithme considère que la direction de la normale du point n&apos;est pas correctement calculée c&apos;est pourquoi le parcours de la grille est effectué dans la direction de la normale et dans le sens opposé.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="89"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="90"/>
        <source>Normals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="102"/>
        <source>Hough space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="110"/>
        <source>Rayon minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="111"/>
        <source>Rayon maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="112"/>
        <source>Résolution du rayon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="112"/>
        <source>Indiquez le pas entre les rayons minimum et maximum (4ème dimension de la grille 4D)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="113"/>
        <source>Résolution x/y/z de la grille 4D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepcreatehoughspace.cpp" line="113"/>
        <source>La valeur doit être au minimum 2 fois la résolution du rayon.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportCircles</name>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="48"/>
        <source>Enregistre cercles de clusters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="54"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="77"/>
        <source>Input PB_SGLF Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="78"/>
        <source>Input NiveauZ Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="79"/>
        <source>Input Cluster Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="80"/>
        <source>Cercle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="81"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcircles.cpp" line="82"/>
        <source>FileName</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportCylindersOfBillons</name>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="47"/>
        <source>Enregistre cylindres de billons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="53"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="76"/>
        <source>Input billons Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="77"/>
        <source>Input cluster Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="78"/>
        <source>Cylinder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportcylindersofbillons.cpp" line="80"/>
        <source>Position Mnt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportCylindersOfBillonsv2</name>
    <message>
        <location filename="../step/lsis_exportcylindersofbillonsv2.cpp" line="47"/>
        <source>Enregistre cylindres de billons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_exportcylindersofbillonsv2.cpp" line="53"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_exportcylindersofbillonsv2.cpp" line="76"/>
        <source>Input billons Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_exportcylindersofbillonsv2.cpp" line="77"/>
        <source>Cylinder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_exportcylindersofbillonsv2.cpp" line="79"/>
        <source>Position Mnt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportOneDBHPerSnake</name>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="47"/>
        <source>Enregistre un DHP pour chaque snake(de niveau 0) dans la scène</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="53"/>
        <source>Chaque cercle obtient un DHP basé sur une moyenne pour une tranche définie par l&apos;utilisateur. Cette étape extracte également:
- Diamètre dans l&apos;intervalle de hauteur
- Hauteur de la première fourche
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="78"/>
        <source>Étape d&apos;entrée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="80"/>
        <source>Groupe général</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="81"/>
        <source>DTM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="82"/>
        <source>Groupe des snakes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="83"/>
        <source>Stats arbres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="99"/>
        <source>Hauteur minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="99"/>
        <source>Renseignez la hauteur de départ pour la moyenne du rayon de chaque arbre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="100"/>
        <source>Hauteur maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexportonedbhpersnake.cpp" line="100"/>
        <source>Renseignez la hauteur de fin pour la moyenne du rayon de chaque arbre</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportToAsciiXYZNxNyNz</name>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="41"/>
        <source>Enregistre un fichier en xyz, NxNyNz optionnel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="47"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="70"/>
        <source>Input scene Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="71"/>
        <source>Input Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="72"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttoasciixyznxnynz.cpp" line="73"/>
        <source>FileName</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExportTreeData</name>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="32"/>
        <source>Exporter les statistiques des arbres détectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="38"/>
        <source>Cette étape permet d&apos;exporter toutes les statistiques des snakes, du parentage :parent/enfant, fourche, cercle(rayon, postion, direction, etc).
Il est important de noter que l&apos;ID du tuboide est un ID unique, celui du parent est -1 si le tuboide est la branche principale et l&apos;ID du niveau
est le niveau de fourche(0: branche principale, 1: niveau 1 de fourche, etc.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="57"/>
        <source>Étape d&apos;entrée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="59"/>
        <source>Groupe général</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="60"/>
        <source>Groupe des snakes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepexporttreedata.cpp" line="61"/>
        <source>Stats arbres</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExtractSubCloudsFromPositions</name>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="48"/>
        <source>Extrait des sous nuages de points a partir des estimations de position de troncs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="75"/>
        <source>Result Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="76"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="77"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="82"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="91"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="92"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="78"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="80"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="83"/>
        <source>Nuage de points initial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="90"/>
        <source>Result Extraction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositions.cpp" line="93"/>
        <source>Sub Cloud around position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExtractSubCloudsFromPositionsInFile</name>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="49"/>
        <source>Extrait des sous nuages de points a partir des estimations de position de troncs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="76"/>
        <source>Result Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="77"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="78"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="86"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="87"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="79"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="85"/>
        <source>Result Extraction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfile.cpp" line="88"/>
        <source>Sub Cloud around position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil</name>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="72"/>
        <source>Extrait des sous nuages de points a partir des estimations de position de troncs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="99"/>
        <source>Result Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="100"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="101"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="109"/>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="110"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="102"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="108"/>
        <source>Result Extraction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="111"/>
        <source>Sub Cloud around position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="354"/>
        <source>Unable to convert the ascii format to a integer at line 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="365"/>
        <source>Unable to convert the ascii format to a integer at line 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="376"/>
        <source>Unable to convert the ascii format to a floatting point at line 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="387"/>
        <source>Unable to convert the ascii format to a floatting point at line 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="398"/>
        <source>Unable to convert the ascii format to a floatting point at line 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp" line="409"/>
        <source>Unable to convert the ascii format to a floatting point at line 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepFilterSnake</name>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="44"/>
        <source>Filtre les snakes basé sur la variation du diamètre des cercles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="50"/>
        <source>No detailed description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="73"/>
        <source>Input PB_SGLF Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="74"/>
        <source>Input NiveauZ Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="75"/>
        <source>Input Cluster Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="76"/>
        <source>Cercle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="90"/>
        <source>Limite de variation du rayon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="90"/>
        <source>Mètres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="154"/>
        <source>Nombre de snakes avant filtrage : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepfiltersnake.cpp" line="155"/>
        <source>Nombre de snakes éliminés : %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepPreLocalisationTroncs</name>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="51"/>
        <source>Fait une transformee de Hough avec une seule case en Z (s&apos;applique sur une tranche uniquement) On prend les maximas locaux uniquement (pas de contours actifs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="57"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="78"/>
        <source>Result Normal Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="79"/>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="88"/>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="89"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="80"/>
        <source>Normal Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="87"/>
        <source>Result Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepprelocalisationtroncs.cpp" line="90"/>
        <source>Circles (stems location)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepReadSimpleTreeCsv</name>
    <message>
        <location filename="../step/lsis_stepreadsimpletreecsv.cpp" line="37"/>
        <source>Charge un arbre simpletree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepreadsimpletreecsv.cpp" line="71"/>
        <source>ResultTree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepreadsimpletreecsv.cpp" line="72"/>
        <source>GroupBranche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepreadsimpletreecsv.cpp" line="73"/>
        <source>GroupCylinder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepreadsimpletreecsv.cpp" line="74"/>
        <source>Cylindre</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepSlicePointCloud</name>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="40"/>
        <source>Tranche une scene en transects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="69"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="70"/>
        <source>Scene a trancher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="93"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="94"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepslicepointcloud.cpp" line="95"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepSnakesInHoughSpace</name>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="77"/>
        <source>3 - Créer des courbes caractéristiques dans un espace de Hough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="83"/>
        <source>Utilise les valeurs élevées contenues dans un espace de Hough (une grille 4D) pour faire croitre des courbes caractéristiques. La quatrième dimension de la grille représentant les rayons des cercles issus de la transformée de Hough.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="250"/>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="253"/>
        <source>Hough space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="252"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="256"/>
        <source>Dtm (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="266"/>
        <source>Snake group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="269"/>
        <source>Circle group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="267"/>
        <source>Tree Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="268"/>
        <source>MeshLevel0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="270"/>
        <source>CircleLevel0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="288"/>
        <source>Nombre d&apos;arbre recherché</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="288"/>
        <source>Renseignez le nombre d&apos;arbre qu&apos;il existe dans la scène</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="289"/>
        <source>Hauteur maximum de l&apos;arbre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="290"/>
        <source>Longueur minimum de la courbe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="290"/>
        <source>Indiquez la longueur minimum d&apos;une courbe. Si une courbe n&apos;a pas la longueur requise elle n&apos;apparaitra pas dans les résultats.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="292"/>
        <source>&lt;html&gt;&lt;b&gt;Initialisation de la recherche&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="293"/>
        <source>Hauteur minimum des graines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="293"/>
        <source>Définissez la hauteur minimum à partir du sol où la recherche à le droit de débuter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="294"/>
        <source>Hauteur maximum des graines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="294"/>
        <source>Définissez la hauteur maximum à partir du sol où la recherche à le droit de débuter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="296"/>
        <source>Mode avancée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="298"/>
        <source>&lt;html&gt;&lt;b&gt;Energie des contours actifs&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="299"/>
        <source>Alpha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="299"/>
        <source>Joue sur l&apos;élasticité de la courbe. Plus &quot;Alpha&quot; est petit et plus la courbe est élastique.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="300"/>
        <source>Beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="300"/>
        <source>Joue sur la flexion de la courbe. Si &quot;Beta&quot; est grand la courbe sera plus lisse et si il est petit il permet à la courbe de former des vagues.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="301"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="301"/>
        <source>Joue sur l&apos;attraction des maxima. Plus &quot;Gamma&quot; est fort et plus la courbe est attirée par les forts maxima.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="303"/>
        <source>&lt;html&gt;&lt;b&gt;Initialisation de la recherche des maxima&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="304"/>
        <source>Nombre de points minimum d&apos;un cercle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="304"/>
        <source>Indiquez le nombre de points minimum qu&apos;un cercle doit avoir pour être considéré comme maxima par l&apos;algorithme de recherche.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="305"/>
        <source>Taille de fenêtre d&apos;analyse des maxima locaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="305"/>
        <source>Lors de l&apos;initialisation l&apos;algorithme recherche tous les maxima locaux. Si deux maxima sont contenus dans la fenêtre d&apos;analyse le plus grand des deux (ou si ils sont égaux : le premier trouvé) est gardé en mémoire. La valeur indiquée représente le nombre de cellules. Plus cette valeur est grande et plus les maxima seront rassemblés.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="313"/>
        <source>&lt;html&gt;&lt;b&gt;Croissance des contours actifs&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="314"/>
        <source>Vitesse de croissance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="314"/>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="327"/>
        <source>Choix entre vitesse et précision. Augmenter la valeur si vous souhaitez que le traitement aille plus vite cependant il sera moins précis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="315"/>
        <source>Angle du cone de recherche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="315"/>
        <source>[°]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="317"/>
        <source>Longueur du cone de recherche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="318"/>
        <source>Seuil de fiabilité</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="318"/>
        <source>Représente la fiabilité de la croissance entre 0 et 1. Si le calcul de la fiabilité est inférieur à cette valeur l&apos;algorithme arrête la croissance de la courbe courante. La fiabilité est par exemple réduite lors d&apos;un départ de branchaison.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="323"/>
        <source>&lt;html&gt;&lt;b&gt;Optimisation&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="324"/>
        <source>Nombre de pas d&apos;optimisation maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="324"/>
        <source>Indiquez une valeur forte pour que l&apos;algorithme est assez de temps pour optimiser la forme de la courbe. Evitez une valeur trop forte afin d&apos;arrêtez l&apos;optimisation dans un temps raisonable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="327"/>
        <source>Vitesse d&apos;optimisation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="328"/>
        <source>Seuil de mouvement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="328"/>
        <source>Représente la différence entre deux optimisations de la courbe. Si cette différence est inférieur au seuil indiqué l&apos;algorithme arrête l&apos;optimisation de la courbe courante.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="331"/>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="331"/>
        <source>Active ou non la recherche de fourches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="332"/>
        <source>Nombre de niveau de fourches par arbre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_stepsnakesinhoughspace.cpp" line="332"/>
        <source>Représente le nombre de niveau de fourches à rechercher par arbre. Un niveau plus élevé peut rendre le calcul incroyablement long dépendamment du nombre de fourcheset des fourches potentielles à différents niveaux.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LSIS_StepTrancheSurMntOptionnel</name>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="47"/>
        <source>Fait une tranche au dessus du mnt ou a hauteur fixe si pas de mnt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="53"/>
        <source>No detailled description for this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="74"/>
        <source>Resultt PointCloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="75"/>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="81"/>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="89"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="76"/>
        <source>Point Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="78"/>
        <source>Result MNT Optionnel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="82"/>
        <source>Mnt Optionnel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="88"/>
        <source>Result Point Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../step/lsis_steptranchesurmntoptionnel.cpp" line="90"/>
        <source>Tranche au dessus du Mnt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lsis_pluginmanager.cpp" line="76"/>
        <location filename="../lsis_pluginmanager.cpp" line="77"/>
        <location filename="../lsis_pluginmanager.cpp" line="78"/>
        <location filename="../lsis_pluginmanager.cpp" line="79"/>
        <location filename="../lsis_pluginmanager.cpp" line="80"/>
        <location filename="../lsis_pluginmanager.cpp" line="81"/>
        <source>Step - Défilement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lsis_pluginmanager.cpp" line="122"/>
        <location filename="../lsis_pluginmanager.cpp" line="123"/>
        <source>Plugin STEP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
