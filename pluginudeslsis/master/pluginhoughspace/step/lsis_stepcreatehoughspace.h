#ifndef LSIS_STEPCREATEHOUGHSPACE_H
#define LSIS_STEPCREATEHOUGHSPACE_H

#include "ct_tools/model/ct_autorenamemodels.h"
#include "ct_step/abstract/ct_abstractstep.h"
#include "tools/lsis_debugtool.h"

class CT_PointsAttributesNormal;

/*!
 * \class LSIS_StepCreateHoughSpace
 * \ingroup Steps_LSIS
 * \brief <b>Cree un espace de Hough.</b>
 *
 * 
 *
 *
 */
class LSIS_StepCreateHoughSpace: public CT_AbstractStep, public LSIS_DebugTool
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LSIS_StepCreateHoughSpace(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    // LSIS_DebugTool
    void setProgress(const int &p) { CT_AbstractStep::setProgress(p); }

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute( const CT_PointsAttributesNormal* normals );

private:

    // Step parameters
    double      _minr;
    double      _minx;
    double      _miny;
    double      _minz;
    double      _maxr;
    double      _maxx;
    double      _maxy;
    double      _maxz;
    double      _resr;
    double      _resx;
    double      _resy;
    double      _resz;

    CT_ResultGroup*         _outResultHoughSpace;
    CT_AutoRenameModels     _autoRenameModelsHoughSpace;
};

#endif // LSIS_STEPCREATEHOUGHSPACE_H
