#ifndef LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILE_H
#define LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILE_H

#include "ct_step/abstract/ct_abstractstep.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_circle.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Qt dependencies
#include <QFile>
#include <QDir>

/*!
 * \class LSIS_StepExtractSubCloudsFromPositionsInFile
 * \ingroup Steps_LSIS
 * \brief <b>Extrait des sous nuages de points a partir des estimations de position de troncs.</b>
 *
 * 
 *
 * \param _posFile 
 * \param _size 
 *
 */

class LSIS_StepExtractSubCloudsFromPositionsInFile : public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    LSIS_StepExtractSubCloudsFromPositionsInFile(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute(QString& fileCloudFullName,
                 QVector< CT_Circle* >& circles);

    void computeAndSave( CT_AbstractItemDrawableWithPointCloud* pointCloud,
                         QVector< CT_Circle* >& circles,
                         QString outDirPath );

    void computeAndSave( QString& fileCloudFullName,
                         QVector< CT_Circle* >& circles,
                         QString outDirPath );

    bool isPointIn2dBox ( const CT_Point& point,
                          float xMin, float ymin,
                          float xmax, float ymax );

    void updateBBox( CT_Point& bot, CT_Point& top, const CT_Point& point );

private:

    // Step parameters
    double                  _size;    /*!<  */
    bool                    _saveMode;
    QString                 _inputCloudFileName;
    QString                 _outputDirPath;
    CT_ResultGroup*         _rsltSubClouds;
    CT_StandardItemGroup*   _grpGrpPointClouds;
};

#endif // LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILE_H
