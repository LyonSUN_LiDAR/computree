#ifndef NORM_OCTREEEXPORTER_H
#define NORM_OCTREEEXPORTER_H

#include "ct_exporter/abstract/ct_abstractexporter.h"

class NORM_OctreeExporter : public CT_AbstractExporter
{
    Q_OBJECT
public:
    NORM_OctreeExporter();
    ~NORM_OctreeExporter();

    virtual QString getExporterCustomName() const;

    CT_StepsMenu::LevelPredefined getExporterSubMenuName() const;

    void init();

    bool setItemDrawableToExport(const QList<CT_AbstractItemDrawable*> &list);

    bool configureExport();

    virtual CT_AbstractExporter* copy() const;

protected:

    bool protectedExportToFile();
};

#endif // NORM_OCTREEEXPORTER_H
