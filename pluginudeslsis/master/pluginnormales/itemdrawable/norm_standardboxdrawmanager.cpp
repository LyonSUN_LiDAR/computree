#include "norm_standardboxdrawmanager.h"

#include "norm_box.h"

NORM_StandardBoxDrawManager::NORM_StandardBoxDrawManager(QString drawConfigurationName)
    : CT_StandardAbstractShapeDrawManager(drawConfigurationName.isEmpty() ? "NORM_Box" : drawConfigurationName)
{
}

void NORM_StandardBoxDrawManager::draw(GraphicsViewInterface &view,
                                          PainterInterface &painter,
                                          const CT_AbstractItemDrawable &itemDrawable) const
{
    const NORM_Box &item = dynamic_cast<const NORM_Box&>(itemDrawable);

    painter.drawCube(item.getBotX(), item.getBotY(), item.getBotZ(),
                     item.getTopX(), item.getTopY(), item.getTopZ() );
}
