/************************************************************************************
* Filename :  norm_abstractoctree.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_ABSTRACTOCTREE_H
#define NORM_ABSTRACTOCTREE_H

#include "ct_point.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

class NORM_AbstractNode;

/*!
 * \brief The NORM_AbstractOctree class
 *
 * Classe abstraite d'octree
 * Tous les octrees crees par la suite doivent deriver de cette classe
 * et implementer les methodes virtuelles si elles veulent etre compatibles avec les classe norm_octreenodeXXX
 */
class NORM_AbstractOctree
{
public :
    /*!
     * \brief bot
     *
     * Getter
     *
     * \return bottom left point of the node
     */
    virtual CT_Point bot() const = 0;

    /*!
     * \brief top
     *
     * Getter
     *
     * \return upper right point of the node
     */
    virtual CT_Point top() const = 0;

    /*!
     * \brief minNbPoints
     *
     * Getter
     *
     * \return minimum number of point for a cell to be subdivisible
     */
    virtual size_t minNbPoints() const = 0;

    /*!
     * \brief maxDepth
     *
     * Getter
     *
     * \return maximum depth for a cellto be subdivisible
     */
    virtual size_t maxDepth() const = 0;

    /*!
     * \brief modifiablepointCloudIndexRegistered
     *
     * Getter
     *
     * \return the (morton order) sorted index cloud of the octree
     */
    virtual CT_MPCIR modifiablepointCloudIndexRegistered() const = 0;

    /*!
     * \brief contains
     *
     * Indicates if a point is inside an octree
     *
     * \param p : querry point
     * \return true if p is in the bounding cube of the octree, false otherwise
     */
    virtual bool contains( const CT_Point& p ) = 0;

    /*!
     * \brief initRoot
     *
     * Init (allocates and initialises) the root node
     */
    virtual void initRoot() = 0;
};

#endif // NORM_ABSTRACTOCTREE_H
