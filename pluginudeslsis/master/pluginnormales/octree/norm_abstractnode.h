/************************************************************************************
* Filename :  norm_abstractnode.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_ABSTRACTNODE_H
#define NORM_ABSTRACTNODE_H

#include "interfaces.h"
#include "norm_abstractoctree.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;

/*!
 * \brief The NORM_AbstractNode class
 *
 * Classe abstraite representant un noeud d'octree
 */
class NORM_AbstractNode
{
public:
    /*!
     * \brief setOctree
     *
     * Setter de la classe
     *
     * \param octree : octree a affecter
     */
    virtual void setOctree( NORM_AbstractOctree* octree ) = 0;

    /*!
     * \brief drawRecursive
     *
     * Dessine de maniere recursive sur une interface graphique tous les noeuds feuilles atteints depuis le premier noeud appelant.
     *
     * \param view : vue sur laquelle dessiner
     * \param painter : painter qui permet de dessiner
     * \param idChild : indice du noeud courant dans l'ordre des fils, entre 0 et 7
     */
    virtual void drawRecursive( GraphicsViewInterface& view,
                                PainterInterface& painter,
                                size_t idChild) const = 0;

    /*!
     * \brief printInfosRecursive
     *
     * Affiche les informations de tous les noeuds feuilles atteints depuis le noeud appelant
     */
    virtual void printInfosRecursive() = 0;

    /*!
     * \brief createOctreeRecursive
     *
     * Subdivise les noeuds de maniere recursive en fonction des criteres de l'octree
     * \warning Cette methode est tres importante c'est elle qu'il faut redefinir lors de la creation d'un nouvel octree avec de nouvelles regles
     */
    virtual void createOctreeRecursive() = 0;

    virtual void createOctreeRecursive( int minPcaDepth ) = 0;

    /*!
     * \brief leafNodeAtCodeRecursive
     *
     * Acces a la feuille de l'arbre ayant le code demande (peut s'arreter avant d'avoir atteint la fin du code)
     * Optionnellement on peut limiter la profondeur de recherche manuellement
     *
     * \param codex : code demande
     * \param codey : code demande
     * \param codez : code demande
     * \param maxDepth : profondeur de recherche max
     * \return
     */
    virtual NORM_AbstractNode* leafNodeAtCodeRecursive( size_t codex,
                                                        size_t codey,
                                                        size_t codez,
                                                        size_t maxDepth = std::numeric_limits<size_t>::max() ) = 0;

    /*!
     * \brief setIdAttributeRecursive
     *
     * Cree un nuage d'attributs entier.
     * Chaque attribut contient l'identifiant du noeud dans lequel se trouve le point.
     * Cet identifiant est entre 0 et 7 selon le numero de fils du noeud courant.
     *
     * \param attributs : attributs entiers de sortie
     * \param currentID : identifiant du noeud courant
     */
    virtual void setIdAttributeRecursive(AttributEntier* attributs, size_t currentID ) = 0;

    /*!
     * \brief setColorAttributeRecursive
     *
     * Cree un nuage de couleurs.
     * La couleur de chaque point est fonction de l'identifiant du noeud dans lequel il se trouve.
     *
     * \param attributs : attributs couleur de sortie
     * \param currentID : identifiant du noeud courant
     */
    virtual void setColorAttributeRecursive(CT_AbstractColorCloud *attributs, size_t currentID ) = 0;

    /*!
     * \brief getCentersOfNeighboursAtSameLevel
     *
     * Calcule l'ensemble des points centraux des voisins d'une cellule en considerant que ces voisins sont a la meme profondeur
     *
     * \param outCentersOfNeighbours : vecteur contenant les centres des voisins
     */
    virtual void getCentersOfNeighboursAtSameLevel(QVector<CT_Point> &outCentersOfNeighbours) = 0;

    /*!
     * \brief leavesInTraversalDirectionRecursive
     * \param traversalDirection
     * \param outNeiLeafInTraversalDirection
     */
    virtual void leavesInTraversalDirectionRecursive(const CT_Point &traversalDirection,
                                                     QVector<NORM_AbstractNode*> &outNeiLeafInTraversalDirection) = 0;

    /*!
     * \brief getAllLeavesRecursive
     *
     * Accede a toutes les feuilles atteignables a partir du noeud appelant
     *
     * \param outLeafNodes : ensemble des feuilles atteintes
     */
    virtual void getAllLeavesRecursive ( QVector< NORM_AbstractNode* >& outLeafNodes ) = 0;

    virtual void getAllLeavesRecursiveInSphere ( QVector< NORM_AbstractNode* >& outLeafNodes, const CT_Point& center, float radius ) = 0;

    /*!
     * \brief intersectsSphere
     *
     * Indique si la cellule de l'octree intersecte (ou est inclue dans) une sphere
     *
     * \param center : centre de la sphere
     * \param radius : rayon de la sphere
     * \return true si la cellule intersecte la sphere, false sinon
     */
    virtual bool intersectsSphere( const CT_Point& center, float radius ) = 0;

    /*!
     * \brief getIndexCloud
     *
     * Permet d'acceder au sous-nuage de points inclu dans la cellule sous la forme de nuage d'indices
     *
     * \return le sous nuage d'indice des points contenus dans la cellule
     */
    virtual CT_PointCloudIndexVector* getIndexCloud() = 0;
};

#endif // NORM_ABSTRACTNODE_H
