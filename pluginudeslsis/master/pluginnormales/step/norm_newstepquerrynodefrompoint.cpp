#include "norm_newstepquerrynodefrompoint.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "new/norm_newoctreev2.h"
#include "new/norm_newoctreenodev2.h"
#include "ct_itemdrawable/ct_scene.h"

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepQuerryNodeFromPoint::NORM_NewStepQuerryNodeFromPoint(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
    _x = 0;
    _y = 0;
    _z = 0;
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepQuerryNodeFromPoint::getStepDescription() const
{
    return tr("a - Debug - Cherche le noeud contenant un point donne ainsi que ses noeuds voisins");
}

// Step detailled description
QString NORM_NewStepQuerryNodeFromPoint::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepQuerryNodeFromPoint::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepQuerryNodeFromPoint::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepQuerryNodeFromPoint(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepQuerryNodeFromPoint::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
}

// Creation and affiliation of OUT models
void NORM_NewStepQuerryNodeFromPoint::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelQuerryPointScene,
                                      new CT_Scene(),
                                      "Querry Point");
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelPointsOfNodeScene,
                                      new CT_Scene(),
                                      "Points of Node");
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelPointsOfNeighbourNodesScene,
                                      new CT_Scene(),
                                      "Points of neighbours node");
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepQuerryNodeFromPoint::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble( "Querry point x", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _x );
    configDialog->addDouble( "Querry point y", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _y );
    configDialog->addDouble( "Querry point z", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _z );
}

void NORM_NewStepQuerryNodeFromPoint::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        if ( itemIn_itmOctree != NULL )
        {
            CT_Point querryPoint;
            querryPoint.setValues( _x, _y, _z );

            // Cree une scene ne contenant qu'un point (le querry point)
            CT_AbstractUndefinedSizePointCloud* querryPointPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            querryPointPointCloud->addPoint( querryPoint );
            CT_NMPCIR depositQuerryPointPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(querryPointPointCloud);
            CT_Scene* querryPointScene = new CT_Scene( _autoRenameModelQuerryPointScene.completeName(),
                                                       res_RsltOctree,
                                                       depositQuerryPointPointCloud );
            grpIn_grpOctree->addItemDrawable(querryPointScene);

            // Recupere le noeud feuille contenant le querry point
            int level = -2;
            NORM_NewOctreeNodeV2* nodeContainingPoint;
            itemIn_itmOctree->getNodeContainingPoint( querryPoint,
                                                      nodeContainingPoint,
                                                      level );

            // Si le noeud existe
            if( nodeContainingPoint != NULL )
            {
                // Cree une scene contenant les points du noeud
                CT_AbstractUndefinedSizePointCloud* nodePointCloud = nodeContainingPoint->getPoints();
                CT_NMPCIR depositNodePointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(nodePointCloud);
                CT_Scene* nodePointScene = new CT_Scene( _autoRenameModelPointsOfNodeScene.completeName(),
                                                         res_RsltOctree,
                                                         depositNodePointCloud );
                grpIn_grpOctree->addItemDrawable(nodePointScene);

                // On recupere ses voisins
                QVector< NORM_NewOctreeNodeV2* > nei;
//                nodeContainingPoint->getAllNeighbours( nei );
                nodeContainingPoint->getFacesNeighbours( nei );
                CT_AbstractUndefinedSizePointCloud* neiPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();
                foreach( NORM_NewOctreeNodeV2* n, nei )
                {
                    n->addPoints( neiPointCloud );
                }
                CT_NMPCIR depositNeiPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(neiPointCloud);
                CT_Scene* neiPointScene = new CT_Scene( _autoRenameModelPointsOfNeighbourNodesScene.completeName(),
                                                         res_RsltOctree,
                                                         depositNeiPointCloud );
                grpIn_grpOctree->addItemDrawable(neiPointScene);
            }
        }
    }
}
