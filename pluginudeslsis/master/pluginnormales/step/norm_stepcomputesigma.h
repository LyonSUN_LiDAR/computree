#ifndef NORM_StepComputeSigma_H
#define NORM_StepComputeSigma_H

#include "ct_step/abstract/ct_abstractstep.h"

/*!
 * \class NORM_StepComputeSigma
 * \ingroup Steps_NORM
 * \brief <b>Calcule un indice de planeite en chaque point.</b>
 *
 * L'indice de planeite se calcule par ACP :
lambda3 / somme_des_labdas
 *
 * \param _neiRadius Rayon du voisinage spherique a prendre en compte pour le calcul des normales (fitting de plan par ACP)
 *
 */

class NORM_StepComputeSigma: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    NORM_StepComputeSigma(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contNORMtual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double      _neiRadius;    /*!< Rayon du voisinage spherique a prendre en compte pour le calcul des normales (fitting de plan par ACP) */
    bool        _preciseNeighbourhood; /*!< Indique si on prend un voisinage spherique ou presque spherique */
};

#endif // NORM_StepComputeSigma_H
