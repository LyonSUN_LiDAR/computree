#include "norm_stepcomputenormalspca.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_iterator/ct_pointiterator.h"

#include "octree/norm_octree.h"
#include "octree/norm_octreenode.h"
#include "tools/norm_acp.h"

#include <QDebug>

// Alias for indexing models
#define DEFin_ResultStdOctree "ResultStdOctree"
#define DEFin_GroupOctree "GroupOctree"
#define DEFin_Octree "Octree"

#define DEFout_ResultNormals "ResultNormals"
#define DEFout_GroupNormals "GroupNormals"
#define DEFout_ItemNormals "ItemNormals"

typedef CT_PointsAttributesNormal AttributeNormals;

// Constructor : initialization of parameters
NORM_StepComputeNormalsPCA::NORM_StepComputeNormalsPCA(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _radius = 0.0001;
    _threshPCA = 0.0001;
    _preciseNeighbourhood = false;
}

// Step description (tooltip of contextual menu)
QString NORM_StepComputeNormalsPCA::getStepDescription() const
{
    return tr("Estimates a normal at each point by plane fitting");
}

// Step detailled description
QString NORM_StepComputeNormalsPCA::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepComputeNormalsPCA::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepComputeNormalsPCA::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepComputeNormalsPCA(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepComputeNormalsPCA::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_ResultStdOctree = createNewInResultModel(DEFin_ResultStdOctree, tr("Normal estimation"));
    resIn_ResultStdOctree->setRootGroup(DEFin_GroupOctree, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_ResultStdOctree->addItemModel(DEFin_GroupOctree, DEFin_Octree, CT_AbstractItemDrawableWithoutPointCloud::staticGetType(), tr("Octree"));

}

// Creation and affiliation of OUT models
void NORM_StepComputeNormalsPCA::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultNormals = createNewOutResultModel(DEFout_ResultNormals, tr("Estimated Normals"));
    res_ResultNormals->setRootGroup(DEFout_GroupNormals, new CT_StandardItemGroup(), tr("Group"));
    res_ResultNormals->addItemModel(DEFout_GroupNormals, DEFout_ItemNormals, new AttributeNormals(), tr("Normals"));

}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepComputeNormalsPCA::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Radius", "", 0.0001, 9999, 4, _radius, 1);
    configDialog->addDouble("Sigma threshold", "%", 0.0001, 100, 4, _threshPCA, 1);
    configDialog->addBool("Precise neighbourhood","","",_preciseNeighbourhood);
}

void NORM_StepComputeNormalsPCA::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_ResultStdOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultNormals = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_GroupOctree(resIn_ResultStdOctree, this, DEFin_GroupOctree);
    const NORM_Octree* itemIn_inItemOctree;
    const CT_AbstractItemGroup* grpIn_inGroupOctree;
    while (itIn_GroupOctree.hasNext() && !isStopped())
    {
        // Access the octree
        grpIn_inGroupOctree = (CT_AbstractItemGroup*) itIn_GroupOctree.next();
        itemIn_inItemOctree = (NORM_Octree*)grpIn_inGroupOctree->firstItemByINModelName(this, DEFin_Octree);
        if ( itemIn_inItemOctree != NULL)
        {
            // And its point cloud
            CT_PCIR inputIndexCloud = itemIn_inItemOctree->indexCloud();

            // On cree le nuage d'attributs qui va servir a contenir les sigmas
            AttributeNormals* outNormalsAttributes = new AttributeNormals(DEFout_ItemNormals, res_ResultNormals, inputIndexCloud );
            CT_AbstractNormalCloud* outNormalCloud = outNormalsAttributes->getNormalCloud();
            CT_Point bot = itemIn_inItemOctree->bot();
            CT_Point top = itemIn_inItemOctree->top();
            outNormalsAttributes->setBoundingBox( bot(0), bot(1), bot(2), top(0), top(1), top(2) );

            // Parcours de tous les points du nuage pour
            // 1 - Calculer un voisinage spherique
            // 2 - sur lequel on fait une acp
            // 3 - qui nous donne le sigma attendu
            // 4 - Si le sigma est inferieur au seuil on met a jour la normale
            CT_PointIterator itPoint( inputIndexCloud );// Iterateur pour le parcours des points
            size_t i = 0;                               // Indice de parcours du nuage d'indices
            size_t nbPoints = inputIndexCloud->size();  // Taille du nuage d'indice
            float floati = 0;                           // Utilise pour avoir la progress bar (identique a i mais en float pour les divisions)
            float floatNbPoints = nbPoints;             // Utilise pour avoir la progress bar (identique a nbPoints mais en float pour les divisions)
            size_t nbErrors = 0;                                // Number of points for which sigma could not be computed (not enough points in the neighbourhood)
            float currSigma = 0;                        // Valeur de sigma pour le point courant
            float lambda1, lambda2, lambda3;                    // Resultats de l'ACP
            CT_Point v1, v2, v3;                                // Vecteurs propres de l'ACP
            CT_Normal currNormal;                               // Normale courante troisieme vecteur propre ou (0,0,0) sous forme de CT_Normale
            while( itPoint.hasNext() && !isStopped() )
            {
                // 1 - Accede au point courrant
                itPoint.next();
                const CT_Point& currPoint = itPoint.currentPoint();

                // 1 - Accede au voisinage psherique (strictement spherique ou plus osuple selon le choix de l'utilisateur)
                CT_PointCloudIndexVector* nei;
                if ( _preciseNeighbourhood )
                {
                    // Recherche du voisinage spherique de curPoint
                    nei = itemIn_inItemOctree->sphericalNeighbourhood( currPoint, _radius );
                }

                else
                {
                    nei = itemIn_inItemOctree->approximateSphericalNeighbourhood( currPoint, _radius );
                }

                // 2 - Si le voisinage spherique contient assez de points pour faire une ACP
                if( nei->size() >= 3 )
                {
                    // 2 - Calcul de l'ACP sur le voisinage
                    NORM_Tools::NORM_PCA::pca( nei,
                                               lambda1, lambda2, lambda3,
                                               v1, v2, v3 );

                    currSigma = lambda3 / (lambda1 + lambda2 + lambda3 );
                }

                // 2 - Le voisinage contenait trop peu de points, on donne une valeur negative
                else
                {
                    currSigma = 1;
                    nbErrors++;
                }

                // 3 - Libere la memoire
                delete nei;

                // 4 - Mise a jour des normales
                if( currSigma < _threshPCA )
                {
                    for( int j = 0 ; j < 3 ; j++ )
                    {
                        currNormal(j) = v3(j);
                    }
                }

                else
                {
                    for( int j = 0 ; j < 3 ; j++ )
                    {
                        currNormal(j) = 0;
                    }
                }

                outNormalCloud->normalAt( i ) = currNormal;
//                outNormalsAttributes->setNormalAt( i, currNormal );

                // Barre de progression
                setProgress( floati * 100 / floatNbPoints );
                i++;
                floati++;
            }

            PS_LOG->addErrorMessage( PS_LOG->step , QString::number( nbErrors ) + " points n'ont pas pu avoir de sigma VERSION FAST");
            qDebug() << "Nb Error = " << nbErrors;

            // Ajouts du sigma au resultat
            CT_StandardItemGroup* grp_outGroupNormals = new CT_StandardItemGroup(DEFout_GroupNormals, res_ResultNormals);
            grp_outGroupNormals->addItemDrawable(outNormalsAttributes);
            res_ResultNormals->addGroup( grp_outGroupNormals );
            /* *********************************************************************************************************************************************** */
        }
    }
}
