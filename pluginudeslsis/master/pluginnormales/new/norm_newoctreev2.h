#ifndef NORM_NEWOCTREE_H
#define NORM_NEWOCTREE_H

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"

#include "ct_point.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_cloudindex/registered/ct_standardmodifiablecloudindexregisteredt.h"

class NORM_NewOctreeNodeV2;
class NORM_NewOctreeDrawManagerV2;
class CT_Scene;
class CT_PointsAttributesNormal;

template< typename DataT >
class CT_PointsAttributesScalarTemplated;

enum LeafCriteria
{
    Lambdas,
    RMSE,
    LambdaPlusRMSE
};

class NORM_NewOctreeV2 : public CT_AbstractItemDrawableWithoutPointCloud
{
public:
//**********************************************//
//         Constructors/Destructors             //
//**********************************************//
    /*!
     * \brief NORM_NewOctreeV2
     * Constructeur vide
     */
    NORM_NewOctreeV2();

    /*!
     * \brief NORM_NewOctreeV2
     *
     *  Constructeur classique d'itemdrawable (vide pour octree)
     *
     * \param model : modele de l'octree
     * \param result : resultat auquel se rattache l'octree
     */
    NORM_NewOctreeV2(const CT_OutAbstractSingularItemModel *model,
                     const CT_AbstractResult *result);

    /*!
     * \brief NORM_NewOctreeV2
     *
     *  Constructeur classique d'itemdrawable (vide pour octree)
     *
     * \param modelName : modele de l'octree sous forme de chaine de caracteres
     * \param result : resultat auquel se rattache l'octree
     */
    NORM_NewOctreeV2(const QString &modelName,
                     const CT_AbstractResult *result);

    /*!
     * \brief NORM_NewOctreeV2
     *
     * Constructeur d'un octree par dessus un nuage de points
     *
     * \param modelName : modele de l'octree sous forme de chaine de caracteres
     * \param result : resultat auquel se rattache l'octree
     * \param scene : scene sur laquelle construire l'octree
     */
    NORM_NewOctreeV2(const QString &modelName,
                     const CT_AbstractResult *result,
                     CT_Scene* scene,
                     int nbPointsMinForPca,
                     float dimensionMaxForPca,
                     LeafCriteria leafCriteria,
                     float sigma2Min,
                     float sigma3Max,
                     float rmseMax,
                     float dimensionMin );

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList);

    /*!
     * \brief ~NORM_NewOctreeV2
     *
     * Destructeur qui detruit la racine (qui va detruire recursivement tous les noeuds de l'octree)
     */
    virtual ~NORM_NewOctreeV2();

//**********************************************//
//         Construction de l'octree             //
//**********************************************//
    /*!
     * \brief getOctreeBoundingCubeFromScene
     *
     * Calcule le cube englobant de l'octree a aprtir de la bbox d'une scene
     *
     * \param scene : nuage de points (scene) de l'octree
     */
    void initialiseOctreeBoundingCubeFromScene(CT_Scene* scene,
                                               float& outDimension,
                                               CT_Point& outCentre );

//**********************************************//
//                  Normales                    //
//**********************************************//
    /*!
     * \brief getNormalsAttribute
     *
     * Calcule les normales pour les points contenus dans les noeuds avec un sigma suffisemment bas
     *
     * \param modelName : nom du modele de sortie de l'attribut
     * \param result : resultat auquel attacher l'attribut
     * \param pointCloudIndexRegistered : PCIR sur lequel creer un attribut entier
     *
     * \return
     */
    CT_PointsAttributesNormal* getNormalsAttribute(QString modelName,
                                                   CT_AbstractResult* result,
                                                   CT_PCIR pointCloudIndexRegistered);

    CT_PointsAttributesNormal* getNormalsAttributeWithLinearInterpolation(QString modelName,
                                                                          CT_AbstractResult* result,
                                                                          CT_PCIR pointCloudIndexRegistered);

    CT_PointsAttributesNormal* getNormalsAttributeWithWendlandInterpolation(QString modelName,
                                                                            CT_AbstractResult* result,
                                                                            CT_PCIR pointCloudIndexRegistered);

//**********************************************//
//                  Surfaces                    //
//**********************************************//
    /*!
     * \brief fitSurfaces
     *
     * Ajuste une surface a tous les noeuds de l'octree ou un tel fitting est possible (noeuds sur lesquels l'acp a ete calculee)
     */
    void fitSurfaces();

    /*!
     * \brief getProjectedPoints
     *
     * Projette tous les points de l'octree sur les surfaces fitees
     *
     * \param outProjectedPoints : tableau des points projetes
     */
    void getProjectedPoints( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

    void getProjectedPointsWithLinearInterpolation(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                   float interpolationRadius);

    /*!
     * \brief getErrorSurface
     *
     * Calcule pour chaque point son ecart vertical a la surface
     *
     * \param modelName : nom du modele de sortie de l'attribut
     * \param result : resultat auquel attacher l'attribut
     * \param pointCloudIndexRegistered : PCIR sur lequel creer un attribut entier
     *
     * \return
     */
    CT_PointsAttributesScalarTemplated<float>* getErrorSurface(QString modelName,
                                                               CT_AbstractResult* result,
                                                               CT_PCIR pointCloudIndexRegistered);

    CT_PointsAttributesScalarTemplated<float>* getGaussianCurvature(QString modelName,
                                                                    CT_AbstractResult* result,
                                                                    CT_PCIR pointCloudIndexRegistered);

    void getCurvatures(QString modelNameGaussianCurvature, QString modelNameDir1, QString modelNameDir2,
                       CT_AbstractResult* result,
                       CT_PCIR pointCloudIndexRegistered,
                       CT_PointsAttributesScalarTemplated<float>*& outGaussianCurvature,
                       CT_PointsAttributesNormal*& outDir1,
                       CT_PointsAttributesNormal*& outDir2 );

    void debugFlipAcpBasis();

    /*!
     * \brief orientSurfaces
     *
     * Oriente les surfaces par propagation
     *
     * \param nSamplePoints : nombre de points a sampler par arete (donc au carre pour le nombre de points par face)
     */
    void orientSurfaces(int nSamplePoints , CT_PointsAttributesScalarTemplated<int>*& debugAttribut, int debugNumAttribut );

//**********************************************//
//      Recherche de points et voisinage        //
//**********************************************//
    /*!
     * \brief getNodeContainingPoint
     *
     * Recherche le noeud contenant un point donne.
     * Le parametre optionel level indique le niveau du noeud souhaite.
     *
     * Si une feuille est atteinte avant la profondeur souhaitee, la recherche s'arete a cette feuille.
     * Si il n'est pas indique, la recherche s'arete a la feuille contenant le point
     *
     * Si le point appartient a un noeud vide (qui n'existe donc pas), le pointeur NULL est retourne
     * Si le point est en dehors de l'octree, le pointeur NULL est retourne
     *
     * \param querryPoint : point a chercher
     * \param outputNode : noeud contenant le point (possiblement NULL)
     * \param outLevelOfNode : niveau du noeud contenant le point (possiblement -1)
     * \param level : niveau de profondeur souhaite (optionel)
     */
    void getNodeContainingPoint(const CT_Point& querryPoint, NORM_NewOctreeNodeV2*& outputNode, int& outLevelOfNode, int level = -1 );

    /*!
     * \brief computeNeighboursOfLeaves
     *
     * Calcule les noeuds voisins feuille de chaque feuille de l'octree
     *
     */
    void computeNeighboursOfLeaves();

//**********************************************//
//                  Affichage                   //
//**********************************************//
    /*!
     * \brief draw
     *
     * Dessine l'octree en fil de fer et en couleur (couleur correspondant au numero de noeud)
     *
     * \param view : interface dans laquelle afficher
     * \param painter : painter utilise
     * \param drawBoxes : est ce qu'on doit afficher les noeuds sous forme de boites ?
     * \param drawCentroid : est ce qu'on doit afficher le barycentre des noeuds ?
     * \param drawPcaV3 : doit on afficher le troisieme vecteur propre de l'acp ?
     * \param sizeV3 : longueur de l'affichage de v3
     */
    void draw(GraphicsViewInterface &view,
              PainterInterface &painter,
              bool drawBoxes,
              bool drawCentroid,
              bool drawPcaV3,
              double sizeV3) const;

    /*!
     * \brief getPointsIdAttribute
     *
     * Remplit un tableau d'attribut avec le numero de noeud dans lequel se trove les points
     *
     * \param modelName : nom du modele de sortie de l'attribut
     * \param result : resultat auquel attacher l'attribut
     * \param pointCloudIndexRegistered : PCIR sur lequel creer un attribut entier
     *
     * \return
     */
    CT_PointsAttributesScalarTemplated<int>* getPointsIdAttribute(QString modelName,
                                                                  CT_AbstractResult* result,
                                                                  CT_PCIR pointCloudIndexRegistered);

    /*!
     * \brief getPointsSigmaAttribute
     *
     * Remplit un tableau d'attribut avec le sigma du noeud dans lequel se trove les points
     *
     * \param modelName : nom du modele de sortie de l'attribut
     * \param result : resultat auquel attacher l'attribut
     * \param pointCloudIndexRegistered : PCIR sur lequel creer un attribut de flottants
     *
     * \return
     */
    CT_PointsAttributesScalarTemplated<float>* getPointsSigmaAttribute(QString modelName,
                                                                       CT_AbstractResult* result,
                                                                       CT_PCIR pointCloudIndexRegistered);

    void getPcaAttributes(CT_PointsAttributesScalarTemplated<float>* outCreatedL1Attribute,
                          CT_PointsAttributesScalarTemplated<float>* outCreatedL2Attribute,
                          CT_PointsAttributesScalarTemplated<float>* outCreatedL3Attribute,
                          CT_PointsAttributesScalarTemplated<float>* outCreatedSigmaAttribute,
                          CT_PointsAttributesScalarTemplated<float>* outCreatedL3L2Attribute );

    /*!
     * \brief boundingCubeContainsPoint
     *
     * Indique si un point est contenu dans le cube englobant de l'octree
     *
     * \param p point a chercher
     *
     * \return : true si le point est dans le bounding cube, false sinon
     */
    bool boundingCubeContainsPoint( const CT_Point& p );

//**********************************************//
//          Point Cloud Compression             //
//**********************************************//
    void saveToFile( QString fileName );

    static NORM_NewOctreeV2* loadFromFile(QString fileName,
                                          const QString& modelName,
                                          const CT_AbstractResult* result);

    void getProjectedPointsFromFile( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

//**********************************************//
//                Debug displays                //
//**********************************************//
    void printAllcentre();

//**********************************************//
//              Getters/setters                 //
//**********************************************//
    float dimensionMin() const;
    void setDimensionMin(float dimensionMin);

    int nbPointsMinForPca() const;
    void setNbPointsMinForPca(int nbPointsMinForPca);

    float sigma3Max() const;
    void setSigma3Max(float sigma3Max);

    float getDimensionMaxForPca() const;
    void setDimensionMaxForPca(float dimensionMaxForPca);

    NORM_NewOctreeNodeV2* getRoot() const;
    void setRoot(NORM_NewOctreeNodeV2* root);

    CT_Scene* getScene() const;
    void setScene(CT_Scene* scene);

    float getSigma2Min() const;
    void setSigma2Min(float sigma2Min);

    float getMaxRMSE() const;
    void setMaxRMSE(float maxRMSE);

private :
    CT_Point    _bot;                       /*!< Point inferieur gauche du cube englobant */
    CT_Point    _top;                       /*!< Point superieur droit du cube englobant */

    int         _nbPointsMinForPca;         /*!< Nombre de points minimum (inclu) dans une feuille de l'octree */
    float       _dimensionMaxForPca;        /*!< Dimension au dessus de laquelle on ne fait pas de PCA, on divise simplement les noeuds jusqu'a arriver a une dimension inferieure */
    float       _sigma2Min;                  /*!< Sigma2 maximum accepte pour arreter la division */
    float       _sigma3Max;                  /*!< Sigma3 maximum accepte pour arreter la division */
    float       _dimensionMin;              /*!< Demi-dimension minimale d'une feuille de l'octre */
    float           _maxRMSE;
    LeafCriteria    _leafCriteria;

    NORM_NewOctreeNodeV2*   _root;          /*!< Noeud racine de l'octree */

    CT_Scene*               _scene;         /*!< Pointeur vers la scene aue contient l'octree */

    const static NORM_NewOctreeDrawManagerV2 NEW_OCTREE_DRAW_MANAGER_V2;    /*!< Draw manager of the octree itemdrawable */
    const static CT_PointAccessor            _pAccessor;
};

#endif // NORM_NEWOCTREE_H
