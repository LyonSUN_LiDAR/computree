#ifndef LSIS_DEBUGTOOL
#define LSIS_DEBUGTOOL

class LSIS_DebugTool
{
public:
    virtual ~LSIS_DebugTool() {}

    virtual void setProgress(const int &p) = 0;
};

#endif // LSIS_DEBUGTOOL

