CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

CONFIG += c++17

MUST_USE_OPENCV = 1

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TARGET = plug_step

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    lsis_pluginentry.h \
    lsis_pluginmanager.h \
    raytracing4d/lsis_beam4d.h \
    streamoverload/streamoverload.h \
    raytracing4d/lsis_abstractvisitorgrid4d.h \
    raytracing4d/lsis_incrementvisitor.h \
    raytracing4d/lsis_incrementvisitor.hpp \
    raytracing4d/lsis_traversalalgorithm4d.h \
    point4d/lsis_point4d.h \
    raytracing4d/lsis_traversalalgorithm4d.hpp \
    assert/assertwithmessage.h \
    houghspace/lsis_houghspace4d.h \
    reader/lsis_readergrid4d.h \
    exporter/lsis_exportergrid4d.h \
    densitygrid/lsis_densitygrid.h \
    pixel/lsis_pixel4d.h \
    raytracing4d/lsis_abstractvisitorgrid4d.hpp \
    pixel/lsis_pixel4ddecrescentvaluesorter.h \
    openactivecontour/lsis_openactivecontour4dcontinuous.h \
    openactivecontour/lsis_openactivecontour4dcontinuous.hpp \
    openactivecontour/lsis_activecontour4dcontinuous.h \
    openactivecontour/lsis_activecontour4dcontinuous.hpp \
    streamoverload/streamoverload.hpp \
    reader/lsis_readerhoughspace4d.h \
    pixel/lsis_pixel4ddecrescentvaluesorter.hpp \
    histogram/lsis_histogram1d.h \
    histogram/lsis_histogram1d.hpp \
    step/lsis_stepcreatehoughspace.h \
    step/lsis_stepsnakesinhoughspace.h \
    step/lsis_steptranchesurmntoptionnel.h \
    step/lsis_stepprelocalisationtroncs.h \
    step/lsis_stepextractsubcloudsfrompositions.h \
    step/lsis_stepextractsubcloudsfrompositionsinfile.h \
#    step/lsis_stepmultireshoughspace.h \
#    step/lsis_stepfoostep.h \
#    step/lsis_stepmaximasfromhoughspace.h \
#    step/lsis_steptestoptionalmnt.h \
#    step/lsis_stepmaximasfromhoughspaceoptionaldtm.h \
    tools/lsis_pcatools.h \
    tools/lsis_pcatools.hpp \
    tools/lsis_mathtools.hpp \
    tools/lsis_mathtools.h \
    step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.h \
    step/lsis_stepaddsnake.h \
    step/lsis_stepaddsetsnakes.h \
    step/lsis_stepaddsetsnakesoverdtm.h \
    tools/lsis_debugtool.h \
    houghspace/lsis_houghspace4d_dense.h \
    houghspace/lsis_houghspace4d_sparse.h \
    step/lsis_stepexporttoasciixyznxnynz.h \
    step/lsis_stepexportcylindersofbillons.h \
#    step/lsis_stepexportcylindersofbillons_copy.h \
    step/lsis_stepslicepointcloud.h \
    step/lsis_stepexportcircles.h \
    step/lsis_newstepfastfilterhoughspace.h \
    raytracing4d/lsis_fastfiltervisitor.h \
    raytracing4d/lsis_fastfiltervisitor.hpp \
    raytracing4d/lsis_effectivefiltervisitor.h \
    raytracing4d/lsis_effectivefiltervisitor.hpp \
    step/lsis_exportcylindersofbillonsv2.h \
    step/lsis_stepreadsimpletreecsv.h \
    step/lsis_stepfiltersnake.h \
    step/lsis_stepexportonedbhpersnake.h \
    step/lsis_stepexporttreedata.h

SOURCES += \
    lsis_pluginentry.cpp \
    lsis_pluginmanager.cpp \
    raytracing4d/lsis_beam4d.cpp \
    streamoverload/streamoverload.cpp \
    raytracing4d/lsis_abstractvisitorgrid4d.cpp \
    raytracing4d/lsis_incrementvisitor.cpp \
    raytracing4d/lsis_traversalalgorithm4d.cpp \
    point4d/lsis_point4d.cpp \
    houghspace/lsis_houghspace4d.cpp \
    reader/lsis_readergrid4d.cpp \
    exporter/lsis_exportergrid4d.cpp \
    densitygrid/lsis_densitygrid.cpp \
    pixel/lsis_pixel4d.cpp \
    pixel/lsis_pixel4ddecrescentvaluesorter.cpp \
    openactivecontour/lsis_openactivecontour4dcontinuous.cpp \
    openactivecontour/lsis_activecontour4dcontinuous.cpp \
    reader/lsis_readerhoughspace4d.cpp \
    histogram/lsis_histogram1d.cpp \
    step/lsis_stepcreatehoughspace.cpp \
    step/lsis_stepsnakesinhoughspace.cpp \
    step/lsis_steptranchesurmntoptionnel.cpp \
    step/lsis_stepprelocalisationtroncs.cpp \
    step/lsis_stepextractsubcloudsfrompositions.cpp \
    step/lsis_stepextractsubcloudsfrompositionsinfile.cpp \
#    step/lsis_stepfoostep.cpp \
#    step/lsis_stepmaximasfromhoughspace.cpp \
#    step/lsis_steptestoptionalmnt.cpp \
#    step/lsis_stepmaximasfromhoughspaceoptionaldtm.cpp \
#    step/lsis_stepmultireshoughspace.cpp \
    tools/lsis_pcatools.cpp \
    step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.cpp \
    step/lsis_stepaddsnake.cpp \
    step/lsis_stepaddsetsnakes.cpp \
    step/lsis_stepaddsetsnakesoverdtm.cpp \
    houghspace/lsis_houghspace4d_dense.cpp \
    houghspace/lsis_houghspace4d_sparse.cpp \
    step/lsis_stepexporttoasciixyznxnynz.cpp \
    step/lsis_stepexportcylindersofbillons.cpp \
#    step/lsis_stepexportcylindersofbillons_copy.cpp \
    step/lsis_stepslicepointcloud.cpp \
    step/lsis_stepexportcircles.cpp \
    step/lsis_newstepfastfilterhoughspace.cpp \
    raytracing4d/lsis_fastfiltervisitor.cpp \
    raytracing4d/lsis_effectivefiltervisitor.cpp \
    step/lsis_exportcylindersofbillonsv2.cpp \
    step/lsis_stepreadsimpletreecsv.cpp \
    step/lsis_stepfiltersnake.cpp \
    step/lsis_stepexportonedbhpersnake.cpp \
    step/lsis_stepexporttreedata.cpp

TRANSLATIONS += languages/pluginhoughspace_en.ts \
                languages/pluginhoughspace_fr.ts

INCLUDEPATH += .

DEPENDPATH *= ${INCLUDEPATH}

