#!/bin/bash

mkdir computreev5
mkdir pluginartsfree
mkdir pluginonf
mkdir pluginonflsis
mkdir pluginlerfob
mkdir pluginignlif
mkdir pluginlvox
mkdir pluginsegma
mkdir plugintoolkit
mkdir pluginifplsis
mkdir pluginrscript
mkdir pluginsimpletree
mkdir pluginudeslsis
mkdir plugingenerate
mkdir computreedevtools

# svn checkout http://rdinnovation.onf.fr/svn/computreev5/ computreev5
svn checkout http://rdinnovation.onf.fr/svn/plugin-arts-free/ pluginartsfree
svn checkout http://rdinnovation.onf.fr/svn/plugin-onf/ pluginonf
svn checkout http://rdinnovation.onf.fr/svn/plugin-onf-lsis/ pluginonflsis
svn checkout http://rdinnovation.onf.fr/svn/plugin-lerfob/ pluginlerfob
svn checkout http://rdinnovation.onf.fr/svn/plugin-ign-lif/ pluginignlif
svn checkout http://rdinnovation.onf.fr/svn/plugin-lvox/ pluginlvox
svn checkout http://rdinnovation.onf.fr/svn/segma/ pluginsegma
svn checkout http://rdinnovation.onf.fr/svn/plugin-toolkit/ plugintoolkit
svn checkout http://rdinnovation.onf.fr/svn/plugin-ifp-lsis/ pluginifplsis
svn checkout http://rdinnovation.onf.fr/svn/plugin-r-script/ pluginrscript
# svn checkout http://rdinnovation.onf.fr/svn/plugin-simpletree/ pluginsimpletree
svn checkout http://rdinnovation.onf.fr/svn/plugin-udes-lsis/ pluginudeslsis
svn checkout http://rdinnovation.onf.fr/svn/plugin-generate/ plugingenerate
# svn checkout http://rdinnovation.onf.fr/svn/computreedevtools/ computreedevtools
