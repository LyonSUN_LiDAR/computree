#include "seg_stepcavityfill.h"

#include "ct_view/ct_stepconfigurabledialog.h"


#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_result/ct_resultgroup.h"

#include "tools/cavityfillmain.h"

#include <QFileInfo>
#include <QDir>

#include <QDebug>

#define DEF_InRes "r"
#define DEF_InGroup "g"
#define DEF_InItem "i"

// Constructor : initialization of parameters
SEG_StepCavityFill::SEG_StepCavityFill(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _filterRadius = 3;
    _laplacianThreshold = 1;
    _medianFilterSize = 3;
    _dilation = 1;
    _heightThreshold = 70;

}

// Step description (tooltip of contextual menu)
QString SEG_StepCavityFill::getStepDescription() const
{
    return tr("1- Remplir les trous");
}

// Step detailled description
QString SEG_StepCavityFill::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepCavityFill::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepCavityFill::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepCavityFill(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepCavityFill::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *result = createNewInResultModelForCopy(DEF_InRes);
    result->setZeroOrMoreRootGroup();
    result->addGroupModel("", DEF_InGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    result->addItemModel(DEF_InGroup, DEF_InItem, CT_Image2D<float>::staticGetType(), tr("Raster"));
}

// Creation and affiliation of OUT models
void SEG_StepCavityFill::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_InRes);
    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_InGroup, _outCavityFillModelName, new CT_Image2D<float>(), tr("Raster filled"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepCavityFill::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt(tr("Rayon du filtre Laplacien"), "px", 3, 999, _filterRadius);
    configDialog->addDouble(tr("Seuil sur le Laplacien"), "", -1e+09, 1e+09, 2, _laplacianThreshold);
    configDialog->addInt(tr("Taille du filtre médian"), "px", 3, 999, _medianFilterSize);
    configDialog->addInt(tr("Dilatation des cavités"), "px", 0, 999, _dilation);
    configDialog->addDouble(tr("Hauteur maximale"), "m", 0, 1e+09, 2, _heightThreshold);
}

void SEG_StepCavityFill::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_InGroup);

    while(it.hasNext()) {

        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();
        CT_Image2D<float> *inGrid = dynamic_cast<CT_Image2D<float>*>(group->firstItemByINModelName(this, DEF_InItem));

        if(inGrid != NULL)
        {
            CT_Image2D<float> *outGrid = new CT_Image2D<float>(_outCavityFillModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());

            CavityFillMain::cavityFillFilter(inGrid->getPointerToData(), inGrid->linDim(), inGrid->colDim(), _filterRadius, 5.0, _laplacianThreshold, _medianFilterSize, _dilation, _heightThreshold, outGrid->getPointerToData());

            outGrid->computeMinMax();

            group->addItemDrawable(outGrid);
        }
    }
}
