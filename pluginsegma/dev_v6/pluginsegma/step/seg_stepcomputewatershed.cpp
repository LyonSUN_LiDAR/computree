#include "seg_stepcomputewatershed.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

SEG_StepComputeWatershed::SEG_StepComputeWatershed() : SuperClass()
{
    _mergeLimits = true;
    _minHeight = 2.0;
}

QString SEG_StepComputeWatershed::description() const
{
    return tr("5- Watershed (flooding)");
}

QString SEG_StepComputeWatershed::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepComputeWatershed::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepComputeWatershed::createNewInstance() const
{
    return new SEG_StepComputeWatershed();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepComputeWatershed::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inHeights, tr("Image (hauteurs)"));
    manager.addItem(_inGroup, _inMaxima, tr("Maxima"));

    manager.addResult(_inResultDTM, tr("MNT"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("MNT"));

}

void SEG_StepComputeWatershed::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outWatershed, tr("Watershed"));
}

void SEG_StepComputeWatershed::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Ne pas affecter les pixels d'une valeur inférieure à"), "m",-99999, 99999, 2, _minHeight);
    postInputConfigDialog->addBool(tr("Affecter les limites à un cluster"), "", "", _mergeLimits);
}

void SEG_StepComputeWatershed::compute()
{
    const CT_Image2D<float>* mnt = nullptr;
    for (const CT_Image2D<float>* imageIn : _inDTM.iterateInputs(_inResultDTM))
    {
            mnt = imageIn;
    }

    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* imageIn : grp->singularItems(_inHeights))
        {
            if (isStopped()) {return;}

            CT_Image2D<qint32>* maximaIn = const_cast<CT_Image2D<qint32>*>(grp->singularItem(_inMaxima));

            if (maximaIn != nullptr)
            {

                Eigen::Vector2d min;
                imageIn->getMinCoordinates(min);
                CT_Image2D<qint32>* watershedImage = new CT_Image2D<qint32>(min(0), min(1), imageIn->xdim(), imageIn->ydim(), imageIn->resolution(), imageIn->level(), -1, 0);
                grp->addSingularItem(_outWatershed, watershedImage);

                watershedImage->getMat() = maximaIn->getMat().clone();

                setProgress(20);

                computeWatershed(imageIn, watershedImage, mnt);

                setProgress(70);

                if (_mergeLimits)
                {
                    mergeLimitsWithClusters(imageIn, watershedImage);
                }

                setProgress(90);

                watershedImage->computeMinMax();

                setProgress(99);
            }
        }
    }
    setProgress(100);
}

void SEG_StepComputeWatershed::computeWatershed(const CT_Image2D<float>* imageIn, CT_Image2D<qint32>* watershedImage, const CT_Image2D<float>* mnt)
{
    QMultiMap<float, size_t> queue;
    size_t index;
    int x, y, x2, y2;

    // Add all neighbours of maxima to the queue
    for (int yy = 0; yy < imageIn->ydim() ; yy++)
    {
        for (int xx = 0; xx < imageIn->xdim() ; xx++)
        {

            // Add to the queue if at least one neighbour pixel is already labelled
            if (watershedImage->value(xx, yy) == 0)
            {

                float zval = imageIn->value(xx, yy);
                if (mnt != nullptr)
                {
                    float zmnt = mnt->valueAtCoords(imageIn->getCellCenterColCoord(xx), imageIn->getCellCenterLinCoord(yy));
                    if (!qFuzzyCompare(zmnt, mnt->NA()))
                    {
                        zval -= zmnt;
                    }
                }

                if (double(zval) <= _minHeight) {watershedImage->setValue(xx, yy, -1);} // flag as checked all pixels <= _minHeight

                qint32 firstVal;
                if (hasExactlyOneNeighbourCluster(xx, yy, watershedImage, firstVal))
                {
                    watershedImage->index(xx, yy, index);
                    queue.insert(imageIn->value(xx, yy), index);
                    watershedImage->setValue(xx, yy, -1); // flag as inserted in queue index
                } else if (firstVal > 0) {
                    watershedImage->setValue(xx, yy, -1); // flag as checked
                }
            }
        }
    }

    qint32 firstVal;
    qint32 val;

    while (queue.size() > 0)
    {
        index = queue.last();
        imageIn->indexToGrid(index, x, y);
        queue.remove(queue.lastKey(), index);

        // Etape 2 : si le pixel n'a qu'un cluster voisin : ajout au cluster, et récupération de ses voisins
        if (hasExactlyOneNeighbourCluster(x, y, watershedImage, firstVal))
        {
            watershedImage->setValue(x, y, firstVal);

            x2 = x - 1; y2 = y - 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x - 1; y2 = y;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x - 1; y2 = y + 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x; y2 = y + 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x + 1; y2 = y + 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x + 1; y2 = y;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x + 1; y2 = y - 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }

            x2 = x; y2 = y - 1;
            val = watershedImage->value(x2, y2);
            if (watershedImage->index(x2, y2, index) && val == 0)
            {
                queue.insert(imageIn->value(x2, y2), index);
                watershedImage->setValue(x2, y2, -1);
            }
        } else if (firstVal > 0) {
            watershedImage->setValue(x, y, -1); // flag as checked
        }

    }
}

bool SEG_StepComputeWatershed::hasExactlyOneNeighbourCluster(int &x, int &y, CT_Image2D<qint32>* watershedImage, qint32 &firstVal)
{
    bool ok = true;

    firstVal = watershedImage->value(x-1, y-1);

    qint32 val = watershedImage->value(x-1, y);
    if (val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x-1, y+1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x, y+1);
    if (ok && val > 0){
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x+1, y+1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x+1, y);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x+1, y-1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(x, y-1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    if (firstVal <= 0) {ok = false;}

    return ok;
}

void SEG_StepComputeWatershed::mergeLimitsWithClusters(const CT_Image2D<float>* imageIn, CT_Image2D<qint32>* watershedImage)
{
    cv::Mat_<float> result = watershedImage->getMat().clone();

    QMap<qint32, float> sumMap;
    QMap<qint32, int> countMap;

    // Add all neighbours of maxima to the queue
    for (int yy = 0; yy < watershedImage->ydim() ; yy++)
    {
        for (int xx = 0; xx < watershedImage->xdim() ; xx++)
        {
            if (watershedImage->value(xx, yy) <= 0 && (double(imageIn->value(xx, yy)) > _minHeight))
            {
                float centerVal = imageIn->value(xx, yy);

                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx - 1, yy    );
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx    , yy - 1);
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx    , yy - 1);
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx + 1, yy    );

                if (sumMap.size() <= 0)
                {
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx - 1, yy - 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx - 1, yy + 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx + 1, yy - 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, xx + 1, yy + 1);
                }

                QMapIterator<qint32, float> itSum(sumMap);
                QMapIterator<qint32, int> itCount(countMap);
                float minVal = std::numeric_limits<float>::max();
                int minCluster = 0;

                while (itSum.hasNext() && itCount.hasNext())
                {
                    itSum.next();
                    itCount.next();

                    float val = itSum.value() / float(itCount.value());

                    if (val < minVal)
                    {
                        minVal = val;
                        minCluster = itSum.key();
                    }
                }

                result(yy, xx) = minCluster;

                sumMap.clear();
                countMap.clear();
            }
        }
    }

    watershedImage->getMat() = result;
}

void SEG_StepComputeWatershed::computeSumAndCount(const CT_Image2D<float>* imageIn, const CT_Image2D<qint32>* watershedImage, float centerVal, QMap<qint32, float> &sumMap, QMap<qint32, int> &countMap, int x, int y)
{
    qint32 cluster = watershedImage->value(x, y);

    if (cluster > 0)
    {
        float sum = sumMap.value(cluster, 0);
        int count = countMap.value(cluster, 0);

        sum += fabs(imageIn->value(x, y) - centerVal);
        count++;

        sumMap.insert(cluster, sum);
        countMap.insert(cluster, count);
    }

}
