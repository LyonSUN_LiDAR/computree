#ifndef SEG_STEPDETECTMAXIMA_H
#define SEG_STEPDETECTMAXIMA_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_referencepoint.h"

class SEG_StepDetectMaxima: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepDetectMaxima();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double      _minHeight;


    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inHeights;
    CT_HandleOutSingularItem<CT_Image2D<qint32> >                   _outMaxima;

    CT_HandleInResultGroup<0,1>                                     _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                           _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inDTM;


};

#endif // SEG_STEPDETECTMAXIMA_H
