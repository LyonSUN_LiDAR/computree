#ifndef SEG_STEPREPLACENABYZERO_H
#define SEG_STEPREPLACENABYZERO_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

class SEG_StepReplaceNAByZero: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepReplaceNAByZero();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:


    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inRaster;
    CT_HandleOutSingularItem<CT_Image2D<float> >                    _outRaster;
};

#endif // SEG_STEPREPLACENABYZERO_H
