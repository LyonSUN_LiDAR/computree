#include "seg_stepseparateclusters.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

SEG_StepSeparateClusters::SEG_StepSeparateClusters() : SuperClass()
{

}

QString SEG_StepSeparateClusters::description() const
{
    return tr("6-  Créer des rasters par couronne");
}

QString SEG_StepSeparateClusters::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepSeparateClusters::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepSeparateClusters::createNewInstance() const
{
    return new SEG_StepSeparateClusters();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepSeparateClusters::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inClusters, tr("Clusters"));
    manager.addItem(_inGroup, _inRaster, tr("Raster"));
}

void SEG_StepSeparateClusters::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _groupCluster, tr("Clusters isolés (grp)"));
    manager.addItem(_groupCluster, _outCluster, tr("Cluster isolé"));
    manager.addItemAttribute(_outCluster, _outAttIDCluster, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDCluster"));
    manager.addItem(_groupCluster, _outMask, tr("Masque"));
}

void SEG_StepSeparateClusters::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* raster : grp->singularItems(_inRaster))
        {
            if (isStopped()) {return;}

            CT_Image2D<qint32>* clusters = const_cast<CT_Image2D<qint32>*>(grp->singularItem(_inClusters));

            if (clusters != nullptr)
            {
                QMap<qint32, SEG_StepSeparateClusters::pixelsArea*> areas;

                for (int y = 0 ; y < clusters->ydim() ; y++)
                {
                    for (int x = 0 ; x < clusters->xdim() ; x++)
                    {
                        qint32 cluster = clusters->value(x, y);

                        if (!areas.contains(cluster)) {areas.insert(cluster, new SEG_StepSeparateClusters::pixelsArea());}
                        SEG_StepSeparateClusters::pixelsArea *area = areas.value(cluster);

                        if (x < area->_xmin) {area->_xmin = x;}
                        if (x > area->_xmax) {area->_xmax = x;}
                        if (y < area->_ymin) {area->_ymin = y;}
                        if (y > area->_ymax) {area->_ymax = y;}
                    }
                }

                QMapIterator<qint32, SEG_StepSeparateClusters::pixelsArea*> it(areas);
                while (it.hasNext())
                {
                    it.next();
                    qint32 cluster = it.key();
                    if (cluster > 0)
                    {
                        SEG_StepSeparateClusters::pixelsArea* area = it.value();

                        // Create a 1-pixel buffer around each crown
                        area->_xmin--;
                        area->_ymin--;
                        area->_xmax++;
                        area->_ymax++;

                        double minx = clusters->minX() + (area->_xmin) * clusters->resolution();
                        double miny = clusters->maxY() - (area->_ymax + 1) * clusters->resolution();
                        int dimx = area->_xmax - area->_xmin + 1;
                        int dimy = area->_ymax - area->_ymin + 1;

                        CT_StandardItemGroup *clusterGroup = new CT_StandardItemGroup();
                        CT_Image2D<float> *clusterImage = new CT_Image2D<float>(minx, miny, dimx, dimy, raster->resolution(), 0, raster->NA(), raster->NA());
                        CT_Image2D<quint8> *clusterMask = new CT_Image2D<quint8>(minx, miny, dimx, dimy, raster->resolution(), 0, 0, 0);



                        for (int y = area->_ymin ; y <= area->_ymax ; y++)
                        {
                            for (int x = area->_xmin ; x <= area->_xmax ; x++)
                            {
                                if (clusters->value(x, y) == cluster)
                                {
                                    int localCol = x - area->_xmin;
                                    int localLin = y - area->_ymin;
                                    clusterMask->setValue(localCol, localLin, 1);
                                    clusterImage->setValue(localCol, localLin, raster->value(x, y));
                                }
                            }
                        }
                        clusterImage->computeMinMax();
                        clusterMask->computeMinMax();

                        grp->addGroup(_groupCluster, clusterGroup);
                        clusterGroup->addSingularItem(_outCluster, clusterImage);
                        clusterImage->addItemAttribute(_outAttIDCluster,  new CT_StdItemAttributeT<qint32>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), cluster));
                        clusterGroup->addSingularItem(_outMask, clusterMask);
                    }
                }

                qDeleteAll(areas.values());
            }
        }
    }

}

