#ifndef COMMONTOOLS_H
#define COMMONTOOLS_H


#define NODATA -9999
#define ZERO 0
#define HOLE_VALUE -999

class CommonTools
{
public:
    CommonTools();

    inline static unsigned long mem_offset(int row, int col, int ncols){ return row*ncols+col;}

    /* Validation de l'ubication du pixel à l'interieur de la section */
    inline static bool isInside(int row, int col, int rows, int cols) {
        return (row >= 0 && col >= 0 && row < rows && col < cols);
    }

};

#endif // COMMONTOOLS_H
