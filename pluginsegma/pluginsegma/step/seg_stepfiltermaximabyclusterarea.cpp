#include "seg_stepfiltermaximabyclusterarea.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_referencepoint.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"
#define DEFin_maxima "maxima"
#define DEFin_cluster "clusters"


// Constructor : initialization of parameters
SEG_StepFilterMaximaByClusterArea::SEG_StepFilterMaximaByClusterArea(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _maxArea = 4.0;
    _createMaximaPts = true;
}

// Step description (tooltip of contextual menu)
QString SEG_StepFilterMaximaByClusterArea::getStepDescription() const
{
    return tr("Filtrer les maxima en fonction de l'aire des clusters");
}

// Step detailled description
QString SEG_StepFilterMaximaByClusterArea::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepFilterMaximaByClusterArea::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepFilterMaximaByClusterArea::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepFilterMaximaByClusterArea(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepFilterMaximaByClusterArea::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Maxima"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_image, CT_Image2D<float>::staticGetType(), tr("Image (hauteurs)"));
    resIn_res->addItemModel(DEFin_grp, DEFin_maxima, CT_Image2D<qint32>::staticGetType(), tr("Maxima"));
    resIn_res->addItemModel(DEFin_grp, DEFin_cluster, CT_Image2D<qint32>::staticGetType(), tr("Clusters"));

}

// Creation and affiliation of OUT models
void SEG_StepFilterMaximaByClusterArea::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _filteredMaxima_ModelName, new CT_Image2D<qint32>(), tr("Maxima filtrés"));
        if (_createMaximaPts)
        {
            resCpy_res->addGroupModel(DEFin_grp, _filteredMaximaPtsGrp_ModelName, new CT_StandardItemGroup(), tr("Maxima filtrés (Pts)"));
            resCpy_res->addItemModel(_filteredMaximaPtsGrp_ModelName, _filteredMaximaPts_ModelName, new CT_ReferencePoint(), tr("Maximum"));
        }
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepFilterMaximaByClusterArea::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Aire minimale pour garder un cluster"), "m²", 0, 999999, 2, _maxArea);
    configDialog->addBool("", "", tr("Créer des points pour les maxima"), _createMaximaPts);
}

void SEG_StepFilterMaximaByClusterArea::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        CT_Image2D<qint32>* maximaIn = (CT_Image2D<qint32>*)grp->firstItemByINModelName(this, DEFin_maxima);
        CT_Image2D<float>* imageIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_image);
        CT_Image2D<qint32>* clustersIn = (CT_Image2D<qint32>*)grp->firstItemByINModelName(this, DEFin_cluster);

        if (maximaIn != NULL)
        {
            Eigen::Vector2d min;
            maximaIn->getMinCoordinates(min);
            CT_Image2D<qint32>* filteredMaxima = new CT_Image2D<qint32>(_filteredMaxima_ModelName.completeName(), res, min(0), min(1), maximaIn->colDim(), maximaIn->linDim(), maximaIn->resolution(), maximaIn->level(), maximaIn->NA(), 0);
            grp->addItemDrawable(filteredMaxima);

            filteredMaxima->getMat() = maximaIn->getMat().clone();

            setProgress(20);

            // Get maxima coordinates list
            QMultiMap<qint32, Eigen::Vector3d*> maximaCoords;
            QMultiMap<double, qint32> maximaHeights;

            for (size_t xx = 0 ; xx < maximaIn->colDim() ; xx++)
            {
                for (size_t yy = 0 ; yy < maximaIn->linDim() ; yy++)
                {
                    qint32 maximaID = maximaIn->value(xx, yy);

                    if (maximaID > 0 && maximaID != maximaIn->NA())
                    {
                        Eigen::Vector3d* coords = new Eigen::Vector3d();
                        if (maximaIn->getCellCenterCoordinates(xx, yy, *coords))
                        {
                            (*coords)(2) = imageIn->value(xx, yy);
                            maximaCoords.insert(maximaID, coords);
                            maximaHeights.insert((*coords)(2), maximaID);
                        }
                    }
                }
            }

            setProgress(25);


            // Compute ordered vector of maxima ids
            QList<qint32> validMaxima;

            QMapIterator<double, qint32> itH(maximaHeights);
            QMap<qint32, int> indices;
            itH.toBack();
            while (itH.hasPrevious())
            {
                itH.previous();
                qint32 cl = itH.value();
                if (!validMaxima.contains(cl))
                {
                    validMaxima.append(cl);
                    indices.insert(cl, validMaxima.size() - 1);
                }
            }


            QVector<qint32> orderedMaxima = validMaxima.toVector();
            int mxSize = orderedMaxima.size();
            validMaxima.clear();

            // Create maxima coords vector
            QVector<Eigen::Vector3d> coords(mxSize);
            for (int i = 0 ; i < mxSize ; i++)
            {
                qint32 id = orderedMaxima.at(i);

                QList<Eigen::Vector3d*> coordinates = maximaCoords.values(id);
                coords[i] = *(coordinates.at(0));

                // Compute position of the current maxima if more than one pixel
                int size = coordinates.size();
                if (size > 1)
                {
                    for (int j = 1 ; j < size ; j++)
                    {
                        Eigen::Vector3d* pos = coordinates.at(j);
                        coords[i](0) += (*pos)(0);
                        coords[i](1) += (*pos)(1);
                        if ((*pos)(2) > coords[i](2)) {coords[i](2) = (*pos)(2);}
                    }

                    coords[i](0) /= size;
                    coords[i](1) /= size;
                }
            }

            double pixelArea = clustersIn->resolution() * clustersIn->resolution();
            QVector<double> areas(mxSize);
            areas.fill(0);
            for (size_t xx = 0 ; xx < clustersIn->colDim() ; xx++)
            {
                for (size_t yy = 0 ; yy < clustersIn->linDim() ; yy++)
                {
                    qint32 idCell = clustersIn->value(xx, yy);

                    if (idCell > 0 && idCell != clustersIn->NA())
                    {
                        int i = indices.value(idCell);
                        areas[i] += pixelArea;
                    }
                }
            }

            setProgress(30);


            // For each maxima, test area
            for (int i = 0 ; i < mxSize ; i++)
            {
                qint32 id = orderedMaxima.at(i);

                if (id > 0)
                {
                    if (areas.at(i) < _maxArea)
                    {
                        orderedMaxima[i] = 0;
                    }
                }

                setProgress(29.0*(float)i / (float)mxSize + 30.0);
            }

            setProgress(60);

            for (int i = 0 ; i < mxSize ; i++)
            {
                qint32 cl = orderedMaxima.at(i);
                if (cl > 0)
                {
                    validMaxima.append(cl);

                    double x = coords[i](0);
                    double y = coords[i](1);
                    double z = coords[i](2);

                    if (_createMaximaPts)
                    {
                        CT_ReferencePoint* refPoint = new CT_ReferencePoint(_filteredMaximaPts_ModelName.completeName(), res, x, y, z, 0);
                        CT_StandardItemGroup* grpPt = new CT_StandardItemGroup(_filteredMaximaPtsGrp_ModelName.completeName(), res);
                        grpPt->addItemDrawable(refPoint);
                        grp->addGroup(grpPt);
                    }
                }
            }

            setProgress(70);


            QMap<qint32, qint32> newIds;
            qint32 cpt = 1;
            // effectively delete toRemove maximum and numbers them in a continuous way
            for (size_t xx = 0 ; xx < filteredMaxima->colDim() ; xx++)
            {
                for (size_t yy = 0 ; yy < filteredMaxima->linDim() ; yy++)
                {
                    qint32 maximaID = filteredMaxima->value(xx, yy);

                    if (maximaID > 0)
                    {
                        if (validMaxima.contains(maximaID))
                        {
                            qint32 newId = newIds.value(maximaID, 0);
                            if (newId == 0)
                            {
                                newId = cpt++;
                                newIds.insert(maximaID, newId);
                            }
                            filteredMaxima->setValue(xx, yy, newId);
                        } else {
                            filteredMaxima->setValue(xx, yy, 0);
                        }
                    }
                }
            }
            newIds.clear();
            setProgress(90);

            filteredMaxima->computeMinMax();

            qDeleteAll(maximaCoords.values());
            setProgress(99);
        }
    }
    setProgress(100);
}


