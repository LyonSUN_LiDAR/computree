#include "lb_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers
#include "step/lb_stepcharacterisecurvature.h"
#include "step/lb_stepcharacterizelog.h"
#include "reader/lb_reader_bil3d.h"

LB_PluginManager::LB_PluginManager() : CT_AbstractStepPlugin()
{
}

LB_PluginManager::~LB_PluginManager()
{
}

QString LB_PluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin IGN-LIF for Computree\n"
           "AU  - Constant, Thiéry\n"
           "PB  - Institut National de l'Information Géographique et Forestière\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-lerfob/wiki\n"
           "ER  - \n";
}

bool LB_PluginManager::loadGenericsStep()
{
    addNewGeometricalShapesStep<LB_StepCharacteriseCurvature>(QObject::tr("Qualité des bois"));
    addNewGeometricalShapesStep<LB_StepCharacterizeLog>(QObject::tr("Qualité des bois"));

    return true;
}

bool LB_PluginManager::loadOpenFileStep()
{
    return true;
}

bool LB_PluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool LB_PluginManager::loadActions()
{
    return true;
}

bool LB_PluginManager::loadExporters()
{
    return true;
}

bool LB_PluginManager::loadReaders()
{
    return true;
}

