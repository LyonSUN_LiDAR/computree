#ifndef LB_STEPCHARACTERISECURVATURE_H
#define LB_STEPCHARACTERISECURVATURE_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_circle.h"

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Geometry"

/*!
 * \class LB_StepCharacteriseCurvature
 * \ingroup Steps_LB
 * \brief <b>Compute the curvatures along logs.</b>
 *
 * Calcul de courbures
 *
 * \param _nbPtsDerivation Number of points used
 * \param _maxRadiusOsculatorCircle
 *
 *
 * <b>Input Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup (logs)... \n
 *          - CT_StandardItemGroup (log Clusters) \n
 *              - CT_ItemDrawable (Circle) \n
 *
 * <b>Output Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup (log)... \n
 *          - <em>CT_ItemDrawable (tangente)</em> \n
 *          - <em>CT_ItemDrawable (normal)</em> \n
 *          - <em>CT_ItemDrawable (binormal)</em> \n
 *          - <em>CT_ItemDrawable (osculator Circle)</em> \n
 */

class LB_StepCharacteriseCurvature: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LB_StepCharacteriseCurvature(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();
    float volumeFrustum( float buttRadius, float topRadius, float height);
    float volumeCylinder( float radius,  float height);
    double distance( Eigen::Quaterniond p1 , Eigen::Quaterniond p2);
    Eigen::Quaterniond derivative(Eigen::Quaterniond *qion, int nbsmoothingpoints);
    //Calcul un vecteur par ajustement sur nbpts.
    Eigen::Vector3d linearRegression(Eigen::Quaterniond *qion, int nbpts );
    Eigen::Quaterniond  symmetric(Eigen::Quaterniond *qion, double a, double b, double c, double d);
    Eigen::Quaterniond  pointReflexion(Eigen::Quaterniond *qion, Eigen::Quaterniond *qcentre);
    Eigen::Vector3d pointToLine(const Eigen::Vector3d  lineOrigin, const Eigen::Vector3d  lineDirection,const  Eigen::Vector3d Point3D);
    double triangleArea(const Eigen::Vector3d vertexA, const Eigen::Vector3d  vertexB, const Eigen::Vector3d vertexC);

private:

    // Step parameters
    int    _nbPtsDerivation;    /*!< Nombre de points pour calcul dérivation par ajustement sur (2*nbPtsDerivation +1)  */
    double    _maxRadiusOsculatorCircle;    /*!< < Rayon Maximal dans l'affichage des rayons de courbures  */

    QString quaternionToString(const Eigen::Quaterniond &quat);
    Eigen::Quaterniond computeNormalQuaternion(const Eigen::Quaterniond &q1, const Eigen::Quaterniond &q2, const Eigen::Quaterniond &q3);

    static bool orderByIncreasingZ(CT_Circle* c1, CT_Circle* c2)
    {
        return c1->getCenterZ() > c2->getCenterZ();
    }


};

#endif // LB_STEPCHARACTERISECURVATURE_H
