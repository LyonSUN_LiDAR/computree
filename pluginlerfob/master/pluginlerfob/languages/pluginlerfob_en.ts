<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>LB_Reader_Bil3D</name>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="72"/>
        <source>Fichiers Bil 3D</source>
        <translation>Bil 3D files</translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="79"/>
        <source>Billon</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="80"/>
        <source>Enveloppe</source>
        <translation>Outer points</translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="81"/>
        <source>Element</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="82"/>
        <source>Cercle (grp)</source>
        <translation>Circle (grp)</translation>
    </message>
    <message>
        <location filename="../reader/lb_reader_bil3d.cpp" line="83"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
</context>
<context>
    <name>LB_StepCharacteriseCurvature</name>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="79"/>
        <source>Calcule les courbures d&apos;une section</source>
        <translation>Compute the curvature of a log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="85"/>
        <source>Cette étape permet de caractériser l&apos;évolution de la courbure de la polyligne centrale de  billons à partir des cercles ajustés sur les clusters horizontaux.On définit comme paramètres:&lt;ul&gt;&lt;li&gt; un nombre de cercles &lt;b&gt;n&lt;/b&gt; qui seront pris en compte de part et d&apos;autre de chaque centre pour calculer les vecteurs tangents et normaux à la ligne des centres en faisant appel à une méthode de régression linéaire. (N.B.: l&apos;épaisseur des clusters horizontaux impacte aussi le résultat.)&lt;/li&gt;&lt;li&gt; un &lt;b&gt;rayon maximal&lt;/b&gt; des cercles osculateurs à afficher.&lt;br&gt;&lt;/ul&gt;En sortie, cette étape fournit:&lt;ul&gt;&lt;li&gt; l&apos;évolution du trièdre de Frenet le long de la polyligne composée des centres de chaque cercle, et dont se déduisent les cercles osculateurs caractérisant la courbure, en amplitude et en orientation. Chaque trièdre est composé des vecteurs tangent, normal, et binormal. Leurs normes est fixée à 10cm pour la représentation. Le cercle osculateur associé à un point de la polyligne est dans le plan défini par les vecteurs tangent et normal associés au point.&lt;br /&gt;Les attributs permettent de relier les rayons de courbure à la hauteur, et à l&apos;abscisse curviligne du point de la polyligne.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>This step allows the characterisation of the change of curvature along the central polyline of a log defined  from the fitted circles based on horizontal clusted. Two parameters are required:&lt;ul&gt;&lt;li&gt; a number of circles &lt;b&gt;n&lt;/b&gt; taken into account on each side of a centre for compute  the tangent and normal vectors to the line of the centres by using a linear regression method.(N.B.:the thickness of the horizontal clustering plays also a role)&lt;/li&gt;&lt;li&gt; a &lt;b&gt;maximal radius&lt;/b&gt; for the displayed osculator circles&lt;br&gt;&lt;/ul&gt;As outputs, this step provides:&lt;ul&gt;&lt;li&gt; The Frenet&apos;s vectors along the polyline defined by the centres ofthe circles, and from which the osculator circles are derived, characterizing the magnitude and the orientation of the curvature.Each Frenet&apos;s trihedron is defined by the unit vectors tangent, normal and binormal. Their lengths is fixed at 0.1 meter for display. The osculator circle attached to a point of the polyline is in the plane defined by the tangent and normal vectors&lt;br /&gt; The attributes link the curvature radius, to the height, and the curvilinear abscissa of the point of the polyline.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="109"/>
        <source>Billons</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="110"/>
        <source>Groupe Cluster</source>
        <translation>Group Cluster</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="111"/>
        <source>Cluster(groupe)</source>
        <translation>Cluster (group)</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="111"/>
        <source>Groupe contenant les clusters contenues dans la section</source>
        <translation>Group including the clusters belonging to the log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="112"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="119"/>
        <source>Courbures</source>
        <translation>Curvatures</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="120"/>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="122"/>
        <source>Courbure</source>
        <translation>Curvature</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="120"/>
        <source>groupe contenant les infos de courbure d&apos;un billon </source>
        <translation>group containing the curvature information of a log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="123"/>
        <source>tangente</source>
        <translation>tangent</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="123"/>
        <source>tangente à la ligne neutre</source>
        <translation>tangent to the central line</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="124"/>
        <source>normale</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="124"/>
        <source>normale à la ligne neutre</source>
        <translation>normal to the central line</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="125"/>
        <source>binormale</source>
        <translation>binormal</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="125"/>
        <source>binormale à la ligne neutre</source>
        <translation>binormal to the central line</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="126"/>
        <source>Cercle osculateur</source>
        <translation>Osculator circle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="126"/>
        <source>Cercle tangent à la ligne neutre</source>
        <translation>Circle tangent to the central line</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="127"/>
        <source>Attributs courbure</source>
        <translation>Curvature attributes</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="131"/>
        <source>Rayon de Courbure</source>
        <translation>Curvature radius</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="134"/>
        <source>Hauteur de référence</source>
        <translation>Reference height</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="137"/>
        <source>Abscisse Curviligne</source>
        <translation>Curvilinear abscissa</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="146"/>
        <source>Nombre n de cercles pour lissage sur 2n +1</source>
        <translation>Number n of circles for smoothing on 2n +1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="147"/>
        <source>Rayon de courbure maximal à afficher</source>
        <translation>Maximal curvature radius to display</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="316"/>
        <source> Courbures Section %1 </source>
        <translation>Curvature for log %1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterisecurvature.cpp" line="464"/>
        <source> Courbures Section %1 non calculées car trop peu de points </source>
        <translation>Curvature of Log %1 not computed because is was not enough points</translation>
    </message>
</context>
<context>
    <name>LB_StepCharacterizeLog</name>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="152"/>
        <source>resSections</source>
        <translation>resSections</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="153"/>
        <source>Section(Billon)</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="154"/>
        <source>Cluster(groupe)</source>
        <translation>Cluster (group)</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="154"/>
        <source>Groupe contenant les clusters contenues dans la section</source>
        <translation>Group including the clusters belonging to the log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="156"/>
        <source>Circle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <source>Cercle ajustÃ© sur un cluster horizontal</source>
        <translation type="vanished">Circle fitted to a horizontal cluster</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="165"/>
        <source>Amplitude de l&apos;inclinaison</source>
        <translation>Magnitude of lean </translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="105"/>
        <source>Caractérisation du billon</source>
        <translation>Log characterization</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="111"/>
        <source>Cette étape permet de caractériser le(s) billon(s) présent(s) dans la scène &lt;br&gt;&lt;br /&gt;En entrée, les données utilisées proviennent de nuages 3D décrivant des billons décomposés en  clusters horizontaux contigus sur lesquels des cercles ont été ajustés.&lt;br /&gt;En sortie, certaines caractéristiques sont rendues visibles à travers des objets les figurant, mais d&apos;autres sont seulement numériques et sont accessibles via l&apos;objet attribut et/ou sont affichées dans la fenêtre Log.&lt;br /&gt;Ces caractéristiques concernent:&lt;br /&gt; &lt;ul&gt;&lt;li&gt; l&apos;&lt;b&gt;inclinaison&lt;/b&gt; globale est établie à partir des centres des cercles d&apos;extrémité dits de fin bout et gros bout. Trois visualisations lui sont associés : &lt;/li&gt;&lt;ul&gt;&lt;li&gt; Un segment incliné &lt;/li&gt;&lt;li&gt; Un segment horizontal, correspondant à la projection horizontale du précédent indiquant la direction et l&apos;amplitude de l&apos;inclinaison au niveau de la base du billon &lt;/li&gt;&lt;li&gt; Un cercle horizontal à la base du billon dont le rayon correspond à la longueur du segment précédent &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; la &lt;b&gt;flexuosité&lt;/b&gt; définie à partir de la polyligne joignant les centres des cercles et exprimée par deux valeurs &lt;li&gt;&lt;ul&gt;&lt;li&gt; la longueur de la polyligne à rapporter à la longueur du segment représentant l&apos;inclinaison globale&apos; &lt;/li&gt;&lt;li&gt; l&apos;aire de la surface gauche définie par la polyligne et le segment représentant l&apos;inclinaison globale, calculée par triangulation&apos; &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; le &lt;b&gt;volume de bois rond &lt;/b&gt; calculé de deux manières différentes, à partir de la somme des volumes de: &lt;/li&gt;&lt;ul&gt;&lt;li&gt;  troncs de cône définis à partir de deux cercles consécutifs&apos; &lt;/li&gt;&lt;li&gt;  cylindres définis à partir de chaque cercle et de la distance le séparant du voisin&apos; &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; le &lt;b&gt;défilement &lt;/b&gt; calculé à partir de la distance séparant deux cercles consécutifs, et de leurs rayons respectifs. Les valeurs moyenne, minimale et maximale sont restituées dana la liste d&apos;attributs  &lt;li&gt;&lt;li&gt; le &lt;b&gt;volume de bois scié  &lt;/b&gt; calculé en tenant compte de la position de la section la plus décalée par rapport à l&apos;&apos;inclinaison globale. Ce volume est calculé à partir la longueur du billon et de l&apos;intersection du cercle associé à cette section avec les deux cercles correspondant aux extrémités du billon. Cette intersection peut aller de la section de fin bout à une valeur nulle, le cas général étant la réunion de quatre 1/4 d&apos;ellipse. Chaque quart d&apos;ellipse est définie pas les longueurs de ses demi-axes a et b, desquels il est possible de définir le rectangle d&apos;aire maximal s&apos;inscrivant à l&apos;intérieur et qui a pour côtés a et b divisés par la racine carrée de 2. &lt;li&gt;&lt;/ul&gt;</source>
        <translation>This step characterizes the log(s) present in the scene. &lt;br /&gt; The inputs are coming from data clouds describing logs split into adjoined horizontal clusters and corresponding horizontal circles.&lt;br /&gt; As outputs; some characteristics are made visible through different objects representing them, but others are only numerical and they are accessible through the exportable attributes but for some of them they are also reported in the log window.&lt;br /&gt; These characteristics are ::&lt;br /&gt; &lt;ul&gt;&lt;li&gt;the&lt;b&gt;lean&lt;/b&gt; of the log is established from the circles centres of both top and butt ends. Three vizualisations are proposed :&lt;/li&gt;&lt;ul&gt;&lt;li&gt; a leaning line segment &lt;/li&gt;&lt;li&gt; A horizontal line segment, corresponding to the horizontal projection of the previous segmentand characterizing the direction and magnitude of leaning at the log butt end. &lt;/li&gt;&lt;li&gt; A horizontal circle at the butt end characterizing the magnitude of leaning whith a radius equal to the length of the previous segment.&lt;/li&gt; &lt;/ul&gt;&lt;li&gt; the &lt;b&gt;flexuosity&lt;/b&gt; defined from the polyline joining the circles centres and expressed by two values &lt;li&gt;&lt;ul&gt;&lt;li&gt; The length of the polyline to refer to the length of the leaning line segment. &lt;/li&gt;&lt;li&gt; the area calculated by triangulation of the 3D surface joining the the polyline to the line segment corresponding to the lean. &lt;/li&gt;&lt;/ul&gt;&lt;li&gt; the &lt;b&gt;roundwood volume  &lt;/b&gt; computed by two differnt ways, by adding the volumes of:&lt;/li&gt;&lt;ul&gt;&lt;li&gt; frustum of cones defined from the the two successive horizontal circles &lt;/li&gt;&lt;li&gt;  cylinders défined from each circle and the distance to the next circle. &lt;/li&gt;&lt;/ul&gt;&lt;li&gt;the &lt;b&gt;taper &lt;/b&gt; computed from the distance between two successive circles, and their respective radii.The average, maximum, and minimum values are recorded into the list of attributes.&lt;li&gt;&lt;li&gt; the&lt;b&gt;volume of sawnwood  &lt;/b&gt; computed by taking into account the location of the most defected section against the global leaning. This volume is computed from the log length et from the intersection between the most deflected circle and those at both ends of the log.The area of this intersection varies between the area of the top end section to zero, and the general case is the union of four quarters of ellipse. Each quarter of ellipse is defined by the lengths of its half axes, a  and b,and it is easy to define the inscribed rectangle with its lengths of sides obtained by dividing a and b by the squared root of 2.&lt;li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="152"/>
        <source>résultat comportant des sections</source>
        <translation>result including logs</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="156"/>
        <source>Cercle ajusté sur un cluster horizontal</source>
        <translation>Circle ajusted on a horizontal cluster</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="163"/>
        <source>Caractéristiques des Billons</source>
        <translation>Logs characteristics</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="163"/>
        <source>Caractéristiques Sections )
</source>
        <translation>Logs characteristics</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="164"/>
        <source>Caractéristiques Billon</source>
        <translation>Log characteristics</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="164"/>
        <source>groupe contenant les characteristiques d&apos;un billon</source>
        <translation>group containing the log characteristics</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="165"/>
        <source>Cercle représentant l&apos;amplitude de l&apos;inclinaison, centré sur la base du billon.</source>
        <translation>Circle showing the magnitude of the lean, centred at the butt end.</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="166"/>
        <source>Projection horizontale inclinaison</source>
        <translation>Horizontal projection of the global leaning of the log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="166"/>
        <source>Segment horizontal dont l&apos;origine est le centre du bas du billon. Il indique l&apos;amplitude de l&apos;inclinaison totale par sa longueur, et sa direction</source>
        <translation>Horizontal line segment with its origine at the centre of the butt end of the log. It indicates the magnitude of the global lean and itshorizontal direction</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="167"/>
        <source>segment inclinaison totale </source>
        <translation>global lean line segment </translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="167"/>
        <source>Inclinaison gloable</source>
        <translation>global lean</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="168"/>
        <source>cylindre équivalent</source>
        <translation>equivalent cylinder</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="168"/>
        <source>cylindre de même volume que le billon et incliné de la même facon </source>
        <translation>cylinder with the same volume and the same lean than the log</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="169"/>
        <source>Demi-diamètre a1</source>
        <translation>Half diameter a1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="169"/>
        <source>Demi-diamètre a1 d1/4 ellipse</source>
        <translation>Half diameter a1 of a quarter of ellipse</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="170"/>
        <source>Demi-diamètre a2</source>
        <translation>Half diameter a2</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="170"/>
        <source>Demi-diamètre a2 d1/4 ellipse</source>
        <translation>Half diameter a2 of a quarter of ellipse</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="171"/>
        <source>Demi-diamètre b1</source>
        <translation>Half diameter b1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="171"/>
        <source>Demi-diamètre b1 d1/4 ellipse</source>
        <translation>Half diameter b1 of a quarter of ellipse</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="172"/>
        <source>Demi-diamètre b2</source>
        <translation>Half diameter b2</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="172"/>
        <source>Demi-diamètre b2 d1/4 ellipse</source>
        <translation>Half diameter b2 of a quarter of ellipse</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="173"/>
        <source>Longueur1</source>
        <translation>Length 1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="173"/>
        <source>Longueur du tronçon inférieur</source>
        <translation>lenght of the lower section (below the most deflected cross-section)</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="174"/>
        <source>Longueur2</source>
        <translation>length 2</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="174"/>
        <source>Longueur du tronçon supérieur</source>
        <translation>length of the upper section (above the most deflected cross-section)</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="175"/>
        <source>Cercle fin bout</source>
        <translation>Top end circle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="176"/>
        <source>Cercle flèche max</source>
        <translation>Most deflected circle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="176"/>
        <source>Cercle où la flèche par rapport à  l&apos;inclinaison globale est maximale</source>
        <translation>Circle for which the deflection to the global lean is maximum</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="177"/>
        <source>Cercle gros bout</source>
        <translation>Butt end circle</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="178"/>
        <source>Attributs du Billon</source>
        <translation>Log attributes</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="182"/>
        <source>Volume1</source>
        <translation>Volume 1</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="185"/>
        <source>Volume2</source>
        <translation>Volume 2</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="188"/>
        <source>Longueur Courbe</source>
        <translation>Curved length</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="191"/>
        <source>Aire flexuosité</source>
        <translation>Area flexuosity</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="194"/>
        <source>Défilement Moyen</source>
        <translation>Average taper</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="197"/>
        <source>Défilement Max</source>
        <translation>Maximum taper</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="200"/>
        <source>Défilement Min</source>
        <translation>Minimal taper</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="568"/>
        <source> Section %1 Attention Problème Volume Sciage</source>
        <translation>Log %1 Warning problem with volume of sawnwood</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="571"/>
        <source> Section %1 Volume Sciage %2 [m3] ( l: %3 a1: %4  a2: %5 b1: %6 b2: %7 m)</source>
        <translation>Log %1 Sawnwood Volume %2 [m3] ( ( l: %3 a1: %4  a2: %5 b1: %6 b2: %7 m)</translation>
    </message>
    <message>
        <source> topCircle NULL</source>
        <translation type="vanished">topCircle NULL</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="418"/>
        <source> Section %1 Volume /Cone %2 m3</source>
        <translation>Log %1 Volume/Cone %2 m3</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="419"/>
        <source> Section %1 Volume /Cylindre %2 m3</source>
        <translation>Log %1 Volume / Cylinder %2 m3</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="420"/>
        <source> Section %1 Défilement [m/m] Moyen %2 Min %3 Max %4 </source>
        <translation>Log %1 Taper[m/m] Average %2 Min %3 Max %4</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="506"/>
        <source> Section %1 Critere lineaire de flexuosite %2 m</source>
        <translation>Log %1 Linear criterion for flexusoity %2 [m]</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="507"/>
        <source> Section %1 Critere surfacique de flexuosite %2 m2</source>
        <translation>Log %1 Area criterion for flexuosity %2[ m²]</translation>
    </message>
    <message>
        <location filename="../step/lb_stepcharacterizelog.cpp" line="508"/>
        <source> Section %1 Fleche maximale %2 m</source>
        <translation>Log %1 Maximal deflection %2 [m]</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lb_pluginmanager.cpp" line="36"/>
        <location filename="../lb_pluginmanager.cpp" line="37"/>
        <source>Qualité des bois</source>
        <translation>Wood quality</translation>
    </message>
</context>
</TS>
