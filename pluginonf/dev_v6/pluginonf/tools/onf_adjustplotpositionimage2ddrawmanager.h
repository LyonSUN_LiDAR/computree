#ifndef ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_H
#define ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"
#include "tools/onf_colorlinearinterpolator.h"

template< typename DataT>
class ONF_AdjustPlotPositionImage2DDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
    ONF_AdjustPlotPositionImage2DDrawManager(QString drawConfigurationName = "");
    virtual ~ONF_AdjustPlotPositionImage2DDrawManager();

    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;

    void setGradient(ONF_ColorLinearInterpolator *colorgradient);
    void setMinMax(int min, int max);
    void setRefZ(double refZ);

protected:

    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;

private:
    ONF_ColorLinearInterpolator* _colorgradient;
    int                       _min;
    int                       _max;
    double                    _refZ;
};

#include "onf_adjustplotpositionimage2ddrawmanager.hpp"

#endif // ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_H
