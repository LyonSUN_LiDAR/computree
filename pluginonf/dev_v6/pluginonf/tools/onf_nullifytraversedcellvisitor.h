#ifndef ONF_nullptrIFYTRAVERSEDCELLVISITOR_H
#define ONF_nullptrIFYTRAVERSEDCELLVISITOR_H

#include "ct_itemdrawable/tools/gridtools/ct_abstractgrid3dbeamvisitor.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"

class ONF_NullifyTraveserdCellVisitor : public CT_AbstractGrid3DBeamVisitor
{
public:

    ONF_NullifyTraveserdCellVisitor(CT_Grid3D_Sparse<int> *grid);

    virtual void visit(const size_t &index, const CT_Beam *beam);

private:
    CT_Grid3D_Sparse<int>*     _grid;
    int  _NA;
};

#endif // ONF_nullptrIFYTRAVERSEDCELLVISITOR_H
