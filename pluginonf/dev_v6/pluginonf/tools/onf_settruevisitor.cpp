#include "onf_settruevisitor.h"

ONF_SetTrueVisitor::ONF_SetTrueVisitor(CT_Grid3D_Sparse<bool> *grid)
{
  _grid = grid;
}

void ONF_SetTrueVisitor::visit(const size_t &index, const CT_Beam *beam)
{
    Q_UNUSED(beam);
    _grid->setValueAtIndex(index, true);
}
