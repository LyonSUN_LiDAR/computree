#ifndef ONF_COREGISTRATION_MONNETMERMIN2014_H
#define ONF_COREGISTRATION_MONNETMERMIN2014_H


#include "ct_itemdrawable/ct_image2d.h"



class ONF_CoRegistration_MonnetMermin2014_TreePosition
{
public:
    ONF_CoRegistration_MonnetMermin2014_TreePosition()
    {
        _x = 0;
        _y = 0;
    }

    double                  _x;
    double                  _y;
    double                  _size;
};


class ONF_CoRegistration_MonnetMermin2014
{
public:   

    static void ONF_CoRegistration_MonnetMermin2014::computeTranslation(CT_Image2D<float>* dhm, QList<ONF_CoRegistration_MonnetMermin2014> treePositions, double &centerX, double &centerY, double radius);
};

#endif // ONF_COREGISTRATION_MONNETMERMIN2014_H
