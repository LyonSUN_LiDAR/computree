/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/onf_metricintensity.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"

#include "ctliblas/tools/las/ct_lasdata.h"
#include "ct_math/ct_mathstatistics.h"


ONF_MetricIntensity::ONF_MetricIntensity() : CT_AbstractMetric_LAS()
{
    declareAttributes();
}

ONF_MetricIntensity::ONF_MetricIntensity(const ONF_MetricIntensity &other) : CT_AbstractMetric_LAS(other)
{
    declareAttributes();
    m_conf = other.m_conf;
}

QString ONF_MetricIntensity::getShortDescription() const
{
    return tr("Métriques d'intensité (LAS H ou LAS Z)");
}

QString ONF_MetricIntensity::getDetailledDescription() const
{
    return tr("");
}

ONF_MetricIntensity::Config ONF_MetricIntensity::metricConfiguration() const
{
    return m_conf;
}

void ONF_MetricIntensity::setMetricConfiguration(const ONF_MetricIntensity::Config &conf)
{
    m_conf = conf;
}

CT_AbstractConfigurableElement *ONF_MetricIntensity::copy() const
{
    return new ONF_MetricIntensity(*this);
}

void ONF_MetricIntensity::computeMetric()
{    
    m_conf.n.value = 0; //ok

    m_conf.n_first.value = 0; //ok
    m_conf.i_max_first.value = -std::numeric_limits<double>::max(); //ok
    m_conf.i_mean_first.value = 0; //ok
    m_conf.i_sd_first.value = 0; //ok
    m_conf.i_cv_first.value = 0; //ok

    m_conf.n_only.value = 0; //ok
    m_conf.i_max_only.value = 0;//ok
    m_conf.i_mean_only.value = 0;//ok
    m_conf.i_sd_only.value = 0; //ok
    m_conf.i_cv_only.value = 0; //ok


    m_conf.n_5_95_first.value = 0; //ok
    m_conf.i_5_95_mean_first.value = 0; //ok
    m_conf.i_5_95_sd_first.value = 0; //ok
    m_conf.i_5_95_cv_first.value = 0; //ok

    m_conf.n_5_95_only.value = 0; //ok
    m_conf.i_5_95_mean_only.value = 0; //ok
    m_conf.i_5_95_sd_only.value = 0; //ok
    m_conf.i_5_95_cv_only.value = 0; //ok

    m_conf.n_10_90_first.value = 0; //ok
    m_conf.i_10_90_mean_first.value = 0; //ok
    m_conf.i_10_90_sd_first.value = 0; //ok
    m_conf.i_10_90_cv_first.value = 0; //ok

    m_conf.n_10_90_only.value = 0; //ok
    m_conf.i_10_90_mean_only.value = 0; //ok
    m_conf.i_10_90_sd_only.value = 0; //ok
    m_conf.i_10_90_cv_only.value = 0; //ok


    m_conf.i_p05_first.value = 0; //ok
    m_conf.i_p10_first.value = 0; //ok
    m_conf.i_p25_first.value = 0; //ok
    m_conf.i_p50_first.value = 0; //ok
    m_conf.i_p75_first.value = 0; //ok
    m_conf.i_p90_first.value = 0; //ok
    m_conf.i_p95_first.value = 0; //ok

    m_conf.i_p05_only.value = 0; //ok
    m_conf.i_p10_only.value = 0; //ok
    m_conf.i_p25_only.value = 0; //ok
    m_conf.i_p50_only.value = 0; //ok
    m_conf.i_p75_only.value = 0; //ok
    m_conf.i_p90_only.value = 0; //ok
    m_conf.i_p95_only.value = 0; //ok

    m_conf.n_top25_first.value = 0; //ok
    m_conf.i_top25_mean_first.value = 0; //ok
    m_conf.i_top25_sd_first.value = 0; //ok
    m_conf.i_top25_cv_first.value = 0; //ok

    m_conf.n_top25_only.value = 0; //ok
    m_conf.i_top25_mean_only.value = 0; //ok
    m_conf.i_top25_sd_only.value = 0; //ok
    m_conf.i_top25_cv_only.value = 0; //ok


    QList<double> i_first;
    QList<double> i_only;
    QMultiMap<double, double> top25_first;
    QMultiMap<double, double> top25_only;
    double minZ_first = std::numeric_limits<double>::max();
    double maxZ_first = -std::numeric_limits<double>::max();

    double minZ_only = std::numeric_limits<double>::max();
    double maxZ_only = -std::numeric_limits<double>::max();

    CT_PointIterator itP(pointCloud());
    while(itP.hasNext())
    {
        const CT_Point& point = itP.next().currentPoint();

        if ((plotArea() == NULL) || plotArea()->contains(point(0), point(1)))
        {
            ++(m_conf.n.value);

            size_t index = itP.currentGlobalIndex();
            if (lasPointCloudIndex()->contains(index))
            {
                size_t lasIndex = lasPointCloudIndex()->indexOf(index);
                CT_LASData lasData;
                lasAttributes()->getLASDataAt(lasIndex, lasData);


                // First
                if (lasData._Return_Number == 1 && lasData._Number_of_Returns >= 1)
                {
                    ++(m_conf.n_first.value);
                    i_first.append(lasData._Intensity);
                    top25_first.insert(point(2), lasData._Intensity);
                    if (point(2) < minZ_first) {minZ_first = point(2);}
                    if (point(2) > maxZ_first) {maxZ_first = point(2);}

                    if (lasData._Intensity > m_conf.i_max_first.value) {m_conf.i_max_first.value = lasData._Intensity;}
                    m_conf.i_mean_first.value += lasData._Intensity;
                    m_conf.i_sd_first.value += lasData._Intensity*lasData._Intensity;
                }

                // Only
                if (lasData._Return_Number == 1 && lasData._Number_of_Returns == 1)
                {
                    ++(m_conf.n_only.value);
                    i_only.append(lasData._Intensity);
                    top25_only.insert(point(2), lasData._Intensity);
                    if (point(2) < minZ_only) {minZ_only = point(2);}
                    if (point(2) > maxZ_only) {maxZ_only = point(2);}

                    if (lasData._Intensity > m_conf.i_max_only.value) {m_conf.i_max_only.value = lasData._Intensity;}
                    m_conf.i_mean_only.value += lasData._Intensity;
                    m_conf.i_sd_only.value += lasData._Intensity*lasData._Intensity;
                }


            }
        }
    }


    if (m_conf.n_first.value > 0)
    {
        m_conf.i_mean_first.value /= (double) (m_conf.n_first.value);
        if (m_conf.n_first.value > 1)
        {
            m_conf.i_sd_first.value = sqrt(((double)(m_conf.n_first.value)/(double)(m_conf.n_first.value - 1.0)) * ((1.0/(double)(m_conf.n_first.value)) * m_conf.i_sd_first.value - m_conf.i_mean_first.value*m_conf.i_mean_first.value));

            if (m_conf.i_mean_first.value != 0)
            {
                m_conf.i_cv_first.value = m_conf.i_sd_first.value / m_conf.i_mean_first.value;
            }
        }

        m_conf.i_p05_first.value = CT_MathStatistics::computeQuantile(i_first, 0.05, true);
        m_conf.i_p10_first.value = CT_MathStatistics::computeQuantile(i_first, 0.10, false);
        m_conf.i_p25_first.value = CT_MathStatistics::computeQuantile(i_first, 0.25, false);
        m_conf.i_p50_first.value = CT_MathStatistics::computeQuantile(i_first, 0.50, false);
        m_conf.i_p75_first.value = CT_MathStatistics::computeQuantile(i_first, 0.75, false);
        m_conf.i_p90_first.value = CT_MathStatistics::computeQuantile(i_first, 0.90, false);
        m_conf.i_p95_first.value = CT_MathStatistics::computeQuantile(i_first, 0.95, false);

        for (int i = 0 ; i < i_first.size() ; i++)
        {
            double intensity = i_first.at(i);

            if (intensity >= m_conf.i_p05_first.value && intensity <= m_conf.i_p95_first.value)
            {
                ++m_conf.n_5_95_first.value;
                m_conf.i_5_95_mean_first.value += intensity;
                m_conf.i_5_95_sd_first.value += intensity*intensity;
            }

            if (intensity >= m_conf.i_p10_first.value && intensity <= m_conf.i_p90_first.value)
            {
                ++m_conf.n_10_90_first.value;
                m_conf.i_10_90_mean_first.value += intensity;
                m_conf.i_10_90_sd_first.value += intensity*intensity;
            }
        }

        if (m_conf.n_5_95_first.value > 0)
        {
            m_conf.i_5_95_mean_first.value /= (double) (m_conf.n_5_95_first.value);
            if (m_conf.n_5_95_first.value > 1)
            {
                m_conf.i_5_95_sd_first.value = sqrt(((double)(m_conf.n_5_95_first.value)/(double)(m_conf.n_5_95_first.value - 1.0)) * ((1.0/(double)(m_conf.n_5_95_first.value)) * m_conf.i_5_95_sd_first.value - m_conf.i_5_95_mean_first.value*m_conf.i_5_95_mean_first.value));

                if (m_conf.n_5_95_first.value != 0)
                {
                    m_conf.i_5_95_cv_first.value = m_conf.i_5_95_sd_first.value / m_conf.i_5_95_mean_first.value;
                }
            }
        }

        if (m_conf.n_10_90_first.value > 0)
        {
            m_conf.i_10_90_mean_first.value /= (double) (m_conf.n_10_90_first.value);
            if (m_conf.n_10_90_first.value > 1)
            {
                m_conf.i_10_90_sd_first.value = sqrt(((double)(m_conf.n_10_90_first.value)/(double)(m_conf.n_10_90_first.value - 1.0)) * ((1.0/(double)(m_conf.n_10_90_first.value)) * m_conf.i_10_90_sd_first.value - m_conf.i_10_90_mean_first.value*m_conf.i_10_90_mean_first.value));

                if (m_conf.n_10_90_first.value != 0)
                {
                    m_conf.i_10_90_cv_first.value = m_conf.i_10_90_sd_first.value / m_conf.i_10_90_mean_first.value;
                }
            }
        }

        minZ_first = maxZ_first - (maxZ_first - minZ_first)*0.25;
        QMapIterator<double, double> itm_first(top25_first);
        while (itm_first.hasNext())
        {
            itm_first.next();
            double z =itm_first.key();
            double intensity = itm_first.value();
            if (z >= minZ_first)
            {
                ++m_conf.n_top25_first.value;
                m_conf.i_top25_mean_first.value += intensity;
                m_conf.i_top25_sd_first.value += intensity*intensity;
            }
        }

        if (m_conf.n_top25_first.value > 0)
        {
            m_conf.i_top25_mean_first.value /= (double) (m_conf.n_top25_first.value);
            if (m_conf.n_top25_first.value > 1)
            {
                m_conf.i_top25_sd_first.value = sqrt(((double)(m_conf.n_top25_first.value)/(double)(m_conf.n_top25_first.value - 1.0)) * ((1.0/(double)(m_conf.n_top25_first.value)) * m_conf.i_top25_sd_first.value - m_conf.i_top25_mean_first.value*m_conf.i_top25_mean_first.value));

                if (m_conf.n_top25_first.value != 0)
                {
                    m_conf.i_top25_cv_first.value = m_conf.i_top25_sd_first.value / m_conf.i_top25_mean_first.value;
                }
            }
        }

    }

    if (m_conf.n_only.value > 0)
    {
        m_conf.i_mean_only.value /= (double) (m_conf.n_only.value);

        if (m_conf.n_only.value > 1)
        {
            m_conf.i_sd_only.value = sqrt(((double)(m_conf.n_only.value)/(double)(m_conf.n_only.value - 1.0)) * ((1.0/(double)(m_conf.n_only.value)) * m_conf.i_sd_only.value - m_conf.i_mean_only.value*m_conf.i_mean_only.value));

            if (m_conf.i_mean_only.value != 0)
            {
                m_conf.i_cv_only.value = m_conf.i_sd_only.value / m_conf.i_mean_only.value;
            }
        }

        m_conf.i_p05_only.value = CT_MathStatistics::computeQuantile(i_only, 0.05, true);
        m_conf.i_p10_only.value = CT_MathStatistics::computeQuantile(i_only, 0.10, false);
        m_conf.i_p25_only.value = CT_MathStatistics::computeQuantile(i_only, 0.25, false);
        m_conf.i_p50_only.value = CT_MathStatistics::computeQuantile(i_only, 0.50, false);
        m_conf.i_p75_only.value = CT_MathStatistics::computeQuantile(i_only, 0.75, false);
        m_conf.i_p90_only.value = CT_MathStatistics::computeQuantile(i_only, 0.90, false);
        m_conf.i_p95_only.value = CT_MathStatistics::computeQuantile(i_only, 0.95, false);

        for (int i = 0 ; i < i_only.size() ; i++)
        {
            double intensity = i_only.at(i);

            if (intensity >= m_conf.i_p05_only.value && intensity <= m_conf.i_p95_only.value)
            {
                ++m_conf.n_5_95_only.value;
                m_conf.i_5_95_mean_only.value += intensity;
                m_conf.i_5_95_sd_only.value += intensity*intensity;
            }

            if (intensity >= m_conf.i_p10_only.value && intensity <= m_conf.i_p90_only.value)
            {
                ++m_conf.n_10_90_only.value;
                m_conf.i_10_90_mean_only.value += intensity;
                m_conf.i_10_90_sd_only.value += intensity*intensity;
            }
        }

        if (m_conf.n_5_95_only.value > 0)
        {
            m_conf.i_5_95_mean_only.value /= (double) (m_conf.n_5_95_only.value);
            if (m_conf.n_5_95_only.value > 1)
            {
                m_conf.i_5_95_sd_only.value = sqrt(((double)(m_conf.n_5_95_only.value)/(double)(m_conf.n_5_95_only.value - 1.0)) * ((1.0/(double)(m_conf.n_5_95_only.value)) * m_conf.i_5_95_sd_only.value - m_conf.i_5_95_mean_only.value*m_conf.i_5_95_mean_only.value));

                if (m_conf.n_5_95_only.value != 0)
                {
                    m_conf.i_5_95_cv_only.value = m_conf.i_5_95_sd_only.value / m_conf.i_5_95_mean_only.value;
                }
            }
        }

        if (m_conf.n_10_90_only.value > 0)
        {
            m_conf.i_10_90_mean_only.value /= (double) (m_conf.n_10_90_only.value);
            if (m_conf.n_10_90_only.value > 1)
            {
                m_conf.i_10_90_sd_only.value = sqrt(((double)(m_conf.n_10_90_only.value)/(double)(m_conf.n_10_90_only.value - 1.0)) * ((1.0/(double)(m_conf.n_10_90_only.value)) * m_conf.i_10_90_sd_only.value - m_conf.i_10_90_mean_only.value*m_conf.i_10_90_mean_only.value));

                if (m_conf.n_10_90_only.value != 0)
                {
                    m_conf.i_10_90_cv_only.value = m_conf.i_10_90_sd_only.value / m_conf.i_10_90_mean_only.value;
                }
            }
        }

        minZ_only = maxZ_only - (maxZ_only - minZ_only)*0.25;
        QMapIterator<double, double> itm_only(top25_only);
        while (itm_only.hasNext())
        {
            itm_only.next();
            double z =itm_only.key();
            double intensity = itm_only.value();
            if (z >= minZ_only)
            {
                ++m_conf.n_top25_only.value;
                m_conf.i_top25_mean_only.value += intensity;
                m_conf.i_top25_sd_only.value += intensity*intensity;
            }
        }

        if (m_conf.n_top25_only.value > 0)
        {
            m_conf.i_top25_mean_only.value /= (double) (m_conf.n_top25_only.value);
            if (m_conf.n_top25_only.value > 1)
            {
                m_conf.i_top25_sd_only.value = sqrt(((double)(m_conf.n_top25_only.value)/(double)(m_conf.n_top25_only.value - 1.0)) * ((1.0/(double)(m_conf.n_top25_only.value)) * m_conf.i_top25_sd_only.value - m_conf.i_top25_mean_only.value*m_conf.i_top25_mean_only.value));

                if (m_conf.n_top25_only.value != 0)
                {
                    m_conf.i_top25_cv_only.value = m_conf.i_top25_sd_only.value / m_conf.i_top25_mean_only.value;
                }
            }
        }

    }

    setAttributeValueVaB(m_conf.n);
    setAttributeValueVaB(m_conf.n_first);
    setAttributeValueVaB(m_conf.i_max_first);
    setAttributeValueVaB(m_conf.i_mean_first);
    setAttributeValueVaB(m_conf.i_sd_first);
    setAttributeValueVaB(m_conf.i_cv_first);
    setAttributeValueVaB(m_conf.n_only);
    setAttributeValueVaB(m_conf.i_max_only);
    setAttributeValueVaB(m_conf.i_mean_only);
    setAttributeValueVaB(m_conf.i_sd_only);
    setAttributeValueVaB(m_conf.i_cv_only);

    setAttributeValueVaB(m_conf.n_5_95_first);
    setAttributeValueVaB(m_conf.i_5_95_mean_first);
    setAttributeValueVaB(m_conf.i_5_95_sd_first);
    setAttributeValueVaB(m_conf.i_5_95_cv_first);
    setAttributeValueVaB(m_conf.n_5_95_only);
    setAttributeValueVaB(m_conf.i_5_95_mean_only);
    setAttributeValueVaB(m_conf.i_5_95_sd_only);
    setAttributeValueVaB(m_conf.i_5_95_cv_only);

    setAttributeValueVaB(m_conf.n_10_90_first);
    setAttributeValueVaB(m_conf.i_10_90_mean_first);
    setAttributeValueVaB(m_conf.i_10_90_sd_first);
    setAttributeValueVaB(m_conf.i_10_90_cv_first);
    setAttributeValueVaB(m_conf.n_10_90_only);
    setAttributeValueVaB(m_conf.i_10_90_mean_only);
    setAttributeValueVaB(m_conf.i_10_90_sd_only);
    setAttributeValueVaB(m_conf.i_10_90_cv_only);

    setAttributeValueVaB(m_conf.n_top25_first);
    setAttributeValueVaB(m_conf.i_top25_mean_first);
    setAttributeValueVaB(m_conf.i_top25_sd_first);
    setAttributeValueVaB(m_conf.i_top25_cv_first);
    setAttributeValueVaB(m_conf.n_top25_only);
    setAttributeValueVaB(m_conf.i_top25_mean_only);
    setAttributeValueVaB(m_conf.i_top25_sd_only);
    setAttributeValueVaB(m_conf.i_top25_cv_only);

    setAttributeValueVaB(m_conf.i_p05_first);
    setAttributeValueVaB(m_conf.i_p10_first);
    setAttributeValueVaB(m_conf.i_p25_first);
    setAttributeValueVaB(m_conf.i_p50_first);
    setAttributeValueVaB(m_conf.i_p75_first);
    setAttributeValueVaB(m_conf.i_p90_first);
    setAttributeValueVaB(m_conf.i_p95_first);
    setAttributeValueVaB(m_conf.i_p05_only);
    setAttributeValueVaB(m_conf.i_p10_only);
    setAttributeValueVaB(m_conf.i_p25_only);
    setAttributeValueVaB(m_conf.i_p50_only);
    setAttributeValueVaB(m_conf.i_p75_only);
    setAttributeValueVaB(m_conf.i_p90_only);
    setAttributeValueVaB(m_conf.i_p95_only);
}

void ONF_MetricIntensity::declareAttributes()
{
    registerAttributeVaB(m_conf.n, CT_AbstractCategory::DATA_NUMBER, tr("n"));
    registerAttributeVaB(m_conf.n_first, CT_AbstractCategory::DATA_NUMBER, tr("n_first"));
    registerAttributeVaB(m_conf.i_max_first, CT_AbstractCategory::DATA_NUMBER, tr("i_max_first"));
    registerAttributeVaB(m_conf.i_mean_first, CT_AbstractCategory::DATA_NUMBER, tr("i_mean_first"));
    registerAttributeVaB(m_conf.i_sd_first, CT_AbstractCategory::DATA_NUMBER, tr("i_sd_first"));
    registerAttributeVaB(m_conf.i_cv_first, CT_AbstractCategory::DATA_NUMBER, tr("i_cv_first"));
    registerAttributeVaB(m_conf.n_only, CT_AbstractCategory::DATA_NUMBER, tr("n_only"));
    registerAttributeVaB(m_conf.i_max_only, CT_AbstractCategory::DATA_NUMBER, tr("i_max_only"));
    registerAttributeVaB(m_conf.i_mean_only, CT_AbstractCategory::DATA_NUMBER, tr("i_mean_only"));
    registerAttributeVaB(m_conf.i_sd_only, CT_AbstractCategory::DATA_NUMBER, tr("i_sd_only"));
    registerAttributeVaB(m_conf.i_cv_only, CT_AbstractCategory::DATA_NUMBER, tr("i_cv_only"));

    registerAttributeVaB(m_conf.n_5_95_first, CT_AbstractCategory::DATA_NUMBER, tr("n_5_95_first"));
    registerAttributeVaB(m_conf.i_5_95_mean_first, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_mean_first"));
    registerAttributeVaB(m_conf.i_5_95_sd_first, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_sd_first"));
    registerAttributeVaB(m_conf.i_5_95_cv_first, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_cv_first"));
    registerAttributeVaB(m_conf.n_5_95_only, CT_AbstractCategory::DATA_NUMBER, tr("n_5_95_only"));
    registerAttributeVaB(m_conf.i_5_95_mean_only, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_mean_only"));
    registerAttributeVaB(m_conf.i_5_95_sd_only, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_sd_only"));
    registerAttributeVaB(m_conf.i_5_95_cv_only, CT_AbstractCategory::DATA_NUMBER, tr("i_5_95_cv_only"));

    registerAttributeVaB(m_conf.n_10_90_first, CT_AbstractCategory::DATA_NUMBER, tr("n_10_90_first"));
    registerAttributeVaB(m_conf.i_10_90_mean_first, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_mean_first"));
    registerAttributeVaB(m_conf.i_10_90_sd_first, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_sd_first"));
    registerAttributeVaB(m_conf.i_10_90_cv_first, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_cv_first"));
    registerAttributeVaB(m_conf.n_10_90_only, CT_AbstractCategory::DATA_NUMBER, tr("n_10_90_only"));
    registerAttributeVaB(m_conf.i_10_90_mean_only, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_mean_only"));
    registerAttributeVaB(m_conf.i_10_90_sd_only, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_sd_only"));
    registerAttributeVaB(m_conf.i_10_90_cv_only, CT_AbstractCategory::DATA_NUMBER, tr("i_10_90_cv_only"));

    registerAttributeVaB(m_conf.n_top25_first, CT_AbstractCategory::DATA_NUMBER, tr("n_top25_first"));
    registerAttributeVaB(m_conf.i_top25_mean_first, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_mean_first"));
    registerAttributeVaB(m_conf.i_top25_sd_first, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_sd_first"));
    registerAttributeVaB(m_conf.i_top25_cv_first, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_cv_first"));
    registerAttributeVaB(m_conf.n_top25_only, CT_AbstractCategory::DATA_NUMBER, tr("n_top25_only"));
    registerAttributeVaB(m_conf.i_top25_mean_only, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_mean_only"));
    registerAttributeVaB(m_conf.i_top25_sd_only, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_sd_only"));
    registerAttributeVaB(m_conf.i_top25_cv_only, CT_AbstractCategory::DATA_NUMBER, tr("i_top25_cv_only"));

    registerAttributeVaB(m_conf.i_p05_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p05_first"));
    registerAttributeVaB(m_conf.i_p10_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p10_first"));
    registerAttributeVaB(m_conf.i_p25_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p25_first"));
    registerAttributeVaB(m_conf.i_p50_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p50_first"));
    registerAttributeVaB(m_conf.i_p75_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p75_first"));
    registerAttributeVaB(m_conf.i_p90_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p90_first"));
    registerAttributeVaB(m_conf.i_p95_first, CT_AbstractCategory::DATA_NUMBER, tr("i_p95_first"));
    registerAttributeVaB(m_conf.i_p05_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p05_only"));
    registerAttributeVaB(m_conf.i_p10_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p10_only"));
    registerAttributeVaB(m_conf.i_p25_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p25_only"));
    registerAttributeVaB(m_conf.i_p50_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p50_only"));
    registerAttributeVaB(m_conf.i_p75_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p75_only"));
    registerAttributeVaB(m_conf.i_p90_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p90_only"));
    registerAttributeVaB(m_conf.i_p95_only, CT_AbstractCategory::DATA_NUMBER, tr("i_p95_only"));
}

