/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcompare3dgridscontents.h"

ONF_StepCompare3DGridsContents::ONF_StepCompare3DGridsContents() : SuperClass()
{
    _threshold = 1;
}

QString ONF_StepCompare3DGridsContents::description() const
{
    return tr("Comparer deux grilles 3D");
}

QString ONF_StepCompare3DGridsContents::detailledDescription() const
{
    return tr("Il est préférable que les grilles aient la même résolution et le même calage spatial.<br>"
              "Considérant A = Grille initiale (avant), B = Grille finale (après)"
              "En sortie l'étape renvoie une grille contenant :<br>"
              "* 00 : A =  NA,    B =  NA<br>"
              "* 01 : A =  NA,    B <  Seuil<br>"
              "* 02 : A =  NA,    B >= Seuil<br>"
              "* 10 : A <  Seuil, B =  NA<br>"
              "* 11 : A <  Seuil, B <  Seuil<br>"
              "* 12 : A <  Seuil, B >= Seuil<br>"
              "* 20 : A >= Seuil, B =  NA<br>"
              "* 21 : A >= Seuil, B <  Seuil<br>"
              "* 22 : A >= Seuil, B >= Seuil<br>");
}

QString ONF_StepCompare3DGridsContents::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCompare3DGridsContents::createNewInstance() const
{
    return new ONF_StepCompare3DGridsContents();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCompare3DGridsContents::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult1, tr("Grille A (avant)"));
    manager.setZeroOrMoreRootGroup(_inResult1, _inZeroOrMoreRootGroup1);
    manager.addGroup(_inZeroOrMoreRootGroup1, _inGroup1, tr("Groupe"));
    manager.addItem(_inGroup1, _inGrid1, tr("Grille A (avant)"));

    manager.addResult(_inResult2, tr("Grille B (après)"));
    manager.setZeroOrMoreRootGroup(_inResult2, _inZeroOrMoreRootGroup2);
    manager.addGroup(_inZeroOrMoreRootGroup2, _inGroup2, tr("Groupe"));
    manager.addItem(_inGroup2, _inGrid2, tr("Grille B (après)"));
}

void ONF_StepCompare3DGridsContents::declareOutputModels(CT_StepOutModelStructureManager& manager)
{

    manager.addResult(_outResult, tr("Comparaison des grilles 3D"));
    manager.setRootGroup(_outResult, _outRootGroup);
    manager.addItem(_outRootGroup, _outGrid, tr("Grille de comparaison"));
}

void ONF_StepCompare3DGridsContents::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble("Valeur minimum pour considérer une case remplie", "", -1e+09, 1e+09, 4, _threshold, 1);
}

void ONF_StepCompare3DGridsContents::compute()
{

    // run only turn one
    for (const CT_AbstractGrid3D* grid1 : _inGrid1.iterateInputs(_inResult1))
    {
        // run only turn one
        for (const CT_AbstractGrid3D* grid2 : _inGrid2.iterateInputs(_inResult2))
        {
            if (grid1 != nullptr && grid2 != nullptr)
            {
                double xmin = grid1->minX();
                double ymin = grid1->minY();
                double zmin = grid1->minZ();
                double xmax = grid1->maxX();
                double ymax = grid1->maxY();
                double zmax = grid1->maxZ();

                double res = grid1->resolution();

                if (grid2->minX() < xmin) {xmin = grid2->minX();}
                if (grid2->minY() < ymin) {ymin = grid2->minY();}
                if (grid2->minZ() < zmin) {zmin = grid2->minZ();}
                if (grid2->maxX() > xmax) {xmax = grid2->maxX();}
                if (grid2->maxY() > ymax) {ymax = grid2->maxY();}
                if (grid2->maxZ() > zmax) {zmax = grid2->maxZ();}

                if (grid2->resolution() < res) {res = grid2->resolution();}

                for (CT_ResultGroup* result : _outResult.iterateOutputs())
                {
                    // Création de la grille de sortie
                    CT_StandardItemGroup* outGrp= new CT_StandardItemGroup();
                    result->addRootGroup(_outRootGroup, outGrp);

                    CT_Grid3D<short>* gridOut = CT_Grid3D<short>::createGrid3DFromXYZCoords(xmin, ymin, zmin, xmax, ymax, zmax, res, -1, -1, false);
                    outGrp->addSingularItem(_outGrid, gridOut);

                    gridOut->setDefaultColor(QColor(255, 255, 255));
                    gridOut->addColorForValue(00, QColor(255, 125, 125));
                    gridOut->addColorForValue(01, QColor(125, 0, 0));
                    gridOut->addColorForValue(02, QColor(0, 0, 125));
                    gridOut->addColorForValue(10, QColor(255, 255, 125));
                    gridOut->addColorForValue(11, QColor(255, 255, 0));
                    gridOut->addColorForValue(12, QColor(0, 255, 0));
                    gridOut->addColorForValue(20, QColor(125, 125, 255));
                    gridOut->addColorForValue(21, QColor(255, 0, 0));
                    gridOut->addColorForValue(22, QColor(0, 0, 255));

                    // Remplissage de la grille de sortie
                    for (size_t n = 0 ; n < gridOut->nCells() ; n++)
                    {
                        Eigen::Vector3d center;
                        gridOut->getCellCenterCoordinates(n, center);

                        float val1, val2;
                        size_t index1, index2;

                        if (grid1->indexAtXYZ(center(0), center(1), center(2), index1))
                        {
                            val1 = float(grid1->valueAtIndexAsDouble(index1));
                        } else {
                            val1 = NAN;
                        }

                        if (grid2->indexAtXYZ(center(0), center(1), center(2), index2))
                        {
                            val2 = float(grid2->valueAtIndexAsDouble(index2));
                        } else {
                            val2 = NAN;
                        }

                        if (std::isnan(val1)) {
                            if (std::isnan(val2)) {
                                gridOut->setValueAtIndex(n, 00);
                            } else if (val2 < float(_threshold)) {
                                gridOut->setValueAtIndex(n, 01);
                            } else {
                                gridOut->setValueAtIndex(n, 02);
                            }
                        } else if (val1 < float(_threshold)) {
                            if (std::isnan(val2)) {
                                gridOut->setValueAtIndex(n, 10);
                            } else if (val2 < float(_threshold)) {
                                gridOut->setValueAtIndex(n, 11);
                            } else {
                                gridOut->setValueAtIndex(n, 12);
                            }
                        } else {
                            if (std::isnan(val2)) {
                                gridOut->setValueAtIndex(n, 20);
                            } else if (val2 < float(_threshold)) {
                                gridOut->setValueAtIndex(n, 21);
                            } else {
                                gridOut->setValueAtIndex(n, 22);
                            }
                        }
                    }
                    gridOut->computeMinMax();
                }
            }
            if (grid2 != nullptr) {break;}
        }
        if (grid1 != nullptr) {break;}
    }

}
