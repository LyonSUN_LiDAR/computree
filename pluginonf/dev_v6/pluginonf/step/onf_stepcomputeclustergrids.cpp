/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputeclustergrids.h"

#include <QFileInfo>

ONF_StepComputeClusterGrids::ONF_StepComputeClusterGrids() : SuperClass()
{
    _res = 0.5;
    _dilatation = 1;
}

QString ONF_StepComputeClusterGrids::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Créer des grilles booléennes par cluster");
}

QString ONF_StepComputeClusterGrids::detailledDescription() const
{
    return tr("Pour chaque grille d'entrée, cette étape génère un grille voxel booléenne avec true pour les cases contenant un point<br>"
              "Les grilles sont sparse, il est donc possible d'utiliser des résolutions fines."
              "Un attribut stocke le nom du cluster");
}

CT_VirtualAbstractStep* ONF_StepComputeClusterGrids::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepComputeClusterGrids();
}

void ONF_StepComputeClusterGrids::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène(s)"));

    if (_otherItem)
    {
        manager.addItem(_inGroup, _inItemWithName, tr("ItemWithName"));
        manager.addItemAttribute(_inItemWithName, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("Name"));
    } else {
        manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("Name"));
    }

}

void ONF_StepComputeClusterGrids::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outBoolGrid, tr("Cluster"));
    manager.addItemAttribute(_outBoolGrid, _outClusterNameAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ClusterName"));
}

void ONF_StepComputeClusterGrids::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool(tr("Nom de fichier dans un autre item"), "", "", _otherItem);
}

void ONF_StepComputeClusterGrids::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution de la grille"),tr("meters"),0.0001,10000,3, _res );
    postInputConfigDialog->addInt(tr("Dilatation"),tr("Cellules"),0,999, _dilatation);
}

void ONF_StepComputeClusterGrids::compute()
{
    int nclusters = 0;
    for (const CT_AbstractItemDrawableWithPointCloud* scene : _inScene.iterateOutputs(_inResult))
    {
        Q_UNUSED(scene);
        nclusters++;
    }

    int cpt = 0;
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractSingularItemDrawable *itemWithName = nullptr;
            const CT_AbstractItemAttribute* att = nullptr;

            if (_otherItem)
            {
                itemWithName = group->singularItem(_inItemWithName);
                if (itemWithName != nullptr)
                {
                    att = itemWithName->itemAttribute(_inAtt);
                }
            } else {
                itemWithName = static_cast<const CT_AbstractSingularItemDrawable*>(scene);
                att = scene->itemAttribute(_inAtt);
            }

            QString clusterName;
            if (att != nullptr)
            {
                clusterName = att->toString(itemWithName, nullptr);

                QFileInfo fileinfo(clusterName);
                if (fileinfo.exists())
                {
                    clusterName = fileinfo.baseName();
                }
            }

            double minX = (std::floor(scene->minX() - 1) / _res) * _res;
            double minY = (std::floor(scene->minY() - 1) / _res) * _res;
            double minZ = (std::floor(scene->minZ() - 1) / _res) * _res;

            while (minX < scene->minX()) {minX += _res;};
            while (minY < scene->minY()) {minY += _res;};
            while (minZ < scene->minZ()) {minZ += _res;};

            while (minX > scene->minX()) {minX -= _res;};
            while (minY > scene->minY()) {minY -= _res;};
            while (minZ > scene->minZ()) {minZ -= _res;};

            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* boolGrid = CT_Grid3D_Sparse<bool>::createGrid3DFromXYZCoords(minX, minY, minZ, scene->maxX(), scene->maxY(), scene->maxZ(), _res, false, false);

            CT_PointIterator itP(scene->pointCloudIndex()) ;
            while(itP.hasNext())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t indice;

                if (boolGrid->indexAtXYZ(point(0), point(1), point(2), indice))
                {
                    // Hits Computing
                    boolGrid->setValueAtIndex(indice, true);

                    if (_dilatation > 0)
                    {
                        int col, lin, levz;
                        boolGrid->indexToGrid(indice, col, lin, levz);

                        for (int xx = col - _dilatation; xx <= col +_dilatation ; xx++)
                        {
                            if (xx >= 0 && xx < boolGrid->xdim())
                            {
                                for (int yy = lin - _dilatation; yy <= lin + _dilatation ; yy++)
                                {
                                    if (yy >= 0 && yy < boolGrid->ydim())
                                    {
                                        for (int zz = levz - _dilatation ; zz <= levz + _dilatation ; zz++)
                                        {
                                            if (zz >= 0 && zz < boolGrid->zdim())
                                            {
                                                boolGrid->setValue(xx, yy, zz, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                } else {
                    qDebug() << "Le point n'est pas dans la grille";
                }
            }

            group->addSingularItem(_outBoolGrid, boolGrid);
            boolGrid->addItemAttribute(_outClusterNameAtt, new CT_StdItemAttributeT<QString>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), clusterName));
        }

        setProgress(float(100.0 * cpt++ / nclusters));
    }

    setProgress(99.0f);
}

