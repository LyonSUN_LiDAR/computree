/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_steploadpositionsformatching.h"
#include "ct_log/ct_logmanager.h"

#include <QFile>
#include <QTextStream>

ONF_StepLoadPositionsForMatching::ONF_StepLoadPositionsForMatching() : CT_StepBeginLoop()
{
    _neededFields << "ID_Plot" << "ID" << "X" << "Y" << "VALUE";

    _refFileName = "";
    _transFileName = "";

    _refHeader = true;
    _transHeader = true;

    _refSeparator = "\t";
    _transSeparator = "\t";

    _refDecimal = ".";
    _transDecimal = ".";

    _refLocale = QLocale(QLocale::English, QLocale::UnitedKingdom).name();
    _transLocale = QLocale(QLocale::English, QLocale::UnitedKingdom).name();

    _refSkip = 0;
    _transSkip = 0;
}

QString ONF_StepLoadPositionsForMatching::description() const
{
    return tr("Fichiers de positions (2) pour mise en correspondance");
}

QString ONF_StepLoadPositionsForMatching::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepLoadPositionsForMatching::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepLoadPositionsForMatching::createNewInstance() const
{
    return new ONF_StepLoadPositionsForMatching();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepLoadPositionsForMatching::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    manager.setNotNeedInputResult();
}

void ONF_StepLoadPositionsForMatching::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResult(_outResultRef, tr("Positions de référence"));
    manager.setRootGroup(_outResultRef, _outGrpRef, tr("Groupe"));
    manager.addItem(_outGrpRef, _outRef, tr("Position de référence"));
    manager.addItemAttribute(_outRef, _outRefVal, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_HEIGHT), tr("Valeur"));
    manager.addItemAttribute(_outRef, _outRefID, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDtree"));
    manager.addItemAttribute(_outRef, _outRefIDPlot, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDplot"));

    manager.addResult(_outResultTrans, tr("Positions à transformer"));
    manager.setRootGroup(_outResultTrans, _outGrpTrans, tr("Groupe"));
    manager.addItem(_outGrpTrans, _outTrans, tr("Position à transformer"));
    manager.addItemAttribute(_outTrans, _outTransVal, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_HEIGHT), tr("Valeur"));
    manager.addItemAttribute(_outTrans, _outTransID, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDtree"));
    manager.addItemAttribute(_outTrans, _outTransIDplot, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDplot"));
}

void ONF_StepLoadPositionsForMatching::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addAsciiFileChoice("Fichier de positions de référence", "Fichier ASCII (*.txt ; *.asc)", true, _neededFields, _refFileName, _refHeader, _refSeparator, _refDecimal, _refLocale, _refSkip, _refColumns);
    postInputConfigDialog->addAsciiFileChoice("Fichier de positions à transformer", "Fichier ASCII (*.txt ; *.asc)", true, _neededFields, _transFileName, _transHeader, _transSeparator, _transDecimal, _transLocale, _transSkip, _transColumns);
}

void ONF_StepLoadPositionsForMatching::compute()
{
    SuperClass::compute();

    int colIDplot_ref  = _refColumns.value("ID_Plot", -1);
    int colIDplot_trans  = _transColumns.value("ID_Plot", -1);
    bool multiPlots = (colIDplot_ref >= 0 && colIDplot_trans >= 0);

    int  currentTurn = _counter->currentTurn();

    // Au premier tour : création de la liste des placettes
    if (multiPlots && currentTurn == 1)
    {
        _plotsIds.clear();

        QFile fRef00(_refFileName);
        if (fRef00.exists() && fRef00.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&fRef00);
            stream.setLocale(_refLocale);

            for (int i = 0 ; i < _refSkip ; i++) {stream.readLine();}
            if (_refHeader) {stream.readLine();}

            while (!stream.atEnd())
            {
                QString line = stream.readLine();
                if (!line.isEmpty())
                {
                    QStringList values = line.split(_refSeparator);

                    if (values.size() > colIDplot_ref)
                    {
                        const QString &val = values.at(colIDplot_ref);
                        if (!_plotsIds.contains(val)) {_plotsIds.append(val);}
                    }
                }
            }
            fRef00.close();
        }
        _counter->setNTurns(_plotsIds.size());
        if (_plotsIds.size() <= 0) {return;}
    }

    QString currentPlot = "";
    if (multiPlots) {currentPlot = _plotsIds.at(currentTurn - 1);}
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Placette en cours de traitement : %1")).arg(currentPlot));

    for (CT_ResultGroup* resultRef : _outResultRef.iterateOutputs())
    {
        QFile fRef(_refFileName);
        if (fRef.exists() && fRef.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&fRef);
            stream.setLocale(_refLocale);

            int colID  = _refColumns.value("ID", -1);
            int colX   = _refColumns.value("X", -1);
            int colY   = _refColumns.value("Y", -1);
            int colVal = _refColumns.value("VALUE", -1);

            if (colID >=0 && colX >= 0 && colY >= 0 && colVal >= 0)
            {

                int colMax = colID;
                if (colX   > colMax) {colMax = colX;}
                if (colY   > colMax) {colMax = colY;}
                if (colVal > colMax) {colMax = colVal;}

                for (int i = 0 ; i < _refSkip ; i++) {stream.readLine();}
                if (_refHeader) {stream.readLine();}

                size_t cpt = 1;
                while (!stream.atEnd())
                {
                    QString line = stream.readLine();
                    cpt++;
                    if (!line.isEmpty())
                    {
                        QStringList values = line.split(_refSeparator);
                        if (values.size() >= colMax)
                        {
                            QString plot = "";
                            if (multiPlots) {plot = values.at(colIDplot_ref);}

                            if (plot == currentPlot)
                            {
                                bool okX, okY, okVal;
                                double x = _refLocale.toDouble(values.at(colX), &okX);
                                double y = _refLocale.toDouble(values.at(colY), &okY);
                                float val = _refLocale.toFloat(values.at(colVal), &okVal);
                                QString id = values.at(colID);

                                if (okX && okY && okVal)
                                {
                                    CT_StandardItemGroup* grpRef= new CT_StandardItemGroup();
                                    resultRef->addRootGroup(_outGrpRef, grpRef);

                                    CT_Point2D* item_ref = new CT_Point2D(new CT_Point2DData(x,y));
                                    grpRef->addSingularItem(_outRef, item_ref);

                                    item_ref->addItemAttribute(_outRefVal, new CT_StdItemAttributeT<float>(CT_AbstractCategory::DATA_NUMBER, val));
                                    item_ref->addItemAttribute(_outRefID, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, id));
                                    item_ref->addItemAttribute(_outRefIDPlot, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, plot));
                                } else {
                                    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier REF non valide")).arg(cpt));
                                }
                            }
                        }
                    }
                }
            }
            fRef.close();
        }
    }

    for (CT_ResultGroup* resultTrans : _outResultTrans.iterateOutputs())
    {
        QFile fTrans(_transFileName);

        if (fTrans.exists() && fTrans.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&fTrans);
            stream.setLocale(_transLocale);

            int colID = _transColumns.value("ID", -1);
            int colX = _transColumns.value("X", -1);
            int colY = _transColumns.value("Y", -1);
            int colVal = _transColumns.value("VALUE", -1);

            if (colID >=0 && colX >= 0 && colY >= 0 && colVal >= 0)
            {

                int colMax = colID;
                if (colX   > colMax) {colMax = colX;}
                if (colY   > colMax) {colMax = colY;}
                if (colVal > colMax) {colMax = colVal;}

                for (int i = 0 ; i < _transSkip ; i++) {stream.readLine();}
                if (_transHeader) {stream.readLine();}

                size_t cpt = 1;
                while (!stream.atEnd())
                {
                    QString line = stream.readLine();
                    if (!line.isEmpty())
                    {
                        QStringList values = line.split(_transSeparator);
                        if (values.size() >= colMax)
                        {
                            QString plot = "";
                            if (multiPlots) {plot = values.at(colIDplot_trans);}

                            if (plot == currentPlot)
                            {

                                bool okX, okY, okVal;
                                double x = _transLocale.toDouble(values.at(colX), &okX);
                                double y = _transLocale.toDouble(values.at(colY), &okY);
                                float val = _transLocale.toFloat(values.at(colVal), &okVal);
                                QString id = values.at(colID);

                                if (okX && okY && okVal)
                                {
                                    CT_StandardItemGroup* grp_grpTrans= new CT_StandardItemGroup();
                                    resultTrans->addRootGroup(_outGrpTrans, grp_grpTrans);

                                    CT_Point2D* item_trans = new CT_Point2D(new CT_Point2DData(x,y));
                                    grp_grpTrans->addSingularItem(_outTrans, item_trans);

                                    item_trans->addItemAttribute(_outTransVal, new CT_StdItemAttributeT<float>(CT_AbstractCategory::DATA_NUMBER, val));
                                    item_trans->addItemAttribute(_outTransID, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, id));
                                    item_trans->addItemAttribute(_outTransIDplot, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID, plot));
                                } else {
                                    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier TRANS non valide")).arg(cpt));
                                }
                            }
                        }
                    }
                }
            }

            fTrans.close();
        }
    }

}
