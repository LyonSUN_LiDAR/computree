/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepextractlogbuffer.h"

#include "ct_math/ct_mathpoint.h"

ONF_StepExtractLogBuffer::ONF_StepExtractLogBuffer() : SuperClass()
{
    _circleIncrement = 0.02;
    _buffer = 0.1;
}

QString ONF_StepExtractLogBuffer::description() const
{
    return tr("Extraire les points autour d'un Billon");
}

QString ONF_StepExtractLogBuffer::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepExtractLogBuffer::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepExtractLogBuffer::createNewInstance() const
{
    return new ONF_StepExtractLogBuffer();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepExtractLogBuffer::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Points"), tr(""), true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Points"));

    manager.addResult(_inRcircles, tr("Cerles"));
    manager.setZeroOrMoreRootGroup(_inRcircles, _inZeroOrMoreRootGroupCircles);
    manager.addGroup(_inZeroOrMoreRootGroupCircles, _inGrpLog, tr("Billon"));
    manager.addGroup(_inGrpLog, _inGrpCircles, tr("Groupe"));
    manager.addItem(_inGrpCircles, _inCircle, tr("Cercle"));
}

void ONF_StepExtractLogBuffer::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inRcircles);
    manager.addItem(_inGrpLog, _outPoint, tr("Points extraits"));
}

void ONF_StepExtractLogBuffer::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble("Accroissement de la taille des cercles :", "cm", 0, 1e+09, 2, _circleIncrement, 100);
    postInputConfigDialog->addDouble("Taille du buffer", "cm", 0, 1e+09, 2, _buffer, 100);
}

void ONF_StepExtractLogBuffer::compute()
{                
    QMap<CT_StandardItemGroup*, QList<QPair<CT_CircleData, CT_CircleData> > > circlesMap;
    QMap<CT_StandardItemGroup*, CT_PointCluster*> clustersMap;

    for (CT_StandardItemGroup* grpCpy_grpLog : _inGrpLog.iterateOutputs(_inRcircles))
    {
        QList<QPair<CT_CircleData, CT_CircleData> > circlesList;

        for (const CT_StandardItemGroup* grpCpy_grpCircles : grpCpy_grpLog->groups(_inGrpCircles))
        {
            for (const CT_Circle* itemCpy_circle : grpCpy_grpCircles->singularItems(_inCircle))
            {
                if (isStopped()) {return;}
                const CT_CircleData &circle = static_cast<const CT_CircleData &>(itemCpy_circle->getData());

                CT_CircleData smallCircle(circle.getCenter(), Eigen::Vector3d(0,0,1), circle.getRadius() + _circleIncrement);
                CT_CircleData bigCircle(circle.getCenter(), Eigen::Vector3d(0,0,1), circle.getRadius() + _circleIncrement + _buffer);

                circlesList.append(QPair<CT_CircleData, CT_CircleData>(smallCircle, bigCircle));
            }
        }

        if (circlesList.size() >= 2)
        {
            circlesMap.insert(grpCpy_grpLog, circlesList);
            clustersMap.insert(grpCpy_grpLog, new CT_PointCluster());
        }
    }




    for (const CT_StandardItemGroup* grp_inGrpPts : _inGroup.iterateInputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* item_inPts = grp_inGrpPts->singularItem(_inScene);

        CT_PointIterator itP(item_inPts->pointCloudIndex());
        while (itP.hasNext())
        {
            const CT_Point &point = itP.next().currentPoint();
            size_t index = itP.currentGlobalIndex();

            QMapIterator<CT_StandardItemGroup*, CT_PointCluster*> itClusters(clustersMap);
            while (itClusters.hasNext())
            {
                itClusters.next();

                CT_StandardItemGroup* groupe = itClusters.key();
                CT_PointCluster* cluster = itClusters.value();

                const QList<QPair<CT_CircleData, CT_CircleData> > &list = circlesMap.value(groupe);

                if (point(2) >= list.first().first.getCenter()(2) && point(2) <= list.last().first.getCenter()(2))
                {
                    double zmin, zmax;

                    int sizeList = list.size();

                    for (int i = 0 ; i < sizeList ; i++)
                    {
                        const QPair<CT_CircleData, CT_CircleData> &pair = list.at(i);

                        zmin = pair.first.getCenter()(2);
                        zmax = zmin;

                        if (i > 0)
                        {
                            const QPair<CT_CircleData, CT_CircleData> &pairAvant = list.at(i - 1);
                            zmin = (zmin + pairAvant.first.getCenter()(2)) / 2.0;
                        }

                        if (i < sizeList - 1)
                        {
                            const QPair<CT_CircleData, CT_CircleData> &pairApres= list.at(i + 1);
                            zmax = (zmax + pairApres.first.getCenter()(2)) / 2.0;
                        }

                        if (point(2) >= zmin &&
                                point(2) <= zmax &&
                                !contains(pair.first, point) &&
                                contains(pair.second, point))
                        {
                            cluster->addPoint(index);
                        }
                    }
                }
            }
        }
    }

    QMapIterator<CT_StandardItemGroup*, CT_PointCluster*> itClusters(clustersMap);
    while (itClusters.hasNext())
    {
        itClusters.next();

        CT_StandardItemGroup* groupe = itClusters.key();
        CT_PointCluster* cluster = itClusters.value();

        if (cluster->pointCloudIndex()->size() >0)
        {
            groupe->addSingularItem(_outPoint, cluster);
        } else {
            delete cluster;
        }
    }
}

bool ONF_StepExtractLogBuffer::contains(const CT_CircleData &circle, const Eigen::Vector3d &point) const
{
    double dist = CT_MathPoint::distance2D(circle.getCenter(), point);
    return dist <= circle.getRadius();
}

