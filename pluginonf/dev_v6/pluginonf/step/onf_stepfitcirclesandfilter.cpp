/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfitcirclesandfilter.h"

ONF_StepFitCirclesAndFilter::ONF_StepFitCirclesAndFilter() : SuperClass()
{
    _max_error = 0.01;
    _min_radius = 0.02;
    _max_radius = 1.20;
    _activeFiltering = true;
}

QString ONF_StepFitCirclesAndFilter::description() const
{
    return tr("Ajuster/Filtrer un Cercle horizontal par Cluster");
}

QString ONF_StepFitCirclesAndFilter::detailledDescription() const
{
    return tr("Cette étape ajoute un cercle dans chaque cluster d'entrée.<br>"
              "Les cercles sont ajustés par moindres carrés sur les groupes de points.<br>"
              "Les paramètres de l'étape permettent d'activer optionnellement un  <b>filtrage</b> de cercles.<br>"
              "Les criètres de filtrages sont le <b>rayon minimum</b>, le <b>rayon maximum</b> et l' <b>erreur d'ajustement du cercle</b> maximale autorisée.");
}

CT_VirtualAbstractStep* ONF_StepFitCirclesAndFilter::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepFitCirclesAndFilter();
}

//////////////////// PROTECTED //////////////////

void ONF_StepFitCirclesAndFilter::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Clusters"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup, tr("Cluster (Grp)"));
    manager.addItem(_inGroup, _inCluster, tr("Points(s)"));
}

void ONF_StepFitCirclesAndFilter::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addBool("", "", tr("Filtrer les cercles sur les critres suivants"), _activeFiltering);
    postInputConfigDialog->addDouble(tr("Rayon minimum  :"), "cm", 0, 1000, 2, _min_radius, 100);
    postInputConfigDialog->addDouble(tr("Rayon maximum  :"), "cm", 0, 1000, 2, _max_radius, 100);
    postInputConfigDialog->addDouble(tr("Erreur maximum :"), "", 0, 99999.99, 4, _max_error);

}

void ONF_StepFitCirclesAndFilter::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outCircle, tr("Cercle"));
}

void ONF_StepFitCirclesAndFilter::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* item = group->singularItem(_inCluster);

        if(item != nullptr)
        {
            CT_CircleData *cData = CT_CircleData::staticCreateZAxisAlignedCircleDataFromPointCloud(*item->pointCloudIndex(), item->centerZ());

            // et on ajoute un cercle
            if(cData != nullptr)
            {
                if (!_activeFiltering || ((cData->getError() < _max_error) && (cData->getRadius() > _min_radius) && (cData->getRadius() < _max_radius)))
                {
                    group->addSingularItem(_outCircle, new CT_Circle(cData));
                }
                else
                {
                    delete cData;
                }
            }
        }
    }

}
