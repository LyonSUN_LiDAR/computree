/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputecumulativenrtable.h"

#include <QFile>
#include <QTextStream>
#include <iostream>

ONF_StepComputeCumulativeNRTable::ONF_StepComputeCumulativeNRTable() : SuperClass()
{
    _fileName << "NRTable.txt";
    _mat.resize(1,1);
    _mat(0,0) = 0;

    _numberOfHightReturnValues = 0;
}

QString ONF_StepComputeCumulativeNRTable::description() const
{
    return tr("Export N-R Table");
}

QString ONF_StepComputeCumulativeNRTable::detailledDescription() const
{
    return tr("Tableau croisé de comptage du nombre de points par croisement Numéro de retour / Nombre de retours. ");
}

QString ONF_StepComputeCumulativeNRTable::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepComputeCumulativeNRTable::createNewInstance() const
{
    return new ONF_StepComputeCumulativeNRTable();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeCumulativeNRTable::declareInputModels(CT_StepInModelStructureManager& manager)
{   
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène(s)"));
    manager.addItem(_inGroup, _inLasAtt, tr("Attributs LAS"));

    manager.addResult(_inResultCounter, tr("Résultat compteur"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultCounter, _inZeroOrMoreRootGroupCounter);
    manager.addItem(_inZeroOrMoreRootGroupCounter, _inCounter, tr("Compteur"));
}

void ONF_StepComputeCumulativeNRTable::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepComputeCumulativeNRTable::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addFileChoice(tr("Choose Export file"), CT_FileChoiceButton::OneNewFile, "Text file (*.txt)", _fileName);
}

void ONF_StepComputeCumulativeNRTable::compute()
{
    bool last_turn = false;
    bool counterok = false;
    for (const CT_LoopCounter* counter : _inCounter.iterateInputs(_inResultCounter))
    {
        counterok = true;
        if (counter->currentTurn() == counter->nTurns())
        {
            last_turn = true;
        }
        if (counter != nullptr) {break;}
    }

    CT_LASData lasData;

    // parcours des item
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* points : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_StdLASPointsAttributesContainer* lasAtt = group->singularItem(_inLasAtt);

            if (lasAtt != nullptr)
            {
                const CT_AbstractPointCloudIndex *lasPointCloudIndex = lasAtt->pointsAttributesAt(CT_LasDefine::Return_Number)->pointCloudIndex();
                size_t maxAttSize = lasPointCloudIndex->size();
                CT_PointIterator itP(points->pointCloudIndex());
                while (itP.hasNext())
                {
                    size_t index = itP.next().currentGlobalIndex();
                    size_t lasIndex = lasPointCloudIndex->indexOf(index);

                    if (lasIndex < maxAttSize)
                    {
                        lasAtt->getLASDataAt(lasIndex, lasData);
                    }

                    if (lasData._Return_Number > 20 || lasData._Number_of_Returns > 20)
                    {
                        _numberOfHightReturnValues++;
                    } else {
                        resizeMatIfNeeded(lasData._Return_Number, lasData._Number_of_Returns);
                        _mat(lasData._Return_Number, lasData._Number_of_Returns) += 1;
                    }
                }
            }
        }
    }

    if (_fileName.size() > 0 && _fileName.first() != "" && (counterok == false || last_turn))
    {
        QFile file(_fileName.first());

        if(file.open(QFile::WriteOnly))
        {
            QTextStream stream(&file);

            stream << "\t\tN\n";
            stream << "\t\t";
            for (int j = 0 ; j < _mat.cols() ; j++)
            {
                stream << j << "\t";
            }
            stream << "\n";

            for (int i = 0 ; i < _mat.rows() ; i++)
            {
                if (i == (_mat.rows() - 1))
                {
                    stream << "R\t" << i << "\t";

                } else {
                    stream << "\t" << i << "\t";
                }

                for (int j = 0 ; j < _mat.cols() ; j++)
                {
                    stream << _mat(i,j) << "\t";
                }
                stream << "\n";
            }
            stream << "\n";
            stream << "Number of N or R > 20 " << _numberOfHightReturnValues << "\n";

            file.close();
        }
    }

}

void ONF_StepComputeCumulativeNRTable::resizeMatIfNeeded(int row, int col)
{
    int nrow = int(_mat.rows());
    int ncol = int(_mat.cols());

    if (row >= nrow)
    {
        _mat.conservativeResize(row + 1, ncol);
        for (int i = nrow ; i <= row ; i++)
        {
            for (int j = 0 ; j < ncol ; j++)
            {
                _mat(i,j) = 0;
            }
        }
    }

    nrow = int(_mat.rows());

    if (col >= ncol)
    {
        _mat.conservativeResize(Eigen::NoChange, col + 1);
        for (int i = 0 ; i < nrow ; i++)
        {
            for (int j = ncol ; j <= col ; j++)
            {
                _mat(i,j) = 0;
            }
        }
    }
}
