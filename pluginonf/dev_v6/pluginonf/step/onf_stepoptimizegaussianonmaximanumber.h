#ifndef ONF_STEPOPTIMIZEGAUSSIANONMAXIMANUMBER_H
#define ONF_STEPOPTIMIZEGAUSSIANONMAXIMANUMBER_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepOptimizeGaussianOnMaximaNumber: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepOptimizeGaussianOnMaximaNumber();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Declaration of autoRenames Variables (groups or items added to In models copies)

    // Step parameters
    double    _sigmaStep;
    double    _sigmaMax;
    double      _minHeight;
    double      _CoefMult;

};

#endif // ONF_STEPOPTIMIZEGAUSSIANONMAXIMANUMBER_H
