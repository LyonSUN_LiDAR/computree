#include "onf_stepcreatecolorcomposite.h"

ONF_StepCreateColorComposite::ONF_StepCreateColorComposite() : SuperClass()
{
}

QString ONF_StepCreateColorComposite::description() const
{
    return tr("Create color composite");
}

QString ONF_StepCreateColorComposite::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepCreateColorComposite::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCreateColorComposite::createNewInstance() const
{
    return new ONF_StepCreateColorComposite();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCreateColorComposite::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("2D Images"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inRed, tr("Red band"));
    manager.addItem(_inGroup, _inGreen, tr("Green band"));
    manager.addItem(_inGroup, _inBlue, tr("Blue band"));

    manager.addResult(_inResultDEM, tr("DEM"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultDEM, _inZeroOrMoreRootGroupDEM);
    manager.addGroup(_inZeroOrMoreRootGroupDEM, _inGroupDEM);
    manager.addItem(_inGroupDEM, _inDEM, tr("DEM"));
}

void ONF_StepCreateColorComposite::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outColorcomposite, tr("Color Composite"));
}

void ONF_StepCreateColorComposite::compute()
{
    CT_Image2D<float>* zvalue = nullptr;
    for (const CT_Image2D<float>* imageIn : _inDEM.iterateInputs(_inResultDEM))
    {
        zvalue = const_cast<CT_Image2D<float>*>(imageIn);
    }

    // COPIED results browsing
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
            if (isStopped()) {return;}
            CT_AbstractImage2D* red   = const_cast<CT_AbstractImage2D*>(grp->singularItem(_inRed));
            CT_AbstractImage2D* green = const_cast<CT_AbstractImage2D*>(grp->singularItem(_inGreen));
            CT_AbstractImage2D* blue  = const_cast<CT_AbstractImage2D*>(grp->singularItem(_inBlue));

            if (green == nullptr) {green = red;}
            if (blue == nullptr) {blue = red;}

            if (red != nullptr && green != nullptr && blue != nullptr)
            {
                CT_ColorComposite* colorComposite = new CT_ColorComposite(red, green, blue, zvalue);
                grp->addSingularItem(_outColorcomposite, colorComposite);
            }

    }
}

