#include "onf_stepcomputepointheightfromtin.h"

ONF_StepComputePointHeightFromTIN::ONF_StepComputePointHeightFromTIN() : SuperClass()
{
    _ptsAttribute = false;
    _ptsCloud = true;
    _addLAS = true;
}

QString ONF_StepComputePointHeightFromTIN::description() const
{
    return tr("Calculer la hauteur des points à l'aide d'un TIN");
}

QString ONF_StepComputePointHeightFromTIN::detailledDescription() const
{
    return tr("Cette étape soustrait à l'altitude des points la coordonnées du TIN."
              "En sortie, elle peut (options) générer :<br>"
              "- Un attribut de hauteur sur le nuage d'altitudes<br>"
              "- Un nuage de point<br>"
              "- Recopier les attributs LAS (dans le cas de la création d'un nouveau nuage)");
}


CT_VirtualAbstractStep* ONF_StepComputePointHeightFromTIN::createNewInstance() const
{
    return new ONF_StepComputePointHeightFromTIN();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputePointHeightFromTIN::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));

    if (_addLAS)
    {
        manager.addItem(_inGroup, _inLAS, tr("Attributs LAS"));
    }

    manager.addResult(_inResultTIN, tr("TIN"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultTIN, _inZeroOrMoreRootGroupTIN);
    manager.addGroup(_inZeroOrMoreRootGroupTIN, _inGroupTIN);
    manager.addItem(_inGroupTIN, _inTIN, tr("TIN"));
}

void ONF_StepComputePointHeightFromTIN::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);

    if (_ptsAttribute)
    {
        manager.addItem(_inGroup, _outHeightAtt, tr("Height cloud"));
    }

    if (_ptsCloud)
    {
        manager.addItem(_inGroup, _outScene, tr("Scene"));
        if (_addLAS)
        {
            manager.addItem(_inGroup, _outLASreturnNumber, tr("Return Number"));
            manager.addItem(_inGroup, _outLASnumberOfReturns, tr("Number of Returns"));
            manager.addItem(_inGroup, _outLASclassificationFlags, tr("Classification Flags"));
            manager.addItem(_inGroup, _outLASscannerChannel, tr("Scanner Channel"));
            manager.addItem(_inGroup, _outLASscanDirectionFlags, tr("Scan Direction Flag"));
            manager.addItem(_inGroup, _outLASedgeOfFlightLine, tr("Edge of Flight Line"));
            manager.addItem(_inGroup, _outLASintensity, tr("Intensité"));
            manager.addItem(_inGroup, _outLASclassification, tr("Classification"));
            manager.addItem(_inGroup, _outLASscanAngleRank, tr("Scan Angle"));
            manager.addItem(_inGroup, _outLASuserData, tr("User Data"));
            manager.addItem(_inGroup, _outLASpointSourceID, tr("Point Source ID"));
            manager.addItem(_inGroup, _outLASgpsTime, tr("GPS Time"));
            manager.addItem(_inGroup, _outLAScolor, tr("Color"));
            manager.addItem(_inGroup, _outLAScolorR, tr("Red"));
            manager.addItem(_inGroup, _outLAScolorG, tr("Green"));
            manager.addItem(_inGroup, _outLAScolorB, tr("Blue"));
            manager.addItem(_inGroup, _outLASnir, tr("NIR"));
            manager.addItem(_inGroup, _outLASwavePacket, tr("Wave Packet Descriptor Index"));
            manager.addItem(_inGroup, _outLASbyteOffsetWaveform, tr("Byte offset to waveform data"));
            manager.addItem(_inGroup, _outLASwaveformPacketSize, tr("Waveform packet size in bytes"));
            manager.addItem(_inGroup, _outLASreturnPointWaveform, tr("Return Point Waveform Location"));
            manager.addItem(_inGroup, _outLASAttributes, tr("LAS Attributs (H)"));
        }
    }
}

void ONF_StepComputePointHeightFromTIN::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool(tr("Créer des attributs de points hauteur"), "", "", _ptsAttribute);
    preInputConfigDialog->addEmpty();
    preInputConfigDialog->addBool(tr("Créer un nuage de points hauteur"), "", "", _ptsCloud);
    preInputConfigDialog->addBool(tr("Transférer les attributs las au nuage de points hauteur"), "", "", _addLAS);
}

void ONF_StepComputePointHeightFromTIN::compute()
{
    const CT_Triangulation2D* tin = nullptr;
    for (const CT_Triangulation2D* tinIn : _inTIN.iterateInputs(_inResultTIN))
    {
        tin = tinIn;
    }

    if (tin != nullptr)
    {
        for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
        {
            for (const CT_AbstractItemDrawableWithPointCloud* scene : grp->singularItems(_inScene))
            {
                if (isStopped()) {return;}

                const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();
                size_t n_points = pointCloudIndex->size();

                CT_PointIterator itP(pointCloudIndex);

                // On declare un tableau d'attributs double que l'on va remplir avec les coordonnées correspondant a l'axe demandé
                CT_StandardCloudStdVectorT<float> *attribute = nullptr;
                if (_ptsAttribute) {attribute = new CT_StandardCloudStdVectorT<float>();}

                CT_NMPCIR heightCloud;
                if (_ptsCloud) {heightCloud = PS_REPOSITORY->createNewPointCloud(n_points);}
                CT_MutablePointIterator itPM(heightCloud);

                float minAttribute = std::numeric_limits<float>::max();
                float maxAttribute = -std::numeric_limits<float>::max();

                if (_ptsAttribute || _ptsCloud)
                {
                    size_t i = 0;

                    CT_DelaunayTriangulation *triangulation = tin->getDelaunayT();
                    double zTin = 0;
                    CT_DelaunayTriangle* refTri = nullptr;

                    // On applique la translation a tous les points du nuage
                    while (itP.hasNext() && !isStopped())
                    {
                        CT_Point point = itP.next().currentPoint();

                        refTri = const_cast<CT_DelaunayTriangle*>(triangulation->getZCoordForXY(point(0), point(1), zTin, refTri));

                        float h = float(point(2));
                        if (!std::isnan(zTin))
                        {
                            h -= float(zTin);
                        }

                        if (_ptsAttribute) {attribute->addT(h);}

                        if (_ptsCloud)
                        {
                            point.setZ(double(h));
                            itPM.next().replaceCurrentPoint(point);
                        }

                        if (_ptsAttribute && h < minAttribute) {minAttribute = h;}
                        if (_ptsAttribute && h > maxAttribute) {maxAttribute = h;}

                        setProgress(float(100.0*i++ / n_points));
                    }

                    if (i > 0)
                    {
                        if (_ptsAttribute)
                        {
                            CT_PointsAttributesScalarTemplated<float>*  outAttribute  = new CT_PointsAttributesScalarTemplated<float>(scene->pointCloudIndexRegistered(), attribute, minAttribute, maxAttribute);
                            grp->addSingularItem(_outHeightAtt, outAttribute);
                        }

                        if (_ptsCloud)
                        {
                            CT_Scene*  outScene  = new CT_Scene(heightCloud);
                            outScene->updateBoundingBox();

                            grp->addSingularItem(_outScene, outScene);

                            // Ajout des données LAS
                            if (_addLAS && _ptsCloud)
                            {
                                const CT_StdLASPointsAttributesContainer* attributeLAS = grp->singularItem(_inLAS);

                                if (attributeLAS != nullptr)
                                {
                                    // Retrieve attributes
                                    QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> itLAS(attributeLAS->lasPointsAttributes());
                                    if (!itLAS.hasNext()) {return;}

                                    CT_AbstractPointAttributesScalar *firstAttribute = itLAS.next().value();
                                    if (firstAttribute == nullptr) {return;}

                                    const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->pointCloudIndex();

                                    if (pointCloudIndexLAS == nullptr) {return;}

                                    CT_AbstractPointAttributesScalar* att_Return_Number = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Number));
                                    CT_AbstractPointAttributesScalar* att_Number_of_Returns = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Number_of_Returns));
                                    CT_AbstractPointAttributesScalar* att_Classification_Flags = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Classification_Flags));
                                    CT_AbstractPointAttributesScalar* att_Scanner_Channel = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Scanner_Channel));
                                    CT_AbstractPointAttributesScalar* att_Scan_Direction_Flag = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Direction_Flag));
                                    CT_AbstractPointAttributesScalar* att_Edge_of_Flight_Line = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line));
                                    CT_AbstractPointAttributesScalar* att_Intensity = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity));
                                    CT_AbstractPointAttributesScalar* att_Classification = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Classification));
                                    CT_AbstractPointAttributesScalar* att_Scan_Angle_Rank = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Angle_Rank));
                                    CT_AbstractPointAttributesScalar* att_User_Data = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::User_Data));
                                    CT_AbstractPointAttributesScalar* att_Point_Source_ID = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Point_Source_ID));
                                    CT_AbstractPointAttributesScalar* att_GPS_Time  = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time));
                                    CT_AbstractPointAttributesScalar* att_Red = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Red));
                                    CT_AbstractPointAttributesScalar* att_Green = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Green));
                                    CT_AbstractPointAttributesScalar* att_Blue = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Blue));
                                    CT_AbstractPointAttributesScalar* att_NIR = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::NIR));
                                    CT_AbstractPointAttributesScalar* att_Wave_Packet_Descriptor_Index = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index));
                                    CT_AbstractPointAttributesScalar* att_Byte_offset_to_waveform_data = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data));
                                    CT_AbstractPointAttributesScalar* att_Waveform_packet_size_in_bytes = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Waveform_packet_size_in_bytes));
                                    CT_AbstractPointAttributesScalar* att_Return_Point_Waveform_Location = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location));

                                    CT_StdLASPointsAttributesContainer *container = new CT_StdLASPointsAttributesContainer();

                                    CT_AbstractPointAttributesScalar* att_Return_Number_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Number_of_Returns_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Classification_Flags_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Scanner_Channel_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Scan_Direction_Flag_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Edge_of_Flight_Line_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Intensity_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Classification_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Scan_Angle_Rank_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_User_Data_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Point_Source_ID_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_GPS_Time_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Red_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Green_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Blue_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_NIR_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Wave_Packet_Descriptor_Index_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Byte_offset_to_waveform_data_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Waveform_packet_size_in_bytes_H  = nullptr;
                                    CT_AbstractPointAttributesScalar* att_Return_Point_Waveform_Location_H  = nullptr;

                                    if (att_Return_Number != nullptr)
                                    {
                                        att_Return_Number_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Return_Number->copy());
                                        att_Return_Number_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Number_of_Returns != nullptr)
                                    {
                                        att_Number_of_Returns_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Number_of_Returns->copy());
                                        att_Number_of_Returns_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Classification_Flags != nullptr)
                                    {
                                        att_Classification_Flags_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Classification_Flags->copy());
                                        att_Classification_Flags_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Scanner_Channel != nullptr)
                                    {
                                        att_Scanner_Channel_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Scanner_Channel->copy());
                                        att_Scanner_Channel_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Scan_Direction_Flag != nullptr)
                                    {
                                        att_Scan_Direction_Flag_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Scan_Direction_Flag->copy());
                                        att_Scan_Direction_Flag_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Edge_of_Flight_Line != nullptr)
                                    {
                                        att_Edge_of_Flight_Line_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Edge_of_Flight_Line->copy());
                                        att_Edge_of_Flight_Line_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Intensity != nullptr)
                                    {
                                        att_Intensity_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Intensity->copy());
                                        att_Intensity_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Classification != nullptr)
                                    {
                                        att_Classification_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Classification->copy());
                                        att_Classification_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Scan_Angle_Rank != nullptr)
                                    {
                                        att_Scan_Angle_Rank_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Scan_Angle_Rank->copy());
                                        att_Scan_Angle_Rank_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_User_Data != nullptr)
                                    {
                                        att_User_Data_H = static_cast<CT_AbstractPointAttributesScalar*>(att_User_Data->copy());
                                        att_User_Data_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Point_Source_ID != nullptr)
                                    {
                                        att_Point_Source_ID_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Point_Source_ID->copy());
                                        att_Point_Source_ID_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_GPS_Time != nullptr)
                                    {
                                        att_GPS_Time_H  = static_cast<CT_AbstractPointAttributesScalar*>(att_GPS_Time->copy());
                                        att_GPS_Time_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Red != nullptr)
                                    {
                                        att_Red_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Red->copy());
                                        att_Red_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Green != nullptr)
                                    {
                                        att_Green_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Green->copy());
                                        att_Green_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Blue != nullptr)
                                    {
                                        att_Blue_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Blue->copy());
                                        att_Blue_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_NIR != nullptr)
                                    {
                                        att_NIR_H = static_cast<CT_AbstractPointAttributesScalar*>(att_NIR->copy());
                                        att_NIR_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Wave_Packet_Descriptor_Index != nullptr)
                                    {
                                        att_Wave_Packet_Descriptor_Index_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Wave_Packet_Descriptor_Index->copy());
                                        att_Wave_Packet_Descriptor_Index_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Byte_offset_to_waveform_data != nullptr)
                                    {
                                        att_Byte_offset_to_waveform_data_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Byte_offset_to_waveform_data->copy());
                                        att_Byte_offset_to_waveform_data_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Waveform_packet_size_in_bytes != nullptr)
                                    {
                                        att_Waveform_packet_size_in_bytes_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Waveform_packet_size_in_bytes->copy());
                                        att_Waveform_packet_size_in_bytes_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    if (att_Return_Point_Waveform_Location != nullptr)
                                    {
                                        att_Return_Point_Waveform_Location_H = static_cast<CT_AbstractPointAttributesScalar*>(att_Return_Point_Waveform_Location->copy());
                                        att_Return_Point_Waveform_Location_H->setPointCloudIndexRegistered(heightCloud);
                                    }

                                    // Put copied data in new container
                                    container->insertPointsAttributesAt(CT_LasDefine::Intensity, att_Intensity_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Return_Number, att_Return_Number_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Number_of_Returns, att_Number_of_Returns_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Classification_Flags, att_Classification_Flags_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Scanner_Channel, att_Scanner_Channel_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Scan_Direction_Flag, att_Scan_Direction_Flag_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line, att_Edge_of_Flight_Line_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Classification, att_Classification_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Scan_Angle_Rank, att_Scan_Angle_Rank_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::User_Data, att_User_Data_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Point_Source_ID, att_Point_Source_ID_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::GPS_Time, att_GPS_Time_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Red, att_Red_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Green, att_Green_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Blue, att_Blue_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::NIR, att_NIR_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index, att_Wave_Packet_Descriptor_Index_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data, att_Byte_offset_to_waveform_data_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Waveform_packet_size_in_bytes, att_Waveform_packet_size_in_bytes_H);
                                    container->insertPointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location, att_Return_Point_Waveform_Location_H);

                                    grp->addSingularItem(_outLASAttributes, container);
                                    if (att_Return_Number_H != nullptr) {grp->addSingularItem(_outLASreturnNumber, att_Return_Number_H);}
                                    if (att_Number_of_Returns_H != nullptr) {grp->addSingularItem(_outLASnumberOfReturns, att_Number_of_Returns_H);}
                                    if (att_Classification_Flags_H != nullptr) {grp->addSingularItem(_outLASclassificationFlags, att_Classification_Flags_H);}
                                    if (att_Scanner_Channel_H != nullptr) {grp->addSingularItem(_outLASscannerChannel, att_Scanner_Channel_H);}
                                    if (att_Scan_Direction_Flag_H != nullptr) {grp->addSingularItem(_outLASscanDirectionFlags, att_Scan_Direction_Flag_H);}
                                    if (att_Edge_of_Flight_Line_H != nullptr) {grp->addSingularItem(_outLASedgeOfFlightLine, att_Edge_of_Flight_Line_H);}
                                    if (att_Intensity_H != nullptr) {grp->addSingularItem(_outLASintensity, att_Intensity_H);}
                                    if (att_Classification_H != nullptr) {grp->addSingularItem(_outLASclassification, att_Classification_H);}
                                    if (att_Scan_Angle_Rank_H != nullptr) {grp->addSingularItem(_outLASscanAngleRank, att_Scan_Angle_Rank_H);}
                                    if (att_User_Data_H != nullptr) {grp->addSingularItem(_outLASuserData, att_User_Data_H);}
                                    if (att_Point_Source_ID_H != nullptr) {grp->addSingularItem(_outLASpointSourceID, att_Point_Source_ID_H);}
                                    if (att_GPS_Time_H != nullptr) {grp->addSingularItem(_outLASgpsTime, att_GPS_Time_H);}
                                    if (att_Red_H != nullptr) {grp->addSingularItem(_outLAScolorR, att_Red_H);}
                                    if (att_Green_H != nullptr) {grp->addSingularItem(_outLAScolorG, att_Green_H);}
                                    if (att_Blue_H != nullptr) {grp->addSingularItem(_outLAScolorB, att_Blue_H);}
                                    if (att_NIR_H != nullptr) {grp->addSingularItem(_outLASnir, att_NIR_H);}
                                    if (att_Wave_Packet_Descriptor_Index_H != nullptr) {grp->addSingularItem(_outLASwavePacket, att_Wave_Packet_Descriptor_Index_H);}
                                    if (att_Byte_offset_to_waveform_data_H != nullptr) {grp->addSingularItem(_outLASbyteOffsetWaveform, att_Byte_offset_to_waveform_data_H);}
                                    if (att_Waveform_packet_size_in_bytes_H != nullptr) {grp->addSingularItem(_outLASwaveformPacketSize, att_Waveform_packet_size_in_bytes_H);}
                                    if (att_Return_Point_Waveform_Location_H != nullptr) {grp->addSingularItem(_outLASreturnPointWaveform, att_Return_Point_Waveform_Location_H);}
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
