/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepsegmentcrownsfromstemclusters.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

ONF_StepSegmentCrownsFromStemClusters::ONF_StepSegmentCrownsFromStemClusters() : SuperClass()
{
    _distMax = 1.0;
}

QString ONF_StepSegmentCrownsFromStemClusters::description() const
{
    return tr("Segmenter des houppiers à partir de Clusters tiges");
}

QString ONF_StepSegmentCrownsFromStemClusters::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepSegmentCrownsFromStemClusters::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepSegmentCrownsFromStemClusters::createNewInstance() const
{
    return new ONF_StepSegmentCrownsFromStemClusters();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSegmentCrownsFromStemClusters::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inRes = createNewInResultModelForCopy(_inRes, tr("Tiges détéctées"));
    res_inRes->setZeroOrMoreRootGroup();
    res_inRes->addGroupModel("", _inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRes->addItemModel(_inGrp, _inScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène complète"));
    res_inRes->addGroupModel(_inGrp, _inGrpPos, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRes->addItemModel(_inGrpPos, _inPosition, CT_PointCluster::staticGetType(), tr("Tige"));
}

void ONF_StepSegmentCrownsFromStemClusters::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(_inRes);

    if(resCpy_res != nullptr) {
        resCpy_res->addItemModel(_inGrpPos, _outScene, new CT_PointCluster(), tr("Cluster segmenté"));
        resCpy_res->addItemAttributeModel(_outScene, _outAttZmax, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_Z), tr("Zmax"));
    }
}

void ONF_StepSegmentCrownsFromStemClusters::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Distance Max"), "m", 0.01, 9999, 2, _distMax);
}

void ONF_StepSegmentCrownsFromStemClusters::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    CT_ResultGroupIterator itCpy_grp(res, this, _inGrp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        setProgress(1);

        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        CT_AbstractItemDrawableWithPointCloud* _inScene = (CT_AbstractItemDrawableWithPointCloud*)grp->firstItemByINModelName(this, _inScene);

        if (_inScene != nullptr)
        {
            // List of stems
            QList<StemData*> stems;

            CT_GroupIterator itCpy_grpPos(grp, this, _inGrpPos);
            while (itCpy_grpPos.hasNext() && !isStopped())
            {
                CT_StandardItemGroup* grpPos = (CT_StandardItemGroup*) itCpy_grpPos.next();
                CT_PointCluster* cluster = (CT_PointCluster*) grpPos->firstItemByINModelName(this, _inPosition);
                if (cluster != nullptr)
                {
                    StemData* stem = new StemData(grpPos, _distMax);
                    CT_PointIterator itP(cluster->pointCloudIndex());
                    while(itP.hasNext() && (!isStopped()))
                    {
                        const CT_Point &point = itP.next().currentPoint();
                        size_t index = itP.currentGlobalIndex();

                        stem->addPoint(new PointData(index, point(0), point(1), point(2)));
                    }
                    stems.append(stem);
                }
            }
            setProgress(10);

            const CT_AbstractPointCloudIndex *pointCloudIndex = _inScene->pointCloudIndex();
            size_t n_points = pointCloudIndex->size();

            size_t cpt = 0;
            QList<PointData*> points;
            CT_PointIterator itScene(_inScene->pointCloudIndex());
            while(itScene.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itScene.next().currentPoint();
                size_t index = itScene.currentGlobalIndex();

                points.append(new PointData(index, point(0), point(1), point(2)));
                setProgress(40.0*cpt++/n_points + 10.0);
            }

            setProgress(40.0);
            qSort(points.begin(), points.end(), ONF_StepSegmentCrownsFromStemClusters::lessThan);
            setProgress(50.0);

            cpt = 0;
            for (int pi = 0 ; pi < points.size() ; pi++)
            {
                PointData* ptData = points.at(pi);

                bool alreadyExist = false;
                StemData* closestStem = nullptr;
                double smallestDist = std::numeric_limits<double>::max();
                for (int i = 0 ; i < stems.size() && !alreadyExist; i++)
                {
                    StemData* stem = stems.at(i);

                    if (ptData->_z >= stem->maxZ())
                    {
                        double dist = stem->getDistance(ptData , alreadyExist);
                        if (dist < smallestDist && dist < _distMax)
                        {
                            smallestDist = dist;
                            closestStem = stem;
                        }
                    }
                }

                if (closestStem == nullptr)
                {
                    delete ptData;
                } else {
                    closestStem->addPoint(ptData);
                }

                setProgress(30.0*cpt++/n_points + 50.0);
            }

            points.clear();

            for (int i = 0 ; i < stems.size() ; i++)
            {
                StemData* stem = stems.at(i);
                if (stem->size() > 0)
                {
                    CT_PointCluster *outCluster = new CT_PointCluster(_outScene.completeName(), res);

                    for (int j = 0 ; j < stem->size() ; j++)
                    {
                        outCluster->addPoint(stem->_points.at(j)->_index);
                    }

                    // creation et ajout de la scene
                    outCluster->addItemAttribute(new CT_StdItemAttributeT<double>(_outAttZmax.completeName(), CT_AbstractCategory::DATA_Z, res, stem->maxZ()));

                    stem->_group->addSingularItem(outCluster);
                }

                delete stem;
            }

            setProgress(99);
        }
    }
    setProgress(100);
}

