#include "onf_stepcomputerelativeintensityattribute.h"

ONF_StepComputeRelativeIntensityAttribute::ONF_StepComputeRelativeIntensityAttribute() : SuperClass()
{
}

QString ONF_StepComputeRelativeIntensityAttribute::description() const
{
    return tr("Calculer l'intensité relative");
}

QString ONF_StepComputeRelativeIntensityAttribute::detailledDescription() const
{
    return tr("L'intensité relative est calculée de façon à ce que la somme des intensité d'un rayon (tous les retours) fasse 1.");
}


CT_VirtualAbstractStep* ONF_StepComputeRelativeIntensityAttribute::createNewInstance() const
{
    return new ONF_StepComputeRelativeIntensityAttribute();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeRelativeIntensityAttribute::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItem(_inGroup, _inLas, tr("Attributs LAS"));
}

void ONF_StepComputeRelativeIntensityAttribute::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outAtt, tr("Relative intensity"));
}

void ONF_StepComputeRelativeIntensityAttribute::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : grp->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_StdLASPointsAttributesContainer* attributeLAS = grp->singularItem(_inLas);

            if (attributeLAS != nullptr)
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();

                // Retrieve attributes
                QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> it(attributeLAS->lasPointsAttributes());
                if (!it.hasNext()) {return;}

                CT_AbstractPointAttributesScalar *firstAttribute = it.next().value();
                if (firstAttribute == nullptr) {return;}

                const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->pointCloudIndex();
                if (pointCloudIndexLAS == nullptr) {return;}

                CT_AbstractPointAttributesScalar* attributeGPS          = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time));
                CT_AbstractPointAttributesScalar* attributeIntensity    = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity));

                if (attributeIntensity == nullptr || attributeGPS == nullptr) {return;}

                QList<CandidatePoint*> candidatePoints;

                size_t pointIndex = 0;
                CT_PointIterator itP(pointCloudIndex);
                while (itP.hasNext() && !isStopped())
                {
                    size_t globalIndex = itP.next().currentGlobalIndex();
                    size_t localIndex = pointCloudIndexLAS->indexOf(globalIndex);

                    double gpsTime = 0;   // Récupération du temps GPS pour le point
                    double intensity = 0; // Récupération de l'intensité pour le point
                    if (localIndex < pointCloudIndexLAS->size())
                    {
                        gpsTime = attributeGPS->dValueAt(localIndex);
                        intensity = attributeIntensity->dValueAt(localIndex);
                    }

                    candidatePoints.append(new CandidatePoint(pointIndex++, gpsTime, intensity));
                }

                std::sort(candidatePoints.begin(), candidatePoints.end(), sortByGPSTime);

                double intensitySum = 0;
                int ibegin = 0;

                if (candidatePoints.size() > 0) {intensitySum = candidatePoints.first()->_intensity;}
                for (int i = 1 ; i < candidatePoints.size() ; i++)
                {
                    CandidatePoint* cp0 = candidatePoints.at(i-1);
                    CandidatePoint* cp1 = candidatePoints.at(i);

                    if (!qFuzzyCompare(cp1->_gpsTime, cp0->_gpsTime))
                    {
                        for (int j = ibegin ; j < i ; j++)
                        {
                            candidatePoints.at(j)->_sumIntensity = intensitySum;
                        }
                        intensitySum = cp1->_intensity;
                        ibegin = i;
                    } else {
                        intensitySum += cp1->_intensity;
                    }
                }

                CT_StandardCloudStdVectorT<double> *attribute = nullptr;
                attribute = new CT_StandardCloudStdVectorT<double>(pointCloudIndex->size());

                double minAttribute = std::numeric_limits<double>::max();
                double maxAttribute = -std::numeric_limits<double>::max();

                for (int i = 0 ; i < candidatePoints.size() ; i++)
                {
                    CandidatePoint* cp = candidatePoints.at(i);
                    double intensityRel = 0;
                    if (cp->_sumIntensity > 0) {intensityRel = cp->_intensity / cp->_sumIntensity;}

                    attribute->replaceT(cp->_pointIndex, intensityRel);

                    if (intensityRel < minAttribute) {minAttribute = intensityRel;}
                    if (intensityRel > maxAttribute) {maxAttribute = intensityRel;}
                }

                if (candidatePoints.size() > 0)
                {
                    CT_PointsAttributesScalarTemplated<double>*  outAttribute  = new CT_PointsAttributesScalarTemplated<double>(scene->pointCloudIndexRegistered(),
                                                                                                                                     attribute,
                                                                                                                                     minAttribute,
                                                                                                                                     maxAttribute);
                    grp->addSingularItem(_outAtt, outAttribute);
                }

                qDeleteAll(candidatePoints);
            }
        }
    }

}
