/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepaddlasdatatoplots.h"

ONF_StepAddLASDataToPlots::ONF_StepAddLASDataToPlots() : SuperClass()
{
    _separateResult = false;
}

QString ONF_StepAddLASDataToPlots::description() const
{
    return tr("4- Ajoute les données LAS aux placettes");
}

QString ONF_StepAddLASDataToPlots::detailledDescription() const
{
    return tr("Cette étape peut être ajoutée après une étape générant des placettes à partir d'une scène.<br>"
              "Elle permet de récupérer pour chaque placette les données LAS adéquates, à partir des données LAS de la scène mère.<br>"
              "Ces données sont passée sous forme de référence, la mémoire occupée n'augmente donc pas.");
}

QString ONF_StepAddLASDataToPlots::URL() const
{
    return SuperClass::URL();
}

CT_VirtualAbstractStep* ONF_StepAddLASDataToPlots::createNewInstance() const
{
    return new ONF_StepAddLASDataToPlots();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepAddLASDataToPlots::declareInputModels(CT_StepInModelStructureManager& manager)
{  
    manager.addResult(_inResult, tr("Placettes"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGrpLASAll, tr("Groupe Scene complète"));
    if (!_separateResult)
    {
        manager.addItem(_inGrpLASAll, _inLASAll, tr("Données LAS complètes"));
    }

    manager.addGroup(_inGrpLASAll, _inGrpPlot, tr("Groupe Placette"));
    manager.addItem(_inGrpPlot, _inPlotPoints, tr("SPoints de la placette"));

    if (_separateResult)
    {
        manager.addResult(_inResLAS, tr("Données LAS"));
        manager.setZeroOrMoreRootGroup(_inResLAS, _inZeroOrMoreRootGroupLAS);
        manager.addItem(_inZeroOrMoreRootGroupLAS, _inLASAll2, tr("Données LAS complètes"));
    }
}

void ONF_StepAddLASDataToPlots::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGrpPlot, _outLASData, tr("Données LAS placette"));
}

void ONF_StepAddLASDataToPlots::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool(tr("Données LAS dans un résultat séparé"), "", "", _separateResult);
}

void ONF_StepAddLASDataToPlots::compute()
{

    CT_StdLASPointsAttributesContainer* inLASAll = nullptr;

    if (_separateResult)
    {
        for (const CT_StdLASPointsAttributesContainer* lasData : _inLASAll2.iterateInputs(_inResLAS))
        {
            inLASAll = const_cast<CT_StdLASPointsAttributesContainer*>(lasData);
        }
    }

    for (CT_StandardItemGroup* group : _inGrpLASAll.iterateOutputs(_inResult))
    {
        if (!_separateResult)
        {
            inLASAll = const_cast<CT_StdLASPointsAttributesContainer*>(group->singularItem(_inLASAll));
        }

        if (inLASAll != nullptr)
        {
            for (const CT_StandardItemGroup* groupPlot : group->groups(_inGrpPlot))
            {
                if (isStopped()) {return;}

                const CT_AbstractItemDrawableWithPointCloud* plotPoints = groupPlot->singularItem(_inPlotPoints);

                if (plotPoints != nullptr)
                {
                    CT_StdLASPointsAttributesContainerShortcut* containerShortcut = new CT_StdLASPointsAttributesContainerShortcut(inLASAll);
                    const_cast<CT_StandardItemGroup*>(groupPlot)->addSingularItem(_outLASData, containerShortcut);
                }
            }
        }
    }
}
