/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPIMPORTSEGMAFILESFORMATCHNING_H
#define ONF_STEPIMPORTSEGMAFILESFORMATCHNING_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

#include "ct_itemdrawable/ct_point2d.h"

class ONF_StepImportSegmaFilesForMatching: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepImportSegmaFilesForMatching();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    QStringList    _refFile;
    QStringList    _transFile;

    CT_HandleOutResultGroup                                                             _outResultRef;
    CT_HandleOutStdGroup                                                                _outGrpRef;
    CT_HandleOutSingularItem<CT_Point2D>                                                _outRef;
    CT_HandleOutStdItemAttribute<float>                                                 _outRefVal;
    CT_HandleOutStdItemAttribute<QString>                                               _outRefID;

    CT_HandleOutResultGroup                                                             _outResultTrans;
    CT_HandleOutStdGroup                                                                _outGrpTrans;
    CT_HandleOutSingularItem<CT_Point2D>                                                _outTrans;
    CT_HandleOutStdItemAttribute<float>                                                 _outTransVal;
    CT_HandleOutStdItemAttribute<QString>                                               _outTransID;

};

#endif // ONF_STEPIMPORTSEGMAFILESFORMATCHNING_H
