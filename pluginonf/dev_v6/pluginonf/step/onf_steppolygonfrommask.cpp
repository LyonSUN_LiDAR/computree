/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_steppolygonfrommask.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"

ONF_StepPolygonFromMask::ONF_StepPolygonFromMask() : SuperClass()
{
    _mode = 0;
}

QString ONF_StepPolygonFromMask::description() const
{
    return tr("Création de polygones à partir de masques");
}

QString ONF_StepPolygonFromMask::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepPolygonFromMask::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepPolygonFromMask();
}

//////////////////// PROTECTED //////////////////

void ONF_StepPolygonFromMask::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    CT_ButtonGroup &bg_mode = postInputConfigDialog->addButtonGroup(_mode);

    postInputConfigDialog->addExcludeValue("", "", tr("Un unique polygone par masque"), bg_mode, 0);
    postInputConfigDialog->addExcludeValue("", "", tr("Un ou plusieurs polygones par masque"), bg_mode, 1);
}

void ONF_StepPolygonFromMask::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy * resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Dalles"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup, CT_AbstractItemGroup::staticGetType(), tr("Grp"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInMask, CT_Image2D<quint8>::staticGetType(), tr("Masque"));

    resultModel->addItemModel(DEF_SearchInGroup, _inItemWithXY, CT_AbstractSingularItemDrawable::staticGetType(), tr("XYRef (optionnel)"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resultModel->addItemAttributeModel(_inItemWithXY, _inXattribute, QList<QString>() << CT_AbstractCategory::DATA_X, CT_AbstractCategory::DOUBLE, tr("X"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resultModel->addItemAttributeModel(_inItemWithXY, _inYattribute, QList<QString>() << CT_AbstractCategory::DATA_Y, CT_AbstractCategory::DOUBLE, tr("Y"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
}

void ONF_StepPolygonFromMask::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != nullptr) {
        res->addGroupModel(DEF_SearchInGroup, _outPolygonGrpModelName, new CT_StandardItemGroup(), tr("Groupe"));

        if (_mode == 0)
        {
            res->addItemModel(DEF_SearchInGroup, _outPolygonModelName, new CT_Polygon2D(), tr("Polygone"));
        } else {
            res->addItemModel(_outPolygonGrpModelName, _outPolygonModelName, new CT_Polygon2D(), tr("Polygone"));
        }

        res->addItemAttributeModel(_outPolygonModelName, _att_X, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_X), tr("X_Ref"));
        res->addItemAttributeModel(_outPolygonModelName, _att_Y, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_Y), tr("Y_Ref"));
    }
}

void ONF_StepPolygonFromMask::compute()
{
    CT_ResultGroup *outRes = getOutResultList().first();

    CT_ResultGroupIterator it(outRes, this, DEF_SearchInGroup);
    while (it.hasNext() && (!isStopped()))
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();
        CT_Image2D<quint8> *mask = (CT_Image2D<quint8>*)group->firstItemByINModelName(this, DEF_SearchInMask);
        CT_AbstractSingularItemDrawable *refItem = (CT_AbstractSingularItemDrawable*)group->firstItemByINModelName(this, _inItemWithXY);

        if(mask != nullptr)
        {
            std::vector<std::vector<cv::Point> > contours;
            cv::Mat_<quint8> maskTmp = mask->getMat().clone();
            cv::findContours(maskTmp, contours, CV_RETR_EXTERNAL, CV_CHA_inAPPROX_NONE);
            maskTmp.release();

            int n = contours.size();
            if (_mode == 0) {n = 1;}

            for (int i = 0 ; i < n ; i++)
            {
                const std::vector<cv::Point> &contour = contours.at(i);

                QVector<Eigen::Vector2d *> vertices;

                double demiRes = mask->resolution() / 2.0;

                int xxLast = 0;
                int yyLast = 0;

                if (contour.size() > 0)
                {
                    const cv::Point &vert = contour.at(contour.size() - 1);
                    xxLast = vert.x;
                    yyLast = vert.y;
                }

                for (int j = 0 ; j < contour.size() ; j++)
                {
                    const cv::Point &vert = contour.at(j);
                    int xx = vert.x;
                    int yy = vert.y;

                    double x = mask->getCellCenterColCoord(xx);
                    double y = mask->getCellCenterLinCoord(yy);

                    bool sides[4];
                    sides[0] = (mask->value(xx+1, yy) == 0);
                    sides[1] = (mask->value(xx, yy-1) == 0);
                    sides[2] = (mask->value(xx-1, yy) == 0);
                    sides[3] = (mask->value(xx, yy+1) == 0);

                    if (sides[0] && sides[2] && !sides[1] && !sides[3])
                    {
                        if (yy < yyLast)
                        {
                            vertices.append(new Eigen::Vector2d(x + demiRes, y + demiRes));
                        } else {
                            vertices.append(new Eigen::Vector2d(x - demiRes, y - demiRes));
                        }
                    } else if (sides[1] && sides[3] && !sides[0] && !sides[2])
                    {
                        if (xx < xxLast)
                        {
                            vertices.append(new Eigen::Vector2d(x - demiRes, y + demiRes));
                        } else {
                            vertices.append(new Eigen::Vector2d(x + demiRes, y - demiRes));
                        }
                    } else if (sides[0] && sides[1] && sides[2] && sides[3])
                    {
                        vertices.append(new Eigen::Vector2d(x + demiRes, y + demiRes));
                        vertices.append(new Eigen::Vector2d(x - demiRes, y + demiRes));
                        vertices.append(new Eigen::Vector2d(x - demiRes, y - demiRes));
                        vertices.append(new Eigen::Vector2d(x + demiRes, y - demiRes));
                    } else if (sides[0] && !sides[3])
                    {
                        vertices.append(new Eigen::Vector2d(x + demiRes, y + demiRes));
                        if (sides[1])
                        {
                            vertices.append(new Eigen::Vector2d(x - demiRes, y + demiRes));
                            if (sides[2])
                            {
                                vertices.append(new Eigen::Vector2d(x - demiRes, y - demiRes));
                            }
                        }
                    } else if (sides[1] && !sides[0])
                    {
                        vertices.append(new Eigen::Vector2d(x - demiRes, y + demiRes));
                        if (sides[2])
                        {
                            vertices.append(new Eigen::Vector2d(x - demiRes, y - demiRes));
                            if (sides[3])
                            {
                                vertices.append(new Eigen::Vector2d(x + demiRes, y - demiRes));
                            }
                        }
                    } else if (sides[2] && !sides[1])
                    {
                        vertices.append(new Eigen::Vector2d(x - demiRes, y - demiRes));
                        if (sides[3])
                        {
                            vertices.append(new Eigen::Vector2d(x + demiRes, y - demiRes));
                            if (sides[0])
                            {
                                vertices.append(new Eigen::Vector2d(x + demiRes, y + demiRes));
                            }
                        }
                    } else if (sides[3] && !sides[2])
                    {
                        vertices.append(new Eigen::Vector2d(x + demiRes, y - demiRes));
                        if (sides[0])
                        {
                            vertices.append(new Eigen::Vector2d(x + demiRes, y + demiRes));
                            if (sides[1])
                            {
                                vertices.append(new Eigen::Vector2d(x - demiRes, y + demiRes));
                            }
                        }
                    }
                    xxLast = xx;
                    yyLast = yy;
                }

                if (vertices.size() > 0)
                {
                    CT_Polygon2DData* dataPoly = new CT_Polygon2DData(vertices, false);
                    CT_Polygon2D* poly = new CT_Polygon2D(_outPolygonModelName.completeName(), outRes, dataPoly);

                    if (refItem != nullptr)
                    {
                        CT_AbstractItemAttribute *attributeX = refItem->firstItemAttributeByINModelName(outRes, this, _inXattribute);
                        CT_AbstractItemAttribute *attributeY = refItem->firstItemAttributeByINModelName(outRes, this, _inYattribute);

                        if (attributeX != nullptr && attributeY != nullptr)
                        {
                            bool okX, okY;
                            double valX = attributeX->toDouble(refItem, &okX);
                            double valY = attributeY->toDouble(refItem, &okY);

                            if (okX && okY)
                            {
                                poly->addItemAttribute(new CT_StdItemAttributeT<double>(_att_X.completeName(), CT_AbstractCategory::DATA_X, outRes, valX));
                                poly->addItemAttribute(new CT_StdItemAttributeT<double>(_att_Y.completeName(), CT_AbstractCategory::DATA_Y, outRes, valY));
                            }
                        }
                    }

                    if (_mode == 0)
                    {
                        group->addSingularItem(poly);
                    } else {
                        CT_StandardItemGroup* outGrpPoly = new CT_StandardItemGroup(_outPolygonGrpModelName.completeName(), outRes);
                        outGrpPoly->addSingularItem(poly);
                        group->addGroup(outGrpPoly);
                    }
                }
            }
        }
    }
}

