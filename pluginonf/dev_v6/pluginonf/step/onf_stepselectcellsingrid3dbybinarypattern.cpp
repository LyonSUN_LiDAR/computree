/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepselectcellsingrid3dbybinarypattern.h"

ONF_StepSelectCellsInGrid3DByBinaryPattern::ONF_StepSelectCellsInGrid3DByBinaryPattern() : SuperClass()
{
    _inThreshold = 1;

    _pattern =  "1,1,0,0,0\n"
                "1,1,0,0,0\n"
                "1,1,0,0,0\n"
                "1,1,0,0,0\n"
                "1,1,0,0,0\n"
                "1,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n"
                "0,0,0,0,0\n";

    _outThresholdAbsolute = 20;
    _outThresholdRelative = 0.9;
    _selectMode = 1;
}

QString ONF_StepSelectCellsInGrid3DByBinaryPattern::description() const
{
    return tr("Créer Grille Booléenne à partir d'une Grille de Convolution");
}

QString ONF_StepSelectCellsInGrid3DByBinaryPattern::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepSelectCellsInGrid3DByBinaryPattern::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepSelectCellsInGrid3DByBinaryPattern::createNewInstance() const
{
    return new ONF_StepSelectCellsInGrid3DByBinaryPattern();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectCellsInGrid3DByBinaryPattern::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroup *res_inRgrid = createNewInResultModel(_inRgrid, tr("Grille"));
    res_inRgrid->setRootGroup(_inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRgrid->addItemModel(_inGrp, _inGrid, CT_AbstractGrid3D::staticGetType(), tr("Grille"));

}

void ONF_StepSelectCellsInGrid3DByBinaryPattern::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *res_rgrid = createNewOutResultModel(_outRgrid, tr("Grille de séléction"));
    res_rgrid->setRootGroup(_outGrp, new CT_StandardItemGroup(), tr("Groupe"));
    res_rgrid->addItemModel(_outGrp, _outGrid, new CT_Grid3D_Sparse<double>(), tr("Grille de comptage"));
    res_rgrid->addItemModel(_outGrp, _outGridBool, new CT_Grid3D_Sparse<bool>(), tr("Grille de séléction"));

}

void ONF_StepSelectCellsInGrid3DByBinaryPattern::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Valeur minimale de la grille d'entrée"), "", -1e+09, 1e+09, 4, _inThreshold, 1);
    postInputConfigDialog->addString(tr("Motif binaire"), "", _pattern);
    postInputConfigDialog->addEmpty();
    postInputConfigDialog->addTitle(tr("Choix du mode de fitrage :"));

    CT_ButtonGroup &bg_gridMode = postInputConfigDialog->addButtonGroup(_selectMode);
    postInputConfigDialog->addExcludeValue("", "", tr("En valeur absolue :"), bg_gridMode, 0);
    postInputConfigDialog->addInt(tr("Seuil de séléction"), "", 0, 1e+09, _outThresholdAbsolute);

    postInputConfigDialog->addExcludeValue("", "", tr("En % de la valeur maximale :"), bg_gridMode, 1);
    postInputConfigDialog->addDouble(tr("Seuil de séléction (en % du max)"), "", 0, 100, 2, _outThresholdRelative, 100);

}

void ONF_StepSelectCellsInGrid3DByBinaryPattern::compute()
{
    QList<PatternCell> parsedPattern;
    if (!parsePattern(parsedPattern)) {return;}

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* res_inRgrid = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rgrid = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator it_inGrp(res_inRgrid, this, _inGrp);
    while (it_inGrp.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grp_inGrp = (CT_AbstractItemGroup*) it_inGrp.next();

        CT_AbstractGrid3D* inGrid = (CT_AbstractGrid3D*)grp_inGrp->firstItemByINModelName(this, _inGrid);
        if (inGrid != nullptr)
        {

            CT_StandardItemGroup* grp= new CT_StandardItemGroup(_outGrp, res_rgrid);
            res_rgrid->addGroup(grp);

            CT_Grid3D_Sparse<double>* outGrid = new CT_Grid3D_Sparse<double>(_outGrid, res_rgrid, inGrid->minX(), inGrid->minY(), inGrid->minZ(), inGrid->xdim(), inGrid->ydim(), inGrid->zdim(), inGrid->resolution(), -1, -1);
            grp->addSingularItem(outGrid);

            CT_Grid3D_Sparse<int>* sparseInGrid = dynamic_cast<CT_Grid3D_Sparse<int>*>(inGrid);

            if (sparseInGrid != nullptr)
            {
                QList<size_t> indices;
                sparseInGrid->getIndicesWithData(indices);

                for (int i = 0 ; i < indices.size() ; i++)
                {
                    size_t indexOut = indices.at(i);
                    double sum = 0;

                    size_t row, col, lev;
                    if (inGrid->indexToGrid(indexOut, col, row, lev))
                    {
                        if (inGrid->valueAtIndexAsDouble(indexOut) > 0)
                        {
                            QListIterator<PatternCell> it(parsedPattern);
                            while (it.hasNext())
                            {
                                const PatternCell &pat = it.next();

                                size_t modifiedIndex;
                                if (inGrid->index(col + pat._colRel, row + pat._rowRel, lev + pat._levRel, modifiedIndex))
                                {
                                    if (inGrid->valueAtIndexAsDouble(modifiedIndex) > _inThreshold)
                                    {
                                        sum += pat._val;
                                    }
                                }
                            }
                            outGrid->setValueAtIndex(indexOut, sum);
                        }
                    }

                }
            } else {
                for (size_t indexOut = 0 ; indexOut < outGrid->nCells() ; indexOut++)
                {
                    double sum = 0;

                    size_t row, col, lev;
                    if (inGrid->indexToGrid(indexOut, col, row, lev))
                    {
                        if (inGrid->valueAtIndexAsDouble(indexOut) > 0)
                        {
                            QListIterator<PatternCell> it(parsedPattern);
                            while (it.hasNext())
                            {
                                const PatternCell &pat = it.next();

                                size_t modifiedIndex;
                                if (inGrid->index(col + pat._colRel, row + pat._rowRel, lev + pat._levRel, modifiedIndex))
                                {
                                    if (inGrid->valueAtIndexAsDouble(modifiedIndex) > _inThreshold)
                                    {
                                        sum += pat._val;
                                    }
                                }
                            }
                            outGrid->setValueAtIndex(indexOut, sum);
                        }
                    }

                }
            }

            outGrid->computeMinMax();

            CT_Grid3D_Sparse<bool>* outGridBool = new CT_Grid3D_Sparse<bool>(_outGridBool, res_rgrid, inGrid->minX(), inGrid->minY(), inGrid->minZ(), inGrid->xdim(), inGrid->ydim(), inGrid->zdim(), inGrid->resolution(), false, false);
            grp->addSingularItem(outGridBool);

            int threshold = _outThresholdAbsolute;
            if (_selectMode == 1)
            {
                threshold = outGrid->dataMax()*_outThresholdRelative;
            }

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString("Seuil de séléction : %1 (valeurs observées : %2 - %3)").arg(threshold).arg(outGrid->dataMin()).arg(outGrid->dataMax()));

            if (sparseInGrid != nullptr)
            {
                QList<size_t> indices;
                sparseInGrid->getIndicesWithData(indices);

                for (int i = 0 ; i < indices.size() ; i++)
                {
                    size_t indexOut = indices.at(i);
                    double value = outGrid->valueAtIndexAsDouble(indexOut);
                    if (value >= threshold)
                    {
                        outGridBool->setValueAtIndex(indexOut, true);
                    }
                }
            } else {
                for (size_t indice = 0 ; indice < outGrid->nCells() ; indice++)
                {
                    double value = outGrid->valueAtIndexAsDouble(indice);
                    if (value >= threshold)
                    {
                        outGridBool->setValueAtIndex(indice, true);
                    }
                }
            }
            setProgress(100.0);
        }
    }

}

bool ONF_StepSelectCellsInGrid3DByBinaryPattern::parsePattern(QList<PatternCell> &parsedPattern)
{
    // Pattern parsing
    QStringList levels = _pattern.split("\n", QString::SkipEmptyParts);
    int nlevels = levels.size();
    if (nlevels < 1) {return false;}
    if (nlevels % 2 == 0) {return false;}

    int maxLevel = nlevels / 2;

    for (int lev = 0 ; lev < nlevels ; lev++)
    {
        int levelNumber = maxLevel - lev;

        QStringList cells = levels.at(lev).split(",");
        int ncells = cells.size();

        if (ncells > 0)
        {
            bool ok;
            double val = cells.at(0).toDouble(&ok);
            if (ok && val != 0)
            {
                parsedPattern.append(PatternCell(0, 0, levelNumber, val));
            }

            for (int c = 1 ; c < ncells ; c++)
            {
                val = cells.at(c).toDouble(&ok);
                if (ok && val != 0)
                {
                    for (int j = -c ; j < c ; j++)
                    {
                        parsedPattern.append(PatternCell(-c,  j, levelNumber, val));
                        parsedPattern.append(PatternCell( j,  c, levelNumber, val));
                        parsedPattern.append(PatternCell( c, -j, levelNumber, val));
                        parsedPattern.append(PatternCell(-j, -c, levelNumber, val));
                    }
                }
            }
        }
    }
    return true;
}
