#ifndef ONF_STEPCOMPUTEPOINTHEIGHTFROMTIN_H
#define ONF_STEPCOMPUTEPOINTHEIGHTFROMTIN_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesscalarmaskt.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"

class ONF_StepComputePointHeightFromTIN : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    struct PointCore0_5
    {
        quint8  entire; // Edge of Flight Line (1 bit) - Scan Direction Flag (1 bit) - Number of Returns (3 bits) - Return Number (3 bits)

        typedef quint8 MASK;
    };

    struct PointCore6_10
    {
        quint16 entire; // Number of Returns (4 bits) - Return Number (4 bits)
                        // Edge of Flight Line (1 bit) - Scan Direction Flag (1 bit) - Scanner Channel (2 bits) - Classification Flags (4 bits)

        typedef quint16 MASK;
    };

    ONF_StepComputePointHeightFromTIN();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    bool                    _ptsAttribute;
    bool                    _ptsCloud;
    bool                    _addLAS;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_StdLASPointsAttributesContainer>                         _inLAS;

    CT_HandleInResultGroup<>                                            _inResultTIN;
    CT_HandleInStdZeroOrMoreGroup                                       _inZeroOrMoreRootGroupTIN;
    CT_HandleInStdGroup<>                                               _inGroupTIN;
    CT_HandleInSingularItem<CT_Triangulation2D>                         _inTIN;


    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<float> >        _outHeightAtt;
    CT_HandleOutSingularItem<CT_Scene>                                          _outScene;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASreturnNumber;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASnumberOfReturns;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASclassificationFlags;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASscannerChannel;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASscanDirectionFlags;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarMaskT<PointCore6_10> >    _outLASedgeOfFlightLine;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLASintensity;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint8> >       _outLASclassification;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<qint16> >       _outLASscanAngleRank;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint8> >       _outLASuserData;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLASpointSourceID;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<double> >       _outLASgpsTime;
    CT_HandleOutSingularItem<CT_PointsAttributesColor >                         _outLAScolor;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLAScolorR;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLAScolorG;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLAScolorB;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint16> >      _outLASnir;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint8> >       _outLASwavePacket;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint64> >      _outLASbyteOffsetWaveform;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<quint32> >      _outLASwaveformPacketSize;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<float> >        _outLASreturnPointWaveform;
    CT_HandleOutSingularItem<CT_StdLASPointsAttributesContainer >               _outLASAttributes;


};

#endif // ONF_STEPCOMPUTEPOINTHEIGHTFROMTIN_H
