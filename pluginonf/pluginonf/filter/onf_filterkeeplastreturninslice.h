/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_FILTERKEEPLASTRETURNINSLICE_H
#define ONF_FILTERKEEPLASTRETURNINSLICE_H


#include "ctliblas/filters/abstract/ct_abstractfilter_las.h"
#include "ct_iterator/ct_pointiterator.h"


class ONF_FilterKeepLastReturnInSlice : public CT_AbstractFilter_LAS
{
    Q_OBJECT
public:

    struct CandidatePoint {
        CandidatePoint(size_t globalIndex, double z, double gpsTime)
        {
            _globalIndex = globalIndex;
            _z = z;
            _gpsTime = gpsTime;
        }

        size_t  _globalIndex;
        double  _z;
        double _gpsTime;
    };

    static bool sortByTimeAndZ (CandidatePoint* a, CandidatePoint* b) {

        if (a->_gpsTime < b->_gpsTime) {return true;}
        else if (a->_gpsTime > b->_gpsTime) {return false;}
        else return (a->_z < b->_z);
    }



    ONF_FilterKeepLastReturnInSlice();
    ONF_FilterKeepLastReturnInSlice(const ONF_FilterKeepLastReturnInSlice &other);

    QString getDetailledDisplayableName();
    QString getShortDescription() const;

    bool filterPointCloudIndex();

    bool validatePoint(const CT_PointIterator& pointIt) { Q_UNUSED(pointIt); return false; }
    virtual bool validatePoint(const CT_PointIterator &pointIt, const CT_LASData &LASData) { Q_UNUSED(pointIt); Q_UNUSED(LASData); return false; }

    CT_AbstractConfigurableWidget* createConfigurationWidget();

    SettingsNodeGroup* getAllSettings() const;
    bool setAllSettings(const SettingsNodeGroup *settings);

    virtual void saveSettings(SettingsWriterInterface& writer) {}
    virtual bool restoreSettings(SettingsReaderInterface& reader) {return true;}

    CT_AbstractConfigurableElement* copy() const;

private:
    double      _minZ;
    double     _maxZ;

};


#endif // ONF_FILTERKEEPLASTRETURNINSLICE_H
