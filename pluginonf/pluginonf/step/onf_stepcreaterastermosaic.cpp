#include "onf_stepcreaterastermosaic.h"

#include "ct_itemdrawable/ct_image2d.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_iterator/ct_resultitemiterator.h"
#include "ct_global/ct_context.h"


#include <QDebug>

#define DEF_inResult_emprise "inResult"
#define DEF_inGroupEmprise  "inGroup"
#define DEF_inItemArea   "DEF_SearchInArea"

#define DEFin_resRaster "res"
#define DEFin_groupRaster "grp"
#define DEFin_raster "raster"


ONF_StepCreateRasterMosaic::ONF_StepCreateRasterMosaic(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

ONF_StepCreateRasterMosaic::~ONF_StepCreateRasterMosaic()
{
}

QString ONF_StepCreateRasterMosaic::getStepDescription() const
{
    return tr("Redallage raster");
}

QString ONF_StepCreateRasterMosaic::getStepDetailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepCreateRasterMosaic::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new ONF_StepCreateRasterMosaic(dataInit);
}

//////////////////// PROTECTED //////////////////

void ONF_StepCreateRasterMosaic::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inResult_emprise, tr("Emprises cibles"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGroupEmprise, CT_AbstractItemGroup::staticGetType(), tr("Groupe"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resultModel->addItemModel(DEF_inGroupEmprise, DEF_inItemArea, CT_AbstractSingularItemDrawable::staticGetType(), tr("Emprise cible"));

    CT_InResultModelGroup *resIn_res = createNewInResultModel(DEFin_resRaster, tr("Rasters d'entrée"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_groupRaster, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_groupRaster, DEFin_raster, CT_AbstractImage2D::staticGetType(), tr("Raster d'entrée"));
}

// Redefine in children steps to complete ConfigurationDialog
void ONF_StepCreateRasterMosaic::createPostConfigurationDialog()
{
}

// Redefine in children steps to complete out Models
void ONF_StepCreateRasterMosaic::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_inResult_emprise);

    if(resultModel != NULL)
    {
        resultModel->addItemModel(DEF_inGroupEmprise, _outRasterModelName, new CT_Image2D<float>(), tr("Raster"));
    }
}

// Redefine in children steps to complete compute method
void ONF_StepCreateRasterMosaic::compute()
{   
    CT_ResultGroup *inResult = getInputResults().at(0);
    CT_ResultGroup *outResult = getOutResultList().at(0);


    bool rasterNull = true;
    _min(0) = std::numeric_limits<double>::max();
    _min(1) = std::numeric_limits<double>::max();

    CT_ResultItemIterator itIn(inResult, this, DEFin_raster);
    while (itIn.hasNext())
    {
        CT_AbstractImage2D *rasterIn = (CT_AbstractImage2D*) itIn.next();

        if (rasterIn != NULL)
        {
            rasterNull = false;

            Eigen::Vector2d minTmp;
            rasterIn->getMinCoordinates(minTmp);

            if (minTmp(0) < _min(0)) {_min(0) = minTmp(0);}
            if (minTmp(1) < _min(1)) {_min(1) = minTmp(1);}
            _resolution = rasterIn->resolution();
        }
    }

    if (rasterNull) {return;}

    CT_ResultGroupIterator itOut2(outResult, this, DEF_inGroupEmprise);
    while (itOut2.hasNext() && (!isStopped()))
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*) itOut2.next();

        CT_AbstractSingularItemDrawable* areaIn = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, DEF_inItemArea);
        if (areaIn != NULL)
        {
            Eigen::Vector3d minArea, maxArea;
            double minRasterX, minRasterY, maxRasterX, maxRasterY;

            size_t dimx = 0;
            size_t dimy = 0;

            areaIn->getBoundingBox(minArea, maxArea);

            minRasterX = _min(0);
            minRasterY = _min(1);

            while (minRasterX < minArea(0)) {minRasterX += _resolution;}
            while (minRasterY < minArea(1)) {minRasterY += _resolution;}

            maxRasterX = minRasterX;
            maxRasterY = minRasterY;

            while (maxRasterX < maxArea(0)) {maxRasterX += _resolution;++dimx;}
            while (maxRasterY < maxArea(1)) {maxRasterY += _resolution;++dimy;}

            double refLevel = 0.0;
            float naVal = -std::numeric_limits<float>::max();
            CT_Image2D<float>* rasterOut = new CT_Image2D<float>(_outRasterModelName.completeName(), (CT_AbstractResult*)outResult, minRasterX, minRasterY, dimx, dimy, _resolution, refLevel, naVal, naVal);


            CT_ResultItemIterator itIn2(inResult, this, DEFin_raster);
            while (itIn2.hasNext())
            {
                CT_AbstractImage2D *rasterIn = (CT_AbstractImage2D*) itIn2.next();

                if (rasterIn != NULL)
                {
                    for (size_t xx = 0 ; xx < dimx ; xx++)
                    {
                        for (size_t yy = 0 ; yy < dimy ; yy++)
                        {
                            Eigen::Vector3d center;
                            size_t index;
                            rasterOut->getCellCenterCoordinates(xx, yy, center);

                            if (rasterIn->indexAtCoords(center(0), center(1), index))
                            {
                                double val = rasterIn->valueAtIndexAsDouble(index);
                                if (val != rasterIn->NAAsDouble())
                                {
                                    rasterOut->setValue(xx, yy, (float)val);
                                }
                            }
                        }
                    }
                }
            }

            rasterOut->computeMinMax();
            group->addItemDrawable(rasterOut);
        }
    }

    setProgress( 100 );
}

