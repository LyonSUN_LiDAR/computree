/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONSELECTSCENEFOREACHPOSITION_H
#define ONF_ACTIONSELECTSCENEFOREACHPOSITION_H


#include "views/actions/onf_actionselectsceneforeachpositionoptions.h"
#include "ctlibstdactions/views/actions/ct_actionselectitemdrawablegvoptions.h"
#include "ctlibstdactions/action/tools/ct_actionselecttool.h"

#include "ct_actions/abstract/ct_abstractactionforgraphicsview.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_result/abstract/ct_abstractresult.h"
#include "tools/onf_colorlinearinterpolator.h"
#include <QRect>
#include <QDebug>

class ONF_ActionSelectSceneForEachPosition_treePosition
{
public:
    ONF_ActionSelectSceneForEachPosition_treePosition()
    {
        _base = Eigen::Vector3d(0,0,0);
        _apex = Eigen::Vector3d(0,0,0);
        _hasApex = false;

        _dbh = 0;
        _height = 0;
        _idPlot = "";
        _idTree = "";
        _species = "";
        _grp = NULL;
    }

    ~ONF_ActionSelectSceneForEachPosition_treePosition()
    {
        _grp = NULL;
        _scenesIds.clear();
    }

    void addScene(int sceneId, const Eigen::Vector3d& apex)
    {
        _scenesIds.append(sceneId);
        if (apex(2) > _apex(2))
        {
            _apex = apex;
            _hasApex = true;
        }
    }

    Eigen::Vector3d         _base;
    Eigen::Vector3d         _apex;
    bool                    _hasApex;

    float                   _dbh;
    float                   _height;
    QString                 _idPlot;
    QString                 _idTree;
    QString                 _species;
    CT_StandardItemGroup*   _grp;

    QList<int>              _scenesIds;
};

class ONF_ActionSelectSceneForEachPosition_dataContainer
{
public:
    ONF_ActionSelectSceneForEachPosition_dataContainer();

    ~ONF_ActionSelectSceneForEachPosition_dataContainer()
    {
        _positions.clear();
        _scenes.clear();
        _scenesApex.clear();
        _matched.clear();

        qDeleteAll(_toDelete);
        _toDelete.clear();
    }

    QList<ONF_ActionSelectSceneForEachPosition_treePosition*>   _positions;

    QList<CT_AbstractItemDrawable*>                             _scenes;
    QList<Eigen::Vector3d>                                      _scenesApex;
    QVector<bool>                                               _matched;
    QList<CT_AbstractItemDrawable*>                             _toDelete;

};

class ONF_ActionSelectSceneForEachPosition : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    ONF_ActionSelectSceneForEachPosition(ONF_ActionSelectSceneForEachPosition_dataContainer* dataContainer);

    ~ONF_ActionSelectSceneForEachPosition();

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);
    bool wheelEvent(QWheelEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void draw(GraphicsViewInterface &view, PainterInterface &painter);
    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    CT_AbstractAction* copy() const;

    void colorizeScenes();

    void updatePositionApex(ONF_ActionSelectSceneForEachPosition_treePosition* treePos);

public slots:
    void addCluster();
    void removeCluster();
    void switchCluster();
    void initColors();
    void initSegmentMode();
    void validateSegmentMode();
    void hideNotSelectedToggled();

private:

    ONF_ActionSelectSceneForEachPosition_dataContainer*     _dataContainer;
    CT_ActionSelectItemDrawableGVOptions*                   _selectOption;
    CT_ActionSelectTool*                                    _selectTool;
    CT_AbstractItemDrawableWithPointCloud*                  _currentScene;

    QColor                                                  _unmatchedColor;
    QList<QColor>                                           _automaticColorList;
    QVector<QColor>                                         _sceneColors;
    int                                                     _colorNum;
    bool                                                    _segmentMode;


    QColor                                                  _cylinderColor1;
    QColor                                                  _cylinderColor2;
    QColor                                                  _selectedCylinderColor1;
    QColor                                                  _selectedCylinderColor2;
    QColor                                                  _highlightedCylindersColor1;
    QColor                                                  _highlightedCylindersColor2;

    Qt::MouseButtons                                        _buttonsPressed;
    QPoint                                                  _lastPos;
    ONF_ActionSelectSceneForEachPosition_treePosition*      _selectedPos;
    ONF_ActionSelectSceneForEachPosition_treePosition*      _previousSelectedPos;

    ONF_ActionSelectSceneForEachPosition_treePosition *getPositionFromDirection(Eigen::Vector3d &origin, Eigen::Vector3d &direction);
    bool getCoordsForMousePosition(QPoint p, double &x, double &y);
    void drawCylinder(GraphicsViewInterface &view, PainterInterface &painter, ONF_ActionSelectSceneForEachPosition_treePosition* treePos) const;    
    QColor nextColor();

private slots:
    void redrawOverlay();
    void redrawOverlayAnd3D();
    void updateColorization();


};


#endif // ONF_ACTIONSELECTSCENEFOREACHPOSITION_H
