/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONMODIFYDEMOPTIONS_H
#define ONF_ACTIONMODIFYDEMOPTIONS_H

#include "ct_view/actions/abstract/ct_gabstractactionoptions.h"
#include "actions/onf_actionmodifydem.h"

namespace Ui {
class ONF_ActionModifyDEMOptions;
}

class ONF_ActionModifyDEMOptions : public CT_GAbstractActionOptions
{
    Q_OBJECT

public:

    explicit ONF_ActionModifyDEMOptions(const ONF_ActionModifyDEM *action);
    ~ONF_ActionModifyDEMOptions();

    GraphicsViewInterface::SelectionMode selectionMode() const;

    void setMultiSelect(bool multi);
    void changeStepValue(bool increase);

    float getStepValue();
    int getBrushSize();
    bool isImageSelected();

    void setImageAvailable(bool available);

private:
    Ui::ONF_ActionModifyDEMOptions *ui;

private slots:
    void on_buttonGroupSelection_buttonReleased(int id);
    void on_buttonGroupMode_buttonReleased(int id);

    void on_tb_up_clicked();
    void on_tb_down_clicked();
    void on_tb_up_ceil_clicked();
    void on_tb_down_floor_clicked();
    void on_tb_raz_clicked();

    void on_tb_smooth_clicked();

    void on_cb_image_toggled(bool checked);

public slots:

    void setSelectionMode(GraphicsViewInterface::SelectionMode mode);

signals:

    void selectionModeChanged(GraphicsViewInterface::SelectionMode mode);
    void levelPoints(bool up, bool maxed);
    void razPoints();
    void smooth();
    void imageStateChanged();


};

#endif // ONF_ACTIONMODIFYDEMOPTIONS_H
