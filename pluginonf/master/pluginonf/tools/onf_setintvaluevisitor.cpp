#include "onf_setintvaluevisitor.h"

ONF_SetIntValueVisitor::ONF_SetIntValueVisitor(CT_Grid3D<int> *grid, int value)
{
  _grid = grid;
  _value = value;
}

void ONF_SetIntValueVisitor::visit(const size_t &index, const CT_Beam *beam)
{
    _grid->setValueAtIndex(index, _value);
}
