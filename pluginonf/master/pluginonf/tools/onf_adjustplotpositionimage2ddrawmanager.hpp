#ifndef ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_HPP
#define ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_HPP

#include "onf_adjustplotpositionimage2ddrawmanager.h"

#include <stdlib.h>
#include <time.h>

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_tools/ct_typeinfo.h"

#include <ctime>

#include <QObject>

template< typename DataT >
ONF_AdjustPlotPositionImage2DDrawManager<DataT>::ONF_AdjustPlotPositionImage2DDrawManager(QString drawConfigurationName)
    : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? CT_Image2D<DataT>::staticName() : drawConfigurationName)
{
    _colorgradient = NULL;
    _min = std::numeric_limits<double>::max();
    _max = std::numeric_limits<double>::max();
    _refZ = 0;
}

template< typename DataT >
ONF_AdjustPlotPositionImage2DDrawManager<DataT>::~ONF_AdjustPlotPositionImage2DDrawManager()
{
}

template< typename DataT >
void ONF_AdjustPlotPositionImage2DDrawManager<DataT>::setGradient(ONF_ColorLinearInterpolator* colorgradient)
{
    _colorgradient = colorgradient;
}


template< typename DataT >
void ONF_AdjustPlotPositionImage2DDrawManager<DataT>::setMinMax(int min, int max)
{
    _min = min;
    _max = max;
}

template< typename DataT >
void ONF_AdjustPlotPositionImage2DDrawManager<DataT>::setRefZ(double refZ)
{
    _refZ = refZ;
}

template< typename DataT >
void ONF_AdjustPlotPositionImage2DDrawManager<DataT>::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    if (_colorgradient == NULL) {return;}

    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const CT_Image2D<DataT> &item = dynamic_cast< const CT_Image2D<DataT>& >(itemDrawable);

    double rangeZ = item.dataMax() - item.dataMin();
    double minZ = item.dataMin() + (double)_min * rangeZ / 100.0;
    double maxZ = item.dataMin() + (double)_max * rangeZ / 100.0;
    if (minZ > maxZ) {minZ = maxZ;}

    double demiRes = item.resolution()/2.0;

    for (size_t cx = 0 ; cx < item.colDim() ; cx++)
    {
        for (size_t ly = 0 ; ly < item.linDim() ; ly++)
        {
            double x = item.getCellCenterColCoord(cx);
            double y = item.getCellCenterLinCoord(ly);
            double value = item.value(cx, ly);

            if (value != item.NA())
            {
                double ratio = (value - minZ)/(maxZ - minZ);

                if (value < minZ) {ratio = 0;}
                if (value > maxZ) {ratio = 1;}

                painter.setColor(_colorgradient->intermediateColor(ratio));

                Eigen::Vector2d tLeft(x - demiRes, y + demiRes);
                Eigen::Vector2d bRight(tLeft(0)+item.resolution(), tLeft(1)-item.resolution());

                painter.fillRectXY(tLeft, bRight, _refZ);
            }
        }
    }
}

template< typename DataT >
CT_ItemDrawableConfiguration ONF_AdjustPlotPositionImage2DDrawManager<DataT>::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));

    return item;
}

// PROTECTED //


#endif // ONF_ADJUSTPLOTPOSITIONIMAGE2DDRAWMANAGER_HPP
