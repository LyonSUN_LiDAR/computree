/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepfiltergridbyvalueandneighborhood.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_iterator/ct_resultitemiterator.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "tools/onf_computehitsthread.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>

#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInGrid   "grid"

ONF_StepFilterGridByValueAndNeighborhood::ONF_StepFilterGridByValueAndNeighborhood(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _threshold = -9999;
    _nbCells = 6;
    _neighborhood = 1;
}

QString ONF_StepFilterGridByValueAndNeighborhood::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Seuiller une grille 3D par valeur et voisinnage");
}

// Step description (tooltip of contextual menu)
QString ONF_StepFilterGridByValueAndNeighborhood::getStepDetailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepFilterGridByValueAndNeighborhood::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepFilterGridByValueAndNeighborhood(dataInit);
}

void ONF_StepFilterGridByValueAndNeighborhood::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grille"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGrid, CT_Grid3D_Sparse<int>::staticGetType(), tr("Grille"), "", CT_InAbstractModel::C_ChooseMultipleIfMultiple);
}

void ONF_StepFilterGridByValueAndNeighborhood::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
    {
        res->addItemModel(DEF_SearchInGroup, _outGrid_ModelName, new CT_Grid3D_Sparse<bool>(), tr("Grille filtrée"));
    }
}

void ONF_StepFilterGridByValueAndNeighborhood::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addInt(tr("Seuil (minimum inclus)"),"", 0, 1e+10, _threshold);
    configDialog->addInt(tr("Nombre minimal de cellules voisines >= seuil"),"", 0, 1e+10, _nbCells);
    configDialog->addInt(tr("Voisinage"),tr("cellules"), 1, 1e+10, _neighborhood);
}

void ONF_StepFilterGridByValueAndNeighborhood::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itOut(outResult, this, DEF_SearchInGroup);
    // iterate over all groups
    while(itOut.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)itOut.next();
        const CT_Grid3D_Sparse<int>* gridIn = (CT_Grid3D_Sparse<int>*)group->firstItemByINModelName(this, DEF_SearchInGrid);

        if (gridIn!=NULL)
        {
            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* outGrid = new CT_Grid3D_Sparse<bool>(_outGrid_ModelName.completeName(), outResult, gridIn->minX(), gridIn->minY(), gridIn->minZ(), gridIn->xdim(), gridIn->ydim(), gridIn->zdim(), gridIn->resolution(), false, false);

            QList<size_t> list;
            gridIn->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);
                size_t colX, linY, levZ;

                if (gridIn->valueAtIndex(index) >= _threshold)
                {
                    if (gridIn->indexToGrid(index, colX, linY, levZ))
                    {
                        QList<int> neighbors = gridIn->neighboursValues(colX, linY, levZ, _neighborhood);
                        int nbValidCell = 0;
                        for (int j = 0 ; j < neighbors.size() ; j++)
                        {
                            if (neighbors.at(j) >= _threshold)
                            {
                               ++nbValidCell;
                            }
                        }

                        if (nbValidCell >= _nbCells)
                        {
                            outGrid->setValueAtIndex(index, true);
                        }
                    }
                }
            }

            outGrid->computeMinMax();
            group->addItemDrawable(outGrid);
        }
    }

    setProgress(99);
}


