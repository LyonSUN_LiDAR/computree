#include "onf_stepcomputepointheightattribute.h"


// Utilise le depot
#include "ct_global/ct_context.h"
#include "ct_model/tools/ct_modelsearchhelper.h"

// Utilise les attributs optionnels
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesscalarmaskt.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_groupiterator.h"
#include "ct_iterator/ct_mutablepointiterator.h"

// Alias for indexing in models
#define DEFin_resSc "resSc"
#define DEFin_scGrp "scGrp"
#define DEFin_sc "sc"
#define DEFin_attLAS "attLAS"

#define DEFin_resDTM "resDTM"
#define DEFin_DTMGrp "DTMGrp"
#define DEFin_DTM "DTM"


// Constructor : initialization of parameters
ONF_StepComputePointHeightAttribute::ONF_StepComputePointHeightAttribute(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _ptsAttribute = false;
    _ptsCloud = true;
    _addLAS = true;
}

// Step description (tooltip of contextual menu)
QString ONF_StepComputePointHeightAttribute::getStepDescription() const
{
    return tr("Calculer la hauteur des points à l'aide d'un MNT");
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepComputePointHeightAttribute::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepComputePointHeightAttribute(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepComputePointHeightAttribute::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy * resultSc = createNewInResultModelForCopy(DEFin_resSc, tr("Scene(s)"));
    resultSc->setZeroOrMoreRootGroup();
    resultSc->addGroupModel("", DEFin_scGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultSc->addItemModel(DEFin_scGrp, DEFin_sc, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene(s)"));

    if (_addLAS)
    {
        resultSc->addItemModel(DEFin_scGrp, DEFin_attLAS, CT_StdLASPointsAttributesContainer::staticGetType(), tr("Attributs LAS"), tr("Attribut LAS"));
    }

    CT_InResultModelGroup *resultDTM = createNewInResultModel(DEFin_resDTM, tr("MNT"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", DEFin_DTMGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(DEFin_DTMGrp, DEFin_DTM, CT_Image2D<float>::staticGetType(), tr("MNT"));
}

// Creation and affiliation of OUT models
void ONF_StepComputePointHeightAttribute::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_resSc);

    if(res != NULL)
    {
        if (_ptsAttribute)
        {
            res->addItemModel(DEFin_scGrp, _outHeightAttributeModelName, new CT_PointsAttributesScalarTemplated<float>(), tr("Height attribute"));
        }
        if (_ptsCloud)
        {
            if (_addLAS)
            {
                res->addItemModel(DEFin_scGrp, _lasreturnNumberOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Return Number"));
                res->addItemModel(DEFin_scGrp, _lasnumberOfReturnsOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Number of Returns"));
                res->addItemModel(DEFin_scGrp, _lasclassificationFlagsOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Classification Flags"));
                res->addItemModel(DEFin_scGrp, _lasscannerChannelOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Scanner Channel"));
                res->addItemModel(DEFin_scGrp, _lasscanDirectionFlagsOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Scan Direction Flag"));
                res->addItemModel(DEFin_scGrp, _lasedgeOfFlightLineOut_AutoRenameModel, new CT_PointsAttributesScalarMaskT<PointCore6_10>(), tr("Edge of Flight Line"));
                res->addItemModel(DEFin_scGrp, _lasintensityOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("Intensité"));
                res->addItemModel(DEFin_scGrp, _lasclassificationOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint8>(), tr("Classification"));
                res->addItemModel(DEFin_scGrp, _lasscanAngleRankOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<qint16>(), tr("Scan Angle"));
                res->addItemModel(DEFin_scGrp, _lasuserDataOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint8>(), tr("User Data"));
                res->addItemModel(DEFin_scGrp, _laspointSourceIDOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("Point Source ID"));
                res->addItemModel(DEFin_scGrp, _lasgpsTimeOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<double>(), tr("GPS Time"));
                res->addItemModel(DEFin_scGrp, _lascolorOut_AutoRenameModel, new CT_PointsAttributesColor(), tr("Color"));
                res->addItemModel(DEFin_scGrp, _lascolorROut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("Red"));
                res->addItemModel(DEFin_scGrp, _lascolorGOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("Green"));
                res->addItemModel(DEFin_scGrp, _lascolorBOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("Blue"));
                res->addItemModel(DEFin_scGrp, _lasnirOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint16>(), tr("NIR"));
                res->addItemModel(DEFin_scGrp, _laswavePacketOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint8>(), tr("Wave Packet Descriptor Index"));
                res->addItemModel(DEFin_scGrp, _lasbyteOffsetWaveformOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint64>(), tr("Byte offset to waveform data"));
                res->addItemModel(DEFin_scGrp, _laswaveformPacketSizeOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<quint32>(), tr("Waveform packet size in bytes"));
                res->addItemModel(DEFin_scGrp, _lasreturnPointWaveformOut_AutoRenameModel, new CT_PointsAttributesScalarTemplated<float>(), tr("Return Point Waveform Location"));
                res->addItemModel(DEFin_scGrp, _lasAttributesOut_AutoRenameModel, new CT_StdLASPointsAttributesContainer(), tr("LAS Attributs (H)"));
            }

            res->addItemModel(DEFin_scGrp, _outHeightCloudModelName, new CT_Scene(), tr("Height cloud"));

        }
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepComputePointHeightAttribute::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addBool(tr("Créer des attributs de points hauteur"), "", "", _ptsAttribute);
    configDialog->addEmpty();
    configDialog->addBool(tr("Créer un nuage de points hauteur"), "", "", _ptsCloud);
    configDialog->addBool(tr("Transférer les attributs las au nuage de points hauteur"), "", "", _addLAS);
}

void ONF_StepComputePointHeightAttribute::compute()
{
    CT_ResultGroup* resin_DTM = getInputResults().at(1);

    CT_Image2D<float>* dtm = NULL;
    CT_ResultItemIterator it(resin_DTM, this, DEFin_DTM);
    if (it.hasNext())
    {
        dtm = (CT_Image2D<float>*) it.next();
    }

    if (dtm != 0)
    {
        CT_ResultGroup* resOut = getOutResultList().first();

        CT_ResultGroupIterator itSc(resOut, this, DEFin_scGrp);
        while (itSc.hasNext())
        {
            CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itSc.next();

            if (grp != NULL)
            {
                CT_AbstractItemDrawableWithPointCloud* scene = (CT_AbstractItemDrawableWithPointCloud*) grp->firstItemByINModelName(this, DEFin_sc);

                if (scene != NULL && !isStopped())
                {
                    const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
                    size_t n_points = pointCloudIndex->size();

                    CT_PointIterator itP(pointCloudIndex);

                    // On declare un tableau d'attributs double que l'on va remplir avec les coordonnées correspondant a l'axe demandé
                    CT_StandardCloudStdVectorT<float> *attribute = NULL;
                    if (_ptsAttribute) {attribute = new CT_StandardCloudStdVectorT<float>();}

                    CT_NMPCIR heightCloud;
                    if (_ptsCloud) {heightCloud = PS_REPOSITORY->createNewPointCloud(n_points);}
                    CT_MutablePointIterator itPM(heightCloud);

                    float minAttribute = std::numeric_limits<float>::max();
                    float maxAttribute = -std::numeric_limits<float>::max();

                    if (_ptsAttribute || _ptsCloud)
                    {
                        size_t i = 0;
                        // On applique la translation a tous les points du nuage
                        while (itP.hasNext() && !isStopped())
                        {
                            CT_Point point = itP.next().currentPoint();
                            float h = (float)point(2) - dtm->valueAtCoords(point(0), point(1));

                            if (_ptsAttribute) {attribute->addT(h);}

                            if (_ptsCloud)
                            {
                                point.setZ(h);
                                itPM.next().replaceCurrentPoint(point);
                            }

                            if (_ptsAttribute && h < minAttribute) {minAttribute = h;}
                            if (_ptsAttribute && h > maxAttribute) {maxAttribute = h;}

                            setProgress( 100.0*i++ / n_points);
                        }

                        if (i > 0)
                        {
                            if (_ptsAttribute)
                            {
                                CT_PointsAttributesScalarTemplated<float>*  itemOut_attribute  = new CT_PointsAttributesScalarTemplated<float>(_outHeightAttributeModelName.completeName(),
                                                                                                                                               resOut,
                                                                                                                                               scene->getPointCloudIndexRegistered(),
                                                                                                                                               attribute,
                                                                                                                                               minAttribute,
                                                                                                                                               maxAttribute);
                                grp->addItemDrawable(itemOut_attribute);
                            }

                            if (_ptsCloud)
                            {
                                CT_Scene*  itemOut_scene  = new CT_Scene(_outHeightCloudModelName.completeName(), resOut, heightCloud);
                                itemOut_scene->updateBoundingBox();

                                grp->addItemDrawable(itemOut_scene);


                                // Ajout des données LAS
                                if (_addLAS && _ptsCloud)
                                {
                                    const CT_StdLASPointsAttributesContainer* attributeLAS = (CT_StdLASPointsAttributesContainer*)grp->firstItemByINModelName(this, DEFin_attLAS);

                                    if (attributeLAS != NULL)
                                    {
                                        // Retrieve attributes
                                        QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> itLAS(attributeLAS->lasPointsAttributes());
                                        if (!itLAS.hasNext()) {return;}

                                        CT_AbstractPointAttributesScalar *firstAttribute = itLAS.next().value();
                                        if (firstAttribute == NULL) {return;}

                                        const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->getPointCloudIndex();
                                        size_t n_points = pointCloudIndexLAS->size();

                                        if (pointCloudIndexLAS == NULL) {return;}

                                        CT_AbstractPointAttributesScalar* att_Return_Number = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Number);
                                        CT_AbstractPointAttributesScalar* att_Number_of_Returns = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Number_of_Returns);
                                        CT_AbstractPointAttributesScalar* att_Classification_Flags = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Classification_Flags);
                                        CT_AbstractPointAttributesScalar* att_Scanner_Channel = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scanner_Channel);
                                        CT_AbstractPointAttributesScalar* att_Scan_Direction_Flag = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Direction_Flag);
                                        CT_AbstractPointAttributesScalar* att_Edge_of_Flight_Line = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line);
                                        CT_AbstractPointAttributesScalar* att_Intensity = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity);
                                        CT_AbstractPointAttributesScalar* att_Classification = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Classification);
                                        CT_AbstractPointAttributesScalar* att_Scan_Angle_Rank = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Angle_Rank);
                                        CT_AbstractPointAttributesScalar* att_User_Data = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::User_Data);
                                        CT_AbstractPointAttributesScalar* att_Point_Source_ID = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Point_Source_ID);
                                        CT_AbstractPointAttributesScalar* att_GPS_Time  = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time);
                                        CT_AbstractPointAttributesScalar* att_Red = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Red);
                                        CT_AbstractPointAttributesScalar* att_Green = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Green);
                                        CT_AbstractPointAttributesScalar* att_Blue = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Blue);
                                        CT_AbstractPointAttributesScalar* att_NIR = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::NIR);
                                        CT_AbstractPointAttributesScalar* att_Wave_Packet_Descriptor_Index = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index);
                                        CT_AbstractPointAttributesScalar* att_Byte_offset_to_waveform_data = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data);
                                        CT_AbstractPointAttributesScalar* att_Waveform_packet_size_in_bytes = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Waveform_packet_size_in_bytes);
                                        CT_AbstractPointAttributesScalar* att_Return_Point_Waveform_Location = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location);

                                        CT_StdLASPointsAttributesContainer *container = new CT_StdLASPointsAttributesContainer(_lasAttributesOut_AutoRenameModel.completeName(), resOut);


                                        CT_AbstractPointAttributesScalar* att_Return_Number_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Number_of_Returns_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Classification_Flags_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Scanner_Channel_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Scan_Direction_Flag_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Edge_of_Flight_Line_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Intensity_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Classification_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Scan_Angle_Rank_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_User_Data_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Point_Source_ID_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_GPS_Time_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Red_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Green_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Blue_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_NIR_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Wave_Packet_Descriptor_Index_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Byte_offset_to_waveform_data_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Waveform_packet_size_in_bytes_H  = NULL;
                                        CT_AbstractPointAttributesScalar* att_Return_Point_Waveform_Location_H  = NULL;

                                        CT_OutAbstractItemModel* model = NULL;

                                        if (att_Return_Number != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasreturnNumberOut_AutoRenameModel.completeName(), resOut);
                                            att_Return_Number_H = (CT_AbstractPointAttributesScalar*) att_Return_Number->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Return_Number_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Number_of_Returns != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasnumberOfReturnsOut_AutoRenameModel.completeName(), resOut);
                                            att_Number_of_Returns_H = (CT_AbstractPointAttributesScalar*) att_Number_of_Returns->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Number_of_Returns_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Classification_Flags != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasclassificationFlagsOut_AutoRenameModel.completeName(), resOut);
                                            att_Classification_Flags_H = (CT_AbstractPointAttributesScalar*) att_Classification_Flags->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Classification_Flags_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Scanner_Channel != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasscannerChannelOut_AutoRenameModel.completeName(), resOut);
                                            att_Scanner_Channel_H = (CT_AbstractPointAttributesScalar*) att_Scanner_Channel->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Scanner_Channel_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Scan_Direction_Flag != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasscanDirectionFlagsOut_AutoRenameModel.completeName(), resOut);
                                            att_Scan_Direction_Flag_H = (CT_AbstractPointAttributesScalar*) att_Scan_Direction_Flag->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Scan_Direction_Flag_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Edge_of_Flight_Line != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasedgeOfFlightLineOut_AutoRenameModel.completeName(), resOut);
                                            att_Edge_of_Flight_Line_H = (CT_AbstractPointAttributesScalar*) att_Edge_of_Flight_Line->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Edge_of_Flight_Line_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Intensity != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasintensityOut_AutoRenameModel.completeName(), resOut);
                                            att_Intensity_H = (CT_AbstractPointAttributesScalar*) att_Intensity->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Intensity_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Classification != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasclassificationOut_AutoRenameModel.completeName(), resOut);
                                            att_Classification_H = (CT_AbstractPointAttributesScalar*) att_Classification->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Classification_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Scan_Angle_Rank != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasscanAngleRankOut_AutoRenameModel.completeName(), resOut);
                                            att_Scan_Angle_Rank_H = (CT_AbstractPointAttributesScalar*) att_Scan_Angle_Rank->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Scan_Angle_Rank_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_User_Data != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasuserDataOut_AutoRenameModel.completeName(), resOut);
                                            att_User_Data_H = (CT_AbstractPointAttributesScalar*) att_User_Data->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_User_Data_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Point_Source_ID != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_laspointSourceIDOut_AutoRenameModel.completeName(), resOut);
                                            att_Point_Source_ID_H = (CT_AbstractPointAttributesScalar*) att_Point_Source_ID->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Point_Source_ID_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_GPS_Time != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasgpsTimeOut_AutoRenameModel.completeName(), resOut);
                                            att_GPS_Time_H  = (CT_AbstractPointAttributesScalar*) att_GPS_Time->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_GPS_Time_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Red != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lascolorROut_AutoRenameModel.completeName(), resOut);
                                            att_Red_H = (CT_AbstractPointAttributesScalar*) att_Red->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Red_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Green != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lascolorGOut_AutoRenameModel.completeName(), resOut);
                                            att_Green_H = (CT_AbstractPointAttributesScalar*) att_Green->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Green_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Blue != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lascolorBOut_AutoRenameModel.completeName(), resOut);
                                            att_Blue_H = (CT_AbstractPointAttributesScalar*) att_Blue->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Blue_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_NIR != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasnirOut_AutoRenameModel.completeName(), resOut);
                                            att_NIR_H = (CT_AbstractPointAttributesScalar*) att_NIR->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_NIR_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Wave_Packet_Descriptor_Index != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_laswavePacketOut_AutoRenameModel.completeName(), resOut);
                                            att_Wave_Packet_Descriptor_Index_H = (CT_AbstractPointAttributesScalar*) att_Wave_Packet_Descriptor_Index->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Wave_Packet_Descriptor_Index_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Byte_offset_to_waveform_data != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasbyteOffsetWaveformOut_AutoRenameModel.completeName(), resOut);
                                            att_Byte_offset_to_waveform_data_H = (CT_AbstractPointAttributesScalar*) att_Byte_offset_to_waveform_data->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Byte_offset_to_waveform_data_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Waveform_packet_size_in_bytes != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_laswaveformPacketSizeOut_AutoRenameModel.completeName(), resOut);
                                            att_Waveform_packet_size_in_bytes_H = (CT_AbstractPointAttributesScalar*) att_Waveform_packet_size_in_bytes->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Waveform_packet_size_in_bytes_H->setPointCloudIndexRegistered(heightCloud);
                                        }

                                        if (att_Return_Point_Waveform_Location != NULL)
                                        {
                                            model = (CT_OutAbstractItemModel*) PS_MODELS->searchModelForCreation(_lasreturnPointWaveformOut_AutoRenameModel.completeName(), resOut);
                                            att_Return_Point_Waveform_Location_H = (CT_AbstractPointAttributesScalar*) att_Return_Point_Waveform_Location->copy(model, resOut, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely);
                                            att_Return_Point_Waveform_Location_H->setPointCloudIndexRegistered(heightCloud);
                                        }


                                        // Put copied data in new container
                                        container->insertPointsAttributesAt(CT_LasDefine::Intensity, att_Intensity_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Return_Number, att_Return_Number_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Number_of_Returns, att_Number_of_Returns_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Classification_Flags, att_Classification_Flags_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Scanner_Channel, att_Scanner_Channel_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Scan_Direction_Flag, att_Scan_Direction_Flag_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line, att_Edge_of_Flight_Line_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Classification, att_Classification_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Scan_Angle_Rank, att_Scan_Angle_Rank_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::User_Data, att_User_Data_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Point_Source_ID, att_Point_Source_ID_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::GPS_Time, att_GPS_Time_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Red, att_Red_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Green, att_Green_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Blue, att_Blue_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::NIR, att_NIR_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index, att_Wave_Packet_Descriptor_Index_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data, att_Byte_offset_to_waveform_data_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Waveform_packet_size_in_bytes, att_Waveform_packet_size_in_bytes_H);
                                        container->insertPointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location, att_Return_Point_Waveform_Location_H);

                                        grp->addItemDrawable(container);
                                        if (att_Return_Number_H != NULL) {grp->addItemDrawable(att_Return_Number_H);}
                                        if (att_Number_of_Returns_H != NULL) {grp->addItemDrawable(att_Number_of_Returns_H);}
                                        if (att_Classification_Flags_H != NULL) {grp->addItemDrawable(att_Classification_Flags_H);}
                                        if (att_Scanner_Channel_H != NULL) {grp->addItemDrawable(att_Scanner_Channel_H);}
                                        if (att_Scan_Direction_Flag_H != NULL) {grp->addItemDrawable(att_Scan_Direction_Flag_H);}
                                        if (att_Edge_of_Flight_Line_H != NULL) {grp->addItemDrawable(att_Edge_of_Flight_Line_H);}
                                        if (att_Intensity_H != NULL) {grp->addItemDrawable(att_Intensity_H);}
                                        if (att_Classification_H != NULL) {grp->addItemDrawable(att_Classification_H);}
                                        if (att_Scan_Angle_Rank_H != NULL) {grp->addItemDrawable(att_Scan_Angle_Rank_H);}
                                        if (att_User_Data_H != NULL) {grp->addItemDrawable(att_User_Data_H);}
                                        if (att_Point_Source_ID_H != NULL) {grp->addItemDrawable(att_Point_Source_ID_H);}
                                        if (att_GPS_Time_H != NULL) {grp->addItemDrawable(att_GPS_Time_H);}
                                        if (att_Red_H != NULL) {grp->addItemDrawable(att_Red_H);}
                                        if (att_Green_H != NULL) {grp->addItemDrawable(att_Green_H);}
                                        if (att_Blue_H != NULL) {grp->addItemDrawable(att_Blue_H);}
                                        if (att_NIR_H != NULL) {grp->addItemDrawable(att_NIR_H);}
                                        if (att_Wave_Packet_Descriptor_Index_H != NULL) {grp->addItemDrawable(att_Wave_Packet_Descriptor_Index_H);}
                                        if (att_Byte_offset_to_waveform_data_H != NULL) {grp->addItemDrawable(att_Byte_offset_to_waveform_data_H);}
                                        if (att_Waveform_packet_size_in_bytes_H != NULL) {grp->addItemDrawable(att_Waveform_packet_size_in_bytes_H);}
                                        if (att_Return_Point_Waveform_Location_H != NULL) {grp->addItemDrawable(att_Return_Point_Waveform_Location_H);}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
