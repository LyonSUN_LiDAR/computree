/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmodifyvoxelsegmentation.h"

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_itemdrawable/ct_attributeslist.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_model/inModel/tools/ct_instdmodelpossibility.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_global/ct_context.h"


#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QtConcurrentMap>
#else
#include <QtConcurrent/QtConcurrentMap>
#endif

#include <QMessageBox>
#include <limits>

// Alias for indexing models
#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInScene  "scene"
#define DEF_SearchInGridPoints  "gridPts"
#define DEF_SearchInGridSeeds   "gridSeeds"
#define DEF_SearchInGridReverseTopology   "gridTopo"


// Constructor : initialization of parameters
ONF_StepModifyVoxelSegmentation::ONF_StepModifyVoxelSegmentation(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _keepValidatedOnly = false;
    m_doc = NULL;

    setManual(true);
}

// Step description (tooltip of contextual menu)
QString ONF_StepModifyVoxelSegmentation::getStepDescription() const
{
    return tr("Modify voxel grid segmentation");
}

// Step detailled description
QString ONF_StepModifyVoxelSegmentation::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString ONF_StepModifyVoxelSegmentation::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepModifyVoxelSegmentation::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepModifyVoxelSegmentation(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepModifyVoxelSegmentation::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grilles"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene à segmenter"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridPoints, CT_Grid3D_Points::staticGetType(), tr("Grille de points"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridSeeds, CT_Grid3D_Sparse<int>::staticGetType(), tr("Grille segmentée"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridReverseTopology, CT_Grid3D_Points::staticGetType(), tr("Grille topologique"));
}


// Creation and affiliation of OUT models
void ONF_StepModifyVoxelSegmentation::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL) {
        res->addItemModel(DEF_SearchInGroup, _outGridModelName, new CT_Grid3D_Sparse<int>(), tr("Grille segmentée corrigée"));
        res->addGroupModel(DEF_SearchInGroup, _outGrpIdModelName, new CT_StandardItemGroup(), tr("IDs"));
        res->addItemModel(_outGrpIdModelName, _outIdItemModelName, new CT_AttributesList(), tr("Labels"));
        res->addItemAttributeModel(_outIdItemModelName, _outattClusterModelName, new CT_StdItemAttributeT<qint32>(CT_AbstractCategory::DATA_ID), tr("VoxelCluster"));
        res->addItemAttributeModel(_outIdItemModelName, _outattLabelModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("Label"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepModifyVoxelSegmentation::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addBool(tr("Ne conserver que les arbres validés ?"), "", "", _keepValidatedOnly);
}

void ONF_StepModifyVoxelSegmentation::compute()
{
    _positionLabels.clear();
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = NULL;
    // Création de la liste des positions 2D
    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    if (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        _scene = (CT_AbstractItemDrawableWithPointCloud*)grp->firstItemByINModelName(this, DEF_SearchInScene);
        _pointGrid = (CT_Grid3D_Points*)grp->firstItemByINModelName(this, DEF_SearchInGridPoints);
        CT_Grid3D_Sparse<int>* seedGrid = (CT_Grid3D_Sparse<int>*)grp->firstItemByINModelName(this, DEF_SearchInGridSeeds);
        _topologyGrid = (CT_Grid3D_Points*)grp->firstItemByINModelName(this, DEF_SearchInGridReverseTopology);

        if (_scene != NULL && _pointGrid != NULL && seedGrid != NULL && _topologyGrid != NULL)
        {
            _outSegmentationGrid = new CT_Grid3D_Sparse<int>(_outGridModelName.completeName(), res_out, seedGrid->minX(), seedGrid->minY(), seedGrid->minZ(), seedGrid->xdim(), seedGrid->ydim(), seedGrid->zdim(), seedGrid->resolution(), -1, -1);

            QList<size_t> list;
            seedGrid->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);
                _outSegmentationGrid->setValueAtIndex(index, seedGrid->valueAtIndex(index));
            }
        }
        _outSegmentationGrid->computeMinMax();
    }

    if (grp != NULL)
    {

        // Début de la partie interactive
        m_doc = NULL;

        m_status = 0;
        requestManualMode();

        m_status = 1;
        requestManualMode();


        if (_keepValidatedOnly)
        {
            QList<size_t> list;
            _pointGrid->getIndicesWithPoints(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t cellIndex = list.at(i);
                int clusterIndex = _outSegmentationGrid->valueAtIndex(cellIndex);
                if (!_validated[clusterIndex])
                {
                    _outSegmentationGrid->setValueAtIndex(cellIndex, -1);
                }
            }
        }

        QMapIterator<int, QString>itLab(_positionLabels);
        while (itLab.hasNext())
        {
            itLab.next();
            int cluster = itLab.key();
            QString label = itLab.value();

            if (!_keepValidatedOnly || _validated[cluster])
            {
                CT_StandardItemGroup* itemGrp = new CT_StandardItemGroup(_outGrpIdModelName.completeName(), res_out);
                CT_AttributesList* attList = new CT_AttributesList(_outIdItemModelName.completeName(), res_out);
                attList->addItemAttribute(new CT_StdItemAttributeT<qint32>(_outattClusterModelName.completeName(), CT_AbstractCategory::DATA_ID, res_out, cluster));
                attList->addItemAttribute(new CT_StdItemAttributeT<QString>(_outattLabelModelName.completeName(), CT_AbstractCategory::DATA_VALUE, res_out, label));

                itemGrp->addItemDrawable(attList);
                grp->addGroup(itemGrp);
            }
        }

        // Fin de la partie interactive


        // create output segmented scenes
        _outSegmentationGrid->computeMinMax();
        grp->addItemDrawable(_outSegmentationGrid);
    }

}


void ONF_StepModifyVoxelSegmentation::initManualMode()
{
    // create a new 3D document
    if(m_doc == NULL)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_doc->removeAllItemDrawable();

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionModifyVoxelSegmentation(_scene, _pointGrid, _topologyGrid, _outSegmentationGrid, &_validated, &_positionLabels));

    QMessageBox::information(NULL, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de correction de segmentation."), QMessageBox::Ok);
}

void ONF_StepModifyVoxelSegmentation::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = NULL;

            quitManualMode();
        }
    }
}
