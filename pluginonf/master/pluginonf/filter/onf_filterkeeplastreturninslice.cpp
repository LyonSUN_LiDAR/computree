/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "filter/onf_filterkeeplastreturninslice.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_genericconfigurablewidget.h"

#include <QDebug>

#define checkAndSetValue(ATT, NAME, TYPE) if((value = settings->firstValueByTagName(NAME)) == NULL) { return false; } else { ATT = value->value().value<TYPE>(); }

ONF_FilterKeepLastReturnInSlice::ONF_FilterKeepLastReturnInSlice() : CT_AbstractFilter_LAS()
{
    _minZ = 1.0;
    _maxZ = 8.0;
}

ONF_FilterKeepLastReturnInSlice::ONF_FilterKeepLastReturnInSlice(const ONF_FilterKeepLastReturnInSlice &other) : CT_AbstractFilter_LAS(other)
{
    _minZ = other._minZ;
    _maxZ = other._maxZ;
}

QString ONF_FilterKeepLastReturnInSlice::getDetailledDisplayableName()
{
    return QString("Filtered");
}

CT_AbstractConfigurableWidget *ONF_FilterKeepLastReturnInSlice::createConfigurationWidget()
{
    CT_GenericConfigurableWidget* configDialog = new CT_GenericConfigurableWidget();
    configDialog->addDouble(tr("Z minimum"), "m", -1e+10, 1e+10, 2, _minZ);
    configDialog->addDouble(tr("Z maximum"), "m", -1e+10, 1e+10, 2, _maxZ);

    return configDialog;
}

SettingsNodeGroup *ONF_FilterKeepLastReturnInSlice::getAllSettings() const
{
    SettingsNodeGroup *root = new SettingsNodeGroup("ONF_FilterKeepLastReturnInSlice");

    root->addValue(new SettingsNodeValue("minZ", _minZ));
    root->addValue(new SettingsNodeValue("maxZ", _maxZ));

    return root;
}

bool ONF_FilterKeepLastReturnInSlice::setAllSettings(const SettingsNodeGroup *settings)
{
    if((settings == NULL) || (settings->name() != "ONF_FilterKeepLastReturnInSlice"))
        return false;

    SettingsNodeValue *value = NULL;

    checkAndSetValue(_minZ, "minZ", double);
    checkAndSetValue(_maxZ, "maxZ", double);

    return true;
}

bool ONF_FilterKeepLastReturnInSlice::filterPointCloudIndex()
{
    if(inputPointCloudIndex() == NULL)
        return false;

    CT_PointCloudIndexVector *outCloud = outputPointCloudIndex();

    QList<CandidatePoint*> candidatePoints;

    CT_LASData lasData;
    size_t maxAttSize = lasAttributes()->pointsAttributesAt(CT_LasDefine::GPS_Time)->getPointCloudIndex()->size();

    CT_PointIterator itP(inputPointCloudIndex());

    while(itP.hasNext())
    {
        const CT_Point &point = itP.next().currentPoint();

        if (point(2) > _minZ)
        {
            size_t globalIndex = itP.currentGlobalIndex();
            size_t lasIndex = lasPointCloudIndex()->indexOf(globalIndex);

            if (lasIndex < maxAttSize)
            {
                lasAttributes()->getLASDataAt(lasIndex, lasData);
            }

            candidatePoints.append(new CandidatePoint(globalIndex, point(2), lasData._GPS_Time));
        }

    }

    std::sort(candidatePoints.begin(), candidatePoints.end(), sortByTimeAndZ);

    if (candidatePoints.size() > 0 && candidatePoints.first()->_z < _maxZ)
    {
        outCloud->addIndex(candidatePoints.first()->_globalIndex);
    }

    for (int i = 1 ; i < candidatePoints.size() ; i++)
    {
        CandidatePoint* cp0 = candidatePoints.at(i-1);
        CandidatePoint* cp1 = candidatePoints.at(i);

//        if (cp1->_gpsTime == cp0->_gpsTime) {qDebug() << "ok";}
//        qDebug() << "GPS=" << QString::number(cp0->_gpsTime, 'f', 10) << " z=" << cp0->_z;

        if (cp1->_gpsTime != cp0->_gpsTime)
        {
            if (cp1->_z < _maxZ)
            {
                outCloud->addIndex(cp1->_globalIndex);
            }
        }
    }

    qDeleteAll(candidatePoints);
    return true;
}


QString ONF_FilterKeepLastReturnInSlice::getShortDescription() const
{
    return tr("Garde de dernier retour de chaque rayon dans la tranche");
}

CT_AbstractConfigurableElement *ONF_FilterKeepLastReturnInSlice::copy() const
{
    return new ONF_FilterKeepLastReturnInSlice(*this);
}


