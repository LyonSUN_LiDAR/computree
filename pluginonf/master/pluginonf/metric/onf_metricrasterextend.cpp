/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/onf_metricrasterextend.h"
#include <QDebug>


ONF_MetricRasterExtend::ONF_MetricRasterExtend() : CT_AbstractMetric_Raster()
{
    declareAttributes();
}

ONF_MetricRasterExtend::ONF_MetricRasterExtend(const ONF_MetricRasterExtend &other) : CT_AbstractMetric_Raster(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
}

QString ONF_MetricRasterExtend::getShortDescription() const
{
    return tr("Extention du raster");
}

QString ONF_MetricRasterExtend::getDetailledDescription() const
{
    return tr("Extention du raster");
}

ONF_MetricRasterExtend::Config ONF_MetricRasterExtend::metricConfiguration() const
{
    return m_configAndResults;
}

void ONF_MetricRasterExtend::setMetricConfiguration(const ONF_MetricRasterExtend::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *ONF_MetricRasterExtend::copy() const
{
    return new ONF_MetricRasterExtend(*this);
}

void ONF_MetricRasterExtend::computeMetric()
{    
    m_configAndResults.xmin.value = _inRaster->minX();
    m_configAndResults.xmax.value = _inRaster->maxX();
    m_configAndResults.xsize.value = m_configAndResults.xmax.value - m_configAndResults.xmin.value;
    m_configAndResults.ymin.value = _inRaster->minY();
    m_configAndResults.ymax.value = _inRaster->maxY();
    m_configAndResults.ysize.value = m_configAndResults.ymax.value - m_configAndResults.ymin.value;
    m_configAndResults.min.value = _inRaster->minValueAsDouble();
    m_configAndResults.max.value = _inRaster->maxValueAsDouble();
    m_configAndResults.na.value = _inRaster->NAAsDouble();

    setAttributeValueVaB(m_configAndResults.xmin);
    setAttributeValueVaB(m_configAndResults.xmax);
    setAttributeValueVaB(m_configAndResults.xsize);
    setAttributeValueVaB(m_configAndResults.ymin);
    setAttributeValueVaB(m_configAndResults.ymax);
    setAttributeValueVaB(m_configAndResults.ysize);
    setAttributeValueVaB(m_configAndResults.min);
    setAttributeValueVaB(m_configAndResults.max);
    setAttributeValueVaB(m_configAndResults.na);
}

void ONF_MetricRasterExtend::declareAttributes()
{
    registerAttributeVaB(m_configAndResults.xmin, CT_AbstractCategory::DATA_NUMBER, tr("xmin"));
    registerAttributeVaB(m_configAndResults.xmax, CT_AbstractCategory::DATA_NUMBER, tr("xmax"));
    registerAttributeVaB(m_configAndResults.xsize, CT_AbstractCategory::DATA_NUMBER, tr("xsize"));
    registerAttributeVaB(m_configAndResults.ymin, CT_AbstractCategory::DATA_NUMBER, tr("ymin"));
    registerAttributeVaB(m_configAndResults.ymax, CT_AbstractCategory::DATA_NUMBER, tr("ymax"));
    registerAttributeVaB(m_configAndResults.ysize, CT_AbstractCategory::DATA_NUMBER, tr("ysize"));
    registerAttributeVaB(m_configAndResults.min, CT_AbstractCategory::DATA_NUMBER, tr("min"));
    registerAttributeVaB(m_configAndResults.max, CT_AbstractCategory::DATA_NUMBER, tr("max"));
    registerAttributeVaB(m_configAndResults.na, CT_AbstractCategory::DATA_NUMBER, tr("na"));
}

