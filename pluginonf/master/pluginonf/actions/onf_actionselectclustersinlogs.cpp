#include "onf_actionselectclustersinlogs.h"

#include <QMouseEvent>
#include <QKeyEvent>
#include <QIcon>
#include <QPainter>
#include <math.h>

#include <QDebug>

#include "views/actions/onf_actionselectclustersinlogsoptions.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawable.h"

ONF_ActionSelectClustersInLogs::ONF_ActionSelectClustersInLogs(QHash<CT_AbstractItemDrawable *, quint64> *clusterToLog, QMultiHash<quint64, CT_AbstractItemDrawable *> *logToCluster, QList<CT_AbstractItemDrawable *> *validatedItems ) : CT_AbstractActionForGraphicsView()
{
    m_status = 0;
    m_selectionMode = GraphicsViewInterface::SELECT_ONE;
    m_selectionTool = Point;
    m_keyModifiers = Qt::NoModifier;

    m_clusterToLog = clusterToLog;
    m_logToCluster = logToCluster;
    m_validatedItems = validatedItems;

    connect(&m_rectangleTools, SIGNAL(mustBeRedraw()), this, SLOT(redrawOverlay()), Qt::DirectConnection);
    connect(&m_polygonTools, SIGNAL(mustBeRedraw()), this, SLOT(redrawOverlay()), Qt::DirectConnection);

    m_validationColor = QColor(0,0,255);
    m_whiteColor = QColor(255,255,255);
}

QString ONF_ActionSelectClustersInLogs::uniqueName() const
{
    return "ONF_ActionSelectClustersInLogs";
}

QString ONF_ActionSelectClustersInLogs::title() const
{
    return tr("Sélection");
}

QString ONF_ActionSelectClustersInLogs::description() const
{
    return tr("Sélection d'éléments");
}

QIcon ONF_ActionSelectClustersInLogs::icon() const
{
    return QIcon(":/icons/cursor.png");
}

QString ONF_ActionSelectClustersInLogs::type() const
{
    return CT_AbstractAction::TYPE_SELECTION;
}

void ONF_ActionSelectClustersInLogs::init()
{
    CT_AbstractActionForGraphicsView::init();

    if(nOptions() == 0)
    {
        // create the option widget if it was not already created
        ONF_ActionSelectClustersInLogsOptions *option = new ONF_ActionSelectClustersInLogsOptions(this);


        // add the options to the graphics view
        graphicsView()->addActionOptions(option);


        connect(option, SIGNAL(validate()), this, SLOT(validate()));
        connect(option, SIGNAL(unvalidate()), this, SLOT(unvalidate()));

        // register the option to the superclass, so the hideOptions and showOptions
        // is managed automatically
        registerOption(option);


    }
}

bool ONF_ActionSelectClustersInLogs::mousePressEvent(QMouseEvent *e)
{
    if((m_status == 0) && (e->button() != Qt::LeftButton))
        return false;

    bool ok = false;

    if(selectionTool() == Polygon)
        ok = m_polygonTools.mousePressEvent(e);
    else if(selectionTool() == Rectangle)
        ok = m_rectangleTools.mousePressEvent(e);
    else if(e->button() == Qt::LeftButton) {
        m_lastMousePos = e->pos();
        m_status = 1;
        return false;
    }

    m_status = ok ? 1 : 0;

    return ok;
}

bool ONF_ActionSelectClustersInLogs::mouseMoveEvent(QMouseEvent *e)
{
    if(m_status > 0)
    {
        if(selectionTool() == Polygon)
            return m_polygonTools.mouseMoveEvent(e);
        else if(selectionTool() == Rectangle)
            return m_rectangleTools.mouseMoveEvent(e);
        else {
            m_status = 0;
            return false;
        }

        return true;
    }

    return false;
}

bool ONF_ActionSelectClustersInLogs::mouseReleaseEvent(QMouseEvent *e)
{
    if(m_status > 0)
    {
        if(selectionTool() == Polygon)
            return m_polygonTools.mouseReleaseEvent(e);
        else if(selectionTool() == Rectangle)
            return m_rectangleTools.mouseReleaseEvent(e);
        else {
            pick();
            return false;
        }
        return true;
    }

    return false;
}

bool ONF_ActionSelectClustersInLogs::wheelEvent(QWheelEvent *e)
{
    if(m_status > 0)
    {
        if(selectionTool() == Polygon)
            return m_polygonTools.wheelEvent(e);
        else if(selectionTool() == Rectangle)
            return m_rectangleTools.wheelEvent(e);

        return false;
    }

    return false;
}

bool ONF_ActionSelectClustersInLogs::keyPressEvent(QKeyEvent *e)
{
    setKeyModifiers(e->modifiers());

    if(m_status > 0) {
        if(selectionTool() == Polygon)
            return m_polygonTools.keyPressEvent(e);
        else if(selectionTool() == Rectangle)
            return m_rectangleTools.keyPressEvent(e);
    } else {
        if(e->key() == Qt::Key_A) {
            validate();
            return true;
        } else if(e->key() == Qt::Key_Z) {
            unvalidate();
            return true;
        } else if(e->key() == Qt::Key_E) {
            ONF_ActionSelectClustersInLogsOptions *option = (ONF_ActionSelectClustersInLogsOptions*)optionAt(0);
            option->toggleClusterSelection();
            return true;
        }
    }

    return false;
}

bool ONF_ActionSelectClustersInLogs::keyReleaseEvent(QKeyEvent *e)
{
    setKeyModifiers(e->modifiers());

    if(m_status > 0) {

        if(selectionTool() == Polygon) {

            if(e->key() == Qt::Key_Delete) {

                m_polygonTools.removeLastPoint();
                return true;

            } else if((e->key() == Qt::Key_Enter)
                      || (e->key() == Qt::Key_Return)) {

                if(!m_polygonTools.isPolygonClosed()) {
                    m_polygonTools.closePolygon();
                    return true;
                }

            }
        }

        if((selectionTool() == Rectangle)
                || (selectionTool() == Polygon)) {

            if(e->key() == Qt::Key_Escape) {
                m_polygonTools.clear();
                m_rectangleTools.clear();
                m_status = 0;
                return true;
            } else if((e->key() == Qt::Key_Enter)
                    || (e->key() == Qt::Key_Return)) {

                pick();
                return true;
            }
        }

    }

    return false;
}

void ONF_ActionSelectClustersInLogs::drawOverlay(GraphicsViewInterface &view, QPainter &painter)
{
    Q_UNUSED(view)

    if(m_status > 0)
    {
        if(selectionTool() == Polygon)
            return m_polygonTools.drawOverlay(painter);
        else if(selectionTool() == Rectangle)
            return m_rectangleTools.drawOverlay(painter);

    }
}

CT_AbstractAction* ONF_ActionSelectClustersInLogs::copy() const
{
    return new ONF_ActionSelectClustersInLogs(m_clusterToLog, m_logToCluster, m_validatedItems);
}

bool ONF_ActionSelectClustersInLogs::setSelectionMode(GraphicsViewInterface::SelectionMode mode)
{
    if(m_selectionMode != mode) {
        m_selectionMode = mode;

        emit selectionModeChanged(selectionMode());
    }

    return true;
}

bool ONF_ActionSelectClustersInLogs::setSelectionTool(ONF_ActionSelectClustersInLogs::SelectionTool tool)
{
    m_polygonTools.clear();
    m_rectangleTools.clear();

    m_selectionTool = tool;
    return true;
}

void ONF_ActionSelectClustersInLogs::toggleSelection()
{
    graphicsView()->setSelectionMode(selectionMode());

    if(graphicsView()->mustSelectPoints())
        graphicsView()->togglePointsSelected();
    else if(graphicsView()->mustSelectEdges())
        graphicsView()->toggleEdgesSelected();
    else if(graphicsView()->mustSelectFaces())
        graphicsView()->toggleFacesSelected();
    else if(graphicsView()->mustSelectItems())
        graphicsView()->toggleItemsSelected();
}

GraphicsViewInterface::SelectionMode ONF_ActionSelectClustersInLogs::selectionMode() const
{
    GraphicsViewInterface::SelectionMode basic = selectionModeToBasic(m_selectionMode);

    if(m_keyModifiers.testFlag(Qt::ShiftModifier)) {

        if((basic == GraphicsViewInterface::SELECT) || (basic == GraphicsViewInterface::SELECT_ONE))
           return GraphicsViewInterface::SelectionMode(m_selectionMode + 1); // SELECT -> ADD
        else if((basic == GraphicsViewInterface::REMOVE) || (basic == GraphicsViewInterface::REMOVE_ONE))
            return GraphicsViewInterface::SelectionMode(m_selectionMode - 1); // REMOVE -> ADD

    } else if(m_keyModifiers.testFlag(Qt::ControlModifier)) {
        if((basic == GraphicsViewInterface::SELECT) || (basic == GraphicsViewInterface::SELECT_ONE))
           return GraphicsViewInterface::SelectionMode(m_selectionMode + 2); // SELECT -> REMOVE
        else if((basic == GraphicsViewInterface::ADD) || (basic == GraphicsViewInterface::ADD_ONE))
            return GraphicsViewInterface::SelectionMode(m_selectionMode + 1); // ADD -> REMOVE
    }

    return m_selectionMode;
}

ONF_ActionSelectClustersInLogs::SelectionTool ONF_ActionSelectClustersInLogs::selectionTool() const
{
    return m_selectionTool;
}


GraphicsViewInterface::SelectionMode ONF_ActionSelectClustersInLogs::selectionModeToBasic(GraphicsViewInterface::SelectionMode mode) const
{
    int m = mode;

    while(m > GraphicsViewInterface::REMOVE_ONE)
        m -= GraphicsViewInterface::REMOVE_ONE;

    return (GraphicsViewInterface::SelectionMode)m;
}

void ONF_ActionSelectClustersInLogs::setKeyModifiers(Qt::KeyboardModifiers m)
{
    if(m_keyModifiers != m) {
        m_keyModifiers = m;
        emit selectionModeChanged(selectionMode());
    }
}

void ONF_ActionSelectClustersInLogs::pick()
{    
    ONF_ActionSelectClustersInLogsOptions *option = (ONF_ActionSelectClustersInLogsOptions*)optionAt(0);

    QPolygon final;

    if(selectionTool() == Rectangle)
        final = m_rectangleTools.getPolygon();
    else if(selectionTool() == Polygon)
        final = m_polygonTools.getPolygon();
    else {
        QRect r(0, 0, 3, 3);
        r.moveCenter(m_lastMousePos);
        final = r;
    }

    m_polygonTools.clear();
    m_rectangleTools.clear();

    graphicsView()->setSelectionMode(selectionMode());
    graphicsView()->select(final);

    if (!option->clusterSelection())
    {
        QList<CT_AbstractItemDrawable*> selectedItems = graphicsView()->getSelectedItems();

        QList<quint64> logs;
        for (int i = 0 ; i < selectedItems.size() ; i++)
        {
            CT_AbstractItemDrawable* item = selectedItems.at(i);

            quint64 logID = m_clusterToLog->value(item);
            if (!logs.contains(logID)) {logs.append(logID);}
        }

        for (int i = 0 ; i < logs.size() ; i++)
        {
            QList<CT_AbstractItemDrawable*> items = m_logToCluster->values(logs.at(i));

            for (int j = 0 ; j < items.size() ; j++)
            {
                items.at(j)->setSelected(true);
            }
        }
    }

    option->setSelectionTool(ONF_ActionSelectClustersInLogs::Point);
    setSelectionTool(ONF_ActionSelectClustersInLogs::Point);

    m_status = 0;
}

void ONF_ActionSelectClustersInLogs::redrawOverlay()
{
    document()->redrawGraphics();
}

void ONF_ActionSelectClustersInLogs::validate()
{
    QList<CT_AbstractItemDrawable*> selectedItems = graphicsView()->getSelectedItems();

    for (int i = 0 ; i < selectedItems.size() ; i++)
    {
        CT_AbstractItemDrawable* item = selectedItems.at(i);
        if (!(*m_validatedItems).contains(item))
        {
            (*m_validatedItems).append(item);
            document()->setColor(item, m_validationColor);
        }
    }
    document()->setSelectAllItemDrawable(false);
    document()->redrawGraphics();
}

void ONF_ActionSelectClustersInLogs::unvalidate()
{
    QList<CT_AbstractItemDrawable*> selectedItems = graphicsView()->getSelectedItems();

    for (int i = 0 ; i < selectedItems.size() ; i++)
    {
        CT_AbstractItemDrawable* item = selectedItems.at(i);
        if ((*m_validatedItems).contains(item))
        {
            (*m_validatedItems).removeOne(item);
            document()->setColor(item, m_whiteColor);
        }
    }
    document()->setSelectAllItemDrawable(false);
    document()->redrawGraphics();
}
