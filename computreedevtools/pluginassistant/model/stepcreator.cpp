#include "stepcreator.h"

#include <QDir>
#include <QFile>
#include <QTextStream>

#include "model/step/tools.h"

StepCreator::StepCreator(QString directory) : QObject()
{
    _directory = directory;
    _name = "";
    _pluginCode = "";
    _stepBaseName = "";
    _canBeAddedFirst = false;
    _shortDescription = "No short description for this step";
    _detailledDescription = "No detailled description for this step";

    _inModelCreator = new InModelCreator();
    _outModelCreator = new OutModelCreator();
    _copyModelCreator = new CopyModelCreator(_inModelCreator->getStandardItemModel());
    _parametersModelCreator = new ParametersCreator();
}

StepCreator::~StepCreator()
{
    delete _inModelCreator;
    delete _outModelCreator;
    delete _copyModelCreator;
    delete _parametersModelCreator;
}

QString StepCreator::getStepName()
{
    return _name;
}

QString StepCreator::createStepFiles()
{
    QString errors = "";

    if (_pluginCode == "") {return tr("Pas de code de plugin spécifié !");}
    if (_stepBaseName == "") {return tr("Pas de nom d'étape spécifié !");}

    _name = QString("%1_Step%2").arg(_pluginCode).arg(_stepBaseName);

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas, veuillez en choisir un autre svp.");
    }

    dir.mkdir("step");

    if (!create_StepFile_h()) {errors.append(tr("Impossible de créer le fichier étape (.h)\n"));}
    if (!create_StepFile_cpp()) {errors.append(tr("Impossible de créer le fichier étape (.cpp)\n"));}     

    return errors;
}

bool StepCreator::create_StepFile_h()
{
    QFile file(QString("%1/step/%2.h").arg(_directory).arg(_name.toLower()));

    QString parentClass = "CT_AbstractStep";
    if (_canBeAddedFirst) {parentClass = "CT_AbstractStepCanBeAddedFirst";}

    QString autoRenamesDeclarations = _copyModelCreator->getAutoRenamesDeclarations();

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream.setCodec("UTF-8");

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";
        stream << "#include \"ct_step/abstract/" << parentClass.toLower() << ".h\"\n";
        stream << "\n";

        if (autoRenamesDeclarations.size() > 0)
        {
            stream << "// Inclusion of auto-indexation system\n";
            stream << "#include \"ct_tools/model/ct_autorenamemodels.h\"\n";
            stream << "\n";
        }

        stream << "/*!\n";
        stream << " * \\class " << _name << "\n";
        stream << " * \\ingroup Steps_" << _pluginCode << "\n";
        stream << " * \\brief <b>" + _shortDescription + ".</b>\n";
        stream << " *\n";
        stream << " * " + _detailledDescription + "\n";
        stream << " *\n";
        stream << _parametersModelCreator->getParamatersDoc();
        stream << " *\n";
        stream << " */\n";
        stream << "\n";
        stream << "class " << _name << ": public " << parentClass << "\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "\n";
        stream << "public:\n";
        stream << "\n";
        stream << "    /*! \\brief Step constructor\n";
        stream << "     * \n";
        stream << "     * Create a new instance of the step\n";
        stream << "     * \n";
        stream << "     * \\param dataInit Step parameters object\n";
        stream << "     */\n";
        stream << "    " << _name << "(CT_StepInitializeData &dataInit);\n";
        stream << "\n";
        stream << "    /*! \\brief Step description\n";
        stream << "     * \n";
        stream << "     * Return a description of the step function\n";
        stream << "     */\n";
        stream << "    QString getStepDescription() const;\n";
        stream << "\n";
        stream << "    /*! \\brief Step detailled description\n";
        stream << "     * \n";
        stream << "     * Return a detailled description of the step function\n";
        stream << "     */\n";
        stream << "    QString getStepDetailledDescription() const;\n";
        stream << "\n";
        stream << "    /*! \\brief Step URL\n";
        stream << "     * \n";
        stream << "     * Return a URL of a wiki for this step\n";
        stream << "     */\n";
        stream << "    QString getStepURL() const;\n";
        stream << "\n";
        stream << "    /*! \\brief Step copy\n";
        stream << "     * \n";
        stream << "     * Step copy, used when a step is added by step contextual menu\n";
        stream << "     */\n";
        stream << "    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);\n";
        stream << "\n";
        stream << "protected:\n";
        stream << "\n";
        stream << "    /*! \\brief Input results specification\n";
        stream << "     * \n";
        stream << "     * Specification of input results models needed by the step (IN)\n";
        stream << "     */\n";
        stream << "    void createInResultModelListProtected();\n";
        stream << "\n";
        stream << "    /*! \\brief Parameters DialogBox\n";
        stream << "     * \n";
        stream << "     * DialogBox asking for step parameters\n";
        stream << "     */\n";
        stream << "    void createPostConfigurationDialog();\n";
        stream << "\n";
        stream << "    /*! \\brief Output results specification\n";
        stream << "     * \n";
        stream << "     * Specification of output results models created by the step (OUT)\n";
        stream << "     */\n";
        stream << "    void createOutResultModelListProtected();\n";
        stream << "\n";
        stream << "    /*! \\brief Algorithm of the step\n";
        stream << "     * \n";
        stream << "     * Step computation, using input results, and creating output results\n";
        stream << "     */\n";
        stream << "    void compute();\n";
        stream << "\n";
        stream << "private:\n";

        if (autoRenamesDeclarations.size() > 0)
        {
            stream << "\n";
            stream << Tools::getIndentation(1) << "// Declaration of autoRenames Variables (groups or items added to In models copies)\n";
            stream << autoRenamesDeclarations;
        }

        stream << "\n";

        stream << "    // Step parameters\n";
        QString str = _parametersModelCreator->getParametersDeclaration();
        if (str.size() > 0)
        {
            stream << str;
        } else {
            stream << "    // No parameter for this step";
        }

        stream << "\n";
        stream << "};\n";
        stream << "\n";
        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool StepCreator::create_StepFile_cpp()
{
    QFile file(QString("%1/step/%2.cpp").arg(_directory).arg(_name.toLower()));

    QString includes = getIncludes();

    QString defines = _inModelCreator->getInDefines() + _outModelCreator->getOutDefines() + _copyModelCreator->getCopyDefines();
    QString inModels = _inModelCreator->getCreateInResultModelListProtectedContent();
    QString copyModels = _copyModelCreator->getCreateOutResultModelListProtectedContent();
    QString outModels = _outModelCreator->getCreateOutResultModelListProtectedContent();
    QString paramDialog = _parametersModelCreator->getParametersDialogCommands();

    if (paramDialog.size() > 0)
    {
        includes.append("#include \"ct_view/ct_stepconfigurabledialog.h\"\n");
    }

    QString inResultRecovery = _inModelCreator->getResultRecovery();
    int nextRank = 0;
    QString copyResultRecovery = _copyModelCreator->getResultRecovery(nextRank);
    QString outResultRecovery = _outModelCreator->getResultRecovery(nextRank);

    QString parentClass = "CT_AbstractStep";
    if (_canBeAddedFirst) {parentClass = "CT_AbstractStepCanBeAddedFirst";}

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream.setCodec("UTF-8");

        stream << "#include \"" << _name.toLower() << ".h\"\n";
        stream << "\n";

        if (includes.size() > 0)
        {
            stream << includes;
        }
        stream << "\n";


        if (defines.size() > 0)
        {
            stream << "// Alias for indexing models\n";
            stream << defines;
            stream << "\n";
        }

        stream << "// Constructor : initialization of parameters\n";
        stream << _name << "::" << _name << "(CT_StepInitializeData &dataInit) : " << parentClass << "(dataInit)\n";
        stream << "{\n";
        stream << _parametersModelCreator->getParametersInitialization();
        stream << "}\n";
        stream << "\n";
        stream << "// Step description (tooltip of contextual menu)\n";
        stream << "QString " << _name << "::getStepDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"" << _shortDescription << "\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "// Step detailled description\n";
        stream << "QString " << _name << "::getStepDetailledDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"" << detailledDescriptionForMethod() << "\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "// Step URL\n";
        stream << "QString " << _name << "::getStepURL() const\n";
        stream << "{\n";
        stream << "    //return tr(\"STEP URL HERE\");\n";
        stream << "    return CT_AbstractStep::getStepURL(); //by default URL of the plugin\n";
        stream << "}\n";
        stream << "\n";
        stream << "// Step copy method\n";
        stream << "CT_VirtualAbstractStep* " << _name << "::createNewInstance(CT_StepInitializeData &dataInit)\n";
        stream << "{\n";
        stream << "    return new " << _name << "(dataInit);\n";
        stream << "}\n";
        stream << "\n";
        stream << "//////////////////// PROTECTED METHODS //////////////////\n";
        stream << "\n";
        stream << "// Creation and affiliation of IN models\n";
        stream << "void " << _name << "::createInResultModelListProtected()\n";
        stream << "{\n";

        if (inModels.size() > 0)
        {
            stream << inModels;
        } else {
            stream << "    // No in result is needed\n";
            stream << "    setNotNeedInputResult();\n";
        }

        stream << "}\n";
        stream << "\n";
        stream << "// Creation and affiliation of OUT models\n";
        stream << "void " << _name << "::createOutResultModelListProtected()\n";
        stream << "{\n";

        if (outModels.size() > 0 || copyModels.size() > 0)
        {
            stream << copyModels;
            if (outModels.size() > 0 && copyModels.size() > 0)
            {
                stream << "\n";
                stream << "\n";
            }
            stream << outModels;
        } else {
            stream << "    // No OUT model definition => create an empty result\n";
            stream << "    createNewOutResultModel(\"emptyResult\");\n";
        }

        stream << "}\n";
        stream << "\n";
        stream << "// Semi-automatic creation of step parameters DialogBox\n";
        stream << "void " << _name << "::createPostConfigurationDialog()\n";
        stream << "{\n";

        if (paramDialog.size() > 0)
        {
            stream << "    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();\n";
            stream << "\n";
            stream << paramDialog;
        } else {
            stream << "    // No parameter dialog for this step\n";
        }

        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::compute()\n";
        stream << "{\n";
        stream << "    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!\n";
        stream << "\n";

        if (inResultRecovery.size() > 0)
        {
            stream << inResultRecovery;
            stream << "\n";
        }

        if (copyResultRecovery.size() > 0)
        {
            stream << copyResultRecovery;
            stream << "\n";
        }

        if (outResultRecovery.size() > 0)
        {
            stream << outResultRecovery;
            stream << "\n";
        }

        stream << "    // IN results browsing\n";
        stream << _inModelCreator->getComputeContent();
        stream << "\n";
        stream << "\n";

        stream << "    // COPIED results browsing\n";
        stream << _copyModelCreator->getComputeContent();
        stream << "\n";
        stream << "\n";        

        stream << "    // OUT results creation (move it to the appropried place in the code)\n";
        stream << _outModelCreator->getComputeContent();
        stream << "\n";
        stream << "\n";
        stream << "}\n";


        file.close();
        return true;
    }
    return false;
}


QString StepCreator::getIncludes()
{
    QSet<QString> list;
    _inModelCreator->getIncludes(list);
    _outModelCreator->getIncludes(list);
    _copyModelCreator->getIncludes(list);

    return Tools::getQStringListConcat(list);
}

QString StepCreator::detailledDescriptionForMethod()
{
    QString result = _detailledDescription;
    result.replace("\n", "<br>\"\n\"");
    return result;
}

