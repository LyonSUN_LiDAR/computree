#ifndef READERCREATOR_H
#define READERCREATOR_H

#include <QObject>

class ReaderCreator : public QObject
{
    Q_OBJECT
public:
    explicit ReaderCreator(QString directory, QString name);

    QString createReaderFiles();

private:

    QString     _directory;
    QString     _name;


    bool create_ReaderFile_h();
    bool create_ReaderFile_cpp();
};

#endif // READERCREATOR_H
