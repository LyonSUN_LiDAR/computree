#include "copygroupmodel.h"
#include "view/step/models/copygroupwidget.h"
#include "model/step/models/copyitemmodel.h"
#include "model/step/tools.h"
#include "assert.h"

COPYGroupModel::COPYGroupModel() : AbstractCopyModel()
{
    _widget = new COPYGroupWidget(this);
    _status = AbstractCopyModel::S_Added;
    setData(QVariant(QColor(Qt::blue)),Qt::ForegroundRole);
    setText(getPrefixedAlias());
}

void COPYGroupModel::init(QString alias, QString name, QString desc)
{
    ((COPYGroupWidget*)_widget)->init(alias, name, desc);
    _status = AbstractCopyModel::S_Copy;
    setData(QVariant(QColor(Qt::black)),Qt::ForegroundRole);
    setText(getAlias() + " (copie)");
}

void COPYGroupModel::init(INGroupModel *inModel)
{
    init(inModel->getAlias(), inModel->getDisplayableName(), inModel->getDescription());
}

QString COPYGroupModel::getName()
{
    return "grpCpy_" + getAlias();
}

void COPYGroupModel::getIncludes(QSet<QString> &list)
{
    list.insert("#include \"ct_itemdrawable/tools/iterator/ct_groupiterator.h\"\n");
    getChildrenIncludes(list);
}

QString COPYGroupModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName)
{
    QString result = "";

    if (getStatus() == S_Added) {

        result += Tools::getIndentation(2) + resultModelName;

        if (parentDEF == "") {
            result += "->addGroupModel(\"\", ";
        } else {
            result += "->addGroupModel(" + parentDEF +", ";
        }

        result += getAutoRenameName() + ", new CT_StandardItemGroup()";

        if (getDisplayableName() != "" || getDescription() != "")
        {
            result += ", " + Tools::trs(getDisplayableName());
        }

        if (getDescription() != "")
        {
            result += ", " + Tools::trs(getDescription());
        }

        result += ");\n";
    } else if (getStatus() == S_DeletedCopy){

        result += Tools::getIndentation(2) + resultModelName;
        result += "->removeGroupModel(" + getDef() +");\n";
    }

    result += "\n";

    if (getStatus() == S_Copy)
    {
        getChildrenCreateOutResultModelListProtectedContent(result, getDef(), resultModelName);
    } else if (getStatus() == S_Added){
        getChildrenCreateOutResultModelListProtectedContent(result, getAutoRenameName(), resultModelName);
    }
    return result;
}

QString COPYGroupModel::getComputeContent(QString resultName, QString parentName, int indent, bool rootGroup)
{
    QString result = "";

    if (getStatus() == S_Copy)
    {
        QString iterator = "CT_GroupIterator";
        if (rootGroup) {iterator = "CT_ResultGroupIterator";}

        result += Tools::getIndentation(indent) + iterator + " itCpy_" + getAlias() + "(" + parentName + ", this, " + getDef() + ");\n";
        result += Tools::getIndentation(indent) + "while (itCpy_" + getAlias() + ".hasNext() && !isStopped())\n";
        result += Tools::getIndentation(indent) + "{\n";
        result += Tools::getIndentation(indent+1) + "CT_StandardItemGroup* " + getName() + " = (CT_StandardItemGroup*) itCpy_" + getAlias() + ".next();\n";
        result += Tools::getIndentation(indent+1) + "\n";

        getChildrenComputeContent(result, resultName, indent +1);

        result += Tools::getIndentation(indent) + "}\n";
        result += Tools::getIndentation(indent) + "\n";

    } else if (getStatus() == S_Added) {
        result += Tools::getIndentation(indent) + "CT_StandardItemGroup* " + getName() + "= new CT_StandardItemGroup(" + getAutoRenameName() + ".completeName(), " + resultName + ");\n";
        result += Tools::getIndentation(indent) + parentName + "->addGroup(" + getName() + ");\n";
        result += Tools::getIndentation(indent) + "\n";

        getChildrenComputeContent(result, resultName, indent);
    }

    return result;
}
