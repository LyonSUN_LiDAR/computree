#include "outitemmodel.h"
#include "view/step/models/outitemwidget.h"
#include "model/step/tools.h"

OUTItemModel::OUTItemModel() : AbstractOutModel()
{
    _widget = new OUTItemWidget(this);
    setText(getPrefixedAlias());
}

QString OUTItemModel::getItemType()
{
    return ((OUTItemWidget*) _widget)->getItemType();
}

QString OUTItemModel::getItemTemplate()
{
    return ((OUTItemWidget*) _widget)->getTemplate();
}

QString OUTItemModel::getItemTypeWithTemplate()
{
    return ((OUTItemWidget*) _widget)->getItemTypeWithTemplate();
}

QString OUTItemModel::getName()
{
    return "item_" + getAlias();
}

QString OUTItemModel::getItemOutCode(int indent, QString name, QString modelName, QString resultName)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType == NULL) {return "";}
    QString result = itemType->getOutCode(indent, name, modelName, resultName);
    return result;
}

void OUTItemModel::getIncludes(QSet<QString> &list)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType != NULL)
    {
        list.insert(itemType->getInclude());
    }
}

QString OUTItemModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    Q_UNUSED(rootGroup);

    QString result = "";

    result += Tools::getIndentation(1) + resultModelName;
    result += "->addItemModel(" + parentDEF + ", " + getDef() + ", new " + getItemTypeWithTemplate() + "()";

    if (getDisplayableName() != "" || getDescription() != "")
    {
        result += ", " + Tools::trs(getDisplayableName());
    }

    if (getDescription() != "")
    {
        result += ", " + Tools::trs(getDescription());
    }

    result += ");\n";

    return result;
}

QString OUTItemModel::getComputeContent(QString resultName, QString parentName)
{
    QString result = "";

    QString itemCode = getItemOutCode(1, getName(), getDef(), resultName);
    if (itemCode.size() > 0)
    {
        result += itemCode;
        result += Tools::getIndentation(1) + parentName + "->addItemDrawable(" + getName() + ");\n";
        result += "\n";
    }

    return result;
}
