#include "model/step/models/abstractinmodel.h"

AbstractInModel::AbstractInModel() : QStandardItem()
{
}

AbstractInModel::~AbstractInModel()
{
    delete _widget;
}

AbstractInWidget* AbstractInModel::getWidget()
{
    return _widget;
}

QString AbstractInModel::getDef()
{
    return _widget->getDEF();
}

QString AbstractInModel::getPrefixedAlias()
{
    return _widget->getPrefixedAliad();
}

QString AbstractInModel::getAlias()
{
    return _widget->getAlias();
}

QString AbstractInModel::getDisplayableName()
{
    return _widget->getDisplayableName();
}

QString AbstractInModel::getDescription()
{
    return _widget->getDescription();
}

bool AbstractInModel::isValid()
{
    for (int i = 0 ; i < rowCount() ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) child(i);
        if (!item->isValid()) {return false;}
    }
    return getWidget()->isvalid();
}

QString AbstractInModel::getInModelsDefines()
{
    QString result = "#define ";
    result.append(getDef());
    result.append(QString(" \"%1\"\n").arg(getAlias()));

    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) child(i);
        result.append(item->getInModelsDefines());
    }
    return result;
}


void AbstractInModel::getChildrenIncludes(QSet<QString> &list)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) child(i);
        item->getIncludes(list);
    }
}

void AbstractInModel::getChildrenCreateInResultModelListProtectedContent(QString &result, QString parentDEF, QString resultModelName)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) child(i);

        QString childContent = item->getCreateInResultModelListProtectedContent(parentDEF, resultModelName, false);
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractInModel::getChildrenComputeContent(QString &result, int indent)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractInModel* item = (AbstractInModel*) child(i);

        QString childContent = item->getComputeContent(getName(), indent, false);
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractInModel::onAliasChange()
{
    setText(getPrefixedAlias());
}
