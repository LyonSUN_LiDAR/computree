#include "model/step/tools.h"
#include "model/step/itemtypes/ct_scene.h"
#include "model/step/itemtypes/ct_pointcluster.h"
#include "model/step/itemtypes/ct_referencepoint.h"
#include "model/step/itemtypes/ct_abstractshape.h"
#include "model/step/itemtypes/ct_circle.h"
#include "model/step/itemtypes/ct_ellipse.h"
#include "model/step/itemtypes/ct_cylinder.h"
#include "model/step/itemtypes/ct_line.h"
#include "model/step/itemtypes/ct_grid2d.h"
#include "model/step/itemtypes/ct_grid3d.h"
#include "model/step/itemtypes/ct_beam.h"
#include "model/step/itemtypes/ct_scanner.h"
#include "model/step/itemtypes/ct_triangulation2d.h"
#include "model/step/itemtypes/ct_abstractitemdrawablewithpointcloud.h"
#include "model/step/itemtypes/ct_abstractitemdrawablewithoutpointcloud.h"
#include "model/step/itemtypes/ct_abstractsingularitemdrawable.h"

QMap<QString, AbstractItemType*> Tools::ITEMTYPE = Tools::initItemTypes();


QMap<QString, AbstractItemType*> Tools::initItemTypes()
{
    QMap<QString, AbstractItemType*> map;
    addItemType(map, new CT_Scene());
    addItemType(map, new CT_PointCluster());
    addItemType(map, new CT_ReferencePoint());

    addItemType(map, new CT_AbstractShape());
    addItemType(map, new CT_Circle());
    addItemType(map, new CT_Ellipse());
    addItemType(map, new CT_Line());
    addItemType(map, new CT_Cylinder());
    addItemType(map, new CT_Grid2D());
    addItemType(map, new CT_Grid3D());
    addItemType(map, new CT_Beam());
    addItemType(map, new CT_Scanner());
    addItemType(map, new CT_Triangulation2D());

    addItemType(map, new CT_AbstractItemDrawableWithPointCloud());
    addItemType(map, new CT_AbstractItemDrawableWithoutPointCloud());
    addItemType(map, new CT_AbstractSingularItemDrawable());

    return map;
}

void Tools::addItemType(QMap<QString, AbstractItemType*> &map, AbstractItemType* itemType)
{
    map.insert(itemType->getTypeName(), itemType);
}
