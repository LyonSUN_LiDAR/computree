#ifndef CT_POINTCLUSTER_H
#define CT_POINTCLUSTER_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_PointCluster : public AbstractItemType
{
public:
    CT_PointCluster();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_POINTCLUSTER_H
