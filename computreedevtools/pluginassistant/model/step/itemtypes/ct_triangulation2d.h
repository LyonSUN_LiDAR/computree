#ifndef CT_TRIANGULATION2D_H
#define CT_TRIANGULATION2D_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Triangulation2D : public AbstractItemType
{
public:
    CT_Triangulation2D();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_TRIANGULATION2D_H
