#include "ct_abstractshape.h"
#include "model/step/tools.h"

CT_AbstractShape::CT_AbstractShape() : AbstractItemType()
{
}

QString CT_AbstractShape::getTypeName()
{
    return "CT_AbstractShape";
}

QString CT_AbstractShape::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_AbstractShape::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    Q_UNUSED(modelName);
    Q_UNUSED(resultName);
    return "";
}
