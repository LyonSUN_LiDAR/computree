#ifndef CT_GRID3D_H
#define CT_GRID3D_H

#include "model/step/itemtypes/abstracttemplateteditemtype.h"

class CT_Grid3D : public AbstractTemplatedItemType
{
public:
    CT_Grid3D();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_GRID3D_H
