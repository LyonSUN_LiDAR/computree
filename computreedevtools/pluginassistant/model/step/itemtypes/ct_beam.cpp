#include "ct_beam.h"
#include "model/step/tools.h"

CT_Beam::CT_Beam() : AbstractItemType()
{
}

QString CT_Beam::getTypeName()
{
    return "CT_Beam";
}

QString CT_Beam::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Beam::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
