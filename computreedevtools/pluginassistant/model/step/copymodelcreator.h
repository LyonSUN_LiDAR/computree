#ifndef COPYMODELCREATOR_H
#define COPYMODELCREATOR_H

#include <QObject>
#include <QStandardItemModel>
#include "model/step/models/abstractcopymodel.h"
#include "model/step/models/abstractinmodel.h"

class CopyModelCreator : public QObject
{
    Q_OBJECT
public:
    explicit CopyModelCreator(QStandardItemModel *inModel, QObject *parent = 0);

    void init();

    static void recursiveAddChildren(AbstractCopyModel* copyModel, AbstractInModel* inModel);

    inline QStandardItemModel *getStandardItemModel() {return _model;}

    QString getAutoRenamesDeclarations();
    void getIncludes(QSet<QString> &list);
    QString getCreateOutResultModelListProtectedContent();
    QString getResultRecovery(int &nextRank);
    QString getComputeContent();
    QString getCopyDefines();


private:
    QStandardItemModel *_model;
    QStandardItemModel *_inModel;
};

#endif // COPYMODELCREATOR_H
