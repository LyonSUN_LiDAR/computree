#include "filtercreator.h"

#include <QTextStream>
#include <QDir>

FilterCreator::FilterCreator(QString directory, QString name) : QObject()
{
    _directory = directory;
    _name = name;
}

QString FilterCreator::createFilterFiles()
{
    QString errors = "";

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas', veuillez en choisir un autre svp");
    }

    dir.mkdir("filter");

    if (!create_FilterFile_h()) {errors.append(tr("Impossible de créer le fichier filter (.h)\n"));}
    if (!create_FilterFile_cpp()) {errors.append(tr("Impossible de créer le fichier filter (.cpp)\n"));}

    return errors;
}

bool FilterCreator::create_FilterFile_h()
{
    QFile file(QString("%1/filter/%2.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";
        stream << "\n";
        stream << "#include \"ct_filter/abstract/ct_abstractfilter_las.h\"\n";
        stream << "\n";
        stream << "class " << _name << " : public CT_AbstractFilter_LAS\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "public:\n";
        stream << "\n";
        stream << "    " << _name << "();\n";
        stream << "    " << _name << "(const " << _name << "* other);\n";
        stream << "\n";
        stream << "    QString getName();\n";
        stream << "\n";
        stream << "    void createConfigurationDialog();\n";
        stream << "    void updateParamtersAfterConfiguration();\n";
        stream << "\n";
        stream << "    QString getShortDescription() const;\n";
        stream << "    QString getDetailledDescription() const;\n";
        stream << "\n";
        stream << "    QString getParametersAsString() const;\n";
        stream << "    virtual bool setParametersFromString(QString parameters);\n";
        stream << "\n";
        stream << "    void validatePoint(CT_PointIterator& pointIt, CT_LASData &LADData) const;\n";
        stream << "\n";
        stream << "\n";
        stream << "    CT_AbstractConfigurableElement* copy() const;\n";
        stream << "};\n";
        stream << "\n";
        stream << "\n";
        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool FilterCreator::create_FilterFile_cpp()
{
    QFile file(QString("%1/filter/%2.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"filter/" << _name.toLower() << ".h\"\n";
        stream << "#include \"ct_pointcloudindex/ct_pointcloudindexvector.h\"\n";
        stream << "#include \"ct_iterator/ct_pointiterator.h\"\n";
        stream << "\n";
        stream << _name << "::" << _name << "() : CT_AbstractFilter_LAS()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << _name << "::" << _name << "(const " << _name << " *other) : CT_AbstractFilter_LAS(other)\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getName()\n";
        stream << "{\n";
        stream << "    return QString(\"PB_Name\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::createConfigurationDialog()\n";
        stream << "{\n";
        stream << "    CT_StepConfigurableDialog* configDialog = addConfigurationDialog();\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::updateParamtersAfterConfiguration()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getParametersAsString() const\n";
        stream << "{\n";
        stream << "    QString result = \"\";\n";
        stream << "    return result;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::setParametersFromString(QString parameters)\n";
        stream << "{\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "\n";
        stream << "QString " << _name << "::getShortDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"short description here\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getDetailledDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::validatePoint(CT_PointIterator &pointIt, CT_LASData &lasData) const\n";
        stream << "{\n";
        stream << "    _outCloud->addIndex(pointIt.currentGlobalIndex());\n";
        stream << "}\n";
        stream << "\n";
        stream << "CT_AbstractConfigurableElement *" << _name << "::copy() const\n";
        stream << "{\n";
        stream << "    " << _name << "* filter = new " << _name << "(this);\n";
        stream << "    // Copy the parameters values here !\n";
        stream << "    return filter;\n";
        stream << "}\n";


        file.close();
        return true;
    }
    return false;
}

