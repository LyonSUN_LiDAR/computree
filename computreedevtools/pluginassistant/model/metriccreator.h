#ifndef METRICCREATOR_H
#define METRICCREATOR_H

#include <QObject>

class MetricCreator : public QObject
{
    Q_OBJECT
public:
    explicit MetricCreator(QString directory, QString name);

    QString createMetricFiles();

private:

    QString     _directory;
    QString     _name;


    bool create_MetricFile_h();
    bool create_MetricFile_cpp();

};

#endif // METRICCREATOR_H
