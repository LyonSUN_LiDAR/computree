#ifndef PLUGINCREATOR_H
#define PLUGINCREATOR_H

#include <QObject>

class PluginCreator : public QObject
{
    Q_OBJECT
public:
    explicit PluginCreator(QString directory, QString name, QString code);

    QString createPluginFiles();

    static bool isValidPluginParameters(const QString &directory, const QString &name, const QString &code);
    static bool isValidPluginDirectory(const QString &directory);
    static bool isValidNameAndCode(const QString &name, const QString &code);
    static QString getCodeAndNameFromEntryFile(const QString &fileName, QString &code, QString &name);

private:
    QString     _pluginDirectory;
    QString     _pluginName;
    QString     _pluginCode;

    QString _target;

    QString _proName;
    QString _entryName;
    QString _managerName;



    bool createPluginProject_pro();
    bool createPluginEntry_h();
    bool createPluginEntry_cpp();
    bool createPluginManager_h();
    bool createPluginManager_cpp();

};

#endif // PLUGINCREATOR_H
