#include "view/step/createstepdialog.h"
#include "ui_createstepdialog.h"

CreateStepDialog::CreateStepDialog(QString directory, QString pluginCode, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateStepDialog)
{
    ui->setupUi(this);

    _stepCreator = new StepCreator(directory);
    _stepCreator->setPluginCode(pluginCode);

    _errors = "La création des fichiers n'a pas été éxécutée !\n";

    _inModelDialog = new INModelDialog(_stepCreator->getInModelCreator(), this);
    _outModelDialog = new OUTModelDialog(_stepCreator->getOutModelCreator(), this);
    _copyModelDialog = new COPYModelDialog(_stepCreator->getCopyModelCreator(), this);
    _parametersModelDialog = new ParameterModelDialog(_stepCreator->getParametersModelCreator(), this);
}

CreateStepDialog::~CreateStepDialog()
{
    delete _inModelDialog;
    delete _outModelDialog;
    delete _copyModelDialog;
    delete _parametersModelDialog;
    delete _stepCreator;
    delete ui;
}

void CreateStepDialog::on_le_code_textChanged(const QString &arg1)
{
    _stepCreator->setStepBaseName(arg1);
}

void CreateStepDialog::on_le_description_textChanged(const QString &arg1)
{
    _stepCreator->setShortDescription(arg1);
}

void CreateStepDialog::on_te_detailledDescription_textChanged()
{  
    _stepCreator->setDetailledDescription(ui->te_detailledDescription->toPlainText());
}

void CreateStepDialog::on_pb_parameters_clicked()
{
    _parametersModelDialog->exec();
}

void CreateStepDialog::on_pb_modelin_clicked()
{
    _inModelDialog->exec();
}

void CreateStepDialog::on_pb_modelcopy_clicked()
{
    if (_inModelDialog->hasBeenModified())
    {
        _copyModelDialog->init();
        _inModelDialog->setModified(false);
    }
    _copyModelDialog->exec();
}

void CreateStepDialog::on_pb_modelout_clicked()
{
    _outModelDialog->exec();
}

void CreateStepDialog::on_pb_createFiles_clicked()
{
    _errors = _stepCreator->createStepFiles();
}
