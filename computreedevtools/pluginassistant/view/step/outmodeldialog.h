#ifndef OUTMODELDIALOG_H
#define OUTMODELDIALOG_H

#include <QDialog>

#include "qstandarditemmodel.h"
#include "qboxlayout.h"
#include "view/step/models/abstractoutwidget.h"
#include "model/step/outmodelcreator.h"

namespace Ui {
    class OUTModelDialog;
}

class OUTModelDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OUTModelDialog(OutModelCreator *modelCreator, QWidget *parent = 0);
    ~OUTModelDialog();

    void closeEvent(QCloseEvent *event);

private slots:
    void on_pb_addResult_clicked();
    void on_pb_delete_clicked();
    void on_pb_addGroup_clicked();
    void on_pb_addItem_clicked();
    void on_treeView_clicked(const QModelIndex &index);
    void on_pb_clear_clicked();
    void on_buttonBox_rejected();

    void accept();

private:
    Ui::OUTModelDialog *ui;
    OutModelCreator *_modelCreator;
    QStandardItemModel *_model;
    QVBoxLayout *_layout;
    AbstractOutWidget *_activeWidget;


};

#endif // OUTMODELDIALOG_H
