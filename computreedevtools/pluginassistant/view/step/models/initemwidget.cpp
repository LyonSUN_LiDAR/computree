#include "initemwidget.h"
#include "ui_initemwidget.h"
#include "model/step/models/abstractinmodel.h"
#include "model/step/tools.h"

INItemWidget::INItemWidget(AbstractInModel* model, QWidget *parent) :
    AbstractInWidget(model, parent),
    ui(new Ui::INItemWidget)
{
    ui->setupUi(this);

    ui->cb_finderMode->addItem("Obligatory");
    ui->cb_finderMode->addItem("Optional");
    ui->cb_finderMode->setCurrentIndex(0);

    ui->cb_choiceMode->addItem("ChooseOneIfMultiple");
    ui->cb_choiceMode->addItem("ChooseMultipleIfMultiple");
    ui->cb_choiceMode->setCurrentIndex(0);

    // Ajout des classes d'itemdrawables disponibles
    QMapIterator<QString, AbstractItemType*> it(Tools::ITEMTYPE);
    while (it.hasNext())
    {
        it.next();
        ui->cb_itemType->addItem(it.key());
    }

    ui->cb_itemType->setCurrentIndex(0);

    setFocusProxy(ui->alias);
}

INItemWidget::~INItemWidget()
{
    delete ui;
}

bool INItemWidget::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString INItemWidget::getItemType()
{
    return ui->cb_itemType->currentText();
}

QString INItemWidget::getTemplate()
{
    return ui->cb_template->currentText();
}

QString INItemWidget::getItemTypeWithTemplate()
{
    QString templ = getTemplate();
    if (templ.size() > 0)
    {
        templ = "<" + templ + ">";
    }

    return ui->cb_itemType->currentText() + templ;
}

QString INItemWidget::getPrefixedAliad()
{
    return QString("Item: %1").arg(getAlias());
}

QString INItemWidget::getAlias()
{
    return ui->alias->text();
}

QString INItemWidget::getDEF()
{
    return QString("DEFin_%1").arg(ui->alias->text());
}

QString INItemWidget::getDisplayableName()
{
    return ui->modelName->text();
}

QString INItemWidget::getDescription()
{
    return ui->modelDescription->toPlainText().replace("\n","\\n");
}


INItemWidget::FinderMode INItemWidget::getFinderMode()
{
    if (ui->cb_finderMode->currentText()=="Obligatory")
    {
        return INItemWidget::F_Obligatory;
    } else
    {
        return INItemWidget::F_Optional;
    }
}

INItemWidget::ChoiceMode INItemWidget::getChoiceMode()
{
    if (ui->cb_choiceMode->currentText()=="ChooseOneIfMultiple")
    {
        return INItemWidget::C_OneIfMultiple;
    } else
    {
        return INItemWidget::C_MultipleIfMultiple;
    }
}

void INItemWidget::on_alias_textChanged(const QString &arg1)
{
    ui->alias->setText(arg1.trimmed());
    _model->onAliasChange();
}

void INItemWidget::on_cb_itemType_currentIndexChanged(const QString &arg1)
{
    ui->cb_template->clear();

    AbstractItemType* itemType = Tools::ITEMTYPE.value(arg1, NULL);

    if (itemType != NULL && itemType->isTemplated())
    {
        ui->cb_template->addItems(itemType->getTemplates());
        ui->cb_template->setCurrentIndex(0);
        ui->templateWidget->setVisible(true);
    } else {
        ui->templateWidget->setVisible(false);
    }
}
