#ifndef CREATESTEPDIALOG_H
#define CREATESTEPDIALOG_H

#include <QDialog>
#include "model/stepcreator.h"
#include "view/step/inmodeldialog.h"
#include "view/step/outmodeldialog.h"
#include "view/step/copymodeldialog.h"
#include "view/step/parametermodeldialog.h"


namespace Ui {
class CreateStepDialog;
}

class CreateStepDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateStepDialog(QString directory, QString pluginCode, QWidget *parent = 0);
    ~CreateStepDialog();

    inline QString getErrors() {return _errors;}
    inline QString getSepName() {return _stepCreator->getStepName();}

private slots:
    void on_pb_parameters_clicked();

    void on_le_code_textChanged(const QString &arg1);

    void on_le_description_textChanged(const QString &arg1);

    void on_te_detailledDescription_textChanged();

    void on_pb_modelin_clicked();

    void on_pb_modelcopy_clicked();

    void on_pb_modelout_clicked();

    void on_pb_createFiles_clicked();

private:
    Ui::CreateStepDialog*    ui;
    QString                  _errors;
    StepCreator*             _stepCreator;
    INModelDialog*           _inModelDialog;
    OUTModelDialog*          _outModelDialog;
    COPYModelDialog*         _copyModelDialog;
    ParameterModelDialog*    _parametersModelDialog;

};

#endif // CREATESTEPDIALOG_H
