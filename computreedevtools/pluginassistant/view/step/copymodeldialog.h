#ifndef COPYMODELDIALOG_H
#define COPYMODELDIALOG_H

#include <QDialog>

#include "qstandarditemmodel.h"
#include "qboxlayout.h"
#include "view/step/models/abstractcopywidget.h"
#include "model/step/models/abstractinmodel.h"
#include "model/step/models/abstractcopymodel.h"
#include "model/step/copymodelcreator.h"


namespace Ui {
    class COPYModelDialog;
}

class COPYModelDialog : public QDialog
{
    Q_OBJECT

public:
    explicit COPYModelDialog(CopyModelCreator *modelCreator, QWidget *parent = 0);
    ~COPYModelDialog();

    void init();

    void closeEvent(QCloseEvent *event);

private slots:
    void on_pb_delete_clicked();
    void on_pb_addGroup_clicked();
    void on_pb_addItem_clicked();
    void on_treeView_clicked(const QModelIndex &index);
    void on_buttonBox_rejected();

    void accept();

    void on_pb_cancelDelete_clicked();

private:
    Ui::COPYModelDialog *ui;
    CopyModelCreator *_modelCreator;
    QStandardItemModel *_model;
    QVBoxLayout *_layout;
    AbstractCopyWidget *_activeWidget;

    static void deleteItem(AbstractCopyModel *item, QStandardItemModel *model, AbstractCopyWidget *&activeWidget);
    static void cancelDelete(AbstractCopyModel *item);
};

#endif // COPYMODELDIALOG_H
