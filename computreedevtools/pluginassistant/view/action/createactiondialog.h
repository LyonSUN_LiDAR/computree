#ifndef CREATEACTIONDIALOG_H
#define CREATEACTIONDIALOG_H

#include <QDialog>

namespace Ui {
class CreateActionDialog;
}

class CreateActionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateActionDialog(QString code, QWidget *parent = 0);
    ~CreateActionDialog();

    QString getName();

private:
    Ui::CreateActionDialog *ui;
};

#endif // CREATEACTIONDIALOG_H
