#include "view/filter/createfilterdialog.h"
#include "ui_createfilterdialog.h"

CreateFilterDialog::CreateFilterDialog(QString code, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateFilterDialog)
{
    ui->setupUi(this);

    ui->lb_begin->setText(QString("%1_Filter").arg(code.toUpper()));
    ui->le_name->setText("");
}

CreateFilterDialog::~CreateFilterDialog()
{
    delete ui;
}

QString CreateFilterDialog::getName()
{
    return QString("%1%2").arg(ui->lb_begin->text()).arg(ui->le_name->text());
}


