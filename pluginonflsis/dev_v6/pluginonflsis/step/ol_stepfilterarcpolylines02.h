/****************************************************************************

 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                     and the Laboratoire des Sciences de l'Information et des Systèmes (LSIS), Marseille, France.
                     All rights reserved.

 Contact : alexandre.piboule@onf.fr
           alexandra.bac@esil.univmed.fr

 Developers : Joris Ravaglia (ONF/LSIS)
 With adaptations by : Alexandre PIBOULE (ONF)

 This file is part of PluginONFLSIS library 2.0.

 PluginONFLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginShared is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginShared.  If not, see <http://www.gnu.org/licenses/lgpl.html>.

*****************************************************************************/

#ifndef OL_STEPFILTERARCPOLYLINES02_H
#define OL_STEPFILTERARCPOLYLINES02_H

#include "ct_step/abstract/ct_abstractstep.h"


class CT_AbstractItemGroup;

/*!
 * \class OL_StepFilterArcPolylines02
 * \ingroup Steps_OL
 * \brief <b>Filter polylines to only keep ones looking like arc of circles</b>
 *
 * For each polyline, coordinates are transform in "space of tangents".
 * In this space, X axis represents length beetween successive vertex of the polyline.
 * Y axis represents angle beetween successive segments of the polyline.
 * An interesting property is that in this space an arc of circle is represented by a staigth line.
 * So transformed coordinates of the polyline are plotted and a straigth line is ajusted.
 * After that polylines are optionnally filtered on statictical criteria describing the quality of this fitting.
 * See parameters description for considered criteria.
 *
 * \param _filterRMSE If true drop polylines with excessives RMSE
 * \param _RMSEMax Maximum allowed RMSE
 * \param _filterErrorMax If true drop polylines with excessives maximum error
 * \param _ErrorMax Maximum allowed maximum error
 * \param _filterR2 If true drop polylines with too low R²
 * \param _R2Min R2 Minimum allowed R²
 * \param _filterByArcradius If true drop polylines with excessive radius of arc
 * \param _maxArcRadius Maximum allowed arc radius
 *
 *
 * <b>Input Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup... \n
 *          - CT_PolyLine (polyline) \n
 *          (...)
 *
 * <b>Output Models:</b>
 *
 *  - CT_ResultGroup \n
 *      - CT_StandardItemGroup... \n
 *          - <em>cpy- CT_PolyLine (polyline)</em> \n
 *          - <em>cpy- (...)</em> \n
 *
 */
class OL_StepFilterArcPolylines02 : public CT_AbstractStep
{
    // IMPORTANT pour avoir le nom de l'étape
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    OL_StepFilterArcPolylines02(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    bool        _filterRMSE;        /*!< Filtrage sur la RMSE o/n */
    double      _RMSEMax;           /*!< RMSE maximale pour que la polylines soit acceptée comme arc */
    bool        _filterErrorMax;    /*!< Filtrage sur l'erreur maximale o/n */
    double      _ErrorMax;          /*!< Erreur maximale pour que la polylines soit acceptée comme arc */
    bool        _filterR2;          /*!< Filtrage sur le R2 o/n */
    double      _R2Min;             /*!< R2 minimal pour que la polylines soit acceptée comme arc */
    bool        _filterByArcradius; /*!< Filtrage sur le rayonn estimé de l'arc o/n */
    double      _maxArcRadius;             /*!< Rayon maximal pour l'arc */

    void recursiveRemoveGroupLaterIfEmpty(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const;
};

#endif // OL_STEPFILTERARCPOLYLINES02_H
