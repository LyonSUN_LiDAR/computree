/****************************************************************************

 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                     and the Laboratoire des Sciences de l'Information et des Systémes (LSIS), Marseille, France.
                     All rights reserved.

 Contact : alexandre.piboule@onf.fr
           alexandra.bac@esil.univmed.fr

 Developers : Joris Ravaglia (ONF/LSIS)
 With adaptations by : Alexandre PIBOULE (ONF)

 This file is part of PluginONFLSIS library 2.0.

 PluginONFLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginShared is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginShared.  If not, see <http://www.gnu.org/licenses/lgpl.html>.

*****************************************************************************/

#include "ol_stepcreatepolylines02.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"


#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_itemdrawable/tools/ct_standardcontext.h"
#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/tools/pointclustertools/ct_polylinesalgorithms.h"

#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QtConcurrentMap>
#else
#include <QtConcurrent/QtConcurrentMap>
#endif

#define DEF_SearchInGroup           "g"
#define DEF_SearchInPointCluster    "p"
#define DEF_SearchInResult          "r"

OL_StepCreatePolylines02::OL_StepCreatePolylines02(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _multiThread = true;
}

QString OL_StepCreatePolylines02::getStepDescription() const
{
    return tr("Création d'une polyligne par cluster");
}

CT_VirtualAbstractStep* OL_StepCreatePolylines02::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new OL_StepCreatePolylines02(dataInit);
}

//////////////////// PROTECTED //////////////////

void OL_StepCreatePolylines02::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy * resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Clusters (Points)"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInPointCluster, CT_PointCluster::staticGetType(), tr("Cluster (Points)"));
}

void OL_StepCreatePolylines02::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addBool(tr("Traitement Multithread"),"","",_multiThread);

}

void OL_StepCreatePolylines02::createOutResultModelListProtected()
{     
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
        res->addItemModel(DEF_SearchInGroup, _outPolylinesModelName, new CT_PointCluster(), tr("Polyligne"));
}

void OL_StepCreatePolylines02::compute()
{    
    // on récupére le résultat copié
    _outRes = getOutResultList().first();

    QList<CT_AbstractItemGroup*> multiThreadList;
    CT_StandardContext context;
    context.setStep(this);

    CT_ResultGroupIterator itGrp(_outRes, this, DEF_SearchInGroup);
    while (itGrp.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*) itGrp.next();
        group->setContext(&context);
        if (_multiThread)
        {
            multiThreadList.append(group);
        } else {
            OL_StepCreatePolylines02::staticCreatePolyline(group);
        }
    }

    setProgress(50);

    if (_multiThread)
    {
        QFuture<void> futur = QtConcurrent::map(multiThreadList, OL_StepCreatePolylines02::staticCreatePolyline);
        int progressMin = futur.progressMinimum();
        int progressTotal = futur.progressMaximum() - futur.progressMinimum();
        while (!futur.isFinished())
        {
            if (progressTotal > 0)
            {
                setProgress(50 + 49*(futur.progressValue() - progressMin)/progressTotal);
            }
        }
    }

    multiThreadList.clear();
    setProgress( 100 );
}

void OL_StepCreatePolylines02::staticCreatePolyline(CT_AbstractItemGroup *group)
{
    CT_StandardContext* context = (CT_StandardContext*) group->getContext();
    OL_StepCreatePolylines02* step = (OL_StepCreatePolylines02*) context->step();
    CT_PointCluster* currentCluster = (CT_PointCluster*) group->firstItemByINModelName(step, DEF_SearchInPointCluster);

    CT_PointCluster* polyline = new CT_PointCluster(step->_outPolylinesModelName.completeName(), step->_outRes);
    CT_PolylinesAlgorithms::createPolyline2D(currentCluster, polyline);
    group->addItemDrawable(polyline);

}
