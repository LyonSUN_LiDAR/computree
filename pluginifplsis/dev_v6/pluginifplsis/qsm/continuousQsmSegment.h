/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef HOUGHSPHERE_H
#define HOUGHSPHERE_H

#include "core/localCell/localcellcylinder.h"
#include "plot/implicitfunction.h"

#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>

#include "utils/treetopology.h"

class continuousQsmSegment  : public implicitFunction
{
public:
    continuousQsmSegment(){}
    continuousQsmSegment(cylinder &cyl, pcl::PointCloud<pcl::PointXYZ> &pCloud, int branchID);

    void process(cylinder &cyl);

    const pcl::PointXYZ& getCenter() const {return center;}
    float getRadius() const {return radius;}
    const localCellCylinder& getLocalCell() const {return localR;}

    virtual void fillScalarField(scalarField &field);

    void toString();

    float radius;

    int getBranchId(){return branchId;}

private:

    int branchId;

    pcl::PointXYZ center;

    localCellCylinder localR;
    pcl::PointCloud<pcl::PointXYZ>   pts;

    void keepPtsInBox(pcl::PointCloud<pcl::PointXYZ> &pCloud);
};

#endif // HOUGHSPHERE_H
