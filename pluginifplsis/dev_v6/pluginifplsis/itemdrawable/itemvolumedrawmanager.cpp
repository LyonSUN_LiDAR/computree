/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemvolumedrawmanager.h"
#include "itemvolume.h"

const QString itemVolumeDrawManager::INDEX_CONFIG_SPHERES_VISIBLE = itemVolumeDrawManager::staticInitConfigSpheresVisible();
const QString itemVolumeDrawManager::INDEX_CONFIG_LINE_VISIBLE = itemVolumeDrawManager::staticInitConfigLineVisible();

itemVolumeDrawManager::itemVolumeDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? "Division model" : drawConfigurationName)
{

}

itemVolumeDrawManager::~itemVolumeDrawManager()
{
}

void itemVolumeDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const itemVolume &item = dynamic_cast<const itemVolume&>(itemDrawable);

    //const itemSimpleTree &item = (const itemSimpleTree&)itemDrawable;

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SPHERES_VISIBLE).toBool())
        drawSpheres(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_LINE_VISIBLE).toBool())
        drawLine(view, painter, item);
}

CT_ItemDrawableConfiguration itemVolumeDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(itemVolumeDrawManager::staticInitConfigSpheresVisible(), "Elements", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemVolumeDrawManager::staticInitConfigLineVisible(), "Line", CT_ItemDrawableConfiguration::Bool, false);


    return item;
}

// PROTECTED //

QString itemVolumeDrawManager::staticInitConfigSpheresVisible()
{
    return "DEM_ST";
}
QString itemVolumeDrawManager::staticInitConfigLineVisible()
{
    return "DEM_LI";
}

void itemVolumeDrawManager::drawLine(GraphicsViewInterface &view, PainterInterface &painter, const itemVolume &item) const
{
    volumeDrawerHelper mo = item.getModel();
    pcl::PointCloud<pcl::PointXYZ> pts = mo.getPtsCloud();
    std::vector<std::vector<std::vector<int>>> Compline = mo.getLine();

    for(int k=0;k<Compline.size();k++){

        std::vector<std::vector<int>> line =  Compline.at(k);
        double alpha = (double)k/(double)Compline.size();
        painter.setColor(250*(1-alpha), 250*alpha, 30);

        for(int i=0;i<line.size();i++){
            std::vector<int> l = line.at(i);
            painter.drawLine(pts.at(l.at(0)).x, pts.at(l.at(0)).y ,pts.at(l.at(0)).z,pts.at(l.at(1)).x,pts.at(l.at(1)).y,pts.at(l.at(1)).z);
        }
    }
}

void itemVolumeDrawManager::drawSpheres(GraphicsViewInterface &view, PainterInterface &painter, const itemVolume &item) const
{
    volumeDrawerHelper mo = item.getModel();

    pcl::PointCloud<pcl::PointXYZ> pts = mo.getPtsCloud();
    std::vector<std::vector<std::vector<int>>> elements = mo.getElements();

    for(int k=0;k<elements.size();k++){

        std::vector<std::vector<int>> elts =  elements.at(k);

        //painter.setColor(rand() % 255, rand() % 255, rand() % 255);

        for(int i=0;i<elts.size();i++){

            painter.setColor(rand() % 255, rand() % 255, rand() % 255);

            std::vector<int> tri = elts.at(i);
            painter.drawLine(pts.at(tri.at(0)).x, pts.at(tri.at(0)).y ,pts.at(tri.at(0)).z,pts.at(tri.at(1)).x,pts.at(tri.at(1)).y,pts.at(tri.at(1)).z);
            painter.drawLine(pts.at(tri.at(1)).x, pts.at(tri.at(1)).y ,pts.at(tri.at(1)).z,pts.at(tri.at(2)).x,pts.at(tri.at(2)).y,pts.at(tri.at(2)).z);
            painter.drawLine(pts.at(tri.at(2)).x, pts.at(tri.at(2)).y ,pts.at(tri.at(2)).z,pts.at(tri.at(0)).x,pts.at(tri.at(0)).y,pts.at(tri.at(0)).z);
        }
    }
}
