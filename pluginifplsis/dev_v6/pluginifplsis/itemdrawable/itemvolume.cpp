/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemvolume.h"

const itemVolumeDrawManager itemVolume::volume_DRAW_MANAGER;

itemVolume::itemVolume() : CT_AbstractItemDrawableWithoutPointCloud()
{
    //poly = NULL;
    setBaseDrawManager(&volume_DRAW_MANAGER);
}

itemVolume::itemVolume(const CT_OutAbstractSingularItemModel *model,
                                       const CT_AbstractResult *result,
                                volumeDrawerHelper *pField) : CT_AbstractItemDrawableWithoutPointCloud(model, result), stModel(*pField)
{
    //poly = *polygon;
    setBaseDrawManager(&volume_DRAW_MANAGER);
}

itemVolume::itemVolume(const QString &modelName,
                                       const CT_AbstractResult *result,
                               volumeDrawerHelper *pField) : CT_AbstractItemDrawableWithoutPointCloud(modelName, result), stModel(*pField)
{
    //poly = *polygon;
    setBaseDrawManager(&volume_DRAW_MANAGER);
}

itemVolume::~itemVolume()
{
    //delete poly;
}

QString itemVolume::getType() const
{
    return staticGetType();
}

QString itemVolume::staticGetType()
{
    return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/surf_simpleTree";
}

const volumeDrawerHelper &itemVolume::getModel() const
{
    return stModel;
}

itemVolume* itemVolume::copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList)
{
    itemVolume *t2d = new itemVolume((const CT_OutAbstractSingularItemModel *)model, result, NULL);
    t2d->setId(id());

    t2d->setAlternativeDrawManager(getAlternativeDrawManager());

    return t2d;
}
