/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef SPHERE_H
#define SPHERE_H

#include "localCell/localcellpatch.h"
#include "quadleaf.h"

#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>

class sphereSupport
{
public:
    sphereSupport(quadLeaf* pLeaf, pcl::PointCloud<pcl::PointXYZ> cloud);
    ~sphereSupport();

    //use for test only
    sphereSupport(pcl::PointXYZ pCenter,float pRadius, pcl::PointCloud<pcl::PointXYZ> pCloud);

    const pcl::PointXYZ& getCenter() const {return center;}
    float getRadius() const {return radius;}
    const localCellPatch& getLocalRef() const {return *localR;}

    const quadLeaf& getLeaf() {return *leaf;}

    pcl::PointCloud<pcl::PointXYZ>& getPts(){return pts;}

private:
    quadLeaf* leaf;

    pcl::PointXYZ center;
    float radius;
    localCellPatch * localR;

    pcl::PointCloud<pcl::PointXYZ>   pts;

    void keepPtsInSphere(pcl::PointCloud<pcl::PointXYZ> pCloud);
};

#endif // SPHERE_H
