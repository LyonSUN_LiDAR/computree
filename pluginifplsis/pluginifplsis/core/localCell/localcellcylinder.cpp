/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "localcellcylinder.h"

#include "../leastsquareregsolver.h"

localCellCylinder::localCellCylinder(const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(pPtsGlobalRef)
{
    computeLeastSquareReg(radius,useWeight);
    getGlobalQuadric();
}
localCellCylinder::localCellCylinder(pcl::PointXYZ center, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(center,pPtsGlobalRef)
{
    computeLeastSquareReg(radius,useWeight);
    getGlobalQuadric();
}

localCellCylinder::localCellCylinder(pcl::PointXYZ center, pcl::PointXYZ normal, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : localCell(center,normal,pPtsGlobalRef)
{
    computeLeastSquareRegAngle();
    getGlobalQuadricAngle();

    ptsLocalRef.clear();
    ptsGlobalRef.clear();
}

localCellCylinder::localCellCylinder(pcl::PointXYZ center, pcl::PointXYZ normal, float radius) : localCell(center,normal)
{
    Eigen::Matrix4f m1Local;
    Eigen::Vector4f m2Local(0,0,0,-1);
    m1Local<<1./(radius*radius),0,0,0,
                 0,1./(radius*radius),0,0,
                 0,0,0,0,
                 0,0,0,0;
    m1 = transfo.matrix().transpose() * m1Local * transfo.matrix();
    m2 = m2Local.transpose() * transfo.matrix();
}

void localCellCylinder::computeLeastSquareRegAngle()
{
    leastSquareRegSolver::solveCylinderAngle(ptsLocalRef,  &this->coeff,  &this->error);
}

void localCellCylinder::computeLeastSquareReg(float radius, bool useWeight)
{
    pcl::PointXYZ cen;
    cen.x = centroid[0];
    cen.y = centroid[1];
    cen.z = centroid[2];

    //std::cout<<"before | x : "<<cen.x<<" y : "<<cen.y<<" z : "<<cen.z<<" | r : "<<radius<<" | n : "<<w[0]<<" "<<w[1]<<" "<<w[2]<<" "<<std::endl;
    //leastSquareRegSolver::solveCylinderNoCenter(cen, radius, ptsGlobalRef, &this->coeff);
    //std::cout<<"after | x : "<<coeff.at(0)<<" y : "<<coeff.at(1)<<" z : "<<cen.z<<" | r : "<<coeff.at(2)<<std::endl;

    /*centroid[0] = coeff.at(0);
    centroid[1] = coeff.at(1);

    radiusCylinderFitting = coeff.at(2);

    coeff.clear();

    getBasis();
    applyTransform();*/

    leastSquareRegSolver::solveCylinder(ptsLocalRef,  &this->coeff,  &this->error);

    /*pcl::PointXYZ cen;
//    cen.x = centroid[0];
//    cen.y = centroid[1];
//    cen.z = centroid[2];

    cen.x = 0.;
    cen.y = 0.;
    cen.z = 0.25;

    std::cout<<"before | x : "<<cen.x<<" y : "<<cen.y<<" z : "<<cen.z<<" | r : "<<radius<<" | n : "<<w[0]<<" "<<w[1]<<" "<<w[2]<<" "<<std::endl;
    leastSquareRegSolver::solveCylinderNoCenter(cen, radius, ptsLocalRef, &this->coeff);
    coeff.push_back(radius);
    std::cout<<"after | x : "<<coeff.at(3)<<" y : "<<coeff.at(4)<<" z : "<<coeff.at(5)<<" | r : "<<coeff.at(6)<<std::endl;
    std::cout<<std::endl;
    centroid[0] = centroid[0] + coeff.at(3);
    centroid[1] = centroid[1] + coeff.at(4);
    centroid[2] = centroid[2] + coeff.at(5);
    leastSquareRegSolver::solveCylinder(ptsLocalRef, &this->coeff,&this->error);*/

    getBasis();
}

void localCellCylinder::getGlobalQuadric()
{
    Eigen::Matrix4f m1Local;
    Eigen::Vector4f m2Local(0,0,0,-1);
    m1Local<<coeff.at(0),0,0,0,
                 0,coeff.at(1),0,0,
                 0,0,coeff.at(2),0,
                 0,0,0,0;
    m1 = transfo.matrix().transpose() * m1Local * transfo.matrix();
    m2 = m2Local.transpose() * transfo.matrix();
}

void localCellCylinder::getGlobalQuadricAngle()
{
    Eigen::Matrix4f m1Local;
    Eigen::Vector4f m2Local(0,0,0,-1);
    m1Local<<coeff.at(0),0.5*coeff.at(1),0,0,
                 0.5*coeff.at(1),coeff.at(2),0,0,
                 0,0,0,0,
                 0,0,0,0;
    m1 = transfo.matrix().transpose() * m1Local * transfo.matrix();
    m2 = m2Local.transpose() * transfo.matrix();
}
