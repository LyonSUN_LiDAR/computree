/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "continuousQsmSegment.h"

#include <pcl/common/common.h>
#include <pcl/common/geometry.h>
#include <pcl/filters/crop_box.h>

#include "utils/treetopology.h"

continuousQsmSegment::continuousQsmSegment(cylinder &cyl, pcl::PointCloud<pcl::PointXYZ> &pCloud, int branchID)
{

    branchId = branchID;    

    double delta = 1.1;
    double scale = cyl.radius;

    if(cyl.x1<cyl.x2)
    {
        BBMin.x = cyl.x1 - scale*delta;
        BBMax.x = cyl.x2 + scale*delta;
    }else{
        BBMin.x = cyl.x2 - scale*delta;
        BBMax.x = cyl.x1 + scale*delta;
    }

    if(cyl.y1<cyl.y2)
    {
        BBMin.y = cyl.y1 - scale*delta;
        BBMax.y = cyl.y2 + scale*delta;
    }else{
        BBMin.y = cyl.y2 - scale*delta;
        BBMax.y = cyl.y1 + scale*delta;
    }

    if(cyl.z1<cyl.z2)
    {
        BBMin.z = cyl.z1 - scale*delta;
        BBMax.z = cyl.z2 + scale*delta;
    }else{
        BBMin.z = cyl.z2 - scale*delta;
        BBMax.z = cyl.z1 + scale*delta;
    }

    keepPtsInBox(pCloud);
}

void continuousQsmSegment::process(cylinder &cyl)
{
    float diagBB = (BBMax.getVector3fMap()-BBMin.getVector3fMap()).norm();
    if(cyl.isFirst){
        radius = 0.3*diagBB;
    }else{
        radius = 0.6*diagBB;
    }

    pcl::PointXYZ direction;
    direction.x= cyl.x2 - cyl.x1;
    direction.y= cyl.y2 - cyl.y1;
    direction.z= cyl.z2 - cyl.z1;

    center.x = cyl.x1 + 0.5*direction.x;
    center.y = cyl.y1 + 0.5*direction.y;
    center.z = cyl.z1 + 0.5*direction.z;

    if(pts.size()>1000 && !cyl.isFirst){
        localR = localCellCylinder (center,direction,pts,(float)cyl.radius,1.0);
    }else{
        localR = localCellCylinder (center,direction,(float)cyl.radius);
    }

    pts.clear();
}

void continuousQsmSegment::keepPtsInBox(pcl::PointCloud<pcl::PointXYZ> &pCloud)
{
    for(int i=0;i<pCloud.size();i++)
    {
        pcl::PointXYZ& p = pCloud.at(i);
        if(p.x>BBMin.x && p.x<BBMax.x && p.y>BBMin.y && p.y<BBMax.y && p.z>BBMin.z && p.z<BBMax.z)
        {
            pts.push_back(p);
        }
    }
}

void continuousQsmSegment::fillScalarField(scalarField &field)
{
    pcl::PointXYZ min = field.getBBmin();

    int nbDivX = field.getXNode()-1;
    int nbDivY = field.getYNode()-1;
    int nbDivZ = field.getZNode()-1;

    float sizeCellX = field.getSizeCellX();
    float sizeCellY = field.getSizeCellY();
    float sizeCellZ = field.getSizeCellZ();

    for(int k=0;k<nbDivZ+1;k++)
    {
        for(int j=0;j<nbDivY+1;j++)
        {
            for(int i=0;i<nbDivX+1;i++)
            {

                float x = (float)i*sizeCellX+min.x;
                float y = (float)j*sizeCellY+min.y;
                float z = (float)k*sizeCellZ+min.z;

                if(sqrt((x-center.x)*(x-center.x)+(y-center.y)*(y-center.y)+(z-center.z)*(z-center.z))<radius){

                    Eigen::Vector4f X(x,y,z,1);
                    float res = (float)((X.transpose()*localR.getM1().matrix())*X) + (float)(localR.getM2().transpose() * X);
                    field.setValue(i,j,k,res);
                }
            }
        }
    }
}

void continuousQsmSegment::toString()
{
    std::cout<<"    houghSphere radius : "<<radius<<"\t| center : "<<center.x<<" "<<center.y<<" "<<center.z<<"\t| nb pts : "<<pts.size()<<"\t| bb minZ "<<BBMin.z<<" maxZ "<<BBMax.z<<std::endl;
}
