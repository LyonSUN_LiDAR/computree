#ifndef DEM_HOUGH_H
#define DEM_HOUGH_H

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"

#include "itemhoughdrawmanager.h"

#include "qsm/hough.h"

class itemHough : public CT_AbstractItemDrawableWithoutPointCloud
{
    Q_OBJECT

public:
    itemHough();
    itemHough(const CT_OutAbstractModel *model, const CT_AbstractResult *result, hough *h);
    itemHough(const QString &modelName, const CT_AbstractResult *result,  hough *h);

    ~itemHough();

    /**
      * ATTENTION : ne pas oublier de redéfinir ces deux méthodes si vous hérité de cette classe.
      */
    virtual QString getType() const;
    static QString staticGetType();

    const hough& getHough() const;

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList);
private:

    const static itemHoughDrawManager  dem_hough_DRAW_MANAGER;

protected:

    hough h;
};

#endif // DEM_HOUGH_H
