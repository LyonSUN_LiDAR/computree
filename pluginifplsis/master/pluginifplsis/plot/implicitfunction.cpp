/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "implicitfunction.h"

void implicitFunction::polygonizeLevelSet(int nx, int ny, int nz, float isoLevel)
{
    pcl::PointXYZ min = getBBmin();
    pcl::PointXYZ max = getBBmax();

    float sizeCellX = ((max.x-min.x))/(float)nx;
    float sizeCellY = ((max.y-min.y))/(float)ny;
    float sizeCellZ = ((max.z-min.z))/(float)nz;

    clock_t t = clock();

    scalarField field(min,max,(nx+1),(ny+1),(nz+1));
    fillScalarField(field);

    t = clock() - t;
    std::cout<<"        Scalar field in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;

    surface = isoSurface(&field, isoLevel,nx,ny,nz,sizeCellX,sizeCellY,sizeCellZ,min);
    surface.generateSurface();

    t = clock() - t;
    std::cout<<"        Levelset extraction in "<<((float)t)/CLOCKS_PER_SEC<<" s"<<std::endl;
}

void implicitFunction::polygonizeLevelSet(double d, float isoLevel, bool polygonize)
{
    pcl::PointXYZ min = getBBmin();
    pcl::PointXYZ max = getBBmax();

    int nx = (int)((max.x-min.x)/d)+1;
    int ny = (int)((max.y-min.y)/d)+1;
    int nz = (int)((max.z-min.z)/d)+1;

    scalarField field(min,max,(nx+1),(ny+1),(nz+1));
    fillScalarField(field);

    long smax = nx*ny*nz;
    std::cout<<"        Scalar field size : "<<smax<<" ( "<<nx<<" x "<<ny<<" x "<<nz<<" ) - Size of map : "<<field.getSizeMap()<<std::endl;

    if(polygonize){
        surface = isoSurface(&field, isoLevel,nx,ny,nz,(float)d,(float)d,(float)d,min);
        surface.generateSurface();
    }
}

void implicitFunction::writeObj(std::string file)
{
    //surface.writeObj(file);
}
