#ifndef TOOLS_H
#define TOOLS_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QList>
#include <QDebug>

class Tools : public QObject
{
    Q_OBJECT
public:

    static QString parseLine(QString line, QMultiMap<QString, QString> &lineTable)
    {

        QString firstElement;
        bool first = true;

        QStringList elements = line.split(QRegExp("[;#]"), QString::SkipEmptyParts);

        for (int  i = 0 ; i < elements.size(); i++)
        {
            QString element = elements.at(i);
            element = element.trimmed();

            QStringList pairStr = element.split(QRegExp("="), QString::KeepEmptyParts);

            if (pairStr.size() > 1)
            {
                QString key = pairStr.at(0).trimmed().toLower();
                QString value = pairStr.at(1).trimmed();

                if (first) {firstElement = key;first = false;}

                lineTable.insert(key, value);
            } else if (pairStr.size() == 1)
            {
                QString key = "val";
                QString value = pairStr.at(0).trimmed();

                lineTable.insert(key, value);
            }
        }

        return firstElement;
    }

};

#endif // TOOLS_H
