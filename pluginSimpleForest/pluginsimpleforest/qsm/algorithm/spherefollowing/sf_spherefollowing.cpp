/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_spherefollowing.h"

#include "qsm/algorithm/distance/sf_cloudToModelDistance.h"
#include "qsm/algorithm/postprocessing/sf_removefalseconnections.h"
#include "qsm/build/sf_buildQSM.h"

#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <tests/factory/sf_qsmfactory.h>

SF_SphereFollowing::SF_SphereFollowing()
{
  m_numberOfMedian = 0;
  m_numberOfRansacIterations = 0;
}

void
SF_SphereFollowing::artificialTree()
{
  m_qsm = SF_QSMFactory::qsm(SORTTYPE::LARGEST_GROWTHLENGTH_SECOND);
}

void
SF_SphereFollowing::buildTree()
{
  if (m_cylinders.size() > 2) {
    SF_BuildQSM qsmBuilder(std::move(m_cylinders), 0);
    m_qsm = qsmBuilder.getTree();
  } else {
    throw std::runtime_error("SphereFollowing did not detect cylinders.");
    failedComputation = true;
  }
}

long long
SF_SphereFollowing::getNumberOfRansacIterations() const
{
  return m_numberOfRansacIterations;
}

long long
SF_SphereFollowing::getNumberOfMedian() const
{
  return m_numberOfMedian;
}

void
SF_SphereFollowing::compute()
{
  if (m_cloud->empty()) {
    failedComputation = true;
    throw std::runtime_error("SphereFollowing received an empty cloud.");
  }
  initialize();
  while (!m_map.empty()) {
    Circle circleStruct = (*m_map.begin()).second;
    m_map.erase(m_map.begin());
    pcl::PointIndices::Ptr surfaceIndicesVector = surfaceIndices(circleStruct);
    SF_CloudNormal::Ptr surface = extractCloud(surfaceIndicesVector);
    std::vector<SF_CloudNormal::Ptr> surfaceClusters = clusterEuclidean(surface, circleStruct.m_clusterIndex);
    processClusters(surfaceClusters, circleStruct);
  }
  buildTree();
  SF_RemoveFalseConnections rfc;
  rfc.compute(m_qsm, m_params.m_overLapAngle);
}

float
SF_SphereFollowing::error()
{
  if (failedComputation) {
    return std::numeric_limits<float>::max();
  }
  Sf_CloudToModelDistance<SF_PointNormal> cmd(m_qsm, m_cloud, m_params._distanceParams);
  return cmd.getAverageDistance();
}

pcl::PointIndices::Ptr
SF_SphereFollowing::surfaceIndices(Circle& lastCircle)
{
  const pcl::ModelCoefficients& lastCircleCoeff = lastCircle.m_circleCoeff;
  int index = lastCircle.m_clusterIndex;
  pcl::PointIndices::Ptr surface(new pcl::PointIndices);
  SF_PointNormal center;
  center.x = lastCircleCoeff.values[0];
  center.y = lastCircleCoeff.values[1];
  center.z = lastCircleCoeff.values[2];
  std::vector<int> pointIdxRadiusSearch;
  std::vector<float> pointRadiusSquaredDistance;

  float radius = std::max(static_cast<double>(m_params._sphereFollowingParams._minGlobalRadius),
                          m_params._sphereFollowingParams.m_optimizationParams[index]._sphereRadiusMultiplier *
                            lastCircleCoeff.values[3]);
  const auto epsilonSphere = m_params._sphereFollowingParams.m_optimizationParams[index]._epsilonSphere;
  const float maxRadius = radius + epsilonSphere;
  const float minRadius = radius - epsilonSphere;
  const float minRadiusSqrd = minRadius * minRadius;

  if (m_octree->radiusSearch(center, maxRadius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0) {
    for (size_t i = 0; i < pointIdxRadiusSearch.size(); ++i) {
      auto pointIndex = pointIdxRadiusSearch[i];
      const SF_PointNormal& point = m_cloud->points[pointIndex];
      if (pointRadiusSquaredDistance[i] > minRadiusSqrd) {
        surface->indices.push_back(pointIndex);
      }
      m_octree->deleteVoxelAtPoint(point);
    }
  }
  return surface;
}

SF_CloudNormal::Ptr
SF_SphereFollowing::extractCloud(pcl::PointIndices::Ptr& indices)
{
  SF_CloudNormal::Ptr cloud(new SF_CloudNormal);
  pcl::ExtractIndices<SF_PointNormal> extract;
  extract.setInputCloud(m_cloud);
  extract.setIndices(indices);
  extract.setNegative(false);
  extract.filter(*cloud);
  return cloud;
}

std::vector<SF_CloudNormal::Ptr>
SF_SphereFollowing::clusterEuclidean(pcl::PointCloud<pcl::PointXYZINormal>::Ptr& cloud, int index)
{
  std::vector<SF_CloudNormal::Ptr> clusters;
  std::vector<pcl::PointIndices> clusterIndices;
  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>);
  tree->setInputCloud(cloud);

  pcl::EuclideanClusterExtraction<SF_PointNormal> ec;
  ec.setClusterTolerance(m_params._sphereFollowingParams.m_optimizationParams[index]._euclideanClusteringDistance);
  ec.setMinClusterSize(m_params._sphereFollowingParams._minPtsGeometry);
  ec.setMaxClusterSize(std::numeric_limits<int>::max());
  ec.setSearchMethod(tree);
  ec.setInputCloud(cloud);
  ec.extract(clusterIndices);
  for (std::vector<pcl::PointIndices>::const_iterator it = clusterIndices.begin(); it != clusterIndices.end(); ++it) {
    SF_CloudNormal::Ptr cloudCluster(new SF_CloudNormal);
    for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
      cloudCluster->points.push_back(cloud->points[*pit]);
    if (cloudCluster->points.size() >= static_cast<size_t>(m_params._sphereFollowingParams._minPtsGeometry)) {
      cloudCluster->width = cloudCluster->points.size();
      cloudCluster->height = 1;
      cloudCluster->is_dense = true;
      size_t minClusterIndex = std::numeric_limits<size_t>::max();
      for (size_t i = 0; i < cloudCluster->points.size(); i++) {
        if (cloudCluster->points[i].intensity < minClusterIndex) {
          minClusterIndex = cloudCluster->points[i].intensity;
        }
      }
      cloudCluster->points[0].intensity = minClusterIndex;
      clusters.push_back(cloudCluster);
    }
  }
  return clusters;
}

Eigen::Vector3f
SF_SphereFollowing::getCentroid(pcl::PointCloud<pcl::PointXYZINormal>::Ptr& cloud)
{
  Eigen::Vector4f centroidPoint;
  pcl::compute3DCentroid(*cloud, centroidPoint);
  return centroidPoint.head<3>();
}

float
SF_SphereFollowing::getDistance(const pcl::ModelCoefficients& circle, const Circle& lastCircle)
{
  Eigen::Vector3f diff(lastCircle.m_circleCoeff.values[0] - circle.values[0],
                       lastCircle.m_circleCoeff.values[1] - circle.values[1],
                       lastCircle.m_circleCoeff.values[2] - circle.values[2]);
  return diff.norm();
}

void
SF_SphereFollowing::processClusters(std::vector<SF_CloudNormal::Ptr>& clusters, const Circle& lastCircle)
{
  SF_ParamSpherefollowingBasic<SF_PointNormal> params(m_params);
  Eigen::Vector3f oldSphereCenter(
    lastCircle.m_circleCoeff.values[0], lastCircle.m_circleCoeff.values[1], lastCircle.m_circleCoeff.values[2]);

  if (lastCircle.m_firstSplit != oldSphereCenter && clusters.size() > 1) {
    for (SF_CloudNormal::Ptr& cluster : clusters) {
      Eigen::Vector3f centroid = getCentroid(cluster);
      const auto angle = SF_Math<float>::getAngleBetweenDegf(centroid - oldSphereCenter, oldSphereCenter - lastCircle.m_firstSplit);
      cluster->points[0].curvature = angle;
    }
    std::sort(clusters.begin(), clusters.end(), [](const SF_CloudNormal::Ptr& cloud1, const SF_CloudNormal::Ptr& cloud2) {
      return cloud1->points[0].curvature < cloud2->points[0].curvature;
    });
  }
  for (SF_CloudNormal::Ptr& cluster : clusters) {
    auto intensity = cluster->points[0].intensity;
    ++m_numberOfMedian;
    m_circleFit->compute(cluster);
    pcl::ModelCoefficients coeff = m_circleFit->coeff();
    m_numberOfRansacIterations += m_circleFit->maxIterations();
    if (coeff.values.size() == 4) {
      float heapDistance = lastCircle.m_distance + getDistance(coeff, lastCircle);
      if (cluster != *clusters.begin()) {
        heapDistance += params._sphereFollowingParams._heapDelta;
        pushbackQueue(coeff, heapDistance, intensity, Eigen::Vector3f(coeff.values[0], coeff.values[1], coeff.values[2]));
      } else {
        pushbackQueue(coeff, heapDistance, intensity, lastCircle.m_firstSplit);
      }
      SF_QSMDetectionCylinder cyl(heapDistance, lastCircle.m_circleCoeff);
      cyl.addSecondCircle(std::move(coeff));
      m_cylinders.push_back(cyl);
    }
  }
}

void
SF_SphereFollowing::initialize()
{
  initializeOctree();
  initializeCirlceFit();
  initializeHeap();
}

void
SF_SphereFollowing::initializeCirlceFit()
{
  m_circleFit.reset(new SF_Circle<SF_PointNormal>(m_params._sphereFollowingParams));
}

void
SF_SphereFollowing::initializeOctree()
{
  m_octree.reset(new pcl::octree::OctreePointCloudSearch<SF_PointNormal>(m_params._voxelSize));
  m_octree->setInputCloud(m_cloud);
  m_octree->addPointsFromInputCloud();
}

void
SF_SphereFollowing::initializeHeap()
{
  SF_CloudNormal::Ptr lowestSliceCloud = lowestSlice();
  if (lowestSliceCloud->points.empty()) {
    return;
  }
  ++m_numberOfMedian;
  m_circleFit->compute(lowestSliceCloud);
  m_numberOfRansacIterations += static_cast<long long>(m_circleFit->maxIterations());
  pushbackQueue(m_circleFit->coeff(),
                0.0f,
                0,
                Eigen::Vector3f(m_circleFit->coeff().values[0], m_circleFit->coeff().values[1], m_circleFit->coeff().values[2]));
}

void
SF_SphereFollowing::pushbackQueue(pcl::ModelCoefficients circleCoeff, float distance, int clusterID, Eigen::Vector3f firstSplit)
{
  m_map[distance] = Circle(circleCoeff, distance, clusterID, firstSplit);
}

SF_CloudNormal::Ptr
SF_SphereFollowing::lowestSlice()
{
  const auto minZIt = std::min_element(m_cloud->points.cbegin(),
                                       m_cloud->points.cend(),
                                       [](const SF_PointNormal& point1, const SF_PointNormal& point2) { return point1.z < point2.z; });
  const auto minZ = (*minZIt).z;
  const auto maxZ = minZ + m_params._sphereFollowingParams._heightInitializationSlice;

  SF_CloudNormal::Ptr lowestSlice(new SF_CloudNormal);
  pcl::ConditionAnd<SF_PointNormal>::Ptr rangeCond(new pcl::ConditionAnd<SF_PointNormal>());
  rangeCond->addComparison(
    pcl::FieldComparison<SF_PointNormal>::ConstPtr(new pcl::FieldComparison<SF_PointNormal>("z", pcl::ComparisonOps::GT, minZ)));
  rangeCond->addComparison(
    pcl::FieldComparison<SF_PointNormal>::ConstPtr(new pcl::FieldComparison<SF_PointNormal>("z", pcl::ComparisonOps::LT, maxZ)));
  pcl::ConditionalRemoval<SF_PointNormal> condrem;
  condrem.setCondition(rangeCond);
  condrem.setInputCloud(m_cloud);
  condrem.setKeepOrganized(false);
  condrem.filter(*lowestSlice);
  std::for_each(lowestSlice->points.begin(), lowestSlice->points.end(), [minZ](SF_PointNormal& point) { point.z = minZ; });
  return lowestSlice;
}

/********************************** Getters and Setters *******************************************/
/********************************** Getters and Setters *******************************************/
/********************************** Getters and Setters *******************************************/

std::shared_ptr<SF_ModelQSM>
SF_SphereFollowing::getQSM()
{
  return m_qsm;
}

void
SF_SphereFollowing::setParams(const SF_ParamSpherefollowingBasic<SF_PointNormal>& params)
{
  m_params = params;
}

SF_CloudNormal::Ptr
SF_SphereFollowing::cloud() const
{
  return m_cloud;
}

void
SF_SphereFollowing::setCloud(const SF_CloudNormal::Ptr& cloud)
{
  m_cloud = cloud;
}
