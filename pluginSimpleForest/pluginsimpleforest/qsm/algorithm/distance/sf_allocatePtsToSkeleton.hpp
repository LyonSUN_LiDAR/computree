/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_ALLOCATEPTSTOSKELETON_HPP
#define SF_ALLOCATEPTSTOSKELETON_HPP

#include "sf_allocatePtsToSkeleton.h"

template<typename PointType>
SF_AllocatePointsToSkeleton<PointType>::SF_AllocatePointsToSkeleton(std::shared_ptr<SF_ModelQSM> qsm,
                                                                    SF_CloudToModelDistanceParameters& params,
                                                                    typename pcl::PointCloud<PointType>::Ptr cloud)
  : m_qsm(qsm), m_params(params), m_cloud(cloud)
{
  initialize();
  compute();
}

template<typename PointType>
void
SF_AllocatePointsToSkeleton<PointType>::initialize()
{
  m_buildingBricks = m_qsm->getBuildingBricks();
  const auto size = static_cast<int>(m_buildingBricks.size());
  for (int i = 0; i < size; i++) {
    typename pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>);
    m_brickClusters.push_back(cloud);
  }
  m_qsmSearch.reset(new SF_QSMSearchKdtree<PointType>(m_qsm, m_params));
}

template<typename PointType>
void
SF_AllocatePointsToSkeleton<PointType>::compute()
{
  for (size_t i = 0; i < m_cloud->points.size(); i++) {
    PointType point = m_cloud->points[i];
    std::pair<double, int> distancePair = m_qsmSearch->distance(point);
    int index = distancePair.second;
    double distance = distancePair.first;
    if (index >= 0 && distance < 1.) {
      m_brickClusters[index]->points.push_back(point);
    }
  }
}

template<typename PointType>
std::vector<typename pcl::PointCloud<PointType>::Ptr>
SF_AllocatePointsToSkeleton<PointType>::brickClusters() const
{
  return m_brickClusters;
}

#endif // SF_ALLOCATEPTSTOSKELETON_HPP
