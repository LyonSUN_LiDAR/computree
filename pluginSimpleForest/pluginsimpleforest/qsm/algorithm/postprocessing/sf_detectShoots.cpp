/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_detectShoots.h"

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_DetectShoots::getFirstOrderShoots() const
{
  return firstOrderShoots;
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_DetectShoots::getSecondOrderShoots() const
{
  return secondOrderShoots;
}

SF_DetectShoots::SF_DetectShoots() {}

void
SF_DetectShoots::compute(std::shared_ptr<SF_ModelQSM> qsm)
{
  SF_DetectCrown detectCrown;
  std::shared_ptr<SF_ModelSegment> crown = detectCrown.compute(qsm);
  std::shared_ptr<SF_ModelSegment> stemCylinder = qsm->getRootSegment();
  while (stemCylinder != crown) {
    std::vector<std::shared_ptr<SF_ModelSegment>> children = stemCylinder->getChildren();
    for (size_t i = 1; i < children.size(); ++i) {
      std::shared_ptr<SF_ModelSegment> child = children.at(i);
      std::vector<std::shared_ptr<SF_ModelSegment>> grandChildren = child->getChildren();
      if (grandChildren.size() == 0) {
        firstOrderShoots.push_back(child);
      } else {
        bool isSecondOrder = true;
        for (auto grandChild : grandChildren) {
          if (grandChild->getChildren().size() > 0) {
            isSecondOrder = false;
          }
        }
        if (isSecondOrder) {
          secondOrderShoots.push_back(child);
        }
      }
    }
    stemCylinder = children.at(0);
  }
}
