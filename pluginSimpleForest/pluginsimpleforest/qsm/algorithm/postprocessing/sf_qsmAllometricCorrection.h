/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMALLOMETRYCHECK_H
#define SF_QSMALLOMETRYCHECK_H

#include "qsm/algorithm/sf_QSMCylinder.h"
#include "qsm/model/sf_modelQSM.h"
#include "steps/param/sf_paramAllSteps.h"

class SF_QSMAllometricCorrection
{
  SF_ParamAllometricCorrectionNeighboring m_params;
  void correct(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& cylinder, float minStemZ);
  bool inRange(float model, float predicted, float range);

public:
  SF_QSMAllometricCorrection() = default;
  void compute();
  void setParams(const SF_ParamAllometricCorrectionNeighboring& params);
  SF_ParamAllometricCorrectionNeighboring params() const;
};

#endif // SF_QSMALLOMETRYCHECK_H
