/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_MODEL_TREE_H
#define SF_MODEL_TREE_H

#include "sf_modelSegment.h"
#include <pcl/common/transforms.h>

class SF_ModelQSM : public std::enable_shared_from_this<SF_ModelQSM>
{
  int m_id;
  bool m_hasCorrectedParameters = false;
  bool m_allometryDirty = true;

  double m_aGrowthLength = 0.01;
  double m_bGrowthLength = 1 / 2.49;
  double m_cGrowthLength = 0;
  double m_aGrowthVolume = 0.01;
  double m_bGrowthVolume = 1 / 2.49;
  double m_cGrowthVolume = 0;
  double m_volumeCorrection = 0;

  Eigen::Vector3d m_translation;
  std::string m_species;
  std::shared_ptr<SF_ModelSegment> m_rootSegment;

  void setBranchorder();
  void setSegmentId();
  void setBuildingBrickId();
  void setReverseBranchOrder();
  void setReverseSummedBranchOrder();
  void setBranchId();

public:
  SF_ModelQSM() = default;
  SF_ModelQSM(const int Id);
  std::shared_ptr<SF_ModelQSM> clone();

  virtual std::string toString();
  virtual std::string toHeaderString();

  void translate(const Eigen::Vector3d& translation);
  void transform(const Eigen::Affine3f& transform);

  Eigen::Vector3d translateToOrigin();
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> getRootBuildingBrick() const;
  std::shared_ptr<SF_ModelSegment> crownStartSegment() const;
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> crownStartBrick() const;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> stemBricks() const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getSegments() const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getSegments(const std::shared_ptr<SF_ModelSegment>& segment) const;
  std::vector<std::shared_ptr<SF_ModelSegment>> getLeaveSegments() const;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> getBuildingBricks() const;
  std::vector<std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>> getBranchBuildingBricks() const;
  std::shared_ptr<SF_ModelSegment> getRootSegment() const;
  void setRootSegment(const std::shared_ptr<SF_ModelSegment>& rootSegment);
  double getVolume() const;
  void sort(SF_ModelSegment::SF_SORTTYPE type);
  bool getHasCorrectedParameters() const;
  void setHasCorrectedParameters(bool hasCorrectedParameters);
  double getAGrowthLength() const;
  void setA(double aGrowthLength);
  double getBGrowthLength() const;
  void setB(double bGrowthLength);
  double getCGrowthLength() const;
  void setC(double cGrowthLength);
  double getAGrowthVolume() const;
  void setAGrowthVolume(double aGrowthVolume);
  double getBGrowthVolume() const;
  void setBGrowthVolume(double bGrowthVolume);
  double getCGrowthVolume() const;
  void setCGrowthVolume(double cGrowthVolume);
  int getID() const;
  void setID(int Id);
  void setTranslation(const Eigen::Vector3d& translation);
  void setSpecies(const std::string& species);
  void setAllometryDirty(bool flag);
  bool getAllometryDirty() const;
};

#endif // SF_MODEL_TREE_H
