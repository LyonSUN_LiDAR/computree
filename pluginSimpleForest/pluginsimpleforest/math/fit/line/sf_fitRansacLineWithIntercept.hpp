/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITRANSACLINEWITHINTERCEPT_HPP
#define SF_FITRANSACLINEWITHINTERCEPT_HPP

#include "sf_fitRansacLineWithIntercept.h"

template<typename T>
SF_FitRansacLineWithIntercept<T>::SF_FitRansacLineWithIntercept()
{}

template<typename T>
void
SF_FitRansacLineWithIntercept<T>::compute()
{
  if (SF_FitRansacLine<T>::m_x.size() != SF_FitRansacLine<T>::m_y.size() || SF_FitRansacLine<T>::m_x.size() < 2) {
    throw("Point vectors not well formated in Ransac line fit.");
  }
  // we use a fixed seed here to assure results are reproducable.
  std::mt19937 rng(197666);
  std::uniform_int_distribution<std::mt19937::result_type> dist(0, SF_FitRansacLine<T>::m_x.size() - 1);
  size_t currentMaxNumberInliers = 0;
  for (size_t i = 0; i < SF_FitRansacLine<T>::m_iterations; i++) {
    size_t index1 = dist(rng);
    auto equation = getEquation(index1);
    size_t currentNumberInliers = SF_FitRansacLine<T>::numberInliers(equation);
    if (currentNumberInliers > currentMaxNumberInliers) {
      currentMaxNumberInliers = currentNumberInliers;
      SF_FitRansacLine<T>::m_equation = equation;
    }
  }
  if (currentMaxNumberInliers < SF_FitRansacLine<T>::m_minPts) {
    throw("RANSAC fit did not return enough inliers.");
  }
}

template<typename T>
std::pair<T, T>
SF_FitRansacLineWithIntercept<T>::getEquation(size_t index1)
{
  T x1 = static_cast<T>(0);
  T y1 = m_intercept;
  T x2 = SF_FitRansacLine<T>::m_x[index1];
  T y2 = SF_FitRansacLine<T>::m_y[index1];
  if (x2 == x1) {
    return SF_FitRansacLine<T>::m_errorEquation;
  }
  T slope = (y2 - y1) / (x2 - x1);
  T intercept = y2 - (slope * x2);
  typename std::pair<T, T> result;
  result.first = slope;
  result.second = intercept;
  return result;
}

template<typename T>
void
SF_FitRansacLineWithIntercept<T>::setIntercept(const T& intercept)
{
  m_intercept = intercept;
}

#endif // SF_FITRANSACLINEWITHINTERCEPT_HPP
