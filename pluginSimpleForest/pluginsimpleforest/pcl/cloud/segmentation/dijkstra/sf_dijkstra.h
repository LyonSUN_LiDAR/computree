/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DIJKSTRA_H
#define SF_DIJKSTRA_H

#include <boost/heap/fibonacci_heap.hpp>
#include <pcl/kdtree/kdtree.h>
#include <pcl/octree/octree.h>
#include <pcl/sf_point.h>
#include <steps/param/sf_paramAllSteps.h>

struct heapData;

struct Point
{
public:
  SF_PointNormal _point;
  float _distance;
  bool _visited;
  Point() {}
  Point(SF_PointNormal point, float distance) : _point(point), _distance(distance), _visited(false) {}
};

using Heap = boost::heap::fibonacci_heap<heapData>;

struct heapData
{
  Point _point;
  Heap::handle_type handle;
  heapData(Point point) : _point(point), handle() {}
  bool operator<(heapData const& second) const { return _point._distance > second._point._distance; }
};

class SF_Dijkstra
{
private:
  const float MAXDISTANCE = 1000;
  float _maxDistance;

  Heap _priorityQueue;
  std::vector<Heap::handle_type> _handle;
  std::vector<Point> _points;
  SF_CloudNormal::Ptr _cloudIn;
  SF_CloudNormal::Ptr _cloudInSeeds;
  float _range;
  bool m_useFixDistance = false;
  pcl::KdTreeFLANN<SF_PointNormal>::Ptr _kdtree;
  std::vector<float> _distances;
  std::vector<int> _parentIndices;

  void initialize();
  void initializeHeap();
  void initializeKDTree();
  void transferIntensity();
  int getIndex(const SF_PointNormal& point);
  float getDistance(const SF_PointNormal& p1, const SF_PointNormal& p2);
  std::vector<int> getNeighbors(const SF_PointNormal& point);
  void compute();

public:
  SF_Dijkstra(SF_CloudNormal::Ptr cloudIn, SF_CloudNormal::Ptr cloudInSeeds, float range, bool useFixedDistance = false);
  std::vector<float> getDistances() const;
  float getMaxDistance() const;
  std::vector<int> getParentIndices() const;
  SF_CloudNormal::Ptr getCloudIn() const;
};

#endif // SF_DIJKSTRA_H
