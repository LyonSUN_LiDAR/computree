/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepQSMCorrectShoots.h"

#include "qsm/algorithm/postprocessing/sf_detectShoots.h"

SF_StepQSMCorrectShoots::SF_StepQSMCorrectShoots(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepQSMCorrectShoots::~SF_StepQSMCorrectShoots() {}

QString
SF_StepQSMCorrectShoots::getStepDescription() const
{
  return tr("QSM Correct Shoots");
}

QString
SF_StepQSMCorrectShoots::getStepDetailledDescription() const
{
  return tr("Epicormic shoots are problematic for the allometric correction. Those are in general hugely overesimtated."
            " The algorithm searches from the root to the crown along the stem for shoots and corrects shoot cylinders"
            " with a user given radius.");
}

QString
SF_StepQSMCorrectShoots::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepQSMCorrectShoots::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepQSMCorrectShoots(dataInit);
}

QStringList
SF_StepQSMCorrectShoots::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  return _risCitationList;
}

void
SF_StepQSMCorrectShoots::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>Shoot correction</b>:");

  configDialog->addDouble("Use as correction radius [<em><b>correction radius</b></em>] ", " (m). ", 0.001, 0.01, 3, m_radius);

  configDialog->addBool("To account to model errors also count as a shoot branches which split one time ",
                        "",
                        " apply to second order branches also ",
                        m_correctSecondOrder,
                        ".");

  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepQSMCorrectShoots::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Input result Step Correct Shoots"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("internal QSM"));
}

void
SF_StepQSMCorrectShoots::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString name = tr("Shoot corrected ");
    QString sfCylinders = name;
    sfCylinders.append(tr("QSM"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepQSMCorrectShoots::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  CT_ResultGroupIterator outResItCloud(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResItCloud.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud.next();
    const SF_QSMItem* QSM_Item = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    auto qsm = QSM_Item->getQsm()->clone();
    SF_DetectShoots detectShoots;
    detectShoots.compute(qsm);
    std::vector<std::shared_ptr<SF_ModelSegment>> firstOrderShoots = detectShoots.getFirstOrderShoots();
    for (auto firstOrderShoot : firstOrderShoots) {
      auto bricks = firstOrderShoot->getBuildingBricks();
      for (auto brick : bricks) {
        brick->setRadius(m_radius, FittingType::SHOOTCORRECTION);
      }
    }
    if (!m_correctSecondOrder) {
      SF_ParamAllometricCorrectionNeighboring params;
      params._qsm = qsm;
      _paramList.append(std::move(params));
      continue;
    }
    std::vector<std::shared_ptr<SF_ModelSegment>> secondOrderShoots = detectShoots.getSecondOrderShoots();
    for (auto secondOrderShoot : secondOrderShoots) {
      auto firstOrderShoots = secondOrderShoot->getChildren();
      for (auto firstOrderShoot : firstOrderShoots) {
        auto bricks = firstOrderShoot->getBuildingBricks();
        for (auto brick : bricks) {
          brick->setRadius(m_radius, FittingType::SHOOTCORRECTION);
        }
      }
      double radius = std::sqrt(firstOrderShoots.size() * m_radius * m_radius);
      auto bricks = secondOrderShoot->getBuildingBricks();
      for (auto brick : bricks) {
        brick->setRadius(radius, FittingType::SHOOTCORRECTION);
      }
    }
    SF_ParamAllometricCorrectionNeighboring params;
    params._qsm = qsm;
    _paramList.append(std::move(params));
  }
  SF_AbstractStepQSM::addQSM<SF_ParamAllometricCorrectionNeighboring>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}

void
SF_StepQSMCorrectShoots::createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addText(
    QObject::tr("For For the introduction of using defined parameters in such correction and autoestimating the power parameter cite"),
    "Hackenberg, J.; Spiecker, H.; Calders, K.; Disney, M.; Raumonen, P.");
  configDialog->addText(" (2.2. Tree Modeling - Allometric improvement)",
                        "<em>SimpleTree - An Efficient Open Source Tool to "
                        "Build Tree Models from TLS Clouds.</em>");
  configDialog->addText("", "Forests <b>2015</b>, 6, 4245-4294.");
}
