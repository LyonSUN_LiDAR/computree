/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPDIJKSTRALIGHTRECURSIVE_H
#define SF_STEPDIJKSTRALIGHTRECURSIVE_H

#include "steps/qsm/modelling/sf_stepSpherefollowingBasic.h"

class SF_StepDijkstraLightRecursive : public SF_StepSpherefollowingBasic
{
  Q_OBJECT
public:
  SF_StepDijkstraLightRecursive(CT_StepInitializeData& dataInit);
  ~SF_StepDijkstraLightRecursive();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog);
  void createParamList();
  virtual void createPostConfigurationDialog();

  void configRecursion(CT_StepConfigurableDialog* configDialog);

  void compute();
  QList<SF_ParamSpherefollowingRecursive<SF_PointNormal>> _paramList;

  double m_clusteringDistance = 0.03;
  double m_unfittedDistance = 0.1;
  double m_minPercentage = 0.001;
  double m_maxPercentage = 1.; // TODO
  double m_clusterDownScale = 0.05;
};

#endif // SF_STEPDIJKSTRALIGHTRECURSIVE_H
