#include "lif_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

#include "step/lif_stepdtm.h"
#include "step/lif_steppitfilling.h"
#include "step/lif_steppitfilling02.h"
#include "step/lif_stepoutliersremoving.h"
#include "step/lif_stepptreesegmentation.h"

#include "metric/lif_metricvolume.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers

LIF_PluginManager::LIF_PluginManager() : CT_AbstractStepPlugin()
{
}

LIF_PluginManager::~LIF_PluginManager()
{
}

QString LIF_PluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin IGN-LIF for Computree\n"
           "AU  - Véga, Cédric\n"
           "PB  - Institut National de l'Information Géographique et Forestière, Laboratoire des Inventaires Forestiers\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-ign-lif/wiki\n"
           "ER  - \n";
}


bool LIF_PluginManager::loadGenericsStep()
{
    //addNewRastersStep<LIF_StepDTM>(CT_StepsMenu::LP_DEM);
    //addNewRastersStep<LIF_StepPitFilling>(CT_StepsMenu::LP_DEM);
    addNewRastersStep<LIF_StepPitFilling02>(CT_StepsMenu::LP_DEM);
    addNewRastersStep<LIF_StepOutliersRemoving>(CT_StepsMenu::LP_DEM);
    //addNewPointsStep<LIF_StepPTreeSegmentation>(CT_StepsMenu::LP_Crowns);

    return true;
}

bool LIF_PluginManager::loadMetrics()
{
    addNewMetric(new LIF_MetricVolume());
    return true;
}


bool LIF_PluginManager::loadOpenFileStep()
{
    return true;
}

bool LIF_PluginManager::loadCanBeAddedFirstStep()
{

    return true;
}

bool LIF_PluginManager::loadActions()
{

    return true;
}

bool LIF_PluginManager::loadExporters()
{

    return true;
}

bool LIF_PluginManager::loadReaders()
{

    return true;
}

