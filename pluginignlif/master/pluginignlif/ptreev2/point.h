#ifndef POINT_H
#define POINT_H

#include <QObject>

#include "Eigen/Core"

class Point
{
public:
    Point(int localIndex, size_t globalIndex, double x, double y, double z)
    {
        _localIndex = localIndex;
        _globalIndex = globalIndex;
        _coord(0) = x;
        _coord(1) = y;
        _coord(2) = z;
    }

    int _localIndex;
    size_t _globalIndex;
    Eigen::Vector3d _coord;

    static bool orderByDecreasingZ(Point* p1, Point* p2)
    {
        return p1->_coord(2) > p2->_coord(2);
    }

};


#endif // POINT_H
