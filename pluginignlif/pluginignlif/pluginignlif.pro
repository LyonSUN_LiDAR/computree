CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

CONFIG += c++17
MUST_USE_OPENCV = 1

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

COMPUTREE += ctlibio ctlibmetrics

include($${CT_PREFIX}/include_ct_library.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TARGET = plug_ignlif

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    lif_pluginentry.h \
    lif_pluginmanager.h \
    step/lif_stepdtm.h \
    step/lif_steppitfilling.h \
    step/lif_stepoutliersremoving.h \
#    ptree/arbre.h \
#    ptree/convexHull.h \
#    ptree/critere.h \
#    ptree/filter_segDyn.h \
#    ptree/foret.h \
#    ptree/param.h \
#    ptree/point.h \
#    ptree/resfusion.h \
#    ptree/segmentation.h \
#    ptree/sommet.h \
#    ptree/tools.h \
    step/lif_stepptreesegmentation.h \
    ptreev2/point.h \
    ptreev2/forest.h \
    ptreev2/tree.h \
    ptreev2/convexhull.h \
    ptreev2/param.h \
    ptreev2/segmentation.h \
    metric/lif_metricvolume.h \
    step/lif_steppitfilling02.h

SOURCES += lif_pluginentry.cpp \
    lif_pluginmanager.cpp \
    step/lif_stepdtm.cpp \
    step/lif_steppitfilling.cpp \
    step/lif_stepoutliersremoving.cpp \
#    ptree/arbre.cpp \
#    ptree/critere.cpp \
#    ptree/filter_segDyn.cpp \
#    ptree/foret.cpp \
#    ptree/param.cpp \
#    ptree/point.cpp \
#    ptree/resfusion.cpp \
#    ptree/segmentation.cpp \
#    ptree/sommet.cpp \
#    ptree/tools.cpp \
#    ptree/convexHull.cpp \
    step/lif_stepptreesegmentation.cpp \
    ptreev2/point.cpp \
    ptreev2/forest.cpp \
    ptreev2/tree.cpp \
    ptreev2/convexhull.cpp \
    ptreev2/param.cpp \
    ptreev2/segmentation.cpp \
    metric/lif_metricvolume.cpp \
    step/lif_steppitfilling02.cpp


TRANSLATIONS += languages/pluginignlif_en.ts \
                languages/pluginignlif_fr.ts
