#ifndef FORET_H
#define FORET_H

#include <stdlib.h>
#include "arbre.h"


class Foret
{
public:
    Foret();
    void setlisteArbres(std::vector<Arbre> &listeArbres);
    const std::vector<Arbre>& getlisteArbres() const;
    std::vector<Sommet> getListeSommets() const;
    void push_newTree(Arbre &tree);
    Arbre extractTree(int label) ;
    void replaceTree(Arbre &Tree,int label);
    void removeTree(int label);
    unsigned int getNbTrees();
    std::vector<int> getPointsLabels();
    void relabForest();

    void fusionArbres(Arbre &arbre_fort,Arbre &arbre_faible,double scorefusion);

    Point exrtractPoint(int indice);

    void clearForest();
    ~Foret();
protected :

    std::vector<Arbre> _listeArbres;

};

#endif // FORET_H
