/****************************************************************************
 Copyright (C) 2010-2017 IGN
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cédric Véga
              with some help of Maryem Fadidi and Alexandre Piboule

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/lif_metricvolume.h"

#include <QDebug>

#define checkAndSetValue(ATT, NAME, TYPE) if((value = group->firstValueByTagName(NAME)) == NULL) { return false; } else { ATT = value->value().value<TYPE>(); }


LIF_MetricVolume::LIF_MetricVolume() : CT_AbstractMetric_Raster()
{
    declareAttributes();
    _gap_threshold = 2.0;
}

LIF_MetricVolume::LIF_MetricVolume(const LIF_MetricVolume &other) : CT_AbstractMetric_Raster(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
    _gap_threshold = other._gap_threshold;
}

QString LIF_MetricVolume::getShortDescription() const
{
    return tr("Métriques volume");
}

QString LIF_MetricVolume::getDetailledDescription() const
{

    return tr("Les valeurs suivantes sont calculées :<br>"
              "- volume_in (m3)<br>"
              "- volume_in_gap (m3)<br>"
              "- volume_out (m3)<br>"
              "- volume_out_gap (m3)<br>"
              "- volume_tot (m3)<br>"
              "- volume_tot_gap (m3)<br>"
              "- surface_gap (m²)<br>"
              "- surface_NA (m²)<br>"
              "- h_max (m)<br>"
              "- h_mean (m)<br>"
              "- h_sd (m)<br>"
              "- rumple<br>"
              "- rumple_hmean<br>"
              );
}

CT_AbstractConfigurableWidget* LIF_MetricVolume::createConfigurationWidget()
{
    CT_GenericConfigurableWidget* configDialog = new CT_GenericConfigurableWidget();

    configDialog->addDouble(tr("Seuil de hauteur max pour les trouées"), "m", 0, 99999, 2, _gap_threshold);
    configDialog->addEmpty();

    addAllVaBToWidget(configDialog);

    return configDialog;
}

SettingsNodeGroup *LIF_MetricVolume::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractMetric_Raster::getAllSettings();
    SettingsNodeGroup *group = new SettingsNodeGroup("LIF_MetricVolume");

    group->addValue(new SettingsNodeValue("quantMin", _gap_threshold));

    return root;
}

bool LIF_MetricVolume::setAllSettings(const SettingsNodeGroup *settings)
{
    if(!CT_AbstractMetric_Raster::setAllSettings(settings))
        return false;

    SettingsNodeGroup *group = settings->firstGroupByTagName("LIF_MetricVolume");

    if(group == NULL)
        return false;

    SettingsNodeValue *value = NULL;
    checkAndSetValue(_gap_threshold, "quantMin", double)

            return true;
}



LIF_MetricVolume::Config LIF_MetricVolume::metricConfiguration() const
{
    return m_configAndResults;
}

void LIF_MetricVolume::setMetricConfiguration(const LIF_MetricVolume::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *LIF_MetricVolume::copy() const
{
    return new LIF_MetricVolume(*this);
}


void LIF_MetricVolume::computeMetric()
{
    m_configAndResults.volume_in.value = 0;
    m_configAndResults.volume_in_gap.value = 0;
    m_configAndResults.volume_out.value = 0;
    m_configAndResults.volume_out_gap.value = 0;
    m_configAndResults.volume_tot.value = 0;
    m_configAndResults.volume_tot_gap.value = 0;
    m_configAndResults.surface_gap.value = 0;
    m_configAndResults.surface_NA.value = 0;
    m_configAndResults.h_max.value = -std::numeric_limits<double>::max();
    m_configAndResults.h_mean.value = 0;
    m_configAndResults.h_sd.value = 0;
    m_configAndResults.rumple.value = 0;
    m_configAndResults.rumple_hmean.value = 0;

    double n_nogap = 0.0;
    double n_gap = 0.0;
    double sdsum = 0.0;
    double inNA = _inRaster->NAAsDouble();

    for (size_t index = 0 ; index < _inRaster->nCells() ; index++)
    {
        double val = _inRaster->valueAtIndexAsDouble(index);
        size_t xx, yy;

        if (_inRaster->indexToGrid(index, xx, yy) && val != inNA)
        {
            // Hauteurs
            if (val > m_configAndResults.h_max.value) {m_configAndResults.h_max.value = val;}

            if (val >= _gap_threshold)
            {
                m_configAndResults.h_mean.value += val;
                sdsum += val*val;

                // Volume
                m_configAndResults.volume_in.value += val * _inRaster->resolution() * _inRaster->resolution();

                // Slope
                double slope = computeSlope(val, yy, xx, inNA);

                // Rumple
                n_nogap += 1.0;
                m_configAndResults.rumple.value += std::sqrt(pow(_inRaster->resolution()*slope, 2) + _inRaster->resolution()*_inRaster->resolution()) * _inRaster->resolution();
            } else {
                n_gap += 1.0;

                m_configAndResults.volume_in_gap.value += val * _inRaster->resolution() * _inRaster->resolution();
                m_configAndResults.surface_gap.value += _inRaster->resolution() * _inRaster->resolution();
            }
        } else {
            m_configAndResults.surface_NA.value += _inRaster->resolution()*_inRaster->resolution();
        }
    }

    // Slope
    if (n_nogap > 0)
    {
        m_configAndResults.rumple.value /= n_nogap*_inRaster->resolution()*_inRaster->resolution();
        m_configAndResults.h_mean.value /= n_nogap;
        m_configAndResults.h_sd.value = sqrt(sdsum/n_nogap - pow(m_configAndResults.h_mean.value, 2));
    }

    m_configAndResults.volume_tot.value = n_nogap *m_configAndResults.h_max.value*_inRaster->resolution()*_inRaster->resolution();
    m_configAndResults.volume_tot_gap.value = n_gap *m_configAndResults.h_max.value*_inRaster->resolution()*_inRaster->resolution();

    m_configAndResults.volume_out.value = m_configAndResults.volume_tot.value - m_configAndResults.volume_in.value;
    m_configAndResults.volume_out_gap.value = m_configAndResults.volume_tot_gap.value - m_configAndResults.volume_in_gap.value;


    double n_hmean = 0.0;
    for (size_t index = 0 ; index < _inRaster->nCells() ; index++)
    {
        double val = _inRaster->valueAtIndexAsDouble(index);
        size_t xx, yy;

        if (_inRaster->indexToGrid(index, xx, yy) && val != inNA && val >= m_configAndResults.h_mean.value)
        {
            // Slope
            double slope = computeSlope(val, yy, xx, inNA);

            // Rumple
            n_hmean += 1.0;
            m_configAndResults.rumple_hmean.value += std::sqrt(pow(_inRaster->resolution()*slope, 2) + _inRaster->resolution()*_inRaster->resolution()) * _inRaster->resolution();
        }
    }

    if (n_hmean > 0)
    {
        m_configAndResults.rumple_hmean.value /= n_hmean*_inRaster->resolution()*_inRaster->resolution();
    }


    setAttributeValueVaB(m_configAndResults.volume_in);
    setAttributeValueVaB(m_configAndResults.volume_in_gap);
    setAttributeValueVaB(m_configAndResults.volume_out);
    setAttributeValueVaB(m_configAndResults.volume_out_gap);
    setAttributeValueVaB(m_configAndResults.volume_tot);
    setAttributeValueVaB(m_configAndResults.volume_tot_gap);
    setAttributeValueVaB(m_configAndResults.surface_gap);
    setAttributeValueVaB(m_configAndResults.surface_NA);
    setAttributeValueVaB(m_configAndResults.h_max);
    setAttributeValueVaB(m_configAndResults.h_mean);
    setAttributeValueVaB(m_configAndResults.h_sd);
    setAttributeValueVaB(m_configAndResults.rumple);
    setAttributeValueVaB(m_configAndResults.rumple_hmean);
}

void LIF_MetricVolume::declareAttributes()
{
    registerAttributeVaB(m_configAndResults.volume_in, CT_AbstractCategory::DATA_NUMBER, tr("volume_in"));
    registerAttributeVaB(m_configAndResults.volume_in_gap, CT_AbstractCategory::DATA_NUMBER, tr("volume_in_gap"));
    registerAttributeVaB(m_configAndResults.volume_out, CT_AbstractCategory::DATA_NUMBER, tr("volume_out"));
    registerAttributeVaB(m_configAndResults.volume_out_gap, CT_AbstractCategory::DATA_NUMBER, tr("volume_out_gap"));
    registerAttributeVaB(m_configAndResults.volume_tot, CT_AbstractCategory::DATA_NUMBER, tr("volume_tot"));
    registerAttributeVaB(m_configAndResults.volume_tot_gap, CT_AbstractCategory::DATA_NUMBER, tr("volume_tot_gap"));
    registerAttributeVaB(m_configAndResults.surface_gap, CT_AbstractCategory::DATA_NUMBER, tr("surface_gap"));
    registerAttributeVaB(m_configAndResults.surface_NA, CT_AbstractCategory::DATA_NUMBER, tr("surface_NA"));
    registerAttributeVaB(m_configAndResults.h_max, CT_AbstractCategory::DATA_NUMBER, tr("h_max"));
    registerAttributeVaB(m_configAndResults.h_mean, CT_AbstractCategory::DATA_NUMBER, tr("h_mean"));
    registerAttributeVaB(m_configAndResults.h_sd, CT_AbstractCategory::DATA_NUMBER, tr("h_sd"));
    registerAttributeVaB(m_configAndResults.rumple, CT_AbstractCategory::DATA_NUMBER, tr("rumple"));
    registerAttributeVaB(m_configAndResults.rumple_hmean, CT_AbstractCategory::DATA_NUMBER, tr("rumple_hmean"));
}

double LIF_MetricVolume::computeSlope(double val, size_t yy, size_t xx, double inNA)
{
    size_t index2;
    double a = inNA;
    double b = inNA;
    double c = inNA;
    double d = inNA;
    double f = inNA;
    double g = inNA;
    double h = inNA;
    double i = inNA;

    if (_inRaster->index(xx - 1, yy - 1, index2))
    {a = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx    , yy - 1, index2))
    {b = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx + 1, yy - 1, index2))
    {c = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx - 1, yy    , index2))
    {d = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx + 1, yy    , index2))
    {f = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx - 1, yy + 1, index2))
    {g = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx    , yy + 1, index2))
    {h = _inRaster->valueAtIndexAsDouble(index2);}

    if (_inRaster->index(xx + 1, yy + 1, index2))
    {i = _inRaster->valueAtIndexAsDouble(index2);}

    if (a == inNA) {a = val;}
    if (b == inNA) {b = val;}
    if (c == inNA) {c = val;}
    if (d == inNA) {d = val;}
    if (f == inNA) {f = val;}
    if (g == inNA) {g = val;}
    if (h == inNA) {h = val;}
    if (i == inNA) {i = val;}

    double dzdx = ((c + 2.0*f + i) - (a + 2.0*d + g)) / (8.0 * _inRaster->resolution());
    double dzdy = ((g + 2.0*h + i) - (a + 2.0*b + c)) / (8.0 * _inRaster->resolution());
    double slope = std::sqrt(dzdx*dzdx + dzdy*dzdy);

    return slope;
}



