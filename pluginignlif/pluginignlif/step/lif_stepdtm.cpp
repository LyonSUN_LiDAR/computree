/****************************************************************************
 Copyright (C) 2016 the Institut National de l'Information Geographique et Forestière (IGN), France
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cedric Véga (IGN)
              Jules Morel (IFP / IGN)
              Alexandre Piboule (ONF - transfert in Computree)

 This file is part of PluginIGNLIF library.

 PluginIGNLIF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIGNLIF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginIGNLIF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "lif_stepdtm.h"


#include "ct_global/ct_context.h"

#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_itemdrawable/tools/image2dtools/ct_image2dnaturalneighboursinterpolator.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"


#include <QtConcurrent>
#include <math.h>
#include <stdlib.h>
#include <limits>

#define DEF_SearchInResult   "ires"
#define DEF_SearchInGroup   "igrp"
#define DEF_SearchInScene   "isc"

#define DEF_SearchOutResultVegetation "rv"
#define DEF_SearchOutResultMNT "rmnt"

#define DEF_SearchOutGroup  "gv"
#define DEF_SearchOutSceneVegetation  "scv"
#define DEF_SearchOutSceneSoil  "scs"
#define DEF_SearchOutMNT  "dtm"

#define DEF_SearchOutMNTtmp1  "dtm1"
#define DEF_SearchOutMNTtmp2  "dtm2"
#define DEF_SearchOutMNTtmp3  "dtm3"
#define DEF_SearchOutMNTtmp4  "dtm4"
#define DEF_SearchOutMNTtmp5  "dtm5"

#define DEF_SearchOutMNTmask1  "mask1"
#define DEF_SearchOutMNTmask2  "mask2"
#define DEF_SearchOutMNTmask3  "mask3"
#define DEF_SearchOutMNTmask4  "mask4"
#define DEF_SearchOutMNTmask5  "mask5"



LIF_StepDTM::LIF_StepDTM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _resolution   = 0.5;
    _thresholdLowPoints = 2.0;
    _threshold = 1.0;
    _thresholdFine = 0.2;
}

QString LIF_StepDTM::getStepDescription() const
{
    return tr("LIDAR Ground filtering and DTM computation");
}

QString LIF_StepDTM::getStepDetailledDescription() const
{
    return tr("TO DO");
}

CT_VirtualAbstractStep* LIF_StepDTM::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new LIF_StepDTM(dataInit);
}

/////////////////////// PROTECTED ///////////////////////

void LIF_StepDTM::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_SearchInResult, tr("Scène(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_Scene::staticGetType(), tr("Scène"));
}

void LIF_StepDTM::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Resolution of the DTM"), "m", 0.01, 100, 2, _resolution);
    configDialog->addDouble(tr("Threshold for excessive low zmin levels"), "m", 0.01, 100, 2, _thresholdLowPoints);
    configDialog->addDouble(tr("Threshold"), "m", 0.01, 100, 2, _threshold);
    configDialog->addDouble(tr("Threshold Fine"), "m", 0.01, 100, 2, _thresholdFine);
}

void LIF_StepDTM::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel;

    resultModel = createNewOutResultModel(DEF_SearchOutResultVegetation, tr("DTM"));
    resultModel->setRootGroup(DEF_SearchOutGroup);
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutSceneVegetation, new CT_Scene(), tr("Vegetation points"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutSceneSoil, new CT_Scene(), tr("Ground points"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNT, new CT_Image2D<float>(), tr("DTM"));

    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTtmp1, new CT_Image2D<float>(), tr("DTM tmp1 : ZMIN"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTtmp2, new CT_Image2D<float>(), tr("DTM tmp2 : low"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTtmp3, new CT_Image2D<float>(), tr("DTM tmp3 : end of step 2"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTtmp4, new CT_Image2D<float>(), tr("DTM tmp4 : end of step 3"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTtmp5, new CT_Image2D<float>(), tr("DTM tmp5 : interpol (begin step2)"));

    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTmask1, new CT_Image2D<quint8>(), tr("Mask 1: step2 base"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTmask2, new CT_Image2D<quint8>(), tr("Mask 2: step2 after hole filling"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTmask3, new CT_Image2D<quint8>(), tr("Mask 3: step2 after polygon filling"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTmask4, new CT_Image2D<quint8>(), tr("Mask 4: step2 after erosion"));
    resultModel->addItemModel(DEF_SearchOutGroup, DEF_SearchOutMNTmask5, new CT_Image2D<quint8>(), tr("Mask 5: step2 base"));
}

void LIF_StepDTM::compute()
{
    int ncellsForInterpNeigh = 15 / _resolution;

    // recupere le resultat d'entree
    CT_ResultGroup *inResult = getInputResults().first();

    // recupere les resultats de sortie
    const QList<CT_ResultGroup*> &outResList = getOutResultList();
    CT_ResultGroup *outResult = outResList.at(0);

    CT_ResultItemIterator it(inResult, this, DEF_SearchInScene);
    while (!isStopped() && it.hasNext())
    {
        const CT_Scene *scene = (CT_Scene*)it.next();

        if (scene != NULL)
        {
            CT_StandardItemGroup* grp = new CT_StandardItemGroup(DEF_SearchOutGroup, outResult);
            outResult->addGroup(grp);

            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();

            // DTM XY limits
            double minX = scene->minX();
            double minY = scene->minY();
            double maxX = scene->maxX();
            double maxY = scene->maxY();

            CT_Image2D<float>* dtm = CT_Image2D<float>::createImage2DFromXYCoords(DEF_SearchOutMNT, outResult, minX, minY, maxX, maxY, _resolution, 0, -9999, -9999);

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin step 1"));

            // Step 1
            // Create ZMIN raster
            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext() && !isStopped())
            {
                const CT_Point &point =itP.next().currentPoint();
                dtm->setMinValueAtCoords(point(0), point(1), point(2));
            }

            grp->addItemDrawable((CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNTtmp1, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference));


            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Zmin done"));

            // Set to NA excessive low cells
            for (size_t r = 0 ; r < dtm->linDim() ; r++)
            {
                for (size_t c = 0 ; c < dtm->colDim() ; c++)
                {
                    float mntVal = dtm->value(c, r);

                    if (mntVal != dtm->NA())
                    {
                        bool remove = true;
                        int cpt = 0;

                        for (int dr = -1 ; dr <= 1; dr++)
                        {
                            for (int dc = -1 ; dc <= 1; dc++)
                            {
                                if (dr != 0 || dc != 0)
                                {
                                    float neighbourVal = dtm->value(c + dc, r + dr);
                                    if (neighbourVal != dtm->NA())
                                    {
                                        cpt++;
                                        if ((neighbourVal - mntVal) < _thresholdLowPoints) {remove = false;}
                                    }
                                }
                            }
                        }

                        if (remove && cpt > 0) {dtm->setValue(c, r, dtm->NA());}
                    }
                }
            }

            grp->addItemDrawable((CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNTtmp2, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference));


            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Low values filtering done"));
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin step 2"));


            CT_Image2D<quint8>* mask1 = NULL;
            CT_Image2D<quint8>* mask2 = NULL;
            CT_Image2D<quint8>* mask3 = NULL;
            CT_Image2D<quint8>* mask4 = NULL;



            CT_Image2D<quint8>* cumulativeMask = CT_Image2D<quint8>::createImage2DFromXYCoords(DEF_SearchOutMNTmask5, outResult, minX, minY, maxX, maxY, _resolution, 0, 2, 0);

            CT_Image2D<float>* mntInterpol = NULL;

            int iterationNumber = 1;
            // Begin iterations for step 2
            int naNumber = 1;
            //while (naNumber > 0)
            while (iterationNumber < 2)
            {
                if (mask1 != NULL) {delete mask1;}
                if (mask2 != NULL) {delete mask2;}
                if (mask3 != NULL) {delete mask3;}
                if (mask4 != NULL) {delete mask4;}

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin new iteration (Step 2): %1").arg(iterationNumber++));

                // Interpolation
                // TODO : loop to make sure to interpolate all !
                mntInterpol = (CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNT, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);

                QList<size_t> NAindices;

                size_t index;
                for (size_t r = 0 ; r < dtm->linDim() ; r++)
                {
                    for (size_t c = 0 ; c < dtm->colDim() ; c++)
                    {
                        float value = dtm->value(c,r);
                        if (value == dtm->NA())
                        {
                            dtm->index(c, r, index);
                            NAindices.append(index);
                        }
                    }
                }

                QFuture<void> futur = QtConcurrent::map(NAindices, CT_Image2DNaturalNeighboursInterpolator(dtm, mntInterpol, ncellsForInterpNeigh));

                int progressMin = futur.progressMinimum();
                int progressTotal = futur.progressMaximum() - futur.progressMinimum();
                while (!futur.isFinished())
                {
                    setProgress(60.0*(futur.progressValue() - progressMin)/progressTotal + 20.0);
                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Interpolation done"));


                // Mask creation:  true for cell with height value above at least one neighbour (by _theshold)
                CT_Image2D<quint8>* mask = CT_Image2D<quint8>::createImage2DFromXYCoords(NULL, NULL, minX, minY, maxX, maxY, _resolution, 0, 2, 0);

                for (int r = 0 ; r < mntInterpol->linDim() ; r++)
                {
                    for (int c = 0 ; c < mntInterpol->colDim() ; c++)
                    {
                        float mntVal = mntInterpol->value(c, r);

                        if (mntVal != mntInterpol->NA())
                        {
                            bool maskValue = false;
                            for (int dr = -1 ; dr <= 1 && !maskValue; dr++)
                            {
                                for (int dc = -1 ; dc <= 1 && !maskValue; dc++)
                                {
                                    if (dr != 0 || dc != 0)
                                    {
                                        float neighbourVal = mntInterpol->value(c+dc, r+dr);
                                        if (neighbourVal != mntInterpol->NA() && (mntVal - neighbourVal) > _threshold) {maskValue = true;}
                                    }
                                }
                            }

                            if (maskValue) {mask->setValue(c, r, 1);}
                        }
                    }
                }
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Mask creation done"));

                mask1 = (CT_Image2D<quint8>*) mask->copy(DEF_SearchOutMNTmask1, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);


                // Mask holes filling
                CT_Image2D<quint8>* maskFilled = (CT_Image2D<quint8>*) mask->copy(NULL, NULL, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);

                for (int r = 0 ; r < mask->linDim() ; r++)
                {
                    for (int c = 0 ; c < mask->colDim() ; c++)
                    {
                        if (mask->value(c, r) == 0)
                        {
                            int cpt = 0;

                            for (int dr = -1 ; dr <= 1 ; dr++)
                            {
                                for (int dc = -1 ; dc <= 1 ; dc++)
                                {
                                    if (dr != 0 || dc != 0)
                                    {
                                        bool neighbourVal = mask->value(c + dc, r + dr);
                                        if (neighbourVal == 1) {cpt++;}
                                    }
                                }
                            }

                            if (cpt >= 5) {maskFilled->setValue(c, r, 1);}
                        }
                    }
                }
                delete mask;
                mask = maskFilled;

                mask2 = (CT_Image2D<quint8>*) mask->copy(DEF_SearchOutMNTmask2, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);


                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Hole filling done"));


                // Fill empty polygons in mask
                std::vector<std::vector<cv::Point> > contours2;
                cv::findContours(mask->getMat(), contours2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                cv::Scalar color(1);
                cv::drawContours(mask->getMat(), contours2, -1, color, -1);

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Polygons filling done"));


                mask3 = (CT_Image2D<quint8>*) mask->copy(DEF_SearchOutMNTmask3, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);

                // Erode mask
                bool changed = true;
                while (changed)
                {
                    changed = false;
                    for (int r = 0 ; r < mntInterpol->linDim() ; r++)
                    {
                        for (int c = 0 ; c < mntInterpol->colDim() ; c++)
                        {
                            float mntVal = mntInterpol->value(c, r);

                            if (mntVal != mntInterpol->NA() && mask->value(c, r) == 0)
                            {
                                for (int dr = -1 ; dr <= 1 ; dr++)
                                {
                                    for (int dc = -1 ; dc <= 1 ; dc++)
                                    {
                                        if (dr != 0 || dc != 0)
                                        {
                                            if (mask->value(c + dc, r + dr) == 1 && mntVal > mntInterpol->value(c + dc, r + dr))
                                            {
                                                mask->setValue(c + dc, r + dr, 0);
                                                changed = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                mask4 = (CT_Image2D<quint8>*) mask->copy(DEF_SearchOutMNTmask4, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Mask erosion done"));


                // Apply mask to DTM
                naNumber = 0;
                for (size_t r = 0 ; r < dtm->linDim() ; r++)
                {
                    for (size_t c = 0 ; c < dtm->colDim() ; c++)
                    {
                        if (mask->value(c, r) == 1 && cumulativeMask->value(c, r) == 0)
                        {
                            dtm->setValue(c, r, dtm->NA());
                            cumulativeMask->setValue(c, r, 1);
                            naNumber++;
                        }
                    }
                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Mask applied"));
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("naNumber = %1").arg(naNumber));


                //delete mntInterpol;
                delete mask;
            } // End of iteration in step 2

            grp->addItemDrawable((CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNTtmp3, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference));

            grp->addItemDrawable((CT_Image2D<float>*) mntInterpol->copy(DEF_SearchOutMNTtmp5, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference));

            grp->addItemDrawable(mask1);
            grp->addItemDrawable(mask2);
            grp->addItemDrawable(mask3);
            grp->addItemDrawable(mask4);
            grp->addItemDrawable(cumulativeMask);

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin step 3"));


            cumulativeMask = CT_Image2D<quint8>::createImage2DFromXYCoords(NULL, NULL, minX, minY, maxX, maxY, _resolution, 0, 2, 0);

            // Begin iterations for Step 3
            iterationNumber = 1;
            naNumber = 1;
            //while (naNumber > 0)
            while (iterationNumber < 2)
            {
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin new iteration (Step 3): %1").arg(iterationNumber++));

                // Interpolation
                // TODO : loop to make sure to interpolate all !
                CT_Image2D<float>* mntInterpol = (CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNT, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference);

                QList<size_t> NAindices;

                size_t index;
                for (size_t r = 0 ; r < dtm->linDim() ; r++)
                {
                    for (size_t c = 0 ; c < dtm->colDim() ; c++)
                    {
                        float value = dtm->value(c,r);
                        if (value == dtm->NA())
                        {
                            dtm->index(c, r, index);
                            NAindices.append(index);
                        }
                    }
                }

                QFuture<void> futur = QtConcurrent::map(NAindices, CT_Image2DNaturalNeighboursInterpolator(dtm, mntInterpol, ncellsForInterpNeigh));

                int progressMin = futur.progressMinimum();
                int progressTotal = futur.progressMaximum() - futur.progressMinimum();
                while (!futur.isFinished())
                {
                    setProgress(60.0*(futur.progressValue() - progressMin)/progressTotal + 20.0);
                }
                delete dtm;
                dtm = mntInterpol;

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Interpolation done"));


                // Local derivatives
                CT_Image2D<quint8>* mask = CT_Image2D<quint8>::createImage2DFromXYCoords(NULL, NULL, minX, minY, maxX, maxY, _resolution, 0, 2, 0);

                for (int r = 0 ; r < dtm->linDim() ; r++)
                {
                    for (int c = 0 ; c < dtm->colDim() ; c++)
                    {
                        float mntVal = dtm->value(c, r);

                        double mean = 0;
                        int nBelow = 0;
                        int nNeigh = 0;

                        if (mntVal != dtm->NA())
                        {
                            for (int dr = -1 ; dr <= 1 ; dr++)
                            {
                                for (int dc = -1 ; dc <= 1 ; dc++)
                                {
                                    if (dr != 0 || dc != 0)
                                    {
                                        float neighbourVal = dtm->value(c + dc, r + dr);

                                        if (neighbourVal != dtm->NA())
                                        {
                                            nNeigh++;
                                            if (neighbourVal < mntVal) {nBelow++;}
                                            mean += mntVal - neighbourVal;
                                        }
                                    }
                                }
                            }
                            if (nNeigh > 0) {mean /= nNeigh;}

                            if (nBelow > 6 && mean > _thresholdFine) {mask->setValue(c, r, 1);}
                        }
                    }
                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Local derivatives done"));


                // Apply mask to DTM
                naNumber = 0;
                for (size_t r = 0 ; r < dtm->linDim() ; r++)
                {
                    for (size_t c = 0 ; c < dtm->colDim() ; c++)
                    {
                        if (mask->value(c, r) == 1 && cumulativeMask->value(c, r) == 0)
                        {
                            dtm->setValue(c, r, dtm->NA());
                            cumulativeMask->setValue(c, r, 1);
                            naNumber++;
                        }
                    }
                }
                delete mask;

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Mask applied"));

            } // End of iteration in step 3

            delete cumulativeMask;

            grp->addItemDrawable((CT_Image2D<float>*) dtm->copy(DEF_SearchOutMNTtmp4, outResult, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableReference));


            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Begin step 4"));

            // Step 4 : point classification
            CT_PointCloudIndexVector *vegIndices = new CT_PointCloudIndexVector();
            CT_PointCloudIndexVector *groundIndices = new CT_PointCloudIndexVector();
            vegIndices->setSortType(CT_AbstractCloudIndex::NotSorted);
            groundIndices->setSortType(CT_AbstractCloudIndex::NotSorted);

            itP.toFront();
            while(itP.hasNext() && !isStopped())
            {
                const CT_Point &point =itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                float mntVal = dtm->valueAtCoords(point(0), point(1));
                float upperLimit = mntVal + _thresholdFine;
                float lowerLimit = mntVal - _thresholdFine;

                if (point(2) > upperLimit)
                {
                    vegIndices->addIndex(index);
                } else if (point(2) >= lowerLimit)
                {
                    groundIndices->addIndex(index);
                } // else aberrant -> don't keep
            }

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Classification done"));


            CT_Scene* vegScene = new CT_Scene(DEF_SearchOutSceneVegetation, outResult, PS_REPOSITORY->registerPointCloudIndex(vegIndices));
            vegScene->updateBoundingBox();

            CT_Scene* groundScene = new CT_Scene(DEF_SearchOutSceneSoil, outResult, PS_REPOSITORY->registerPointCloudIndex(groundIndices));
            groundScene->updateBoundingBox();

            grp->addItemDrawable(dtm);

            grp->addItemDrawable(vegScene);
            grp->addItemDrawable(groundScene);
        }

        setProgress(100);
    }
}
