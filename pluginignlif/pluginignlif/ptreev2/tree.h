#ifndef TREE_H
#define TREE_H

#include "param.h"
#include "point.h"

#include "ct_shape2ddata/ct_polygon2ddata.h"

#include <QList>

class Tree
{
public:
    Tree(int k);

    void addPoint(Point* pt);

    int                 _k;
    QList<Point*>       _points;
    Point*              _summit;
    CT_Polygon2DData*   _convexHull;
    double              _minZ;
    bool                _isSelected;

    QList<Tree*>        _children;

    double              _score_size;
    double              _score_regularity;
    double              _score_solidity;
    double              _score_summitOffset;
    double              _score;
    bool                _validated;

    void computeConvexHull();
    void computeScores(Param &p);

    static Tree* mergeTrees(QList<Tree *> treeList, Param &p);

private:

    double getScore_size(int k, Param &p);
    double getScore_regularity();
    double getScore_solidity(Param &p);
    double getScore_summitOffset();

};

#endif // TREE_H
