#ifndef PARAM_H
#define PARAM_H

#include <eigen/Eigen/Core>

class Param{

public :

    Param();
    void setbound(Eigen::Vector3i bound);
    Eigen::Vector3i getbound();

    void setdensity(double d);
    double getdensity();

    void setneighborV(Eigen::Vector2i neighborV);
    Eigen::Vector2i getneighborV();

    void setscale(int scale);
    int getscale();

    void setminDist(double minDist);
    double getminDist();

    void setminH(double minH);
    double getminH();

    void setcoefcompar(double coefcompar);
    double getcoefcompar();

    void setGrd(int grd);
    int getGrd();

private :
    Eigen::Vector3i _bound;
    double _density;
    Eigen::Vector2i _neighborV;
    int _scale;
    double _minDist;
    double _minH;
    int _grd;
    double _coefcompar;

};


#endif // PARAM_H
